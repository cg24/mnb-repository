# Visualisations de données 

## Densité d'espèces observées

La [Biométéo](https://biometeo.dordogne.fr/) consiste à croiser des données environnementales (air, eau) avec des relevés d'observation d'espèces sur une commune sélectionnée par l'utilisateur•rice. Une sélection de 6 espèces est ainsi proposée, et pour chaque taxon sélectionné il est possible d'avoir des informations détaillées, dont le nombre d'observations collectées sur l'ensemble du département de la Dordogne (voir par exemple sur cette fiche du [Fuligule Morillon](https://biometeo.dordogne.fr/taxon/?taxonid=http%3A%2F%2Ftaxref.mnhn.fr%2Flod%2Ftaxon%2F1998%2F12.0)). La carte ci-dessous affiche la densité d'observations en divisant le nombre d'observations relevées pour chaque commune par la surface. Cette valeur est exprimée en nombre d'espèces distinctes observées par km2. Ces données sont accessibles via [le hub de données](doc/hubdedonnees.md). 

<iframe title="Densité d'espèces observées par commune en Dordogne" aria-label="Carte" id="datawrapper-chart-ZImwZ" src="https://datawrapper.dwcdn.net/ZImwZ/3/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="887"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(a){if(void 0!==a.data["datawrapper-height"])for(var e in a.data["datawrapper-height"]){var t=document.getElementById("datawrapper-chart-"+e)||document.querySelector("iframe[src*='"+e+"']");t&&(t.style.height=a.data["datawrapper-height"][e]+"px")}}))}();
</script>



