var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "3800",
        "ok": "3763",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "67",
        "ok": "67",
        "ko": "10453"
    },
    "maxResponseTime": {
        "total": "19097",
        "ok": "19097",
        "ko": "13057"
    },
    "meanResponseTime": {
        "total": "4925",
        "ok": "4866",
        "ko": "10971"
    },
    "standardDeviation": {
        "total": "2481",
        "ok": "2419",
        "ko": "419"
    },
    "percentiles1": {
        "total": "4800",
        "ok": "4777",
        "ko": "11003"
    },
    "percentiles2": {
        "total": "6338",
        "ok": "6295",
        "ko": "11099"
    },
    "percentiles3": {
        "total": "9252",
        "ok": "9183",
        "ko": "11294"
    },
    "percentiles4": {
        "total": "11116",
        "ok": "10544",
        "ko": "12422"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 171,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 39,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3553,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "190",
        "ok": "188.15",
        "ko": "1.85"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "973",
        "ok": "973",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "558",
        "ok": "558",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "191",
        "ok": "191",
        "ko": "-"
    },
    "percentiles1": {
        "total": "564",
        "ok": "564",
        "ko": "-"
    },
    "percentiles2": {
        "total": "722",
        "ok": "722",
        "ko": "-"
    },
    "percentiles3": {
        "total": "840",
        "ok": "840",
        "ko": "-"
    },
    "percentiles4": {
        "total": "876",
        "ok": "876",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 89,
    "percentage": 89
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 11,
    "percentage": 11
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_graphql-riveran-e540f": {
        type: "REQUEST",
        name: "graphql_RiverAndGroundWaterObservation",
path: "graphql_RiverAndGroundWaterObservation",
pathFormatted: "req_graphql-riveran-e540f",
stats: {
    "name": "graphql_RiverAndGroundWaterObservation",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "274",
        "ok": "274",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11979",
        "ok": "11979",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5879",
        "ok": "5879",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2183",
        "ok": "2183",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6002",
        "ok": "6002",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6996",
        "ok": "6996",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9297",
        "ok": "9297",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10734",
        "ok": "10734",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 97,
    "percentage": 97
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-20-taxo-0d791": {
        type: "REQUEST",
        name: "request_20_TaxonMedia",
path: "request_20_TaxonMedia",
pathFormatted: "req_request-20-taxo-0d791",
stats: {
    "name": "request_20_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "98",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "1915",
        "ok": "1915",
        "ko": "11119"
    },
    "maxResponseTime": {
        "total": "15406",
        "ok": "15406",
        "ko": "13057"
    },
    "meanResponseTime": {
        "total": "6078",
        "ok": "5955",
        "ko": "12088"
    },
    "standardDeviation": {
        "total": "2440",
        "ok": "2303",
        "ko": "969"
    },
    "percentiles1": {
        "total": "5917",
        "ok": "5886",
        "ko": "12088"
    },
    "percentiles2": {
        "total": "6975",
        "ok": "6798",
        "ko": "12573"
    },
    "percentiles3": {
        "total": "9946",
        "ok": "9430",
        "ko": "12960"
    },
    "percentiles4": {
        "total": "13080",
        "ok": "12450",
        "ko": "13038"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 98,
    "percentage": 98
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.9",
        "ko": "0.1"
    }
}
    },"req_request-36-9ad97": {
        type: "REQUEST",
        name: "request_36",
path: "request_36",
pathFormatted: "req_request-36-9ad97",
stats: {
    "name": "request_36",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "126",
        "ok": "126",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9992",
        "ok": "9992",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4313",
        "ok": "4313",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1954",
        "ok": "1954",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4060",
        "ok": "4060",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5313",
        "ok": "5313",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8193",
        "ok": "8193",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9011",
        "ok": "9011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 95,
    "percentage": 95
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "102",
        "ok": "102",
        "ko": "10606"
    },
    "maxResponseTime": {
        "total": "11691",
        "ok": "11691",
        "ko": "10606"
    },
    "meanResponseTime": {
        "total": "4455",
        "ok": "4393",
        "ko": "10606"
    },
    "standardDeviation": {
        "total": "2307",
        "ok": "2234",
        "ko": "0"
    },
    "percentiles1": {
        "total": "4009",
        "ok": "3975",
        "ko": "10606"
    },
    "percentiles2": {
        "total": "5504",
        "ok": "5462",
        "ko": "10606"
    },
    "percentiles3": {
        "total": "8977",
        "ok": "8960",
        "ko": "10606"
    },
    "percentiles4": {
        "total": "10617",
        "ok": "10003",
        "ko": "10606"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_graphql-weather-e1b31": {
        type: "REQUEST",
        name: "graphql_weather",
path: "graphql_weather",
pathFormatted: "req_graphql-weather-e1b31",
stats: {
    "name": "graphql_weather",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "919",
        "ok": "919",
        "ko": "11034"
    },
    "maxResponseTime": {
        "total": "19097",
        "ok": "19097",
        "ko": "11034"
    },
    "meanResponseTime": {
        "total": "5136",
        "ok": "5077",
        "ko": "11034"
    },
    "standardDeviation": {
        "total": "2755",
        "ok": "2704",
        "ko": "0"
    },
    "percentiles1": {
        "total": "5031",
        "ok": "5009",
        "ko": "11034"
    },
    "percentiles2": {
        "total": "6748",
        "ok": "6722",
        "ko": "11034"
    },
    "percentiles3": {
        "total": "8970",
        "ok": "8909",
        "ko": "11034"
    },
    "percentiles4": {
        "total": "11115",
        "ok": "9955",
        "ko": "11034"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 95,
    "percentage": 95
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_request-6-font-15e7e": {
        type: "REQUEST",
        name: "request_6_font",
path: "request_6_font",
pathFormatted: "req_request-6-font-15e7e",
stats: {
    "name": "request_6_font",
    "numberOfRequests": {
        "total": "100",
        "ok": "98",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "233",
        "ok": "233",
        "ko": "10862"
    },
    "maxResponseTime": {
        "total": "11262",
        "ok": "10512",
        "ko": "11262"
    },
    "meanResponseTime": {
        "total": "4723",
        "ok": "4593",
        "ko": "11062"
    },
    "standardDeviation": {
        "total": "2365",
        "ok": "2207",
        "ko": "200"
    },
    "percentiles1": {
        "total": "4803",
        "ok": "4502",
        "ko": "11062"
    },
    "percentiles2": {
        "total": "5955",
        "ok": "5897",
        "ko": "11162"
    },
    "percentiles3": {
        "total": "9694",
        "ok": "8939",
        "ko": "11242"
    },
    "percentiles4": {
        "total": "10866",
        "ok": "10117",
        "ko": "11258"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.9",
        "ko": "0.1"
    }
}
    },"req_request-23-taxo-3ea91": {
        type: "REQUEST",
        name: "request_23_TaxonMedia",
path: "request_23_TaxonMedia",
pathFormatted: "req_request-23-taxo-3ea91",
stats: {
    "name": "request_23_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "97",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "1870",
        "ok": "1870",
        "ko": "10560"
    },
    "maxResponseTime": {
        "total": "14498",
        "ok": "14498",
        "ko": "10848"
    },
    "meanResponseTime": {
        "total": "5875",
        "ok": "5726",
        "ko": "10702"
    },
    "standardDeviation": {
        "total": "2401",
        "ok": "2280",
        "ko": "118"
    },
    "percentiles1": {
        "total": "5688",
        "ok": "5626",
        "ko": "10699"
    },
    "percentiles2": {
        "total": "6784",
        "ok": "6670",
        "ko": "10774"
    },
    "percentiles3": {
        "total": "10567",
        "ok": "9361",
        "ko": "10833"
    },
    "percentiles4": {
        "total": "13196",
        "ok": "13236",
        "ko": "10845"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 97,
    "percentage": 97
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.85",
        "ko": "0.15"
    }
}
    },"req_request-28-taxo-f261b": {
        type: "REQUEST",
        name: "request_28_TaxonMedia",
path: "request_28_TaxonMedia",
pathFormatted: "req_request-28-taxo-f261b",
stats: {
    "name": "request_28_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2186",
        "ok": "2186",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13053",
        "ok": "13053",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6029",
        "ok": "6029",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5941",
        "ok": "5941",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6708",
        "ok": "6708",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9193",
        "ok": "9193",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12672",
        "ok": "12672",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 100,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "100",
        "ok": "98",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "1827",
        "ok": "1827",
        "ko": "10607"
    },
    "maxResponseTime": {
        "total": "11294",
        "ok": "9596",
        "ko": "11294"
    },
    "meanResponseTime": {
        "total": "4631",
        "ok": "4502",
        "ko": "10951"
    },
    "standardDeviation": {
        "total": "2142",
        "ok": "1961",
        "ko": "344"
    },
    "percentiles1": {
        "total": "3953",
        "ok": "3818",
        "ko": "10951"
    },
    "percentiles2": {
        "total": "5857",
        "ok": "5838",
        "ko": "11122"
    },
    "percentiles3": {
        "total": "8991",
        "ok": "8841",
        "ko": "11260"
    },
    "percentiles4": {
        "total": "10614",
        "ok": "9403",
        "ko": "11287"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 98,
    "percentage": 98
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.9",
        "ko": "0.1"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "100",
        "ok": "98",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "101",
        "ok": "101",
        "ko": "10577"
    },
    "maxResponseTime": {
        "total": "10699",
        "ok": "9899",
        "ko": "10699"
    },
    "meanResponseTime": {
        "total": "4852",
        "ok": "4734",
        "ko": "10638"
    },
    "standardDeviation": {
        "total": "2426",
        "ok": "2304",
        "ko": "61"
    },
    "percentiles1": {
        "total": "4080",
        "ok": "3936",
        "ko": "10638"
    },
    "percentiles2": {
        "total": "6294",
        "ok": "6019",
        "ko": "10669"
    },
    "percentiles3": {
        "total": "9672",
        "ok": "9057",
        "ko": "10693"
    },
    "percentiles4": {
        "total": "10578",
        "ok": "9861",
        "ko": "10698"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 96,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.9",
        "ko": "0.1"
    }
}
    },"req_graphql-taxon-f4085": {
        type: "REQUEST",
        name: "graphql_Taxon",
path: "graphql_Taxon",
pathFormatted: "req_graphql-taxon-f4085",
stats: {
    "name": "graphql_Taxon",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "10763"
    },
    "maxResponseTime": {
        "total": "13176",
        "ok": "13176",
        "ko": "10763"
    },
    "meanResponseTime": {
        "total": "6193",
        "ok": "6147",
        "ko": "10763"
    },
    "standardDeviation": {
        "total": "2378",
        "ok": "2345",
        "ko": "0"
    },
    "percentiles1": {
        "total": "6353",
        "ok": "6337",
        "ko": "10763"
    },
    "percentiles2": {
        "total": "7507",
        "ok": "7484",
        "ko": "10763"
    },
    "percentiles3": {
        "total": "9985",
        "ok": "9840",
        "ko": "10763"
    },
    "percentiles4": {
        "total": "11188",
        "ok": "11208",
        "ko": "10763"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 96,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_request-35-3745a": {
        type: "REQUEST",
        name: "request_35",
path: "request_35",
pathFormatted: "req_request-35-3745a",
stats: {
    "name": "request_35",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9789",
        "ok": "9789",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4475",
        "ok": "4475",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2143",
        "ok": "2143",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4026",
        "ok": "4026",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5704",
        "ok": "5704",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9100",
        "ok": "9100",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9677",
        "ok": "9677",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 96,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_graphql-news-d7bbe": {
        type: "REQUEST",
        name: "graphQL_news",
path: "graphQL_news",
pathFormatted: "req_graphql-news-d7bbe",
stats: {
    "name": "graphQL_news",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "865",
        "ok": "865",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10160",
        "ok": "10160",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4929",
        "ok": "4929",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2209",
        "ok": "2209",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5143",
        "ok": "5143",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6058",
        "ok": "6058",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8966",
        "ok": "8966",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9186",
        "ok": "9186",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7,
    "percentage": 7
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "158",
        "ok": "158",
        "ko": "11294"
    },
    "maxResponseTime": {
        "total": "11294",
        "ok": "9595",
        "ko": "11294"
    },
    "meanResponseTime": {
        "total": "4161",
        "ok": "4088",
        "ko": "11294"
    },
    "standardDeviation": {
        "total": "2397",
        "ok": "2299",
        "ko": "0"
    },
    "percentiles1": {
        "total": "3756",
        "ok": "3749",
        "ko": "11294"
    },
    "percentiles2": {
        "total": "5199",
        "ok": "5137",
        "ko": "11294"
    },
    "percentiles3": {
        "total": "9083",
        "ok": "8975",
        "ko": "11294"
    },
    "percentiles4": {
        "total": "9612",
        "ok": "9339",
        "ko": "11294"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 9,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 90,
    "percentage": 90
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "631",
        "ok": "631",
        "ko": "10861"
    },
    "maxResponseTime": {
        "total": "10861",
        "ok": "9673",
        "ko": "10861"
    },
    "meanResponseTime": {
        "total": "4789",
        "ok": "4728",
        "ko": "10861"
    },
    "standardDeviation": {
        "total": "2142",
        "ok": "2063",
        "ko": "0"
    },
    "percentiles1": {
        "total": "4648",
        "ok": "4556",
        "ko": "10861"
    },
    "percentiles2": {
        "total": "5917",
        "ok": "5901",
        "ko": "10861"
    },
    "percentiles3": {
        "total": "9087",
        "ok": "8992",
        "ko": "10861"
    },
    "percentiles4": {
        "total": "9685",
        "ok": "9257",
        "ko": "10861"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 97,
    "percentage": 97
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_request-29-taxo-1f5dd": {
        type: "REQUEST",
        name: "request_29_TaxonMedia",
path: "request_29_TaxonMedia",
pathFormatted: "req_request-29-taxo-1f5dd",
stats: {
    "name": "request_29_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1688",
        "ok": "1688",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12411",
        "ok": "12411",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5850",
        "ok": "5850",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2359",
        "ok": "2359",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5862",
        "ok": "5862",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7341",
        "ok": "7341",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9710",
        "ok": "9710",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11596",
        "ok": "11596",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 100,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "100",
        "ok": "97",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "197",
        "ok": "197",
        "ko": "11035"
    },
    "maxResponseTime": {
        "total": "11829",
        "ok": "11829",
        "ko": "11113"
    },
    "meanResponseTime": {
        "total": "4878",
        "ok": "4687",
        "ko": "11062"
    },
    "standardDeviation": {
        "total": "2659",
        "ok": "2464",
        "ko": "36"
    },
    "percentiles1": {
        "total": "4058",
        "ok": "4041",
        "ko": "11039"
    },
    "percentiles2": {
        "total": "6268",
        "ok": "5714",
        "ko": "11076"
    },
    "percentiles3": {
        "total": "9692",
        "ok": "9353",
        "ko": "11106"
    },
    "percentiles4": {
        "total": "11120",
        "ok": "10070",
        "ko": "11112"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.85",
        "ko": "0.15"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "100",
        "ok": "98",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "241",
        "ok": "241",
        "ko": "10960"
    },
    "maxResponseTime": {
        "total": "11278",
        "ok": "9978",
        "ko": "11278"
    },
    "meanResponseTime": {
        "total": "4650",
        "ok": "4518",
        "ko": "11119"
    },
    "standardDeviation": {
        "total": "2502",
        "ok": "2348",
        "ko": "159"
    },
    "percentiles1": {
        "total": "3729",
        "ok": "3715",
        "ko": "11119"
    },
    "percentiles2": {
        "total": "5844",
        "ok": "5678",
        "ko": "11199"
    },
    "percentiles3": {
        "total": "8946",
        "ok": "8887",
        "ko": "11262"
    },
    "percentiles4": {
        "total": "10963",
        "ok": "9240",
        "ko": "11275"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.9",
        "ko": "0.1"
    }
}
    },"req_request-30-taxo-7acf0": {
        type: "REQUEST",
        name: "request_30_TaxonMedia",
path: "request_30_TaxonMedia",
pathFormatted: "req_request-30-taxo-7acf0",
stats: {
    "name": "request_30_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "10662"
    },
    "maxResponseTime": {
        "total": "13662",
        "ok": "13662",
        "ko": "10662"
    },
    "meanResponseTime": {
        "total": "5986",
        "ok": "5939",
        "ko": "10662"
    },
    "standardDeviation": {
        "total": "2327",
        "ok": "2291",
        "ko": "0"
    },
    "percentiles1": {
        "total": "5817",
        "ok": "5793",
        "ko": "10662"
    },
    "percentiles2": {
        "total": "7169",
        "ok": "7129",
        "ko": "10662"
    },
    "percentiles3": {
        "total": "9402",
        "ok": "9359",
        "ko": "10662"
    },
    "percentiles4": {
        "total": "12652",
        "ok": "12662",
        "ko": "10662"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 99,
    "percentage": 99
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "100",
        "ok": "98",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "379",
        "ok": "379",
        "ko": "10668"
    },
    "maxResponseTime": {
        "total": "11075",
        "ok": "9997",
        "ko": "11075"
    },
    "meanResponseTime": {
        "total": "4753",
        "ok": "4628",
        "ko": "10872"
    },
    "standardDeviation": {
        "total": "2306",
        "ok": "2155",
        "ko": "204"
    },
    "percentiles1": {
        "total": "3895",
        "ok": "3842",
        "ko": "10872"
    },
    "percentiles2": {
        "total": "5884",
        "ok": "5813",
        "ko": "10973"
    },
    "percentiles3": {
        "total": "9260",
        "ok": "8976",
        "ko": "11055"
    },
    "percentiles4": {
        "total": "10672",
        "ok": "9705",
        "ko": "11071"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 97,
    "percentage": 97
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.9",
        "ko": "0.1"
    }
}
    },"req_request-26-taxo-41b6f": {
        type: "REQUEST",
        name: "request_26_TaxonMedia",
path: "request_26_TaxonMedia",
pathFormatted: "req_request-26-taxo-41b6f",
stats: {
    "name": "request_26_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "1869",
        "ok": "1869",
        "ko": "11021"
    },
    "maxResponseTime": {
        "total": "14282",
        "ok": "14282",
        "ko": "11021"
    },
    "meanResponseTime": {
        "total": "5930",
        "ok": "5879",
        "ko": "11021"
    },
    "standardDeviation": {
        "total": "2469",
        "ok": "2428",
        "ko": "0"
    },
    "percentiles1": {
        "total": "5547",
        "ok": "5518",
        "ko": "11021"
    },
    "percentiles2": {
        "total": "7102",
        "ok": "7011",
        "ko": "11021"
    },
    "percentiles3": {
        "total": "10306",
        "ok": "9917",
        "ko": "11021"
    },
    "percentiles4": {
        "total": "13744",
        "ok": "13750",
        "ko": "11021"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 99,
    "percentage": 99
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_graphql-news-4c309": {
        type: "REQUEST",
        name: "graphql_News",
path: "graphql_News",
pathFormatted: "req_graphql-news-4c309",
stats: {
    "name": "graphql_News",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "939",
        "ok": "939",
        "ko": "11009"
    },
    "maxResponseTime": {
        "total": "11009",
        "ok": "9723",
        "ko": "11009"
    },
    "meanResponseTime": {
        "total": "5200",
        "ok": "5142",
        "ko": "11009"
    },
    "standardDeviation": {
        "total": "2336",
        "ok": "2273",
        "ko": "0"
    },
    "percentiles1": {
        "total": "5174",
        "ok": "5152",
        "ko": "11009"
    },
    "percentiles2": {
        "total": "6928",
        "ok": "6895",
        "ko": "11009"
    },
    "percentiles3": {
        "total": "9080",
        "ok": "9050",
        "ko": "11009"
    },
    "percentiles4": {
        "total": "9736",
        "ok": "9195",
        "ko": "11009"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 95,
    "percentage": 95
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "174",
        "ok": "174",
        "ko": "10559"
    },
    "maxResponseTime": {
        "total": "10559",
        "ok": "10534",
        "ko": "10559"
    },
    "meanResponseTime": {
        "total": "4671",
        "ok": "4612",
        "ko": "10559"
    },
    "standardDeviation": {
        "total": "2491",
        "ok": "2432",
        "ko": "0"
    },
    "percentiles1": {
        "total": "3855",
        "ok": "3763",
        "ko": "10559"
    },
    "percentiles2": {
        "total": "5906",
        "ok": "5879",
        "ko": "10559"
    },
    "percentiles3": {
        "total": "9084",
        "ok": "9031",
        "ko": "10559"
    },
    "percentiles4": {
        "total": "10534",
        "ok": "9529",
        "ko": "10559"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 95,
    "percentage": 95
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_request-24-taxo-b5f25": {
        type: "REQUEST",
        name: "request_24_TaxonMedia",
path: "request_24_TaxonMedia",
pathFormatted: "req_request-24-taxo-b5f25",
stats: {
    "name": "request_24_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2578",
        "ok": "2578",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14039",
        "ok": "14039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6383",
        "ok": "6383",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2052",
        "ok": "2052",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6290",
        "ok": "6290",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7238",
        "ok": "7238",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9520",
        "ok": "9520",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10464",
        "ok": "10464",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 100,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "351",
        "ok": "351",
        "ko": "11013"
    },
    "maxResponseTime": {
        "total": "11013",
        "ok": "10012",
        "ko": "11013"
    },
    "meanResponseTime": {
        "total": "4737",
        "ok": "4674",
        "ko": "11013"
    },
    "standardDeviation": {
        "total": "2328",
        "ok": "2253",
        "ko": "0"
    },
    "percentiles1": {
        "total": "4300",
        "ok": "4292",
        "ko": "11013"
    },
    "percentiles2": {
        "total": "5822",
        "ok": "5801",
        "ko": "11013"
    },
    "percentiles3": {
        "total": "9251",
        "ok": "9008",
        "ko": "11013"
    },
    "percentiles4": {
        "total": "10022",
        "ok": "9968",
        "ko": "11013"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "100",
        "ok": "96",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "264",
        "ok": "264",
        "ko": "10698"
    },
    "maxResponseTime": {
        "total": "11269",
        "ok": "9616",
        "ko": "11269"
    },
    "meanResponseTime": {
        "total": "5076",
        "ok": "4827",
        "ko": "11046"
    },
    "standardDeviation": {
        "total": "2435",
        "ok": "2151",
        "ko": "211"
    },
    "percentiles1": {
        "total": "4378",
        "ok": "4218",
        "ko": "11108"
    },
    "percentiles2": {
        "total": "6257",
        "ok": "6059",
        "ko": "11154"
    },
    "percentiles3": {
        "total": "9449",
        "ok": "9008",
        "ko": "11246"
    },
    "percentiles4": {
        "total": "11118",
        "ok": "9449",
        "ko": "11264"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 95,
    "percentage": 95
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.8",
        "ko": "0.2"
    }
}
    },"req_request-25-taxo-2081e": {
        type: "REQUEST",
        name: "request_25_TaxonMedia",
path: "request_25_TaxonMedia",
pathFormatted: "req_request-25-taxo-2081e",
stats: {
    "name": "request_25_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14571",
        "ok": "14571",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6201",
        "ok": "6201",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2454",
        "ok": "2454",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6149",
        "ok": "6149",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7169",
        "ok": "7169",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9754",
        "ok": "9754",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12889",
        "ok": "12889",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 100,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-4-matom-e3907": {
        type: "REQUEST",
        name: "request_4_matomo",
path: "request_4_matomo",
pathFormatted: "req_request-4-matom-e3907",
stats: {
    "name": "request_4_matomo",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "710",
        "ok": "710",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7562",
        "ok": "7562",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3400",
        "ok": "3400",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1898",
        "ok": "1898",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2612",
        "ok": "2612",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4488",
        "ok": "4488",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7416",
        "ok": "7416",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7537",
        "ok": "7537",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 8
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 91,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "269",
        "ok": "269",
        "ko": "10453"
    },
    "maxResponseTime": {
        "total": "10453",
        "ok": "9241",
        "ko": "10453"
    },
    "meanResponseTime": {
        "total": "4243",
        "ok": "4181",
        "ko": "10453"
    },
    "standardDeviation": {
        "total": "2003",
        "ok": "1913",
        "ko": "0"
    },
    "percentiles1": {
        "total": "3801",
        "ok": "3786",
        "ko": "10453"
    },
    "percentiles2": {
        "total": "5226",
        "ok": "5223",
        "ko": "10453"
    },
    "percentiles3": {
        "total": "8436",
        "ok": "8187",
        "ko": "10453"
    },
    "percentiles4": {
        "total": "9253",
        "ok": "9030",
        "ko": "10453"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.95",
        "ko": "0.05"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "273",
        "ok": "273",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9243",
        "ok": "9243",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4589",
        "ok": "4589",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2209",
        "ok": "2209",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4154",
        "ok": "4154",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5463",
        "ok": "5463",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8863",
        "ok": "8863",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8985",
        "ok": "8985",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 97,
    "percentage": 97
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_graphql-searchp-61328": {
        type: "REQUEST",
        name: "graphql_SearchPlaces",
path: "graphql_SearchPlaces",
pathFormatted: "req_graphql-searchp-61328",
stats: {
    "name": "graphql_SearchPlaces",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "446",
        "ok": "446",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9488",
        "ok": "9488",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4460",
        "ok": "4460",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2100",
        "ok": "2100",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3882",
        "ok": "3882",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5367",
        "ok": "5367",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8886",
        "ok": "8886",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9446",
        "ok": "9446",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 97,
    "percentage": 97
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "100",
        "ok": "98",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "130",
        "ok": "130",
        "ko": "10849"
    },
    "maxResponseTime": {
        "total": "11089",
        "ok": "11089",
        "ko": "11003"
    },
    "meanResponseTime": {
        "total": "4482",
        "ok": "4350",
        "ko": "10926"
    },
    "standardDeviation": {
        "total": "2285",
        "ok": "2113",
        "ko": "77"
    },
    "percentiles1": {
        "total": "4194",
        "ok": "4160",
        "ko": "10926"
    },
    "percentiles2": {
        "total": "5451",
        "ok": "5443",
        "ko": "10965"
    },
    "percentiles3": {
        "total": "8989",
        "ok": "8713",
        "ko": "10995"
    },
    "percentiles4": {
        "total": "11004",
        "ok": "9737",
        "ko": "11001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.9",
        "ko": "0.1"
    }
}
    },"req_request-34-d9757": {
        type: "REQUEST",
        name: "request_34",
path: "request_34",
pathFormatted: "req_request-34-d9757",
stats: {
    "name": "request_34",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10254",
        "ok": "10254",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4719",
        "ok": "4719",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2187",
        "ok": "2187",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3968",
        "ok": "3968",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5869",
        "ok": "5869",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9005",
        "ok": "9005",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9999",
        "ok": "9999",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 98,
    "percentage": 98
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "848",
        "ok": "848",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7640",
        "ok": "7640",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4283",
        "ok": "4283",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3971",
        "ok": "3971",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5658",
        "ok": "5658",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7263",
        "ok": "7263",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7627",
        "ok": "7627",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 97,
    "percentage": 97
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "100",
        "ok": "98",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "67",
        "ok": "67",
        "ko": "10846"
    },
    "maxResponseTime": {
        "total": "11010",
        "ok": "9979",
        "ko": "11010"
    },
    "meanResponseTime": {
        "total": "4864",
        "ok": "4740",
        "ko": "10928"
    },
    "standardDeviation": {
        "total": "2530",
        "ok": "2401",
        "ko": "82"
    },
    "percentiles1": {
        "total": "4748",
        "ok": "4477",
        "ko": "10928"
    },
    "percentiles2": {
        "total": "6249",
        "ok": "6189",
        "ko": "10969"
    },
    "percentiles3": {
        "total": "9253",
        "ok": "9002",
        "ko": "11002"
    },
    "percentiles4": {
        "total": "10848",
        "ok": "9684",
        "ko": "11008"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "4.9",
        "ko": "0.1"
    }
}
    },"req_request-21-taxo-4adce": {
        type: "REQUEST",
        name: "request_21_TaxonMedia",
path: "request_21_TaxonMedia",
pathFormatted: "req_request-21-taxo-4adce",
stats: {
    "name": "request_21_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12669",
        "ok": "12669",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6173",
        "ok": "6173",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2430",
        "ok": "2430",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6059",
        "ok": "6059",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7396",
        "ok": "7396",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10129",
        "ok": "10129",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12336",
        "ok": "12336",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 100,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9983",
        "ok": "9983",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4563",
        "ok": "4563",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2190",
        "ok": "2190",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4050",
        "ok": "4050",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5935",
        "ok": "5935",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8841",
        "ok": "8841",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9926",
        "ok": "9926",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 96,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
