var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4700",
        "ok": "2635",
        "ko": "2065"
    },
    "minResponseTime": {
        "total": "155",
        "ok": "155",
        "ko": "12206"
    },
    "maxResponseTime": {
        "total": "43751",
        "ok": "43751",
        "ko": "21544"
    },
    "meanResponseTime": {
        "total": "17276",
        "ok": "17844",
        "ko": "16551"
    },
    "standardDeviation": {
        "total": "4635",
        "ok": "5773",
        "ko": "2332"
    },
    "percentiles1": {
        "total": "17017",
        "ok": "18888",
        "ko": "16235"
    },
    "percentiles2": {
        "total": "20009",
        "ok": "20372",
        "ko": "16911"
    },
    "percentiles3": {
        "total": "22197",
        "ok": "23611",
        "ko": "20647"
    },
    "percentiles4": {
        "total": "28210",
        "ok": "36356",
        "ko": "21274"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 48,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 26,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2561,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 2065,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "100",
        "ok": "56.064",
        "ko": "43.936"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "754",
        "ok": "754",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6163",
        "ok": "6163",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1042",
        "ok": "1042",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1633",
        "ok": "1633",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2176",
        "ok": "2176",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3419",
        "ok": "3419",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6156",
        "ok": "6156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 14,
    "percentage": 14
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 85,
    "percentage": 85
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "2.128",
        "ko": "-"
    }
}
    },"req_favicon-16x16-p-7181b": {
        type: "REQUEST",
        name: "favicon-16x16.png",
path: "favicon-16x16.png",
pathFormatted: "req_favicon-16x16-p-7181b",
stats: {
    "name": "favicon-16x16.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "69",
        "ko": "31"
    },
    "minResponseTime": {
        "total": "253",
        "ok": "253",
        "ko": "16057"
    },
    "maxResponseTime": {
        "total": "21584",
        "ok": "21584",
        "ko": "21411"
    },
    "meanResponseTime": {
        "total": "17989",
        "ok": "17548",
        "ko": "18968"
    },
    "standardDeviation": {
        "total": "4703",
        "ok": "5436",
        "ko": "2046"
    },
    "percentiles1": {
        "total": "19282",
        "ok": "19256",
        "ko": "19355"
    },
    "percentiles2": {
        "total": "20476",
        "ok": "20042",
        "ko": "20690"
    },
    "percentiles3": {
        "total": "21416",
        "ok": "21505",
        "ko": "21232"
    },
    "percentiles4": {
        "total": "21565",
        "ok": "21571",
        "ko": "21371"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 31,
    "percentage": 31
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.468",
        "ko": "0.66"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "100",
        "ok": "51",
        "ko": "49"
    },
    "minResponseTime": {
        "total": "2486",
        "ok": "2486",
        "ko": "12431"
    },
    "maxResponseTime": {
        "total": "23823",
        "ok": "23823",
        "ko": "20398"
    },
    "meanResponseTime": {
        "total": "17282",
        "ok": "18447",
        "ko": "16070"
    },
    "standardDeviation": {
        "total": "3018",
        "ok": "3371",
        "ko": "1969"
    },
    "percentiles1": {
        "total": "16845",
        "ok": "18882",
        "ko": "16061"
    },
    "percentiles2": {
        "total": "19632",
        "ok": "20461",
        "ko": "16565"
    },
    "percentiles3": {
        "total": "21992",
        "ok": "22864",
        "ko": "20274"
    },
    "percentiles4": {
        "total": "23550",
        "ok": "23685",
        "ko": "20343"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 51,
    "percentage": 51
},
    "group4": {
    "name": "failed",
    "count": 49,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.085",
        "ko": "1.043"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "100",
        "ok": "53",
        "ko": "47"
    },
    "minResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "12430"
    },
    "maxResponseTime": {
        "total": "23711",
        "ok": "23711",
        "ko": "20396"
    },
    "meanResponseTime": {
        "total": "16426",
        "ok": "16791",
        "ko": "16015"
    },
    "standardDeviation": {
        "total": "4108",
        "ok": "5306",
        "ko": "1961"
    },
    "percentiles1": {
        "total": "16549",
        "ok": "18532",
        "ko": "15973"
    },
    "percentiles2": {
        "total": "19226",
        "ok": "19804",
        "ko": "16549"
    },
    "percentiles3": {
        "total": "20908",
        "ok": "21330",
        "ko": "20281"
    },
    "percentiles4": {
        "total": "22168",
        "ok": "22900",
        "ko": "20346"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 51,
    "percentage": 51
},
    "group4": {
    "name": "failed",
    "count": 47,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.128",
        "ko": "1"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "100",
        "ok": "53",
        "ko": "47"
    },
    "minResponseTime": {
        "total": "195",
        "ok": "195",
        "ko": "12783"
    },
    "maxResponseTime": {
        "total": "23598",
        "ok": "23598",
        "ko": "20398"
    },
    "meanResponseTime": {
        "total": "16657",
        "ok": "17079",
        "ko": "16180"
    },
    "standardDeviation": {
        "total": "4052",
        "ok": "5211",
        "ko": "1972"
    },
    "percentiles1": {
        "total": "16539",
        "ok": "18074",
        "ko": "16106"
    },
    "percentiles2": {
        "total": "19187",
        "ok": "20072",
        "ko": "16572"
    },
    "percentiles3": {
        "total": "21726",
        "ok": "22637",
        "ko": "20280"
    },
    "percentiles4": {
        "total": "23278",
        "ok": "23430",
        "ko": "20346"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 51,
    "percentage": 51
},
    "group4": {
    "name": "failed",
    "count": 47,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.128",
        "ko": "1"
    }
}
    },"req_favicon-32x32-p-14e88": {
        type: "REQUEST",
        name: "favicon-32x32.png",
path: "favicon-32x32.png",
pathFormatted: "req_favicon-32x32-p-14e88",
stats: {
    "name": "favicon-32x32.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "393",
        "ok": "393",
        "ko": "16109"
    },
    "maxResponseTime": {
        "total": "36612",
        "ok": "36612",
        "ko": "21528"
    },
    "meanResponseTime": {
        "total": "19444",
        "ok": "19543",
        "ko": "19252"
    },
    "standardDeviation": {
        "total": "4085",
        "ok": "4824",
        "ko": "1961"
    },
    "percentiles1": {
        "total": "19881",
        "ok": "19817",
        "ko": "20479"
    },
    "percentiles2": {
        "total": "20722",
        "ok": "20734",
        "ko": "20721"
    },
    "percentiles3": {
        "total": "22547",
        "ok": "22731",
        "ko": "21297"
    },
    "percentiles4": {
        "total": "36437",
        "ok": "36497",
        "ko": "21464"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.404",
        "ko": "0.723"
    }
}
    },"req_request-35-3745a": {
        type: "REQUEST",
        name: "request_35",
path: "request_35",
pathFormatted: "req_request-35-3745a",
stats: {
    "name": "request_35",
    "numberOfRequests": {
        "total": "100",
        "ok": "54",
        "ko": "46"
    },
    "minResponseTime": {
        "total": "277",
        "ok": "277",
        "ko": "12842"
    },
    "maxResponseTime": {
        "total": "23792",
        "ok": "23792",
        "ko": "20398"
    },
    "meanResponseTime": {
        "total": "16329",
        "ok": "16584",
        "ko": "16030"
    },
    "standardDeviation": {
        "total": "4680",
        "ok": "6135",
        "ko": "1804"
    },
    "percentiles1": {
        "total": "16499",
        "ok": "18405",
        "ko": "16108"
    },
    "percentiles2": {
        "total": "19230",
        "ok": "20461",
        "ko": "16551"
    },
    "percentiles3": {
        "total": "21565",
        "ok": "22353",
        "ko": "20244"
    },
    "percentiles4": {
        "total": "23700",
        "ok": "23743",
        "ko": "20349"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 50,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 46,
    "percentage": 46
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.149",
        "ko": "0.979"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "100",
        "ok": "51",
        "ko": "49"
    },
    "minResponseTime": {
        "total": "601",
        "ok": "601",
        "ko": "12206"
    },
    "maxResponseTime": {
        "total": "21781",
        "ok": "21781",
        "ko": "20392"
    },
    "meanResponseTime": {
        "total": "16592",
        "ok": "17162",
        "ko": "15999"
    },
    "standardDeviation": {
        "total": "3264",
        "ok": "3965",
        "ko": "2167"
    },
    "percentiles1": {
        "total": "16517",
        "ok": "17738",
        "ko": "15970"
    },
    "percentiles2": {
        "total": "18899",
        "ok": "19245",
        "ko": "16582"
    },
    "percentiles3": {
        "total": "20466",
        "ok": "21475",
        "ko": "20283"
    },
    "percentiles4": {
        "total": "21494",
        "ok": "21636",
        "ko": "20363"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 49,
    "percentage": 49
},
    "group4": {
    "name": "failed",
    "count": 49,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.085",
        "ko": "1.043"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "100",
        "ok": "51",
        "ko": "49"
    },
    "minResponseTime": {
        "total": "156",
        "ok": "156",
        "ko": "12842"
    },
    "maxResponseTime": {
        "total": "24472",
        "ok": "24472",
        "ko": "20396"
    },
    "meanResponseTime": {
        "total": "16989",
        "ok": "17713",
        "ko": "16235"
    },
    "standardDeviation": {
        "total": "3421",
        "ok": "4296",
        "ko": "1888"
    },
    "percentiles1": {
        "total": "16573",
        "ok": "18153",
        "ko": "16123"
    },
    "percentiles2": {
        "total": "19590",
        "ok": "19917",
        "ko": "16579"
    },
    "percentiles3": {
        "total": "21661",
        "ok": "21849",
        "ko": "20279"
    },
    "percentiles4": {
        "total": "22844",
        "ok": "23650",
        "ko": "20344"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 49,
    "percentage": 49
},
    "group4": {
    "name": "failed",
    "count": 49,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.085",
        "ko": "1.043"
    }
}
    },"req_request-25-taxo-2081e": {
        type: "REQUEST",
        name: "request_25_TaxonMedia",
path: "request_25_TaxonMedia",
pathFormatted: "req_request-25-taxo-2081e",
stats: {
    "name": "request_25_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "52",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "12208",
        "ok": "13782",
        "ko": "12208"
    },
    "maxResponseTime": {
        "total": "23328",
        "ok": "23328",
        "ko": "20404"
    },
    "meanResponseTime": {
        "total": "17225",
        "ok": "18409",
        "ko": "15942"
    },
    "standardDeviation": {
        "total": "2570",
        "ok": "2386",
        "ko": "2104"
    },
    "percentiles1": {
        "total": "16664",
        "ok": "19042",
        "ko": "15964"
    },
    "percentiles2": {
        "total": "19273",
        "ok": "19755",
        "ko": "16531"
    },
    "percentiles3": {
        "total": "21852",
        "ok": "22162",
        "ko": "20282"
    },
    "percentiles4": {
        "total": "22906",
        "ok": "23111",
        "ko": "20402"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.106",
        "ko": "1.021"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "100",
        "ok": "57",
        "ko": "43"
    },
    "minResponseTime": {
        "total": "279",
        "ok": "279",
        "ko": "12209"
    },
    "maxResponseTime": {
        "total": "23866",
        "ok": "23866",
        "ko": "20395"
    },
    "meanResponseTime": {
        "total": "16997",
        "ok": "17789",
        "ko": "15946"
    },
    "standardDeviation": {
        "total": "3973",
        "ok": "4784",
        "ko": "2105"
    },
    "percentiles1": {
        "total": "16816",
        "ok": "18697",
        "ko": "15959"
    },
    "percentiles2": {
        "total": "19595",
        "ok": "20487",
        "ko": "16549"
    },
    "percentiles3": {
        "total": "22452",
        "ok": "23178",
        "ko": "20282"
    },
    "percentiles4": {
        "total": "23576",
        "ok": "23702",
        "ko": "20350"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 55,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 43,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.213",
        "ko": "0.915"
    }
}
    },"req_graphql-weather-e1b31": {
        type: "REQUEST",
        name: "graphql_weather",
path: "graphql_weather",
pathFormatted: "req_graphql-weather-e1b31",
stats: {
    "name": "graphql_weather",
    "numberOfRequests": {
        "total": "100",
        "ok": "52",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "11591",
        "ok": "11591",
        "ko": "12778"
    },
    "maxResponseTime": {
        "total": "21804",
        "ok": "21804",
        "ko": "20398"
    },
    "meanResponseTime": {
        "total": "17155",
        "ok": "18121",
        "ko": "16109"
    },
    "standardDeviation": {
        "total": "2346",
        "ok": "2331",
        "ko": "1864"
    },
    "percentiles1": {
        "total": "16573",
        "ok": "18533",
        "ko": "16112"
    },
    "percentiles2": {
        "total": "19315",
        "ok": "19697",
        "ko": "16558"
    },
    "percentiles3": {
        "total": "20912",
        "ok": "21558",
        "ko": "20280"
    },
    "percentiles4": {
        "total": "21801",
        "ok": "21802",
        "ko": "20344"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.106",
        "ko": "1.021"
    }
}
    },"req_request-30-taxo-7acf0": {
        type: "REQUEST",
        name: "request_30_TaxonMedia",
path: "request_30_TaxonMedia",
pathFormatted: "req_request-30-taxo-7acf0",
stats: {
    "name": "request_30_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "54",
        "ko": "46"
    },
    "minResponseTime": {
        "total": "11593",
        "ok": "11593",
        "ko": "12794"
    },
    "maxResponseTime": {
        "total": "31784",
        "ok": "31784",
        "ko": "20398"
    },
    "meanResponseTime": {
        "total": "17566",
        "ok": "18864",
        "ko": "16043"
    },
    "standardDeviation": {
        "total": "3085",
        "ok": "3318",
        "ko": "1863"
    },
    "percentiles1": {
        "total": "16727",
        "ok": "19156",
        "ko": "16015"
    },
    "percentiles2": {
        "total": "19595",
        "ok": "20488",
        "ko": "16525"
    },
    "percentiles3": {
        "total": "22155",
        "ok": "22942",
        "ko": "20278"
    },
    "percentiles4": {
        "total": "27809",
        "ok": "29656",
        "ko": "20346"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 54,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 46,
    "percentage": 46
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.149",
        "ko": "0.979"
    }
}
    },"req_graphql-news-4c309": {
        type: "REQUEST",
        name: "graphql_News",
path: "graphql_News",
pathFormatted: "req_graphql-news-4c309",
stats: {
    "name": "graphql_News",
    "numberOfRequests": {
        "total": "100",
        "ok": "50",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "12418",
        "ok": "13777",
        "ko": "12418"
    },
    "maxResponseTime": {
        "total": "21490",
        "ok": "21490",
        "ko": "20397"
    },
    "meanResponseTime": {
        "total": "17009",
        "ok": "17931",
        "ko": "16086"
    },
    "standardDeviation": {
        "total": "2288",
        "ok": "2032",
        "ko": "2154"
    },
    "percentiles1": {
        "total": "16512",
        "ok": "18122",
        "ko": "16017"
    },
    "percentiles2": {
        "total": "18926",
        "ok": "19222",
        "ko": "16672"
    },
    "percentiles3": {
        "total": "20423",
        "ok": "21472",
        "ko": "20286"
    },
    "percentiles4": {
        "total": "21489",
        "ok": "21490",
        "ko": "20364"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 50,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.064",
        "ko": "1.064"
    }
}
    },"req_request-26-taxo-41b6f": {
        type: "REQUEST",
        name: "request_26_TaxonMedia",
path: "request_26_TaxonMedia",
pathFormatted: "req_request-26-taxo-41b6f",
stats: {
    "name": "request_26_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "53",
        "ko": "47"
    },
    "minResponseTime": {
        "total": "11552",
        "ok": "11552",
        "ko": "12780"
    },
    "maxResponseTime": {
        "total": "23580",
        "ok": "23580",
        "ko": "20397"
    },
    "meanResponseTime": {
        "total": "17339",
        "ok": "18412",
        "ko": "16129"
    },
    "standardDeviation": {
        "total": "2547",
        "ok": "2542",
        "ko": "1938"
    },
    "percentiles1": {
        "total": "16728",
        "ok": "18841",
        "ko": "16058"
    },
    "percentiles2": {
        "total": "19347",
        "ok": "20213",
        "ko": "16511"
    },
    "percentiles3": {
        "total": "21674",
        "ok": "22101",
        "ko": "20281"
    },
    "percentiles4": {
        "total": "23088",
        "ok": "23322",
        "ko": "20346"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 53,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 47,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.128",
        "ko": "1"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "100",
        "ok": "54",
        "ko": "46"
    },
    "minResponseTime": {
        "total": "301",
        "ok": "301",
        "ko": "12760"
    },
    "maxResponseTime": {
        "total": "25529",
        "ok": "25529",
        "ko": "20405"
    },
    "meanResponseTime": {
        "total": "16540",
        "ok": "16732",
        "ko": "16315"
    },
    "standardDeviation": {
        "total": "4447",
        "ok": "5735",
        "ko": "2069"
    },
    "percentiles1": {
        "total": "16570",
        "ok": "17973",
        "ko": "16162"
    },
    "percentiles2": {
        "total": "19243",
        "ok": "19648",
        "ko": "16702"
    },
    "percentiles3": {
        "total": "22151",
        "ok": "22641",
        "ko": "20319"
    },
    "percentiles4": {
        "total": "24213",
        "ok": "24825",
        "ko": "20402"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 50,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 46,
    "percentage": 46
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.149",
        "ko": "0.979"
    }
}
    },"req_request-23-taxo-3ea91": {
        type: "REQUEST",
        name: "request_23_TaxonMedia",
path: "request_23_TaxonMedia",
pathFormatted: "req_request-23-taxo-3ea91",
stats: {
    "name": "request_23_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "53",
        "ko": "47"
    },
    "minResponseTime": {
        "total": "11571",
        "ok": "11571",
        "ko": "12207"
    },
    "maxResponseTime": {
        "total": "23641",
        "ok": "23641",
        "ko": "20395"
    },
    "meanResponseTime": {
        "total": "17256",
        "ok": "18367",
        "ko": "16003"
    },
    "standardDeviation": {
        "total": "2604",
        "ok": "2593",
        "ko": "1968"
    },
    "percentiles1": {
        "total": "16646",
        "ok": "18741",
        "ko": "15970"
    },
    "percentiles2": {
        "total": "19440",
        "ok": "20171",
        "ko": "16540"
    },
    "percentiles3": {
        "total": "21766",
        "ok": "22774",
        "ko": "20275"
    },
    "percentiles4": {
        "total": "23134",
        "ok": "23375",
        "ko": "20346"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 53,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 47,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.128",
        "ko": "1"
    }
}
    },"req_request-4-matom-e3907": {
        type: "REQUEST",
        name: "request_4_matomo",
path: "request_4_matomo",
pathFormatted: "req_request-4-matom-e3907",
stats: {
    "name": "request_4_matomo",
    "numberOfRequests": {
        "total": "100",
        "ok": "50",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "12424",
        "ok": "13679",
        "ko": "12424"
    },
    "maxResponseTime": {
        "total": "20998",
        "ok": "20998",
        "ko": "20579"
    },
    "meanResponseTime": {
        "total": "17032",
        "ok": "17484",
        "ko": "16580"
    },
    "standardDeviation": {
        "total": "2222",
        "ok": "1885",
        "ko": "2432"
    },
    "percentiles1": {
        "total": "16610",
        "ok": "17343",
        "ko": "16394"
    },
    "percentiles2": {
        "total": "18610",
        "ok": "18635",
        "ko": "16988"
    },
    "percentiles3": {
        "total": "20596",
        "ok": "20964",
        "ko": "20538"
    },
    "percentiles4": {
        "total": "20976",
        "ok": "20987",
        "ko": "20574"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 50,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.064",
        "ko": "1.064"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "100",
        "ok": "52",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "12433",
        "ok": "14421",
        "ko": "12433"
    },
    "maxResponseTime": {
        "total": "24943",
        "ok": "24943",
        "ko": "20405"
    },
    "meanResponseTime": {
        "total": "17323",
        "ok": "18406",
        "ko": "16151"
    },
    "standardDeviation": {
        "total": "2454",
        "ok": "2260",
        "ko": "2090"
    },
    "percentiles1": {
        "total": "16753",
        "ok": "18657",
        "ko": "16099"
    },
    "percentiles2": {
        "total": "19377",
        "ok": "19601",
        "ko": "16619"
    },
    "percentiles3": {
        "total": "21475",
        "ok": "21852",
        "ko": "20316"
    },
    "percentiles4": {
        "total": "21952",
        "ok": "23402",
        "ko": "20403"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.106",
        "ko": "1.021"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "100",
        "ok": "54",
        "ko": "46"
    },
    "minResponseTime": {
        "total": "390",
        "ok": "390",
        "ko": "12432"
    },
    "maxResponseTime": {
        "total": "22348",
        "ok": "22348",
        "ko": "20398"
    },
    "meanResponseTime": {
        "total": "16669",
        "ok": "17344",
        "ko": "15877"
    },
    "standardDeviation": {
        "total": "3634",
        "ok": "4479",
        "ko": "1999"
    },
    "percentiles1": {
        "total": "16611",
        "ok": "18421",
        "ko": "15964"
    },
    "percentiles2": {
        "total": "19286",
        "ok": "19794",
        "ko": "16503"
    },
    "percentiles3": {
        "total": "21502",
        "ok": "21924",
        "ko": "20275"
    },
    "percentiles4": {
        "total": "22297",
        "ok": "22320",
        "ko": "20348"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 53,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 46,
    "percentage": 46
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.149",
        "ko": "0.979"
    }
}
    },"req_request-6-font-15e7e": {
        type: "REQUEST",
        name: "request_6_font",
path: "request_6_font",
pathFormatted: "req_request-6-font-15e7e",
stats: {
    "name": "request_6_font",
    "numberOfRequests": {
        "total": "100",
        "ok": "50",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "161",
        "ok": "161",
        "ko": "12207"
    },
    "maxResponseTime": {
        "total": "22552",
        "ok": "22552",
        "ko": "20398"
    },
    "meanResponseTime": {
        "total": "16615",
        "ok": "17306",
        "ko": "15924"
    },
    "standardDeviation": {
        "total": "3840",
        "ok": "4867",
        "ko": "2201"
    },
    "percentiles1": {
        "total": "16531",
        "ok": "18567",
        "ko": "15947"
    },
    "percentiles2": {
        "total": "19371",
        "ok": "19759",
        "ko": "16565"
    },
    "percentiles3": {
        "total": "20955",
        "ok": "22273",
        "ko": "20285"
    },
    "percentiles4": {
        "total": "22446",
        "ok": "22500",
        "ko": "20365"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 47,
    "percentage": 47
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.064",
        "ko": "1.064"
    }
}
    },"req_app-css-13ea8": {
        type: "REQUEST",
        name: "App.css",
path: "App.css",
pathFormatted: "req_app-css-13ea8",
stats: {
    "name": "App.css",
    "numberOfRequests": {
        "total": "100",
        "ok": "64",
        "ko": "36"
    },
    "minResponseTime": {
        "total": "155",
        "ok": "155",
        "ko": "16039"
    },
    "maxResponseTime": {
        "total": "43048",
        "ok": "43048",
        "ko": "21543"
    },
    "meanResponseTime": {
        "total": "20088",
        "ok": "20590",
        "ko": "19196"
    },
    "standardDeviation": {
        "total": "5803",
        "ok": "7049",
        "ko": "1990"
    },
    "percentiles1": {
        "total": "20234",
        "ok": "20194",
        "ko": "20489"
    },
    "percentiles2": {
        "total": "20877",
        "ok": "21428",
        "ko": "20721"
    },
    "percentiles3": {
        "total": "25482",
        "ok": "36595",
        "ko": "21290"
    },
    "percentiles4": {
        "total": "42616",
        "ok": "42773",
        "ko": "21472"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 36,
    "percentage": 36
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.362",
        "ko": "0.766"
    }
}
    },"req_coast-228x228-p-35307": {
        type: "REQUEST",
        name: "coast-228x228.png",
path: "coast-228x228.png",
pathFormatted: "req_coast-228x228-p-35307",
stats: {
    "name": "coast-228x228.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "64",
        "ko": "36"
    },
    "minResponseTime": {
        "total": "497",
        "ok": "497",
        "ko": "16039"
    },
    "maxResponseTime": {
        "total": "42988",
        "ok": "42988",
        "ko": "21536"
    },
    "meanResponseTime": {
        "total": "19785",
        "ok": "20120",
        "ko": "19189"
    },
    "standardDeviation": {
        "total": "4733",
        "ok": "5697",
        "ko": "1993"
    },
    "percentiles1": {
        "total": "20108",
        "ok": "20035",
        "ko": "20480"
    },
    "percentiles2": {
        "total": "20914",
        "ok": "21200",
        "ko": "20716"
    },
    "percentiles3": {
        "total": "22344",
        "ok": "26465",
        "ko": "21285"
    },
    "percentiles4": {
        "total": "36984",
        "ok": "39167",
        "ko": "21460"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 36,
    "percentage": 36
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.362",
        "ko": "0.766"
    }
}
    },"req_request-20-taxo-0d791": {
        type: "REQUEST",
        name: "request_20_TaxonMedia",
path: "request_20_TaxonMedia",
pathFormatted: "req_request-20-taxo-0d791",
stats: {
    "name": "request_20_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "53",
        "ko": "47"
    },
    "minResponseTime": {
        "total": "11939",
        "ok": "11939",
        "ko": "12417"
    },
    "maxResponseTime": {
        "total": "22704",
        "ok": "22704",
        "ko": "20397"
    },
    "meanResponseTime": {
        "total": "17365",
        "ok": "18638",
        "ko": "15929"
    },
    "standardDeviation": {
        "total": "2571",
        "ok": "2386",
        "ko": "1937"
    },
    "percentiles1": {
        "total": "16718",
        "ok": "18902",
        "ko": "15955"
    },
    "percentiles2": {
        "total": "19601",
        "ok": "20470",
        "ko": "16511"
    },
    "percentiles3": {
        "total": "21592",
        "ok": "21882",
        "ko": "20279"
    },
    "percentiles4": {
        "total": "22601",
        "ok": "22650",
        "ko": "20345"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 53,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 47,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.128",
        "ko": "1"
    }
}
    },"req_vendors-3ff1a80-e4689": {
        type: "REQUEST",
        name: "vendors.3ff1a804037601f958d7.js",
path: "vendors.3ff1a804037601f958d7.js",
pathFormatted: "req_vendors-3ff1a80-e4689",
stats: {
    "name": "vendors.3ff1a804037601f958d7.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "16053",
        "ok": "20044",
        "ko": "16053"
    },
    "maxResponseTime": {
        "total": "43568",
        "ok": "43568",
        "ko": "21542"
    },
    "meanResponseTime": {
        "total": "24947",
        "ok": "27884",
        "ko": "19246"
    },
    "standardDeviation": {
        "total": "5297",
        "ok": "3893",
        "ko": "1968"
    },
    "percentiles1": {
        "total": "26327",
        "ok": "27209",
        "ko": "20481"
    },
    "percentiles2": {
        "total": "27631",
        "ok": "28202",
        "ko": "20718"
    },
    "percentiles3": {
        "total": "29971",
        "ok": "35790",
        "ko": "21285"
    },
    "percentiles4": {
        "total": "43346",
        "ok": "43422",
        "ko": "21467"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.404",
        "ko": "0.723"
    }
}
    },"req_request-36-9ad97": {
        type: "REQUEST",
        name: "request_36",
path: "request_36",
pathFormatted: "req_request-36-9ad97",
stats: {
    "name": "request_36",
    "numberOfRequests": {
        "total": "100",
        "ok": "53",
        "ko": "47"
    },
    "minResponseTime": {
        "total": "305",
        "ok": "305",
        "ko": "12786"
    },
    "maxResponseTime": {
        "total": "22901",
        "ok": "22901",
        "ko": "20396"
    },
    "meanResponseTime": {
        "total": "16157",
        "ok": "16195",
        "ko": "16114"
    },
    "standardDeviation": {
        "total": "4633",
        "ok": "6061",
        "ko": "2063"
    },
    "percentiles1": {
        "total": "16474",
        "ok": "17792",
        "ko": "16109"
    },
    "percentiles2": {
        "total": "19239",
        "ok": "19624",
        "ko": "16652"
    },
    "percentiles3": {
        "total": "21249",
        "ok": "21851",
        "ko": "20288"
    },
    "percentiles4": {
        "total": "22814",
        "ok": "22855",
        "ko": "20366"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 48,
    "percentage": 48
},
    "group4": {
    "name": "failed",
    "count": 47,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.128",
        "ko": "1"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "100",
        "ok": "53",
        "ko": "47"
    },
    "minResponseTime": {
        "total": "476",
        "ok": "476",
        "ko": "12404"
    },
    "maxResponseTime": {
        "total": "21491",
        "ok": "21491",
        "ko": "20395"
    },
    "meanResponseTime": {
        "total": "16667",
        "ok": "17389",
        "ko": "15854"
    },
    "standardDeviation": {
        "total": "3134",
        "ok": "3722",
        "ko": "2009"
    },
    "percentiles1": {
        "total": "16511",
        "ok": "18341",
        "ko": "15955"
    },
    "percentiles2": {
        "total": "18841",
        "ok": "19237",
        "ko": "16511"
    },
    "percentiles3": {
        "total": "20907",
        "ok": "21240",
        "ko": "20278"
    },
    "percentiles4": {
        "total": "21487",
        "ok": "21489",
        "ko": "20346"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 47,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.128",
        "ko": "1"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "100",
        "ok": "55",
        "ko": "45"
    },
    "minResponseTime": {
        "total": "273",
        "ok": "273",
        "ko": "12431"
    },
    "maxResponseTime": {
        "total": "21492",
        "ok": "21492",
        "ko": "20395"
    },
    "meanResponseTime": {
        "total": "16365",
        "ok": "16724",
        "ko": "15927"
    },
    "standardDeviation": {
        "total": "3917",
        "ok": "4937",
        "ko": "1989"
    },
    "percentiles1": {
        "total": "16498",
        "ok": "18312",
        "ko": "15971"
    },
    "percentiles2": {
        "total": "18955",
        "ok": "19367",
        "ko": "16507"
    },
    "percentiles3": {
        "total": "20421",
        "ok": "21463",
        "ko": "20279"
    },
    "percentiles4": {
        "total": "21474",
        "ok": "21482",
        "ko": "20367"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 45,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.17",
        "ko": "0.957"
    }
}
    },"req_synaptix-client-67214": {
        type: "REQUEST",
        name: "synaptix-client-toolkit.3ff1a804037601f958d7.js",
path: "synaptix-client-toolkit.3ff1a804037601f958d7.js",
pathFormatted: "req_synaptix-client-67214",
stats: {
    "name": "synaptix-client-toolkit.3ff1a804037601f958d7.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "7035",
        "ok": "7035",
        "ko": "16056"
    },
    "maxResponseTime": {
        "total": "43497",
        "ok": "43497",
        "ko": "21544"
    },
    "meanResponseTime": {
        "total": "20319",
        "ok": "20872",
        "ko": "19246"
    },
    "standardDeviation": {
        "total": "4769",
        "ok": "5618",
        "ko": "1967"
    },
    "percentiles1": {
        "total": "19703",
        "ok": "19666",
        "ko": "20481"
    },
    "percentiles2": {
        "total": "20945",
        "ok": "21216",
        "ko": "20720"
    },
    "percentiles3": {
        "total": "24553",
        "ok": "35365",
        "ko": "21316"
    },
    "percentiles4": {
        "total": "42691",
        "ok": "42968",
        "ko": "21493"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.404",
        "ko": "0.723"
    }
}
    },"req_graphql-news-d7bbe": {
        type: "REQUEST",
        name: "graphQL_news",
path: "graphQL_news",
pathFormatted: "req_graphql-news-d7bbe",
stats: {
    "name": "graphQL_news",
    "numberOfRequests": {
        "total": "100",
        "ok": "53",
        "ko": "47"
    },
    "minResponseTime": {
        "total": "511",
        "ok": "511",
        "ko": "12207"
    },
    "maxResponseTime": {
        "total": "21491",
        "ok": "21491",
        "ko": "20399"
    },
    "meanResponseTime": {
        "total": "16307",
        "ok": "16403",
        "ko": "16199"
    },
    "standardDeviation": {
        "total": "3905",
        "ok": "5018",
        "ko": "2006"
    },
    "percentiles1": {
        "total": "16477",
        "ok": "17936",
        "ko": "16134"
    },
    "percentiles2": {
        "total": "18902",
        "ok": "19236",
        "ko": "16575"
    },
    "percentiles3": {
        "total": "20426",
        "ok": "21159",
        "ko": "20282"
    },
    "percentiles4": {
        "total": "21469",
        "ok": "21480",
        "ko": "20368"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 50,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 47,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.128",
        "ko": "1"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "100",
        "ok": "56",
        "ko": "44"
    },
    "minResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "12760"
    },
    "maxResponseTime": {
        "total": "22176",
        "ok": "22176",
        "ko": "20405"
    },
    "meanResponseTime": {
        "total": "16587",
        "ok": "17061",
        "ko": "15983"
    },
    "standardDeviation": {
        "total": "4132",
        "ok": "5188",
        "ko": "1975"
    },
    "percentiles1": {
        "total": "16528",
        "ok": "18240",
        "ko": "16013"
    },
    "percentiles2": {
        "total": "19447",
        "ok": "20135",
        "ko": "16510"
    },
    "percentiles3": {
        "total": "21704",
        "ok": "21997",
        "ko": "20288"
    },
    "percentiles4": {
        "total": "22165",
        "ok": "22170",
        "ko": "20402"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 44,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.191",
        "ko": "0.936"
    }
}
    },"req_request-34-d9757": {
        type: "REQUEST",
        name: "request_34",
path: "request_34",
pathFormatted: "req_request-34-d9757",
stats: {
    "name": "request_34",
    "numberOfRequests": {
        "total": "100",
        "ok": "53",
        "ko": "47"
    },
    "minResponseTime": {
        "total": "1881",
        "ok": "1881",
        "ko": "12209"
    },
    "maxResponseTime": {
        "total": "23531",
        "ok": "23531",
        "ko": "20394"
    },
    "meanResponseTime": {
        "total": "17027",
        "ok": "17903",
        "ko": "16039"
    },
    "standardDeviation": {
        "total": "3058",
        "ok": "3437",
        "ko": "2176"
    },
    "percentiles1": {
        "total": "16550",
        "ok": "18915",
        "ko": "15954"
    },
    "percentiles2": {
        "total": "19559",
        "ok": "19782",
        "ko": "16650"
    },
    "percentiles3": {
        "total": "21180",
        "ok": "22157",
        "ko": "20287"
    },
    "percentiles4": {
        "total": "22877",
        "ok": "23187",
        "ko": "20365"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 53,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 47,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.128",
        "ko": "1"
    }
}
    },"req_request-21-taxo-4adce": {
        type: "REQUEST",
        name: "request_21_TaxonMedia",
path: "request_21_TaxonMedia",
pathFormatted: "req_request-21-taxo-4adce",
stats: {
    "name": "request_21_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "50",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "11938",
        "ok": "11938",
        "ko": "12256"
    },
    "maxResponseTime": {
        "total": "23146",
        "ok": "23146",
        "ko": "20398"
    },
    "meanResponseTime": {
        "total": "17204",
        "ok": "18445",
        "ko": "15963"
    },
    "standardDeviation": {
        "total": "2597",
        "ok": "2442",
        "ko": "2110"
    },
    "percentiles1": {
        "total": "16546",
        "ok": "18849",
        "ko": "15967"
    },
    "percentiles2": {
        "total": "19608",
        "ok": "20301",
        "ko": "16513"
    },
    "percentiles3": {
        "total": "20879",
        "ok": "21924",
        "ko": "20282"
    },
    "percentiles4": {
        "total": "22892",
        "ok": "23020",
        "ko": "20366"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 50,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.064",
        "ko": "1.064"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "100",
        "ok": "52",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "547",
        "ok": "547",
        "ko": "12417"
    },
    "maxResponseTime": {
        "total": "23652",
        "ok": "23652",
        "ko": "20285"
    },
    "meanResponseTime": {
        "total": "16282",
        "ok": "16626",
        "ko": "15909"
    },
    "standardDeviation": {
        "total": "4047",
        "ok": "5245",
        "ko": "2011"
    },
    "percentiles1": {
        "total": "16489",
        "ok": "17699",
        "ko": "16017"
    },
    "percentiles2": {
        "total": "18891",
        "ok": "19383",
        "ko": "16522"
    },
    "percentiles3": {
        "total": "20876",
        "ok": "21902",
        "ko": "20231"
    },
    "percentiles4": {
        "total": "22455",
        "ok": "23035",
        "ko": "20285"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 51,
    "percentage": 51
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.106",
        "ko": "1.021"
    }
}
    },"req_favicon-ico-8af3a": {
        type: "REQUEST",
        name: "favicon.ico",
path: "favicon.ico",
pathFormatted: "req_favicon-ico-8af3a",
stats: {
    "name": "favicon.ico",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "11794",
        "ok": "11794",
        "ko": "16051"
    },
    "maxResponseTime": {
        "total": "42673",
        "ok": "42673",
        "ko": "21533"
    },
    "meanResponseTime": {
        "total": "20849",
        "ok": "21698",
        "ko": "19271"
    },
    "standardDeviation": {
        "total": "5185",
        "ok": "6101",
        "ko": "1969"
    },
    "percentiles1": {
        "total": "20182",
        "ok": "19994",
        "ko": "20521"
    },
    "percentiles2": {
        "total": "20930",
        "ok": "21276",
        "ko": "20721"
    },
    "percentiles3": {
        "total": "35929",
        "ok": "38691",
        "ko": "21289"
    },
    "percentiles4": {
        "total": "41718",
        "ok": "42055",
        "ko": "21466"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.383",
        "ko": "0.745"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "100",
        "ok": "52",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "12206",
        "ok": "13685",
        "ko": "12206"
    },
    "maxResponseTime": {
        "total": "26524",
        "ok": "26524",
        "ko": "20579"
    },
    "meanResponseTime": {
        "total": "17203",
        "ok": "17799",
        "ko": "16557"
    },
    "standardDeviation": {
        "total": "2530",
        "ok": "2464",
        "ko": "2440"
    },
    "percentiles1": {
        "total": "16728",
        "ok": "17356",
        "ko": "16399"
    },
    "percentiles2": {
        "total": "18619",
        "ok": "18649",
        "ko": "17001"
    },
    "percentiles3": {
        "total": "20962",
        "ok": "21382",
        "ko": "20506"
    },
    "percentiles4": {
        "total": "24474",
        "ok": "25468",
        "ko": "20575"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.106",
        "ko": "1.021"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "100",
        "ok": "50",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "400",
        "ok": "400",
        "ko": "12431"
    },
    "maxResponseTime": {
        "total": "24692",
        "ok": "24692",
        "ko": "20396"
    },
    "meanResponseTime": {
        "total": "17466",
        "ok": "18669",
        "ko": "16263"
    },
    "standardDeviation": {
        "total": "3117",
        "ok": "3529",
        "ko": "2021"
    },
    "percentiles1": {
        "total": "16895",
        "ok": "19256",
        "ko": "16124"
    },
    "percentiles2": {
        "total": "19985",
        "ok": "20579",
        "ko": "16705"
    },
    "percentiles3": {
        "total": "22136",
        "ok": "22556",
        "ko": "20286"
    },
    "percentiles4": {
        "total": "23436",
        "ok": "24070",
        "ko": "20364"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 49,
    "percentage": 49
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.064",
        "ko": "1.064"
    }
}
    },"req_request-29-taxo-1f5dd": {
        type: "REQUEST",
        name: "request_29_TaxonMedia",
path: "request_29_TaxonMedia",
pathFormatted: "req_request-29-taxo-1f5dd",
stats: {
    "name": "request_29_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "52",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "11747",
        "ok": "11747",
        "ko": "12415"
    },
    "maxResponseTime": {
        "total": "23479",
        "ok": "23479",
        "ko": "20396"
    },
    "meanResponseTime": {
        "total": "17228",
        "ok": "18212",
        "ko": "16162"
    },
    "standardDeviation": {
        "total": "2439",
        "ok": "2443",
        "ko": "1933"
    },
    "percentiles1": {
        "total": "16550",
        "ok": "18660",
        "ko": "16154"
    },
    "percentiles2": {
        "total": "19297",
        "ok": "19686",
        "ko": "16619"
    },
    "percentiles3": {
        "total": "20965",
        "ok": "22303",
        "ko": "20276"
    },
    "percentiles4": {
        "total": "22799",
        "ok": "23129",
        "ko": "20345"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.106",
        "ko": "1.021"
    }
}
    },"req_graphql-riveran-e540f": {
        type: "REQUEST",
        name: "graphql_RiverAndGroundWaterObservation",
path: "graphql_RiverAndGroundWaterObservation",
pathFormatted: "req_graphql-riveran-e540f",
stats: {
    "name": "graphql_RiverAndGroundWaterObservation",
    "numberOfRequests": {
        "total": "100",
        "ok": "54",
        "ko": "46"
    },
    "minResponseTime": {
        "total": "3715",
        "ok": "3715",
        "ko": "12207"
    },
    "maxResponseTime": {
        "total": "22802",
        "ok": "22802",
        "ko": "20395"
    },
    "meanResponseTime": {
        "total": "16812",
        "ok": "17422",
        "ko": "16096"
    },
    "standardDeviation": {
        "total": "3218",
        "ok": "3824",
        "ko": "2097"
    },
    "percentiles1": {
        "total": "16512",
        "ok": "18395",
        "ko": "16038"
    },
    "percentiles2": {
        "total": "19327",
        "ok": "19801",
        "ko": "16671"
    },
    "percentiles3": {
        "total": "20865",
        "ok": "22020",
        "ko": "20287"
    },
    "percentiles4": {
        "total": "22389",
        "ok": "22581",
        "ko": "20366"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 54,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 46,
    "percentage": 46
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.149",
        "ko": "0.979"
    }
}
    },"req_graphql-searchp-61328": {
        type: "REQUEST",
        name: "graphql_SearchPlaces",
path: "graphql_SearchPlaces",
pathFormatted: "req_graphql-searchp-61328",
stats: {
    "name": "graphql_SearchPlaces",
    "numberOfRequests": {
        "total": "100",
        "ok": "54",
        "ko": "46"
    },
    "minResponseTime": {
        "total": "711",
        "ok": "711",
        "ko": "12417"
    },
    "maxResponseTime": {
        "total": "21490",
        "ok": "21490",
        "ko": "20399"
    },
    "meanResponseTime": {
        "total": "16172",
        "ok": "16268",
        "ko": "16059"
    },
    "standardDeviation": {
        "total": "3961",
        "ok": "4990",
        "ko": "2205"
    },
    "percentiles1": {
        "total": "16474",
        "ok": "17848",
        "ko": "16014"
    },
    "percentiles2": {
        "total": "18840",
        "ok": "19311",
        "ko": "16578"
    },
    "percentiles3": {
        "total": "20441",
        "ok": "21136",
        "ko": "20285"
    },
    "percentiles4": {
        "total": "21487",
        "ok": "21488",
        "ko": "20368"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 46,
    "percentage": 46
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.149",
        "ko": "0.979"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "100",
        "ok": "54",
        "ko": "46"
    },
    "minResponseTime": {
        "total": "268",
        "ok": "268",
        "ko": "12431"
    },
    "maxResponseTime": {
        "total": "27581",
        "ok": "27581",
        "ko": "20397"
    },
    "meanResponseTime": {
        "total": "16752",
        "ok": "17182",
        "ko": "16247"
    },
    "standardDeviation": {
        "total": "4437",
        "ok": "5674",
        "ko": "2128"
    },
    "percentiles1": {
        "total": "16581",
        "ok": "18219",
        "ko": "16123"
    },
    "percentiles2": {
        "total": "19533",
        "ok": "19930",
        "ko": "16697"
    },
    "percentiles3": {
        "total": "22180",
        "ok": "24134",
        "ko": "20285"
    },
    "percentiles4": {
        "total": "26107",
        "ok": "26792",
        "ko": "20368"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 51,
    "percentage": 51
},
    "group4": {
    "name": "failed",
    "count": 46,
    "percentage": 46
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.149",
        "ko": "0.979"
    }
}
    },"req_request-28-taxo-f261b": {
        type: "REQUEST",
        name: "request_28_TaxonMedia",
path: "request_28_TaxonMedia",
pathFormatted: "req_request-28-taxo-f261b",
stats: {
    "name": "request_28_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "50",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "11743",
        "ok": "11743",
        "ko": "12430"
    },
    "maxResponseTime": {
        "total": "24295",
        "ok": "24295",
        "ko": "20408"
    },
    "meanResponseTime": {
        "total": "17284",
        "ok": "18510",
        "ko": "16059"
    },
    "standardDeviation": {
        "total": "2659",
        "ok": "2586",
        "ko": "2111"
    },
    "percentiles1": {
        "total": "16574",
        "ok": "18929",
        "ko": "16014"
    },
    "percentiles2": {
        "total": "19609",
        "ok": "20230",
        "ko": "16553"
    },
    "percentiles3": {
        "total": "21694",
        "ok": "22092",
        "ko": "20312"
    },
    "percentiles4": {
        "total": "23328",
        "ok": "23816",
        "ko": "20403"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 50,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.064",
        "ko": "1.064"
    }
}
    },"req_favicon-48x48-p-1e880": {
        type: "REQUEST",
        name: "favicon-48x48.png",
path: "favicon-48x48.png",
pathFormatted: "req_favicon-48x48-p-1e880",
stats: {
    "name": "favicon-48x48.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "13785",
        "ok": "13785",
        "ko": "16056"
    },
    "maxResponseTime": {
        "total": "43020",
        "ok": "43020",
        "ko": "21534"
    },
    "meanResponseTime": {
        "total": "20024",
        "ok": "20482",
        "ko": "19174"
    },
    "standardDeviation": {
        "total": "3834",
        "ok": "4459",
        "ko": "1989"
    },
    "percentiles1": {
        "total": "19803",
        "ok": "19743",
        "ko": "20469"
    },
    "percentiles2": {
        "total": "20862",
        "ok": "21159",
        "ko": "20718"
    },
    "percentiles3": {
        "total": "22987",
        "ok": "23883",
        "ko": "21288"
    },
    "percentiles4": {
        "total": "36673",
        "ok": "38917",
        "ko": "21460"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.383",
        "ko": "0.745"
    }
}
    },"req_graphql-taxon-f4085": {
        type: "REQUEST",
        name: "graphql_Taxon",
path: "graphql_Taxon",
pathFormatted: "req_graphql-taxon-f4085",
stats: {
    "name": "graphql_Taxon",
    "numberOfRequests": {
        "total": "100",
        "ok": "52",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "2483",
        "ok": "2483",
        "ko": "12207"
    },
    "maxResponseTime": {
        "total": "24382",
        "ok": "24382",
        "ko": "20399"
    },
    "meanResponseTime": {
        "total": "16892",
        "ok": "17799",
        "ko": "15910"
    },
    "standardDeviation": {
        "total": "3426",
        "ok": "4134",
        "ko": "2022"
    },
    "percentiles1": {
        "total": "16552",
        "ok": "18587",
        "ko": "15963"
    },
    "percentiles2": {
        "total": "19391",
        "ok": "20313",
        "ko": "16512"
    },
    "percentiles3": {
        "total": "21473",
        "ok": "21862",
        "ko": "20279"
    },
    "percentiles4": {
        "total": "23048",
        "ok": "23695",
        "ko": "20345"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.106",
        "ko": "1.021"
    }
}
    },"req_request-24-taxo-b5f25": {
        type: "REQUEST",
        name: "request_24_TaxonMedia",
path: "request_24_TaxonMedia",
pathFormatted: "req_request-24-taxo-b5f25",
stats: {
    "name": "request_24_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "52",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "12432",
        "ok": "13744",
        "ko": "12432"
    },
    "maxResponseTime": {
        "total": "24001",
        "ok": "24001",
        "ko": "20404"
    },
    "meanResponseTime": {
        "total": "17397",
        "ok": "18507",
        "ko": "16194"
    },
    "standardDeviation": {
        "total": "2574",
        "ok": "2557",
        "ko": "1985"
    },
    "percentiles1": {
        "total": "16762",
        "ok": "18863",
        "ko": "16107"
    },
    "percentiles2": {
        "total": "19587",
        "ok": "20115",
        "ko": "16725"
    },
    "percentiles3": {
        "total": "21973",
        "ok": "22324",
        "ko": "20315"
    },
    "percentiles4": {
        "total": "22909",
        "ok": "23438",
        "ko": "20401"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 52,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.106",
        "ko": "1.021"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "100",
        "ok": "52",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "892",
        "ok": "892",
        "ko": "12432"
    },
    "maxResponseTime": {
        "total": "23495",
        "ok": "23495",
        "ko": "20393"
    },
    "meanResponseTime": {
        "total": "17678",
        "ok": "19187",
        "ko": "16045"
    },
    "standardDeviation": {
        "total": "3772",
        "ok": "4296",
        "ko": "2124"
    },
    "percentiles1": {
        "total": "17775",
        "ok": "20001",
        "ko": "15965"
    },
    "percentiles2": {
        "total": "20340",
        "ok": "21659",
        "ko": "16509"
    },
    "percentiles3": {
        "total": "22460",
        "ok": "22824",
        "ko": "20282"
    },
    "percentiles4": {
        "total": "22855",
        "ok": "23166",
        "ko": "20364"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 50,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.106",
        "ko": "1.021"
    }
}
    },"req_main-3ff1a80403-1672c": {
        type: "REQUEST",
        name: "main.3ff1a804037601f958d7.js",
path: "main.3ff1a804037601f958d7.js",
pathFormatted: "req_main-3ff1a80403-1672c",
stats: {
    "name": "main.3ff1a804037601f958d7.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "971",
        "ok": "971",
        "ko": "16053"
    },
    "maxResponseTime": {
        "total": "43751",
        "ok": "43751",
        "ko": "21425"
    },
    "meanResponseTime": {
        "total": "20739",
        "ok": "21560",
        "ko": "19146"
    },
    "standardDeviation": {
        "total": "5336",
        "ok": "6247",
        "ko": "2034"
    },
    "percentiles1": {
        "total": "20583",
        "ok": "21071",
        "ko": "20480"
    },
    "percentiles2": {
        "total": "21971",
        "ok": "22871",
        "ko": "20720"
    },
    "percentiles3": {
        "total": "29512",
        "ok": "34994",
        "ko": "21293"
    },
    "percentiles4": {
        "total": "40078",
        "ok": "41339",
        "ko": "21396"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.128",
        "ok": "1.404",
        "ko": "0.723"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
