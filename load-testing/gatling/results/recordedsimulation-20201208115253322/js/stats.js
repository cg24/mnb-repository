var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4700",
        "ok": "3299",
        "ko": "1401"
    },
    "minResponseTime": {
        "total": "138",
        "ok": "138",
        "ko": "11608"
    },
    "maxResponseTime": {
        "total": "42793",
        "ok": "42793",
        "ko": "23351"
    },
    "meanResponseTime": {
        "total": "16799",
        "ok": "17553",
        "ko": "15022"
    },
    "standardDeviation": {
        "total": "5063",
        "ok": "5820",
        "ko": "1324"
    },
    "percentiles1": {
        "total": "17563",
        "ok": "18466",
        "ko": "14767"
    },
    "percentiles2": {
        "total": "19091",
        "ok": "20098",
        "ko": "15318"
    },
    "percentiles3": {
        "total": "24157",
        "ok": "25267",
        "ko": "17430"
    },
    "percentiles4": {
        "total": "30883",
        "ok": "31301",
        "ko": "17901"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 45,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 31,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3223,
    "percentage": 69
},
    "group4": {
    "name": "failed",
    "count": 1401,
    "percentage": 30
},
    "meanNumberOfRequestsPerSecond": {
        "total": "92.157",
        "ok": "64.686",
        "ko": "27.471"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "583",
        "ok": "583",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7904",
        "ok": "7904",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1770",
        "ok": "1770",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1143",
        "ok": "1143",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1532",
        "ok": "1532",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3524",
        "ok": "3524",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7784",
        "ok": "7784",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 18,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 76,
    "percentage": 76
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.961",
        "ko": "-"
    }
}
    },"req_request-29-taxo-1f5dd": {
        type: "REQUEST",
        name: "request_29_TaxonMedia",
path: "request_29_TaxonMedia",
pathFormatted: "req_request-29-taxo-1f5dd",
stats: {
    "name": "request_29_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "10041",
        "ok": "10041",
        "ko": "11685"
    },
    "maxResponseTime": {
        "total": "36542",
        "ok": "36542",
        "ko": "17795"
    },
    "meanResponseTime": {
        "total": "16992",
        "ok": "18073",
        "ko": "14984"
    },
    "standardDeviation": {
        "total": "3958",
        "ok": "4481",
        "ko": "1127"
    },
    "percentiles1": {
        "total": "17036",
        "ok": "18003",
        "ko": "14751"
    },
    "percentiles2": {
        "total": "18743",
        "ok": "19678",
        "ko": "15269"
    },
    "percentiles3": {
        "total": "23165",
        "ok": "24500",
        "ko": "16783"
    },
    "percentiles4": {
        "total": "31991",
        "ok": "33600",
        "ko": "17467"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.275",
        "ko": "0.686"
    }
}
    },"req_coast-228x228-p-35307": {
        type: "REQUEST",
        name: "coast-228x228.png",
path: "coast-228x228.png",
pathFormatted: "req_coast-228x228-p-35307",
stats: {
    "name": "coast-228x228.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "85",
        "ko": "15"
    },
    "minResponseTime": {
        "total": "593",
        "ok": "593",
        "ko": "11613"
    },
    "maxResponseTime": {
        "total": "30748",
        "ok": "30748",
        "ko": "17903"
    },
    "meanResponseTime": {
        "total": "18559",
        "ok": "19050",
        "ko": "15776"
    },
    "standardDeviation": {
        "total": "4070",
        "ok": "4142",
        "ko": "2027"
    },
    "percentiles1": {
        "total": "18824",
        "ok": "19186",
        "ko": "16430"
    },
    "percentiles2": {
        "total": "20274",
        "ok": "20646",
        "ko": "17667"
    },
    "percentiles3": {
        "total": "22780",
        "ok": "22789",
        "ko": "17893"
    },
    "percentiles4": {
        "total": "28687",
        "ok": "28999",
        "ko": "17901"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 83,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 15,
    "percentage": 15
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.667",
        "ko": "0.294"
    }
}
    },"req_graphql-news-4c309": {
        type: "REQUEST",
        name: "graphql_News",
path: "graphql_News",
pathFormatted: "req_graphql-news-4c309",
stats: {
    "name": "graphql_News",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "1231",
        "ok": "1231",
        "ko": "11608"
    },
    "maxResponseTime": {
        "total": "22009",
        "ok": "22009",
        "ko": "16831"
    },
    "meanResponseTime": {
        "total": "15539",
        "ok": "15875",
        "ko": "14887"
    },
    "standardDeviation": {
        "total": "3854",
        "ok": "4645",
        "ko": "1076"
    },
    "percentiles1": {
        "total": "16521",
        "ok": "17891",
        "ko": "14781"
    },
    "percentiles2": {
        "total": "18303",
        "ok": "18812",
        "ko": "15119"
    },
    "percentiles3": {
        "total": "19620",
        "ok": "19674",
        "ko": "16755"
    },
    "percentiles4": {
        "total": "21715",
        "ok": "21816",
        "ko": "16809"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.294",
        "ko": "0.667"
    }
}
    },"req_request-6-font-15e7e": {
        type: "REQUEST",
        name: "request_6_font",
path: "request_6_font",
pathFormatted: "req_request-6-font-15e7e",
stats: {
    "name": "request_6_font",
    "numberOfRequests": {
        "total": "100",
        "ok": "63",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "1094",
        "ok": "1094",
        "ko": "11684"
    },
    "maxResponseTime": {
        "total": "30463",
        "ok": "30463",
        "ko": "17430"
    },
    "meanResponseTime": {
        "total": "16353",
        "ok": "17250",
        "ko": "14826"
    },
    "standardDeviation": {
        "total": "3979",
        "ok": "4715",
        "ko": "1104"
    },
    "percentiles1": {
        "total": "16606",
        "ok": "18258",
        "ko": "14752"
    },
    "percentiles2": {
        "total": "18905",
        "ok": "19642",
        "ko": "15101"
    },
    "percentiles3": {
        "total": "21261",
        "ok": "23448",
        "ko": "16796"
    },
    "percentiles4": {
        "total": "24458",
        "ok": "26702",
        "ko": "17240"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 37
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.235",
        "ko": "0.725"
    }
}
    },"req_favicon-48x48-p-1e880": {
        type: "REQUEST",
        name: "favicon-48x48.png",
path: "favicon-48x48.png",
pathFormatted: "req_favicon-48x48-p-1e880",
stats: {
    "name": "favicon-48x48.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "85",
        "ko": "15"
    },
    "minResponseTime": {
        "total": "1479",
        "ok": "1479",
        "ko": "11685"
    },
    "maxResponseTime": {
        "total": "28866",
        "ok": "28866",
        "ko": "17924"
    },
    "meanResponseTime": {
        "total": "18055",
        "ok": "18392",
        "ko": "16149"
    },
    "standardDeviation": {
        "total": "3769",
        "ok": "3932",
        "ko": "1688"
    },
    "percentiles1": {
        "total": "18815",
        "ok": "18928",
        "ko": "16605"
    },
    "percentiles2": {
        "total": "19560",
        "ok": "19651",
        "ko": "17666"
    },
    "percentiles3": {
        "total": "21156",
        "ok": "21178",
        "ko": "17896"
    },
    "percentiles4": {
        "total": "26322",
        "ok": "26707",
        "ko": "17918"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 85,
    "percentage": 85
},
    "group4": {
    "name": "failed",
    "count": 15,
    "percentage": 15
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.667",
        "ko": "0.294"
    }
}
    },"req_request-35-3745a": {
        type: "REQUEST",
        name: "request_35",
path: "request_35",
pathFormatted: "req_request-35-3745a",
stats: {
    "name": "request_35",
    "numberOfRequests": {
        "total": "100",
        "ok": "63",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "359",
        "ok": "359",
        "ko": "11682"
    },
    "maxResponseTime": {
        "total": "42440",
        "ok": "42440",
        "ko": "17796"
    },
    "meanResponseTime": {
        "total": "16154",
        "ok": "16886",
        "ko": "14908"
    },
    "standardDeviation": {
        "total": "5364",
        "ok": "6591",
        "ko": "1154"
    },
    "percentiles1": {
        "total": "15571",
        "ok": "18303",
        "ko": "14623"
    },
    "percentiles2": {
        "total": "18678",
        "ok": "20810",
        "ko": "15212"
    },
    "percentiles3": {
        "total": "22309",
        "ok": "22856",
        "ko": "16768"
    },
    "percentiles4": {
        "total": "23901",
        "ok": "30830",
        "ko": "17449"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 60,
    "percentage": 60
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 37
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.235",
        "ko": "0.725"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "100",
        "ok": "63",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "170",
        "ok": "170",
        "ko": "11609"
    },
    "maxResponseTime": {
        "total": "31774",
        "ok": "31774",
        "ko": "17797"
    },
    "meanResponseTime": {
        "total": "16133",
        "ok": "16910",
        "ko": "14812"
    },
    "standardDeviation": {
        "total": "4139",
        "ok": "4954",
        "ko": "1318"
    },
    "percentiles1": {
        "total": "15677",
        "ok": "18167",
        "ko": "14577"
    },
    "percentiles2": {
        "total": "18623",
        "ok": "19706",
        "ko": "15124"
    },
    "percentiles3": {
        "total": "21153",
        "ok": "22652",
        "ko": "16951"
    },
    "percentiles4": {
        "total": "24705",
        "ok": "27347",
        "ko": "17665"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 37
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.235",
        "ko": "0.725"
    }
}
    },"req_synaptix-client-67214": {
        type: "REQUEST",
        name: "synaptix-client-toolkit.3ff1a804037601f958d7.js",
path: "synaptix-client-toolkit.3ff1a804037601f958d7.js",
pathFormatted: "req_synaptix-client-67214",
stats: {
    "name": "synaptix-client-toolkit.3ff1a804037601f958d7.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "88",
        "ko": "12"
    },
    "minResponseTime": {
        "total": "311",
        "ok": "311",
        "ko": "11610"
    },
    "maxResponseTime": {
        "total": "33288",
        "ok": "33288",
        "ko": "17905"
    },
    "meanResponseTime": {
        "total": "18961",
        "ok": "19410",
        "ko": "15674"
    },
    "standardDeviation": {
        "total": "4838",
        "ok": "4930",
        "ko": "2127"
    },
    "percentiles1": {
        "total": "19123",
        "ok": "19465",
        "ko": "16434"
    },
    "percentiles2": {
        "total": "21513",
        "ok": "21723",
        "ko": "17663"
    },
    "percentiles3": {
        "total": "25346",
        "ok": "25660",
        "ko": "17826"
    },
    "percentiles4": {
        "total": "30112",
        "ok": "30497",
        "ko": "17889"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 86,
    "percentage": 86
},
    "group4": {
    "name": "failed",
    "count": 12,
    "percentage": 12
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.725",
        "ko": "0.235"
    }
}
    },"req_request-26-taxo-41b6f": {
        type: "REQUEST",
        name: "request_26_TaxonMedia",
path: "request_26_TaxonMedia",
pathFormatted: "req_request-26-taxo-41b6f",
stats: {
    "name": "request_26_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "10208",
        "ok": "10208",
        "ko": "11611"
    },
    "maxResponseTime": {
        "total": "23444",
        "ok": "23444",
        "ko": "17430"
    },
    "meanResponseTime": {
        "total": "16699",
        "ok": "17580",
        "ko": "14989"
    },
    "standardDeviation": {
        "total": "3321",
        "ok": "3714",
        "ko": "1114"
    },
    "percentiles1": {
        "total": "16717",
        "ok": "18217",
        "ko": "14795"
    },
    "percentiles2": {
        "total": "19035",
        "ok": "20321",
        "ko": "15213"
    },
    "percentiles3": {
        "total": "22133",
        "ok": "22725",
        "ko": "16791"
    },
    "percentiles4": {
        "total": "23066",
        "ok": "23196",
        "ko": "17232"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.294",
        "ko": "0.667"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "100",
        "ok": "63",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "318",
        "ok": "318",
        "ko": "11685"
    },
    "maxResponseTime": {
        "total": "32443",
        "ok": "32443",
        "ko": "17797"
    },
    "meanResponseTime": {
        "total": "16817",
        "ok": "17824",
        "ko": "15102"
    },
    "standardDeviation": {
        "total": "4715",
        "ok": "5625",
        "ko": "1245"
    },
    "percentiles1": {
        "total": "16981",
        "ok": "18338",
        "ko": "14826"
    },
    "percentiles2": {
        "total": "18982",
        "ok": "21251",
        "ko": "15336"
    },
    "percentiles3": {
        "total": "22760",
        "ok": "23494",
        "ko": "17191"
    },
    "percentiles4": {
        "total": "30272",
        "ok": "31083",
        "ko": "17665"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 60,
    "percentage": 60
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 37
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.235",
        "ko": "0.725"
    }
}
    },"req_graphql-taxon-f4085": {
        type: "REQUEST",
        name: "graphql_Taxon",
path: "graphql_Taxon",
pathFormatted: "req_graphql-taxon-f4085",
stats: {
    "name": "graphql_Taxon",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "595",
        "ok": "595",
        "ko": "11614"
    },
    "maxResponseTime": {
        "total": "25272",
        "ok": "25272",
        "ko": "19580"
    },
    "meanResponseTime": {
        "total": "15930",
        "ok": "16452",
        "ko": "14960"
    },
    "standardDeviation": {
        "total": "4159",
        "ok": "4950",
        "ko": "1573"
    },
    "percentiles1": {
        "total": "16833",
        "ok": "18108",
        "ko": "14577"
    },
    "percentiles2": {
        "total": "18461",
        "ok": "18905",
        "ko": "15233"
    },
    "percentiles3": {
        "total": "20150",
        "ok": "20938",
        "ko": "17542"
    },
    "percentiles4": {
        "total": "24541",
        "ok": "24800",
        "ko": "18974"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.275",
        "ko": "0.686"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "100",
        "ok": "62",
        "ko": "38"
    },
    "minResponseTime": {
        "total": "711",
        "ok": "711",
        "ko": "11614"
    },
    "maxResponseTime": {
        "total": "19723",
        "ok": "19723",
        "ko": "17796"
    },
    "meanResponseTime": {
        "total": "15837",
        "ok": "16371",
        "ko": "14965"
    },
    "standardDeviation": {
        "total": "2878",
        "ok": "3426",
        "ko": "1195"
    },
    "percentiles1": {
        "total": "16609",
        "ok": "17814",
        "ko": "14736"
    },
    "percentiles2": {
        "total": "18019",
        "ok": "18471",
        "ko": "15202"
    },
    "percentiles3": {
        "total": "19048",
        "ok": "19615",
        "ko": "16930"
    },
    "percentiles4": {
        "total": "19666",
        "ok": "19688",
        "ko": "17531"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 38,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.216",
        "ko": "0.745"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "559",
        "ok": "559",
        "ko": "11613"
    },
    "maxResponseTime": {
        "total": "22511",
        "ok": "22511",
        "ko": "16831"
    },
    "meanResponseTime": {
        "total": "16072",
        "ok": "16680",
        "ko": "14778"
    },
    "standardDeviation": {
        "total": "3005",
        "ok": "3390",
        "ko": "1159"
    },
    "percentiles1": {
        "total": "16801",
        "ok": "17934",
        "ko": "14728"
    },
    "percentiles2": {
        "total": "18325",
        "ok": "18647",
        "ko": "15105"
    },
    "percentiles3": {
        "total": "19112",
        "ok": "19608",
        "ko": "16758"
    },
    "percentiles4": {
        "total": "19671",
        "ok": "20589",
        "ko": "16812"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.333",
        "ko": "0.627"
    }
}
    },"req_request-34-d9757": {
        type: "REQUEST",
        name: "request_34",
path: "request_34",
pathFormatted: "req_request-34-d9757",
stats: {
    "name": "request_34",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "380",
        "ok": "380",
        "ko": "11686"
    },
    "maxResponseTime": {
        "total": "31097",
        "ok": "31097",
        "ko": "17430"
    },
    "meanResponseTime": {
        "total": "16411",
        "ok": "17177",
        "ko": "14990"
    },
    "standardDeviation": {
        "total": "3641",
        "ok": "4246",
        "ko": "1141"
    },
    "percentiles1": {
        "total": "16715",
        "ok": "18250",
        "ko": "14765"
    },
    "percentiles2": {
        "total": "18505",
        "ok": "19116",
        "ko": "15233"
    },
    "percentiles3": {
        "total": "21150",
        "ok": "21768",
        "ko": "16955"
    },
    "percentiles4": {
        "total": "24225",
        "ok": "26655",
        "ko": "17311"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.275",
        "ko": "0.686"
    }
}
    },"req_request-25-taxo-2081e": {
        type: "REQUEST",
        name: "request_25_TaxonMedia",
path: "request_25_TaxonMedia",
pathFormatted: "req_request-25-taxo-2081e",
stats: {
    "name": "request_25_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "9941",
        "ok": "9941",
        "ko": "11654"
    },
    "maxResponseTime": {
        "total": "23492",
        "ok": "23492",
        "ko": "16904"
    },
    "meanResponseTime": {
        "total": "16569",
        "ok": "17417",
        "ko": "14848"
    },
    "standardDeviation": {
        "total": "3192",
        "ok": "3525",
        "ko": "1108"
    },
    "percentiles1": {
        "total": "16782",
        "ok": "18343",
        "ko": "14577"
    },
    "percentiles2": {
        "total": "18592",
        "ok": "19429",
        "ko": "15173"
    },
    "percentiles3": {
        "total": "21928",
        "ok": "22647",
        "ko": "16775"
    },
    "percentiles4": {
        "total": "22876",
        "ok": "23081",
        "ko": "16875"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.314",
        "ko": "0.647"
    }
}
    },"req_graphql-riveran-e540f": {
        type: "REQUEST",
        name: "graphql_RiverAndGroundWaterObservation",
path: "graphql_RiverAndGroundWaterObservation",
pathFormatted: "req_graphql-riveran-e540f",
stats: {
    "name": "graphql_RiverAndGroundWaterObservation",
    "numberOfRequests": {
        "total": "100",
        "ok": "71",
        "ko": "29"
    },
    "minResponseTime": {
        "total": "1830",
        "ok": "1830",
        "ko": "14165"
    },
    "maxResponseTime": {
        "total": "42361",
        "ok": "42361",
        "ko": "16831"
    },
    "meanResponseTime": {
        "total": "16773",
        "ok": "17509",
        "ko": "14970"
    },
    "standardDeviation": {
        "total": "4760",
        "ok": "5458",
        "ko": "787"
    },
    "percentiles1": {
        "total": "17035",
        "ok": "18194",
        "ko": "14766"
    },
    "percentiles2": {
        "total": "18747",
        "ok": "20248",
        "ko": "15131"
    },
    "percentiles3": {
        "total": "23706",
        "ok": "23985",
        "ko": "16759"
    },
    "percentiles4": {
        "total": "26384",
        "ok": "31064",
        "ko": "16811"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 71,
    "percentage": 71
},
    "group4": {
    "name": "failed",
    "count": 29,
    "percentage": 29
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.392",
        "ko": "0.569"
    }
}
    },"req_request-20-taxo-0d791": {
        type: "REQUEST",
        name: "request_20_TaxonMedia",
path: "request_20_TaxonMedia",
pathFormatted: "req_request-20-taxo-0d791",
stats: {
    "name": "request_20_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "64",
        "ko": "36"
    },
    "minResponseTime": {
        "total": "10073",
        "ok": "10073",
        "ko": "11656"
    },
    "maxResponseTime": {
        "total": "42793",
        "ok": "42793",
        "ko": "17796"
    },
    "meanResponseTime": {
        "total": "16827",
        "ok": "17935",
        "ko": "14857"
    },
    "standardDeviation": {
        "total": "4173",
        "ok": "4789",
        "ko": "1239"
    },
    "percentiles1": {
        "total": "16715",
        "ok": "18284",
        "ko": "14619"
    },
    "percentiles2": {
        "total": "18617",
        "ok": "19484",
        "ko": "15152"
    },
    "percentiles3": {
        "total": "22523",
        "ok": "23984",
        "ko": "16784"
    },
    "percentiles4": {
        "total": "26277",
        "ok": "32283",
        "ko": "17459"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 36,
    "percentage": 36
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.255",
        "ko": "0.706"
    }
}
    },"req_graphql-searchp-61328": {
        type: "REQUEST",
        name: "graphql_SearchPlaces",
path: "graphql_SearchPlaces",
pathFormatted: "req_graphql-searchp-61328",
stats: {
    "name": "graphql_SearchPlaces",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "1054",
        "ok": "1054",
        "ko": "13779"
    },
    "maxResponseTime": {
        "total": "19962",
        "ok": "19962",
        "ko": "16757"
    },
    "meanResponseTime": {
        "total": "15752",
        "ok": "16115",
        "ko": "14982"
    },
    "standardDeviation": {
        "total": "3350",
        "ok": "3973",
        "ko": "818"
    },
    "percentiles1": {
        "total": "16674",
        "ok": "17861",
        "ko": "14794"
    },
    "percentiles2": {
        "total": "18308",
        "ok": "18539",
        "ko": "15156"
    },
    "percentiles3": {
        "total": "19152",
        "ok": "19611",
        "ko": "16709"
    },
    "percentiles4": {
        "total": "19722",
        "ok": "19800",
        "ko": "16755"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.333",
        "ko": "0.627"
    }
}
    },"req_request-36-9ad97": {
        type: "REQUEST",
        name: "request_36",
path: "request_36",
pathFormatted: "req_request-36-9ad97",
stats: {
    "name": "request_36",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "1599",
        "ok": "1599",
        "ko": "11683"
    },
    "maxResponseTime": {
        "total": "25223",
        "ok": "25223",
        "ko": "19711"
    },
    "meanResponseTime": {
        "total": "16728",
        "ok": "17592",
        "ko": "15125"
    },
    "standardDeviation": {
        "total": "3767",
        "ok": "4331",
        "ko": "1326"
    },
    "percentiles1": {
        "total": "16834",
        "ok": "18191",
        "ko": "14775"
    },
    "percentiles2": {
        "total": "19025",
        "ok": "20491",
        "ko": "15280"
    },
    "percentiles3": {
        "total": "22359",
        "ok": "23463",
        "ko": "17136"
    },
    "percentiles4": {
        "total": "25145",
        "ok": "25172",
        "ko": "19022"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.275",
        "ko": "0.686"
    }
}
    },"req_favicon-16x16-p-7181b": {
        type: "REQUEST",
        name: "favicon-16x16.png",
path: "favicon-16x16.png",
pathFormatted: "req_favicon-16x16-p-7181b",
stats: {
    "name": "favicon-16x16.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "85",
        "ko": "15"
    },
    "minResponseTime": {
        "total": "445",
        "ok": "445",
        "ko": "11684"
    },
    "maxResponseTime": {
        "total": "21422",
        "ok": "21304",
        "ko": "21422"
    },
    "meanResponseTime": {
        "total": "17651",
        "ok": "17844",
        "ko": "16561"
    },
    "standardDeviation": {
        "total": "3284",
        "ok": "3415",
        "ko": "2108"
    },
    "percentiles1": {
        "total": "18696",
        "ok": "18777",
        "ko": "16626"
    },
    "percentiles2": {
        "total": "19111",
        "ok": "19148",
        "ko": "17720"
    },
    "percentiles3": {
        "total": "20285",
        "ok": "20232",
        "ko": "18957"
    },
    "percentiles4": {
        "total": "21305",
        "ok": "20572",
        "ko": "20929"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 83,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 15,
    "percentage": 15
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.667",
        "ko": "0.294"
    }
}
    },"req_request-21-taxo-4adce": {
        type: "REQUEST",
        name: "request_21_TaxonMedia",
path: "request_21_TaxonMedia",
pathFormatted: "req_request-21-taxo-4adce",
stats: {
    "name": "request_21_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "64",
        "ko": "36"
    },
    "minResponseTime": {
        "total": "10176",
        "ok": "10176",
        "ko": "11656"
    },
    "maxResponseTime": {
        "total": "24590",
        "ok": "24590",
        "ko": "16832"
    },
    "meanResponseTime": {
        "total": "16742",
        "ok": "17890",
        "ko": "14701"
    },
    "standardDeviation": {
        "total": "3544",
        "ok": "3902",
        "ko": "1148"
    },
    "percentiles1": {
        "total": "16250",
        "ok": "18523",
        "ko": "14730"
    },
    "percentiles2": {
        "total": "19367",
        "ok": "20924",
        "ko": "15060"
    },
    "percentiles3": {
        "total": "22451",
        "ok": "23359",
        "ko": "16755"
    },
    "percentiles4": {
        "total": "24269",
        "ok": "24386",
        "ko": "16810"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 36,
    "percentage": 36
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.255",
        "ko": "0.706"
    }
}
    },"req_request-4-matom-e3907": {
        type: "REQUEST",
        name: "request_4_matomo",
path: "request_4_matomo",
pathFormatted: "req_request-4-matom-e3907",
stats: {
    "name": "request_4_matomo",
    "numberOfRequests": {
        "total": "100",
        "ok": "63",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "9977",
        "ok": "9977",
        "ko": "11611"
    },
    "maxResponseTime": {
        "total": "22303",
        "ok": "22303",
        "ko": "17886"
    },
    "meanResponseTime": {
        "total": "16384",
        "ok": "17076",
        "ko": "15205"
    },
    "standardDeviation": {
        "total": "2653",
        "ok": "2980",
        "ko": "1301"
    },
    "percentiles1": {
        "total": "17146",
        "ok": "18239",
        "ko": "14793"
    },
    "percentiles2": {
        "total": "18438",
        "ok": "18713",
        "ko": "16572"
    },
    "percentiles3": {
        "total": "20069",
        "ok": "20089",
        "ko": "17488"
    },
    "percentiles4": {
        "total": "20810",
        "ok": "21368",
        "ko": "17823"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 63,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 37
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.235",
        "ko": "0.725"
    }
}
    },"req_request-30-taxo-7acf0": {
        type: "REQUEST",
        name: "request_30_TaxonMedia",
path: "request_30_TaxonMedia",
pathFormatted: "req_request-30-taxo-7acf0",
stats: {
    "name": "request_30_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "10150",
        "ok": "10150",
        "ko": "11614"
    },
    "maxResponseTime": {
        "total": "31964",
        "ok": "31964",
        "ko": "17796"
    },
    "meanResponseTime": {
        "total": "17079",
        "ok": "18171",
        "ko": "14861"
    },
    "standardDeviation": {
        "total": "3708",
        "ok": "4016",
        "ko": "1254"
    },
    "percentiles1": {
        "total": "17067",
        "ok": "18619",
        "ko": "14710"
    },
    "percentiles2": {
        "total": "19398",
        "ok": "20858",
        "ko": "15121"
    },
    "percentiles3": {
        "total": "22258",
        "ok": "23431",
        "ko": "16812"
    },
    "percentiles4": {
        "total": "25326",
        "ok": "27539",
        "ko": "17510"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.314",
        "ko": "0.647"
    }
}
    },"req_main-3ff1a80403-1672c": {
        type: "REQUEST",
        name: "main.3ff1a804037601f958d7.js",
path: "main.3ff1a804037601f958d7.js",
pathFormatted: "req_main-3ff1a80403-1672c",
stats: {
    "name": "main.3ff1a804037601f958d7.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "86",
        "ko": "14"
    },
    "minResponseTime": {
        "total": "359",
        "ok": "359",
        "ko": "11683"
    },
    "maxResponseTime": {
        "total": "35610",
        "ok": "35610",
        "ko": "17902"
    },
    "meanResponseTime": {
        "total": "20948",
        "ok": "21778",
        "ko": "15850"
    },
    "standardDeviation": {
        "total": "5669",
        "ok": "5656",
        "ko": "1681"
    },
    "percentiles1": {
        "total": "22702",
        "ok": "23312",
        "ko": "15883"
    },
    "percentiles2": {
        "total": "24535",
        "ok": "24813",
        "ko": "17414"
    },
    "percentiles3": {
        "total": "26785",
        "ok": "27352",
        "ko": "17894"
    },
    "percentiles4": {
        "total": "32294",
        "ok": "32763",
        "ko": "17900"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 84,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 14,
    "percentage": 14
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.686",
        "ko": "0.275"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "411",
        "ok": "411",
        "ko": "11611"
    },
    "maxResponseTime": {
        "total": "27497",
        "ok": "27497",
        "ko": "17795"
    },
    "meanResponseTime": {
        "total": "17166",
        "ok": "18441",
        "ko": "14798"
    },
    "standardDeviation": {
        "total": "4779",
        "ok": "5443",
        "ko": "1267"
    },
    "percentiles1": {
        "total": "16833",
        "ok": "18567",
        "ko": "14623"
    },
    "percentiles2": {
        "total": "21419",
        "ok": "22571",
        "ko": "15090"
    },
    "percentiles3": {
        "total": "24166",
        "ok": "24748",
        "ko": "16806"
    },
    "percentiles4": {
        "total": "25958",
        "ok": "26502",
        "ko": "17491"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 63,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.275",
        "ko": "0.686"
    }
}
    },"req_request-28-taxo-f261b": {
        type: "REQUEST",
        name: "request_28_TaxonMedia",
path: "request_28_TaxonMedia",
pathFormatted: "req_request-28-taxo-f261b",
stats: {
    "name": "request_28_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "10056",
        "ok": "10056",
        "ko": "11685"
    },
    "maxResponseTime": {
        "total": "27678",
        "ok": "27678",
        "ko": "17795"
    },
    "meanResponseTime": {
        "total": "17443",
        "ok": "18652",
        "ko": "14989"
    },
    "standardDeviation": {
        "total": "3783",
        "ok": "4041",
        "ko": "1101"
    },
    "percentiles1": {
        "total": "17554",
        "ok": "18891",
        "ko": "14765"
    },
    "percentiles2": {
        "total": "20483",
        "ok": "21784",
        "ko": "15212"
    },
    "percentiles3": {
        "total": "23666",
        "ok": "23872",
        "ko": "16790"
    },
    "percentiles4": {
        "total": "24764",
        "ok": "25736",
        "ko": "17487"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.314",
        "ko": "0.647"
    }
}
    },"req_graphql-weather-e1b31": {
        type: "REQUEST",
        name: "graphql_weather",
path: "graphql_weather",
pathFormatted: "req_graphql-weather-e1b31",
stats: {
    "name": "graphql_weather",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "2813",
        "ok": "2813",
        "ko": "11609"
    },
    "maxResponseTime": {
        "total": "42349",
        "ok": "42349",
        "ko": "17430"
    },
    "meanResponseTime": {
        "total": "15928",
        "ok": "16428",
        "ko": "14958"
    },
    "standardDeviation": {
        "total": "4945",
        "ok": "5954",
        "ko": "1297"
    },
    "percentiles1": {
        "total": "16603",
        "ok": "17824",
        "ko": "14759"
    },
    "percentiles2": {
        "total": "18254",
        "ok": "18741",
        "ko": "15228"
    },
    "percentiles3": {
        "total": "20797",
        "ok": "24067",
        "ko": "16919"
    },
    "percentiles4": {
        "total": "28239",
        "ok": "33085",
        "ko": "17315"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.294",
        "ko": "0.667"
    }
}
    },"req_favicon-ico-8af3a": {
        type: "REQUEST",
        name: "favicon.ico",
path: "favicon.ico",
pathFormatted: "req_favicon-ico-8af3a",
stats: {
    "name": "favicon.ico",
    "numberOfRequests": {
        "total": "100",
        "ok": "86",
        "ko": "14"
    },
    "minResponseTime": {
        "total": "1070",
        "ok": "1070",
        "ko": "11613"
    },
    "maxResponseTime": {
        "total": "35375",
        "ok": "35375",
        "ko": "17922"
    },
    "meanResponseTime": {
        "total": "19342",
        "ok": "19932",
        "ko": "15716"
    },
    "standardDeviation": {
        "total": "4433",
        "ok": "4432",
        "ko": "2104"
    },
    "percentiles1": {
        "total": "19502",
        "ok": "20809",
        "ko": "15686"
    },
    "percentiles2": {
        "total": "22030",
        "ok": "22197",
        "ko": "17728"
    },
    "percentiles3": {
        "total": "23950",
        "ok": "24006",
        "ko": "17897"
    },
    "percentiles4": {
        "total": "25426",
        "ok": "26833",
        "ko": "17917"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 85,
    "percentage": 85
},
    "group4": {
    "name": "failed",
    "count": 14,
    "percentage": 14
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.686",
        "ko": "0.275"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "587",
        "ok": "587",
        "ko": "11609"
    },
    "maxResponseTime": {
        "total": "32966",
        "ok": "32966",
        "ko": "17431"
    },
    "meanResponseTime": {
        "total": "17035",
        "ok": "18092",
        "ko": "14982"
    },
    "standardDeviation": {
        "total": "4640",
        "ok": "5336",
        "ko": "1294"
    },
    "percentiles1": {
        "total": "16756",
        "ok": "18103",
        "ko": "14828"
    },
    "percentiles2": {
        "total": "19609",
        "ok": "22090",
        "ko": "15244"
    },
    "percentiles3": {
        "total": "24022",
        "ok": "24493",
        "ko": "16918"
    },
    "percentiles4": {
        "total": "28763",
        "ok": "30207",
        "ko": "17315"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.294",
        "ko": "0.667"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "100",
        "ok": "64",
        "ko": "36"
    },
    "minResponseTime": {
        "total": "853",
        "ok": "853",
        "ko": "11610"
    },
    "maxResponseTime": {
        "total": "32884",
        "ok": "32884",
        "ko": "16832"
    },
    "meanResponseTime": {
        "total": "16584",
        "ok": "17590",
        "ko": "14796"
    },
    "standardDeviation": {
        "total": "4054",
        "ok": "4694",
        "ko": "1223"
    },
    "percentiles1": {
        "total": "16673",
        "ok": "18252",
        "ko": "14603"
    },
    "percentiles2": {
        "total": "18633",
        "ok": "20099",
        "ko": "15209"
    },
    "percentiles3": {
        "total": "22058",
        "ok": "22296",
        "ko": "16758"
    },
    "percentiles4": {
        "total": "27365",
        "ok": "29372",
        "ko": "16810"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 63,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 36,
    "percentage": 36
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.255",
        "ko": "0.706"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "100",
        "ok": "62",
        "ko": "38"
    },
    "minResponseTime": {
        "total": "10073",
        "ok": "10073",
        "ko": "11611"
    },
    "maxResponseTime": {
        "total": "22389",
        "ok": "22389",
        "ko": "17935"
    },
    "meanResponseTime": {
        "total": "16548",
        "ok": "17392",
        "ko": "15172"
    },
    "standardDeviation": {
        "total": "2867",
        "ok": "3216",
        "ko": "1304"
    },
    "percentiles1": {
        "total": "17025",
        "ok": "18382",
        "ko": "14770"
    },
    "percentiles2": {
        "total": "18550",
        "ok": "19573",
        "ko": "16272"
    },
    "percentiles3": {
        "total": "20911",
        "ok": "21243",
        "ko": "17473"
    },
    "percentiles4": {
        "total": "22227",
        "ok": "22289",
        "ko": "17851"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 38,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.216",
        "ko": "0.745"
    }
}
    },"req_favicon-32x32-p-14e88": {
        type: "REQUEST",
        name: "favicon-32x32.png",
path: "favicon-32x32.png",
pathFormatted: "req_favicon-32x32-p-14e88",
stats: {
    "name": "favicon-32x32.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "88",
        "ko": "12"
    },
    "minResponseTime": {
        "total": "423",
        "ok": "423",
        "ko": "11614"
    },
    "maxResponseTime": {
        "total": "30567",
        "ok": "30567",
        "ko": "17924"
    },
    "meanResponseTime": {
        "total": "18062",
        "ok": "18325",
        "ko": "16133"
    },
    "standardDeviation": {
        "total": "3777",
        "ok": "3893",
        "ko": "1876"
    },
    "percentiles1": {
        "total": "18784",
        "ok": "18872",
        "ko": "16866"
    },
    "percentiles2": {
        "total": "19287",
        "ok": "19323",
        "ko": "17699"
    },
    "percentiles3": {
        "total": "20475",
        "ok": "20850",
        "ko": "17898"
    },
    "percentiles4": {
        "total": "30515",
        "ok": "30521",
        "ko": "17919"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 87,
    "percentage": 87
},
    "group4": {
    "name": "failed",
    "count": 12,
    "percentage": 12
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.725",
        "ko": "0.235"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "100",
        "ok": "70",
        "ko": "30"
    },
    "minResponseTime": {
        "total": "138",
        "ok": "138",
        "ko": "11610"
    },
    "maxResponseTime": {
        "total": "29151",
        "ok": "29151",
        "ko": "19584"
    },
    "meanResponseTime": {
        "total": "15705",
        "ok": "15923",
        "ko": "15195"
    },
    "standardDeviation": {
        "total": "5622",
        "ok": "6647",
        "ko": "1368"
    },
    "percentiles1": {
        "total": "16674",
        "ok": "17912",
        "ko": "14797"
    },
    "percentiles2": {
        "total": "18969",
        "ok": "20111",
        "ko": "16232"
    },
    "percentiles3": {
        "total": "21890",
        "ok": "23821",
        "ko": "16797"
    },
    "percentiles4": {
        "total": "27201",
        "ok": "27792",
        "ko": "18786"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 30,
    "percentage": 30
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.373",
        "ko": "0.588"
    }
}
    },"req_request-23-taxo-3ea91": {
        type: "REQUEST",
        name: "request_23_TaxonMedia",
path: "request_23_TaxonMedia",
pathFormatted: "req_request-23-taxo-3ea91",
stats: {
    "name": "request_23_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "9950",
        "ok": "9950",
        "ko": "11609"
    },
    "maxResponseTime": {
        "total": "31123",
        "ok": "31123",
        "ko": "17433"
    },
    "meanResponseTime": {
        "total": "16926",
        "ok": "17933",
        "ko": "14882"
    },
    "standardDeviation": {
        "total": "3746",
        "ok": "4148",
        "ko": "1161"
    },
    "percentiles1": {
        "total": "17079",
        "ok": "18364",
        "ko": "14622"
    },
    "percentiles2": {
        "total": "19127",
        "ok": "19749",
        "ko": "15125"
    },
    "percentiles3": {
        "total": "22035",
        "ok": "23104",
        "ko": "16814"
    },
    "percentiles4": {
        "total": "30595",
        "ok": "30771",
        "ko": "17264"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.314",
        "ko": "0.647"
    }
}
    },"req_request-24-taxo-b5f25": {
        type: "REQUEST",
        name: "request_24_TaxonMedia",
path: "request_24_TaxonMedia",
pathFormatted: "req_request-24-taxo-b5f25",
stats: {
    "name": "request_24_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "10182",
        "ok": "10182",
        "ko": "11610"
    },
    "maxResponseTime": {
        "total": "31634",
        "ok": "31634",
        "ko": "16831"
    },
    "meanResponseTime": {
        "total": "17068",
        "ok": "18237",
        "ko": "14799"
    },
    "standardDeviation": {
        "total": "3982",
        "ok": "4389",
        "ko": "1201"
    },
    "percentiles1": {
        "total": "16712",
        "ok": "18455",
        "ko": "14737"
    },
    "percentiles2": {
        "total": "18933",
        "ok": "21649",
        "ko": "15155"
    },
    "percentiles3": {
        "total": "23246",
        "ok": "23581",
        "ko": "16751"
    },
    "percentiles4": {
        "total": "29488",
        "ok": "30225",
        "ko": "16807"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.294",
        "ko": "0.667"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "656",
        "ok": "656",
        "ko": "11612"
    },
    "maxResponseTime": {
        "total": "31342",
        "ok": "31342",
        "ko": "17795"
    },
    "meanResponseTime": {
        "total": "17192",
        "ok": "18464",
        "ko": "14828"
    },
    "standardDeviation": {
        "total": "4995",
        "ok": "5745",
        "ko": "1175"
    },
    "percentiles1": {
        "total": "16715",
        "ok": "18252",
        "ko": "14487"
    },
    "percentiles2": {
        "total": "21407",
        "ok": "22907",
        "ko": "15132"
    },
    "percentiles3": {
        "total": "24309",
        "ok": "26083",
        "ko": "16800"
    },
    "percentiles4": {
        "total": "29660",
        "ok": "30255",
        "ko": "17491"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.275",
        "ko": "0.686"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "100",
        "ok": "64",
        "ko": "36"
    },
    "minResponseTime": {
        "total": "469",
        "ok": "469",
        "ko": "13779"
    },
    "maxResponseTime": {
        "total": "32811",
        "ok": "32811",
        "ko": "17430"
    },
    "meanResponseTime": {
        "total": "15879",
        "ok": "16368",
        "ko": "15011"
    },
    "standardDeviation": {
        "total": "4094",
        "ok": "5003",
        "ko": "938"
    },
    "percentiles1": {
        "total": "15851",
        "ok": "17801",
        "ko": "14737"
    },
    "percentiles2": {
        "total": "18299",
        "ok": "18666",
        "ko": "15152"
    },
    "percentiles3": {
        "total": "19624",
        "ok": "19836",
        "ko": "16895"
    },
    "percentiles4": {
        "total": "28494",
        "ok": "30064",
        "ko": "17308"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 36,
    "percentage": 36
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.255",
        "ko": "0.706"
    }
}
    },"req_vendors-3ff1a80-e4689": {
        type: "REQUEST",
        name: "vendors.3ff1a804037601f958d7.js",
path: "vendors.3ff1a804037601f958d7.js",
pathFormatted: "req_vendors-3ff1a80-e4689",
stats: {
    "name": "vendors.3ff1a804037601f958d7.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "85",
        "ko": "15"
    },
    "minResponseTime": {
        "total": "11609",
        "ok": "19754",
        "ko": "11609"
    },
    "maxResponseTime": {
        "total": "32079",
        "ok": "32079",
        "ko": "17922"
    },
    "meanResponseTime": {
        "total": "27316",
        "ok": "29332",
        "ko": "15892"
    },
    "standardDeviation": {
        "total": "5366",
        "ok": "2461",
        "ko": "2025"
    },
    "percentiles1": {
        "total": "29799",
        "ok": "30120",
        "ko": "16439"
    },
    "percentiles2": {
        "total": "30807",
        "ok": "30951",
        "ko": "17666"
    },
    "percentiles3": {
        "total": "31579",
        "ok": "31641",
        "ko": "17894"
    },
    "percentiles4": {
        "total": "32043",
        "ok": "32049",
        "ko": "17916"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 85,
    "percentage": 85
},
    "group4": {
    "name": "failed",
    "count": 15,
    "percentage": 15
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.667",
        "ko": "0.294"
    }
}
    },"req_graphql-news-d7bbe": {
        type: "REQUEST",
        name: "graphQL_news",
path: "graphQL_news",
pathFormatted: "req_graphql-news-d7bbe",
stats: {
    "name": "graphQL_news",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "739",
        "ok": "739",
        "ko": "11687"
    },
    "maxResponseTime": {
        "total": "28290",
        "ok": "28290",
        "ko": "16832"
    },
    "meanResponseTime": {
        "total": "15676",
        "ok": "16078",
        "ko": "14895"
    },
    "standardDeviation": {
        "total": "4036",
        "ok": "4863",
        "ko": "1044"
    },
    "percentiles1": {
        "total": "16604",
        "ok": "17998",
        "ko": "14733"
    },
    "percentiles2": {
        "total": "18313",
        "ok": "18662",
        "ko": "15203"
    },
    "percentiles3": {
        "total": "19672",
        "ok": "19953",
        "ko": "16754"
    },
    "percentiles4": {
        "total": "21115",
        "ok": "23579",
        "ko": "16807"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.294",
        "ko": "0.667"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "387",
        "ok": "387",
        "ko": "11614"
    },
    "maxResponseTime": {
        "total": "38123",
        "ok": "38123",
        "ko": "16832"
    },
    "meanResponseTime": {
        "total": "16206",
        "ok": "16933",
        "ko": "14731"
    },
    "standardDeviation": {
        "total": "4587",
        "ok": "5389",
        "ko": "1245"
    },
    "percentiles1": {
        "total": "16715",
        "ok": "18228",
        "ko": "14530"
    },
    "percentiles2": {
        "total": "18643",
        "ok": "19306",
        "ko": "15131"
    },
    "percentiles3": {
        "total": "22012",
        "ok": "22203",
        "ko": "16758"
    },
    "percentiles4": {
        "total": "24386",
        "ok": "28965",
        "ko": "16811"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.314",
        "ko": "0.647"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "417",
        "ok": "417",
        "ko": "11682"
    },
    "maxResponseTime": {
        "total": "42577",
        "ok": "42577",
        "ko": "16904"
    },
    "meanResponseTime": {
        "total": "18548",
        "ok": "20288",
        "ko": "14850"
    },
    "standardDeviation": {
        "total": "5967",
        "ok": "6509",
        "ko": "1071"
    },
    "percentiles1": {
        "total": "18733",
        "ok": "21166",
        "ko": "14735"
    },
    "percentiles2": {
        "total": "22756",
        "ok": "23766",
        "ko": "15144"
    },
    "percentiles3": {
        "total": "25623",
        "ok": "27102",
        "ko": "16758"
    },
    "percentiles4": {
        "total": "30259",
        "ok": "34241",
        "ko": "16861"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.333",
        "ko": "0.627"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "100",
        "ok": "70",
        "ko": "30"
    },
    "minResponseTime": {
        "total": "444",
        "ok": "444",
        "ko": "11613"
    },
    "maxResponseTime": {
        "total": "22503",
        "ok": "22503",
        "ko": "16900"
    },
    "meanResponseTime": {
        "total": "15915",
        "ok": "16393",
        "ko": "14801"
    },
    "standardDeviation": {
        "total": "4449",
        "ok": "5205",
        "ko": "999"
    },
    "percentiles1": {
        "total": "16713",
        "ok": "18181",
        "ko": "14601"
    },
    "percentiles2": {
        "total": "18786",
        "ok": "19294",
        "ko": "15052"
    },
    "percentiles3": {
        "total": "21693",
        "ok": "21882",
        "ko": "16716"
    },
    "percentiles4": {
        "total": "22503",
        "ok": "22503",
        "ko": "16856"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 30,
    "percentage": 30
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.373",
        "ko": "0.588"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "833",
        "ok": "833",
        "ko": "11610"
    },
    "maxResponseTime": {
        "total": "39036",
        "ok": "39036",
        "ko": "16832"
    },
    "meanResponseTime": {
        "total": "16556",
        "ok": "17477",
        "ko": "14767"
    },
    "standardDeviation": {
        "total": "4297",
        "ok": "4982",
        "ko": "1127"
    },
    "percentiles1": {
        "total": "16675",
        "ok": "18181",
        "ko": "14729"
    },
    "percentiles2": {
        "total": "18562",
        "ok": "19601",
        "ko": "15095"
    },
    "percentiles3": {
        "total": "21910",
        "ok": "23863",
        "ko": "16757"
    },
    "percentiles4": {
        "total": "28041",
        "ok": "31817",
        "ko": "16811"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.294",
        "ko": "0.667"
    }
}
    },"req_app-css-13ea8": {
        type: "REQUEST",
        name: "App.css",
path: "App.css",
pathFormatted: "req_app-css-13ea8",
stats: {
    "name": "App.css",
    "numberOfRequests": {
        "total": "100",
        "ok": "85",
        "ko": "15"
    },
    "minResponseTime": {
        "total": "302",
        "ok": "302",
        "ko": "11684"
    },
    "maxResponseTime": {
        "total": "27382",
        "ok": "27382",
        "ko": "23351"
    },
    "meanResponseTime": {
        "total": "18729",
        "ok": "19115",
        "ko": "16537"
    },
    "standardDeviation": {
        "total": "3828",
        "ok": "3895",
        "ko": "2468"
    },
    "percentiles1": {
        "total": "18970",
        "ok": "19269",
        "ko": "16440"
    },
    "percentiles2": {
        "total": "20772",
        "ok": "21047",
        "ko": "17757"
    },
    "percentiles3": {
        "total": "23578",
        "ok": "24229",
        "ko": "19539"
    },
    "percentiles4": {
        "total": "26288",
        "ok": "26454",
        "ko": "22589"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 84,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 15,
    "percentage": 15
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.667",
        "ko": "0.294"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "2048",
        "ok": "2048",
        "ko": "11609"
    },
    "maxResponseTime": {
        "total": "42305",
        "ok": "42305",
        "ko": "16900"
    },
    "meanResponseTime": {
        "total": "16540",
        "ok": "17388",
        "ko": "14894"
    },
    "standardDeviation": {
        "total": "4193",
        "ok": "4901",
        "ko": "978"
    },
    "percentiles1": {
        "total": "16763",
        "ok": "18212",
        "ko": "14780"
    },
    "percentiles2": {
        "total": "18478",
        "ok": "18949",
        "ko": "15097"
    },
    "percentiles3": {
        "total": "20757",
        "ok": "21014",
        "ko": "16761"
    },
    "percentiles4": {
        "total": "27839",
        "ok": "32807",
        "ko": "16857"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.294",
        "ko": "0.667"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "100",
        "ok": "63",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "718",
        "ok": "718",
        "ko": "12801"
    },
    "maxResponseTime": {
        "total": "19735",
        "ok": "19735",
        "ko": "17432"
    },
    "meanResponseTime": {
        "total": "15435",
        "ok": "15729",
        "ko": "14934"
    },
    "standardDeviation": {
        "total": "3705",
        "ok": "4582",
        "ko": "977"
    },
    "percentiles1": {
        "total": "15518",
        "ok": "17750",
        "ko": "14706"
    },
    "percentiles2": {
        "total": "18104",
        "ok": "18475",
        "ko": "15120"
    },
    "percentiles3": {
        "total": "19129",
        "ok": "19617",
        "ko": "16795"
    },
    "percentiles4": {
        "total": "19672",
        "ok": "19695",
        "ko": "17242"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 37
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.961",
        "ok": "1.235",
        "ko": "0.725"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
