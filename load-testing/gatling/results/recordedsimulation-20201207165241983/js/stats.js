var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "57476",
        "ok": "33794",
        "ko": "23682"
    },
    "minResponseTime": {
        "total": "15",
        "ok": "15",
        "ko": "213"
    },
    "maxResponseTime": {
        "total": "60340",
        "ok": "59914",
        "ko": "60340"
    },
    "meanResponseTime": {
        "total": "8077",
        "ok": "6344",
        "ko": "10551"
    },
    "standardDeviation": {
        "total": "6574",
        "ok": "7306",
        "ko": "4281"
    },
    "percentiles1": {
        "total": "8652",
        "ok": "4101",
        "ko": "10282"
    },
    "percentiles2": {
        "total": "10414",
        "ok": "7682",
        "ko": "10695"
    },
    "percentiles3": {
        "total": "16384",
        "ok": "20433",
        "ko": "13209"
    },
    "percentiles4": {
        "total": "35750",
        "ok": "38472",
        "ko": "17698"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2527,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 888,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 30379,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 23682,
    "percentage": 41
},
    "meanNumberOfRequestsPerSecond": {
        "total": "237.504",
        "ok": "139.645",
        "ko": "97.86"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "2000",
        "ok": "1206",
        "ko": "794"
    },
    "minResponseTime": {
        "total": "76",
        "ok": "76",
        "ko": "304"
    },
    "maxResponseTime": {
        "total": "60032",
        "ok": "55810",
        "ko": "60032"
    },
    "meanResponseTime": {
        "total": "7325",
        "ok": "5413",
        "ko": "10230"
    },
    "standardDeviation": {
        "total": "5773",
        "ok": "6202",
        "ko": "3399"
    },
    "percentiles1": {
        "total": "8127",
        "ok": "3541",
        "ko": "10120"
    },
    "percentiles2": {
        "total": "10195",
        "ok": "6373",
        "ko": "10465"
    },
    "percentiles3": {
        "total": "13919",
        "ok": "15491",
        "ko": "12365"
    },
    "percentiles4": {
        "total": "29860",
        "ok": "34263",
        "ko": "16898"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 24,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 68,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1114,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 794,
    "percentage": 40
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.264",
        "ok": "4.983",
        "ko": "3.281"
    }
}
    },"req_request-24-taxo-b5f25": {
        type: "REQUEST",
        name: "request_24_TaxonMedia",
path: "request_24_TaxonMedia",
pathFormatted: "req_request-24-taxo-b5f25",
stats: {
    "name": "request_24_TaxonMedia",
    "numberOfRequests": {
        "total": "1206",
        "ok": "679",
        "ko": "527"
    },
    "minResponseTime": {
        "total": "245",
        "ok": "335",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60020",
        "ok": "57672",
        "ko": "60020"
    },
    "meanResponseTime": {
        "total": "9854",
        "ok": "8803",
        "ko": "11208"
    },
    "standardDeviation": {
        "total": "7744",
        "ok": "8109",
        "ko": "7019"
    },
    "percentiles1": {
        "total": "9513",
        "ok": "6135",
        "ko": "10258"
    },
    "percentiles2": {
        "total": "10702",
        "ok": "11366",
        "ko": "10653"
    },
    "percentiles3": {
        "total": "22878",
        "ok": "25321",
        "ko": "14705"
    },
    "percentiles4": {
        "total": "48896",
        "ok": "38174",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 670,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 527,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.806",
        "ko": "2.178"
    }
}
    },"req_request-34-d9757": {
        type: "REQUEST",
        name: "request_34",
path: "request_34",
pathFormatted: "req_request-34-d9757",
stats: {
    "name": "request_34",
    "numberOfRequests": {
        "total": "1206",
        "ok": "695",
        "ko": "511"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "244"
    },
    "maxResponseTime": {
        "total": "60129",
        "ok": "55774",
        "ko": "60129"
    },
    "meanResponseTime": {
        "total": "7806",
        "ok": "5820",
        "ko": "10507"
    },
    "standardDeviation": {
        "total": "6312",
        "ok": "6844",
        "ko": "4203"
    },
    "percentiles1": {
        "total": "8602",
        "ok": "3728",
        "ko": "10305"
    },
    "percentiles2": {
        "total": "10361",
        "ok": "6946",
        "ko": "10689"
    },
    "percentiles3": {
        "total": "13744",
        "ok": "16514",
        "ko": "12880"
    },
    "percentiles4": {
        "total": "33762",
        "ok": "36198",
        "ko": "16797"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 20,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 658,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 511,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.872",
        "ko": "2.112"
    }
}
    },"req_favicon-16x16-p-7181b": {
        type: "REQUEST",
        name: "favicon-16x16.png",
path: "favicon-16x16.png",
pathFormatted: "req_favicon-16x16-p-7181b",
stats: {
    "name": "favicon-16x16.png",
    "numberOfRequests": {
        "total": "1206",
        "ok": "719",
        "ko": "487"
    },
    "minResponseTime": {
        "total": "15",
        "ok": "15",
        "ko": "286"
    },
    "maxResponseTime": {
        "total": "18219",
        "ok": "16381",
        "ko": "18219"
    },
    "meanResponseTime": {
        "total": "6622",
        "ok": "4155",
        "ko": "10263"
    },
    "standardDeviation": {
        "total": "3874",
        "ok": "2881",
        "ko": "1636"
    },
    "percentiles1": {
        "total": "7048",
        "ok": "3453",
        "ko": "10276"
    },
    "percentiles2": {
        "total": "10203",
        "ok": "5939",
        "ko": "10764"
    },
    "percentiles3": {
        "total": "11653",
        "ok": "9856",
        "ko": "12667"
    },
    "percentiles4": {
        "total": "14740",
        "ok": "12596",
        "ko": "16135"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 36,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 33,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 650,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 487,
    "percentage": 40
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.971",
        "ko": "2.012"
    }
}
    },"req_app-css-13ea8": {
        type: "REQUEST",
        name: "App.css",
path: "App.css",
pathFormatted: "req_app-css-13ea8",
stats: {
    "name": "App.css",
    "numberOfRequests": {
        "total": "1206",
        "ok": "704",
        "ko": "502"
    },
    "minResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "365"
    },
    "maxResponseTime": {
        "total": "54033",
        "ok": "54033",
        "ko": "19195"
    },
    "meanResponseTime": {
        "total": "7929",
        "ok": "6211",
        "ko": "10339"
    },
    "standardDeviation": {
        "total": "5516",
        "ok": "6484",
        "ko": "2048"
    },
    "percentiles1": {
        "total": "8661",
        "ok": "4192",
        "ko": "10295"
    },
    "percentiles2": {
        "total": "10409",
        "ok": "7823",
        "ko": "10750"
    },
    "percentiles3": {
        "total": "16152",
        "ok": "17441",
        "ko": "13386"
    },
    "percentiles4": {
        "total": "29093",
        "ok": "32050",
        "ko": "17389"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 32,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 655,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 502,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.909",
        "ko": "2.074"
    }
}
    },"req_graphql-taxon-f4085": {
        type: "REQUEST",
        name: "graphql_Taxon",
path: "graphql_Taxon",
pathFormatted: "req_graphql-taxon-f4085",
stats: {
    "name": "graphql_Taxon",
    "numberOfRequests": {
        "total": "1206",
        "ok": "662",
        "ko": "544"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "287"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "59687",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "7939",
        "ok": "5755",
        "ko": "10597"
    },
    "standardDeviation": {
        "total": "6145",
        "ok": "6634",
        "ko": "4156"
    },
    "percentiles1": {
        "total": "8646",
        "ok": "3887",
        "ko": "10311"
    },
    "percentiles2": {
        "total": "10389",
        "ok": "7020",
        "ko": "10717"
    },
    "percentiles3": {
        "total": "14438",
        "ok": "14904",
        "ko": "13822"
    },
    "percentiles4": {
        "total": "30324",
        "ok": "34343",
        "ko": "18230"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 27,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 23,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 612,
    "percentage": 51
},
    "group4": {
    "name": "failed",
    "count": 544,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.736",
        "ko": "2.248"
    }
}
    },"req_favicon-32x32-p-14e88": {
        type: "REQUEST",
        name: "favicon-32x32.png",
path: "favicon-32x32.png",
pathFormatted: "req_favicon-32x32-p-14e88",
stats: {
    "name": "favicon-32x32.png",
    "numberOfRequests": {
        "total": "1206",
        "ok": "714",
        "ko": "492"
    },
    "minResponseTime": {
        "total": "17",
        "ok": "17",
        "ko": "366"
    },
    "maxResponseTime": {
        "total": "57517",
        "ok": "57517",
        "ko": "18251"
    },
    "meanResponseTime": {
        "total": "7234",
        "ok": "5128",
        "ko": "10291"
    },
    "standardDeviation": {
        "total": "4852",
        "ok": "5181",
        "ko": "1725"
    },
    "percentiles1": {
        "total": "7861",
        "ok": "3821",
        "ko": "10319"
    },
    "percentiles2": {
        "total": "10320",
        "ok": "6676",
        "ko": "10775"
    },
    "percentiles3": {
        "total": "12688",
        "ok": "12331",
        "ko": "12914"
    },
    "percentiles4": {
        "total": "22174",
        "ok": "25992",
        "ko": "16639"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 35,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 23,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 656,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 492,
    "percentage": 41
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.95",
        "ko": "2.033"
    }
}
    },"req_coast-228x228-p-35307": {
        type: "REQUEST",
        name: "coast-228x228.png",
path: "coast-228x228.png",
pathFormatted: "req_coast-228x228-p-35307",
stats: {
    "name": "coast-228x228.png",
    "numberOfRequests": {
        "total": "1206",
        "ok": "704",
        "ko": "502"
    },
    "minResponseTime": {
        "total": "16",
        "ok": "16",
        "ko": "389"
    },
    "maxResponseTime": {
        "total": "60020",
        "ok": "53793",
        "ko": "60020"
    },
    "meanResponseTime": {
        "total": "8324",
        "ok": "6762",
        "ko": "10514"
    },
    "standardDeviation": {
        "total": "6455",
        "ok": "7536",
        "ko": "3497"
    },
    "percentiles1": {
        "total": "8717",
        "ok": "4529",
        "ko": "10306"
    },
    "percentiles2": {
        "total": "10468",
        "ok": "8381",
        "ko": "10723"
    },
    "percentiles3": {
        "total": "16247",
        "ok": "21661",
        "ko": "12835"
    },
    "percentiles4": {
        "total": "36815",
        "ok": "40177",
        "ko": "16726"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 34,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 25,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 645,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 502,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.909",
        "ko": "2.074"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "1206",
        "ok": "689",
        "ko": "517"
    },
    "minResponseTime": {
        "total": "16",
        "ok": "16",
        "ko": "303"
    },
    "maxResponseTime": {
        "total": "42090",
        "ok": "42090",
        "ko": "19997"
    },
    "meanResponseTime": {
        "total": "6945",
        "ok": "4426",
        "ko": "10302"
    },
    "standardDeviation": {
        "total": "4386",
        "ok": "4054",
        "ko": "1804"
    },
    "percentiles1": {
        "total": "7448",
        "ok": "3419",
        "ko": "10280"
    },
    "percentiles2": {
        "total": "10269",
        "ok": "5780",
        "ko": "10656"
    },
    "percentiles3": {
        "total": "12293",
        "ok": "10237",
        "ko": "12967"
    },
    "percentiles4": {
        "total": "17667",
        "ok": "20292",
        "ko": "17183"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 40,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 24,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 625,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 517,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.847",
        "ko": "2.136"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "1206",
        "ok": "672",
        "ko": "534"
    },
    "minResponseTime": {
        "total": "116",
        "ok": "116",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60225",
        "ok": "57288",
        "ko": "60225"
    },
    "meanResponseTime": {
        "total": "9137",
        "ok": "7959",
        "ko": "10619"
    },
    "standardDeviation": {
        "total": "7312",
        "ok": "8488",
        "ko": "5112"
    },
    "percentiles1": {
        "total": "9125",
        "ok": "4997",
        "ko": "10286"
    },
    "percentiles2": {
        "total": "10594",
        "ok": "9680",
        "ko": "10673"
    },
    "percentiles3": {
        "total": "19861",
        "ok": "25334",
        "ko": "12597"
    },
    "percentiles4": {
        "total": "43393",
        "ok": "43633",
        "ko": "18912"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 22,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 9,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 641,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 534,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.777",
        "ko": "2.207"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "1206",
        "ok": "682",
        "ko": "524"
    },
    "minResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "246"
    },
    "maxResponseTime": {
        "total": "60140",
        "ok": "49549",
        "ko": "60140"
    },
    "meanResponseTime": {
        "total": "8978",
        "ok": "7509",
        "ko": "10891"
    },
    "standardDeviation": {
        "total": "6634",
        "ok": "6748",
        "ko": "5963"
    },
    "percentiles1": {
        "total": "8993",
        "ok": "5030",
        "ko": "10289"
    },
    "percentiles2": {
        "total": "10601",
        "ok": "10018",
        "ko": "10659"
    },
    "percentiles3": {
        "total": "18213",
        "ok": "20437",
        "ko": "13241"
    },
    "percentiles4": {
        "total": "36421",
        "ok": "35712",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 16,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 662,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 524,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.818",
        "ko": "2.165"
    }
}
    },"req_request-29-taxo-1f5dd": {
        type: "REQUEST",
        name: "request_29_TaxonMedia",
path: "request_29_TaxonMedia",
pathFormatted: "req_request-29-taxo-1f5dd",
stats: {
    "name": "request_29_TaxonMedia",
    "numberOfRequests": {
        "total": "1206",
        "ok": "662",
        "ko": "544"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "247"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "57984",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "8635",
        "ok": "7282",
        "ko": "10281"
    },
    "standardDeviation": {
        "total": "6644",
        "ok": "8362",
        "ko": "2799"
    },
    "percentiles1": {
        "total": "8732",
        "ok": "4781",
        "ko": "10235"
    },
    "percentiles2": {
        "total": "10452",
        "ok": "8052",
        "ko": "10630"
    },
    "percentiles3": {
        "total": "16694",
        "ok": "24368",
        "ko": "13185"
    },
    "percentiles4": {
        "total": "37554",
        "ok": "46715",
        "ko": "16782"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 15,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 640,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 544,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.736",
        "ko": "2.248"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "1206",
        "ok": "708",
        "ko": "498"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60041",
        "ok": "57950",
        "ko": "60041"
    },
    "meanResponseTime": {
        "total": "7497",
        "ok": "5550",
        "ko": "10266"
    },
    "standardDeviation": {
        "total": "5802",
        "ok": "6496",
        "ko": "2912"
    },
    "percentiles1": {
        "total": "8387",
        "ok": "3741",
        "ko": "10263"
    },
    "percentiles2": {
        "total": "10293",
        "ok": "6805",
        "ko": "10674"
    },
    "percentiles3": {
        "total": "13388",
        "ok": "13700",
        "ko": "13147"
    },
    "percentiles4": {
        "total": "29058",
        "ok": "35878",
        "ko": "16613"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 35,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 24,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 649,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 498,
    "percentage": 41
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.926",
        "ko": "2.058"
    }
}
    },"req_graphql-news-4c309": {
        type: "REQUEST",
        name: "graphql_News",
path: "graphql_News",
pathFormatted: "req_graphql-news-4c309",
stats: {
    "name": "graphql_News",
    "numberOfRequests": {
        "total": "1206",
        "ok": "704",
        "ko": "502"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "246"
    },
    "maxResponseTime": {
        "total": "57504",
        "ok": "57504",
        "ko": "18202"
    },
    "meanResponseTime": {
        "total": "6851",
        "ok": "4551",
        "ko": "10076"
    },
    "standardDeviation": {
        "total": "4642",
        "ok": "4628",
        "ko": "1978"
    },
    "percentiles1": {
        "total": "7523",
        "ok": "3335",
        "ko": "10265"
    },
    "percentiles2": {
        "total": "10263",
        "ok": "5670",
        "ko": "10603"
    },
    "percentiles3": {
        "total": "12329",
        "ok": "11740",
        "ko": "12434"
    },
    "percentiles4": {
        "total": "17743",
        "ok": "19916",
        "ko": "16135"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 24,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 631,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 502,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.909",
        "ko": "2.074"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "1206",
        "ok": "692",
        "ko": "514"
    },
    "minResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "246"
    },
    "maxResponseTime": {
        "total": "60010",
        "ok": "54962",
        "ko": "60010"
    },
    "meanResponseTime": {
        "total": "7955",
        "ok": "6083",
        "ko": "10476"
    },
    "standardDeviation": {
        "total": "6263",
        "ok": "6796",
        "ko": "4335"
    },
    "percentiles1": {
        "total": "8620",
        "ok": "4002",
        "ko": "10270"
    },
    "percentiles2": {
        "total": "10384",
        "ok": "7446",
        "ko": "10709"
    },
    "percentiles3": {
        "total": "15118",
        "ok": "17164",
        "ko": "13681"
    },
    "percentiles4": {
        "total": "31237",
        "ok": "33356",
        "ko": "18137"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 33,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 639,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 514,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.86",
        "ko": "2.124"
    }
}
    },"req_graphql-searchp-61328": {
        type: "REQUEST",
        name: "graphql_SearchPlaces",
path: "graphql_SearchPlaces",
pathFormatted: "req_graphql-searchp-61328",
stats: {
    "name": "graphql_SearchPlaces",
    "numberOfRequests": {
        "total": "1206",
        "ok": "695",
        "ko": "511"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "17675",
        "ok": "17547",
        "ko": "17675"
    },
    "meanResponseTime": {
        "total": "6591",
        "ok": "3934",
        "ko": "10204"
    },
    "standardDeviation": {
        "total": "3907",
        "ok": "2726",
        "ko": "1810"
    },
    "percentiles1": {
        "total": "6830",
        "ok": "3239",
        "ko": "10276"
    },
    "percentiles2": {
        "total": "10211",
        "ok": "5373",
        "ko": "10704"
    },
    "percentiles3": {
        "total": "11514",
        "ok": "9575",
        "ko": "12652"
    },
    "percentiles4": {
        "total": "15285",
        "ok": "11404",
        "ko": "16587"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 42,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 24,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 629,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 511,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.872",
        "ko": "2.112"
    }
}
    },"req_request-26-taxo-41b6f": {
        type: "REQUEST",
        name: "request_26_TaxonMedia",
path: "request_26_TaxonMedia",
pathFormatted: "req_request-26-taxo-41b6f",
stats: {
    "name": "request_26_TaxonMedia",
    "numberOfRequests": {
        "total": "1206",
        "ok": "676",
        "ko": "530"
    },
    "minResponseTime": {
        "total": "243",
        "ok": "243",
        "ko": "328"
    },
    "maxResponseTime": {
        "total": "60050",
        "ok": "57168",
        "ko": "60050"
    },
    "meanResponseTime": {
        "total": "8927",
        "ok": "7550",
        "ko": "10683"
    },
    "standardDeviation": {
        "total": "6928",
        "ok": "8037",
        "ko": "4619"
    },
    "percentiles1": {
        "total": "8761",
        "ok": "4938",
        "ko": "10267"
    },
    "percentiles2": {
        "total": "10534",
        "ok": "8795",
        "ko": "10677"
    },
    "percentiles3": {
        "total": "18081",
        "ok": "25076",
        "ko": "13356"
    },
    "percentiles4": {
        "total": "36705",
        "ok": "39020",
        "ko": "17703"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 11,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 658,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 530,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.793",
        "ko": "2.19"
    }
}
    },"req_request-28-taxo-f261b": {
        type: "REQUEST",
        name: "request_28_TaxonMedia",
path: "request_28_TaxonMedia",
pathFormatted: "req_request-28-taxo-f261b",
stats: {
    "name": "request_28_TaxonMedia",
    "numberOfRequests": {
        "total": "1206",
        "ok": "664",
        "ko": "542"
    },
    "minResponseTime": {
        "total": "245",
        "ok": "275",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "54822",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "8880",
        "ok": "7546",
        "ko": "10514"
    },
    "standardDeviation": {
        "total": "6207",
        "ok": "7244",
        "ko": "4074"
    },
    "percentiles1": {
        "total": "9144",
        "ok": "4971",
        "ko": "10303"
    },
    "percentiles2": {
        "total": "10653",
        "ok": "9722",
        "ko": "10733"
    },
    "percentiles3": {
        "total": "18255",
        "ok": "22132",
        "ko": "12942"
    },
    "percentiles4": {
        "total": "31457",
        "ok": "32332",
        "ko": "16713"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 657,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 542,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.744",
        "ko": "2.24"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "1206",
        "ok": "1199",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "134",
        "ok": "134",
        "ko": "11516"
    },
    "maxResponseTime": {
        "total": "60048",
        "ok": "15046",
        "ko": "60048"
    },
    "meanResponseTime": {
        "total": "1896",
        "ok": "1676",
        "ko": "39562"
    },
    "standardDeviation": {
        "total": "4112",
        "ok": "2327",
        "ko": "23623"
    },
    "percentiles1": {
        "total": "652",
        "ok": "643",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "1802",
        "ok": "1775",
        "ko": "60010"
    },
    "percentiles3": {
        "total": "7280",
        "ok": "7071",
        "ko": "60038"
    },
    "percentiles4": {
        "total": "12395",
        "ok": "10877",
        "ko": "60046"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 633,
    "percentage": 52
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 53,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 513,
    "percentage": 43
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "4.955",
        "ko": "0.029"
    }
}
    },"req_graphql-news-d7bbe": {
        type: "REQUEST",
        name: "graphQL_news",
path: "graphQL_news",
pathFormatted: "req_graphql-news-d7bbe",
stats: {
    "name": "graphQL_news",
    "numberOfRequests": {
        "total": "1206",
        "ok": "694",
        "ko": "512"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60067",
        "ok": "55092",
        "ko": "60067"
    },
    "meanResponseTime": {
        "total": "7086",
        "ok": "4711",
        "ko": "10307"
    },
    "standardDeviation": {
        "total": "4934",
        "ok": "4781",
        "ko": "2885"
    },
    "percentiles1": {
        "total": "7613",
        "ok": "3446",
        "ko": "10259"
    },
    "percentiles2": {
        "total": "10267",
        "ok": "5742",
        "ko": "10705"
    },
    "percentiles3": {
        "total": "12771",
        "ok": "12079",
        "ko": "13080"
    },
    "percentiles4": {
        "total": "19075",
        "ok": "25856",
        "ko": "16502"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 39,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 635,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 512,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.868",
        "ko": "2.116"
    }
}
    },"req_request-4-matom-e3907": {
        type: "REQUEST",
        name: "request_4_matomo",
path: "request_4_matomo",
pathFormatted: "req_request-4-matom-e3907",
stats: {
    "name": "request_4_matomo",
    "numberOfRequests": {
        "total": "1206",
        "ok": "1206",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12088",
        "ok": "12088",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1805",
        "ok": "1805",
        "ko": "-"
    },
    "percentiles1": {
        "total": "398",
        "ok": "398",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1372",
        "ok": "1372",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5515",
        "ok": "5515",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8326",
        "ok": "8326",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 808,
    "percentage": 67
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 65,
    "percentage": 5
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 333,
    "percentage": 28
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "4.983",
        "ko": "-"
    }
}
    },"req_main-e4fad40d75-406e2": {
        type: "REQUEST",
        name: "main.e4fad40d75305c1c3562.js",
path: "main.e4fad40d75305c1c3562.js",
pathFormatted: "req_main-e4fad40d75-406e2",
stats: {
    "name": "main.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "1206",
        "ok": "687",
        "ko": "519"
    },
    "minResponseTime": {
        "total": "162",
        "ok": "162",
        "ko": "366"
    },
    "maxResponseTime": {
        "total": "60113",
        "ok": "55690",
        "ko": "60113"
    },
    "meanResponseTime": {
        "total": "10132",
        "ok": "9245",
        "ko": "11306"
    },
    "standardDeviation": {
        "total": "8065",
        "ok": "8903",
        "ko": "6618"
    },
    "percentiles1": {
        "total": "10003",
        "ok": "6209",
        "ko": "10354"
    },
    "percentiles2": {
        "total": "10871",
        "ok": "11052",
        "ko": "10831"
    },
    "percentiles3": {
        "total": "23524",
        "ok": "27875",
        "ko": "15976"
    },
    "percentiles4": {
        "total": "50465",
        "ok": "43970",
        "ko": "59969"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 14,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 666,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 519,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.839",
        "ko": "2.145"
    }
}
    },"req_request-36-9ad97": {
        type: "REQUEST",
        name: "request_36",
path: "request_36",
pathFormatted: "req_request-36-9ad97",
stats: {
    "name": "request_36",
    "numberOfRequests": {
        "total": "1206",
        "ok": "682",
        "ko": "524"
    },
    "minResponseTime": {
        "total": "44",
        "ok": "44",
        "ko": "287"
    },
    "maxResponseTime": {
        "total": "60139",
        "ok": "58966",
        "ko": "60139"
    },
    "meanResponseTime": {
        "total": "8920",
        "ok": "7383",
        "ko": "10920"
    },
    "standardDeviation": {
        "total": "7605",
        "ok": "8549",
        "ko": "5559"
    },
    "percentiles1": {
        "total": "8790",
        "ok": "4520",
        "ko": "10319"
    },
    "percentiles2": {
        "total": "10578",
        "ok": "8803",
        "ko": "10750"
    },
    "percentiles3": {
        "total": "18005",
        "ok": "24591",
        "ko": "13669"
    },
    "percentiles4": {
        "total": "43065",
        "ok": "42500",
        "ko": "50248"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 28,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 11,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 643,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 524,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.818",
        "ko": "2.165"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "1206",
        "ok": "697",
        "ko": "509"
    },
    "minResponseTime": {
        "total": "49",
        "ok": "49",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "59792",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "8719",
        "ok": "7386",
        "ko": "10544"
    },
    "standardDeviation": {
        "total": "7003",
        "ok": "8237",
        "ko": "4186"
    },
    "percentiles1": {
        "total": "8770",
        "ok": "4557",
        "ko": "10286"
    },
    "percentiles2": {
        "total": "10518",
        "ok": "8903",
        "ko": "10618"
    },
    "percentiles3": {
        "total": "18543",
        "ok": "24523",
        "ko": "13153"
    },
    "percentiles4": {
        "total": "39616",
        "ok": "40055",
        "ko": "17294"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 27,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 21,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 649,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 509,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.88",
        "ko": "2.103"
    }
}
    },"req_graphql-weather-e1b31": {
        type: "REQUEST",
        name: "graphql_weather",
path: "graphql_weather",
pathFormatted: "req_graphql-weather-e1b31",
stats: {
    "name": "graphql_weather",
    "numberOfRequests": {
        "total": "1206",
        "ok": "680",
        "ko": "526"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "44974",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "7115",
        "ok": "4711",
        "ko": "10223"
    },
    "standardDeviation": {
        "total": "4705",
        "ok": "4442",
        "ko": "2850"
    },
    "percentiles1": {
        "total": "8312",
        "ok": "3579",
        "ko": "10260"
    },
    "percentiles2": {
        "total": "10269",
        "ok": "6317",
        "ko": "10610"
    },
    "percentiles3": {
        "total": "12223",
        "ok": "10578",
        "ko": "12801"
    },
    "percentiles4": {
        "total": "17399",
        "ok": "24938",
        "ko": "16383"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 25,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 625,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 526,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.81",
        "ko": "2.174"
    }
}
    },"req_request-20-taxo-0d791": {
        type: "REQUEST",
        name: "request_20_TaxonMedia",
path: "request_20_TaxonMedia",
pathFormatted: "req_request-20-taxo-0d791",
stats: {
    "name": "request_20_TaxonMedia",
    "numberOfRequests": {
        "total": "1206",
        "ok": "670",
        "ko": "536"
    },
    "minResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "328"
    },
    "maxResponseTime": {
        "total": "57499",
        "ok": "57499",
        "ko": "19081"
    },
    "meanResponseTime": {
        "total": "8303",
        "ok": "6751",
        "ko": "10243"
    },
    "standardDeviation": {
        "total": "5689",
        "ok": "7093",
        "ko": "1778"
    },
    "percentiles1": {
        "total": "8718",
        "ok": "4790",
        "ko": "10249"
    },
    "percentiles2": {
        "total": "10375",
        "ok": "8247",
        "ko": "10645"
    },
    "percentiles3": {
        "total": "15730",
        "ok": "20302",
        "ko": "12838"
    },
    "percentiles4": {
        "total": "31532",
        "ok": "38837",
        "ko": "16702"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 650,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 536,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.769",
        "ko": "2.215"
    }
}
    },"req_request-35-3745a": {
        type: "REQUEST",
        name: "request_35",
path: "request_35",
pathFormatted: "req_request-35-3745a",
stats: {
    "name": "request_35",
    "numberOfRequests": {
        "total": "1206",
        "ok": "717",
        "ko": "489"
    },
    "minResponseTime": {
        "total": "57",
        "ok": "57",
        "ko": "343"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "59582",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "8682",
        "ok": "7478",
        "ko": "10447"
    },
    "standardDeviation": {
        "total": "7124",
        "ok": "8317",
        "ko": "4302"
    },
    "percentiles1": {
        "total": "8685",
        "ok": "4488",
        "ko": "10235"
    },
    "percentiles2": {
        "total": "10466",
        "ok": "8650",
        "ko": "10611"
    },
    "percentiles3": {
        "total": "20259",
        "ok": "25315",
        "ko": "13041"
    },
    "percentiles4": {
        "total": "38559",
        "ok": "41986",
        "ko": "17673"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 23,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 681,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 489,
    "percentage": 41
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.963",
        "ko": "2.021"
    }
}
    },"req_graphql-riveran-e540f": {
        type: "REQUEST",
        name: "graphql_RiverAndGroundWaterObservation",
path: "graphql_RiverAndGroundWaterObservation",
pathFormatted: "req_graphql-riveran-e540f",
stats: {
    "name": "graphql_RiverAndGroundWaterObservation",
    "numberOfRequests": {
        "total": "1206",
        "ok": "660",
        "ko": "546"
    },
    "minResponseTime": {
        "total": "68",
        "ok": "68",
        "ko": "288"
    },
    "maxResponseTime": {
        "total": "60014",
        "ok": "59430",
        "ko": "60014"
    },
    "meanResponseTime": {
        "total": "8851",
        "ok": "7514",
        "ko": "10466"
    },
    "standardDeviation": {
        "total": "7004",
        "ok": "8715",
        "ko": "3431"
    },
    "percentiles1": {
        "total": "8912",
        "ok": "4404",
        "ko": "10287"
    },
    "percentiles2": {
        "total": "10537",
        "ok": "8838",
        "ko": "10678"
    },
    "percentiles3": {
        "total": "19804",
        "ok": "25252",
        "ko": "12694"
    },
    "percentiles4": {
        "total": "37940",
        "ok": "47317",
        "ko": "17594"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 21,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 626,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 546,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.727",
        "ko": "2.256"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "1206",
        "ok": "686",
        "ko": "520"
    },
    "minResponseTime": {
        "total": "247",
        "ok": "315",
        "ko": "247"
    },
    "maxResponseTime": {
        "total": "60340",
        "ok": "58523",
        "ko": "60340"
    },
    "meanResponseTime": {
        "total": "9920",
        "ok": "8995",
        "ko": "11141"
    },
    "standardDeviation": {
        "total": "7653",
        "ok": "8131",
        "ko": "6781"
    },
    "percentiles1": {
        "total": "9536",
        "ok": "6372",
        "ko": "10283"
    },
    "percentiles2": {
        "total": "10815",
        "ok": "11287",
        "ko": "10741"
    },
    "percentiles3": {
        "total": "21508",
        "ok": "24366",
        "ko": "14766"
    },
    "percentiles4": {
        "total": "50977",
        "ok": "43846",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 9,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 671,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 520,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.835",
        "ko": "2.149"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "1206",
        "ok": "697",
        "ko": "509"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "19997",
        "ok": "17073",
        "ko": "19997"
    },
    "meanResponseTime": {
        "total": "6597",
        "ok": "3982",
        "ko": "10178"
    },
    "standardDeviation": {
        "total": "3890",
        "ok": "2685",
        "ko": "1948"
    },
    "percentiles1": {
        "total": "7105",
        "ok": "3329",
        "ko": "10235"
    },
    "percentiles2": {
        "total": "10176",
        "ok": "5583",
        "ko": "10569"
    },
    "percentiles3": {
        "total": "11354",
        "ok": "9324",
        "ko": "12647"
    },
    "percentiles4": {
        "total": "15770",
        "ok": "11128",
        "ko": "17380"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 30,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 637,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 509,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.88",
        "ko": "2.103"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "1206",
        "ok": "683",
        "ko": "523"
    },
    "minResponseTime": {
        "total": "65",
        "ok": "65",
        "ko": "333"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "57534",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "8929",
        "ok": "7590",
        "ko": "10679"
    },
    "standardDeviation": {
        "total": "7271",
        "ok": "8293",
        "ko": "5166"
    },
    "percentiles1": {
        "total": "8774",
        "ok": "5189",
        "ko": "10242"
    },
    "percentiles2": {
        "total": "10494",
        "ok": "9161",
        "ko": "10673"
    },
    "percentiles3": {
        "total": "17463",
        "ok": "23584",
        "ko": "13330"
    },
    "percentiles4": {
        "total": "51310",
        "ok": "51893",
        "ko": "19563"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 25,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 641,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 523,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.822",
        "ko": "2.161"
    }
}
    },"req_request-6-font-15e7e": {
        type: "REQUEST",
        name: "request_6_font",
path: "request_6_font",
pathFormatted: "req_request-6-font-15e7e",
stats: {
    "name": "request_6_font",
    "numberOfRequests": {
        "total": "1206",
        "ok": "683",
        "ko": "523"
    },
    "minResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "58724",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "8909",
        "ok": "7638",
        "ko": "10569"
    },
    "standardDeviation": {
        "total": "7463",
        "ok": "9050",
        "ko": "4075"
    },
    "percentiles1": {
        "total": "8766",
        "ok": "4651",
        "ko": "10274"
    },
    "percentiles2": {
        "total": "10492",
        "ok": "8863",
        "ko": "10651"
    },
    "percentiles3": {
        "total": "18834",
        "ok": "26273",
        "ko": "13094"
    },
    "percentiles4": {
        "total": "49687",
        "ok": "51713",
        "ko": "17478"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 20,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 14,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 649,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 523,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.822",
        "ko": "2.161"
    }
}
    },"req_request-23-taxo-3ea91": {
        type: "REQUEST",
        name: "request_23_TaxonMedia",
path: "request_23_TaxonMedia",
pathFormatted: "req_request-23-taxo-3ea91",
stats: {
    "name": "request_23_TaxonMedia",
    "numberOfRequests": {
        "total": "1206",
        "ok": "658",
        "ko": "548"
    },
    "minResponseTime": {
        "total": "197",
        "ok": "197",
        "ko": "288"
    },
    "maxResponseTime": {
        "total": "60082",
        "ok": "59300",
        "ko": "60082"
    },
    "meanResponseTime": {
        "total": "8953",
        "ok": "7625",
        "ko": "10548"
    },
    "standardDeviation": {
        "total": "7173",
        "ok": "8533",
        "ko": "4596"
    },
    "percentiles1": {
        "total": "8816",
        "ok": "4997",
        "ko": "10263"
    },
    "percentiles2": {
        "total": "10469",
        "ok": "8849",
        "ko": "10668"
    },
    "percentiles3": {
        "total": "17472",
        "ok": "23287",
        "ko": "13093"
    },
    "percentiles4": {
        "total": "48815",
        "ok": "51879",
        "ko": "17144"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 645,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 548,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.719",
        "ko": "2.264"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "1206",
        "ok": "668",
        "ko": "538"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "246"
    },
    "maxResponseTime": {
        "total": "17701",
        "ok": "17691",
        "ko": "17701"
    },
    "meanResponseTime": {
        "total": "6770",
        "ok": "3998",
        "ko": "10211"
    },
    "standardDeviation": {
        "total": "3924",
        "ok": "2865",
        "ko": "1715"
    },
    "percentiles1": {
        "total": "7945",
        "ok": "3240",
        "ko": "10278"
    },
    "percentiles2": {
        "total": "10258",
        "ok": "5560",
        "ko": "10631"
    },
    "percentiles3": {
        "total": "11777",
        "ok": "9439",
        "ko": "12682"
    },
    "percentiles4": {
        "total": "15441",
        "ok": "12565",
        "ko": "16009"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 39,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 27,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 602,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 538,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.76",
        "ko": "2.223"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "1206",
        "ok": "671",
        "ko": "535"
    },
    "minResponseTime": {
        "total": "247",
        "ok": "280",
        "ko": "247"
    },
    "maxResponseTime": {
        "total": "60187",
        "ok": "57692",
        "ko": "60187"
    },
    "meanResponseTime": {
        "total": "9286",
        "ok": "7721",
        "ko": "11248"
    },
    "standardDeviation": {
        "total": "7492",
        "ok": "7246",
        "ko": "7331"
    },
    "percentiles1": {
        "total": "9315",
        "ok": "5050",
        "ko": "10308"
    },
    "percentiles2": {
        "total": "10699",
        "ok": "10391",
        "ko": "10734"
    },
    "percentiles3": {
        "total": "19713",
        "ok": "22306",
        "ko": "14269"
    },
    "percentiles4": {
        "total": "40847",
        "ok": "35298",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 15,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 650,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 535,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.773",
        "ko": "2.211"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "1206",
        "ok": "679",
        "ko": "527"
    },
    "minResponseTime": {
        "total": "85",
        "ok": "85",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60011",
        "ok": "57856",
        "ko": "60011"
    },
    "meanResponseTime": {
        "total": "8690",
        "ok": "7210",
        "ko": "10597"
    },
    "standardDeviation": {
        "total": "6763",
        "ok": "7931",
        "ko": "4141"
    },
    "percentiles1": {
        "total": "8750",
        "ok": "4685",
        "ko": "10285"
    },
    "percentiles2": {
        "total": "10522",
        "ok": "8480",
        "ko": "10683"
    },
    "percentiles3": {
        "total": "17608",
        "ok": "22755",
        "ko": "13716"
    },
    "percentiles4": {
        "total": "37915",
        "ok": "39466",
        "ko": "17710"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 22,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 10,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 647,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 527,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.806",
        "ko": "2.178"
    }
}
    },"req_synaptix-client-ff009": {
        type: "REQUEST",
        name: "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
path: "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
pathFormatted: "req_synaptix-client-ff009",
stats: {
    "name": "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "1206",
        "ok": "696",
        "ko": "510"
    },
    "minResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "366"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "59914",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "8337",
        "ok": "6736",
        "ko": "10522"
    },
    "standardDeviation": {
        "total": "6644",
        "ok": "7811",
        "ko": "3582"
    },
    "percentiles1": {
        "total": "8695",
        "ok": "4409",
        "ko": "10332"
    },
    "percentiles2": {
        "total": "10482",
        "ok": "7876",
        "ko": "10807"
    },
    "percentiles3": {
        "total": "16127",
        "ok": "25071",
        "ko": "13274"
    },
    "percentiles4": {
        "total": "34973",
        "ok": "37717",
        "ko": "16903"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 36,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 640,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 510,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.876",
        "ko": "2.107"
    }
}
    },"req_favicon-48x48-p-1e880": {
        type: "REQUEST",
        name: "favicon-48x48.png",
path: "favicon-48x48.png",
pathFormatted: "req_favicon-48x48-p-1e880",
stats: {
    "name": "favicon-48x48.png",
    "numberOfRequests": {
        "total": "1206",
        "ok": "693",
        "ko": "513"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "286"
    },
    "maxResponseTime": {
        "total": "59504",
        "ok": "59504",
        "ko": "19195"
    },
    "meanResponseTime": {
        "total": "7330",
        "ok": "5091",
        "ko": "10354"
    },
    "standardDeviation": {
        "total": "5337",
        "ok": "5960",
        "ko": "1747"
    },
    "percentiles1": {
        "total": "8095",
        "ok": "3681",
        "ko": "10337"
    },
    "percentiles2": {
        "total": "10341",
        "ok": "6565",
        "ko": "10852"
    },
    "percentiles3": {
        "total": "12592",
        "ok": "11901",
        "ko": "12860"
    },
    "percentiles4": {
        "total": "25484",
        "ok": "33334",
        "ko": "16545"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 38,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 19,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 636,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 513,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.864",
        "ko": "2.12"
    }
}
    },"req_vendors-e4fad40-0b16d": {
        type: "REQUEST",
        name: "vendors.e4fad40d75305c1c3562.js",
path: "vendors.e4fad40d75305c1c3562.js",
pathFormatted: "req_vendors-e4fad40-0b16d",
stats: {
    "name": "vendors.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "1206",
        "ok": "711",
        "ko": "495"
    },
    "minResponseTime": {
        "total": "380",
        "ok": "624",
        "ko": "380"
    },
    "maxResponseTime": {
        "total": "60117",
        "ok": "59223",
        "ko": "60117"
    },
    "meanResponseTime": {
        "total": "14592",
        "ok": "16654",
        "ko": "11629"
    },
    "standardDeviation": {
        "total": "9561",
        "ok": "9920",
        "ko": "8154"
    },
    "percentiles1": {
        "total": "10891",
        "ok": "13827",
        "ko": "10335"
    },
    "percentiles2": {
        "total": "16357",
        "ok": "21882",
        "ko": "10902"
    },
    "percentiles3": {
        "total": "35092",
        "ok": "36697",
        "ko": "15658"
    },
    "percentiles4": {
        "total": "59961",
        "ok": "46507",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 706,
    "percentage": 59
},
    "group4": {
    "name": "failed",
    "count": 495,
    "percentage": 41
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.938",
        "ko": "2.045"
    }
}
    },"req_request-30-taxo-7acf0": {
        type: "REQUEST",
        name: "request_30_TaxonMedia",
path: "request_30_TaxonMedia",
pathFormatted: "req_request-30-taxo-7acf0",
stats: {
    "name": "request_30_TaxonMedia",
    "numberOfRequests": {
        "total": "1206",
        "ok": "663",
        "ko": "543"
    },
    "minResponseTime": {
        "total": "246",
        "ok": "246",
        "ko": "246"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "57012",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "9206",
        "ok": "8013",
        "ko": "10662"
    },
    "standardDeviation": {
        "total": "7689",
        "ok": "9108",
        "ko": "5115"
    },
    "percentiles1": {
        "total": "8806",
        "ok": "5009",
        "ko": "10247"
    },
    "percentiles2": {
        "total": "10515",
        "ok": "8918",
        "ko": "10684"
    },
    "percentiles3": {
        "total": "20388",
        "ok": "27574",
        "ko": "13192"
    },
    "percentiles4": {
        "total": "49502",
        "ok": "49618",
        "ko": "19611"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 648,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 543,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.74",
        "ko": "2.244"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "1206",
        "ok": "676",
        "ko": "530"
    },
    "minResponseTime": {
        "total": "17",
        "ok": "17",
        "ko": "343"
    },
    "maxResponseTime": {
        "total": "20466",
        "ok": "20466",
        "ko": "17703"
    },
    "meanResponseTime": {
        "total": "6749",
        "ok": "4007",
        "ko": "10245"
    },
    "standardDeviation": {
        "total": "3976",
        "ok": "2991",
        "ko": "1661"
    },
    "percentiles1": {
        "total": "7808",
        "ok": "3299",
        "ko": "10302"
    },
    "percentiles2": {
        "total": "10272",
        "ok": "5516",
        "ko": "10689"
    },
    "percentiles3": {
        "total": "11644",
        "ok": "9926",
        "ko": "12825"
    },
    "percentiles4": {
        "total": "14606",
        "ok": "12851",
        "ko": "15547"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 53,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 30,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 593,
    "percentage": 49
},
    "group4": {
    "name": "failed",
    "count": 530,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.793",
        "ko": "2.19"
    }
}
    },"req_request-25-taxo-2081e": {
        type: "REQUEST",
        name: "request_25_TaxonMedia",
path: "request_25_TaxonMedia",
pathFormatted: "req_request-25-taxo-2081e",
stats: {
    "name": "request_25_TaxonMedia",
    "numberOfRequests": {
        "total": "1206",
        "ok": "667",
        "ko": "539"
    },
    "minResponseTime": {
        "total": "247",
        "ok": "253",
        "ko": "247"
    },
    "maxResponseTime": {
        "total": "60118",
        "ok": "57556",
        "ko": "60118"
    },
    "meanResponseTime": {
        "total": "9063",
        "ok": "7686",
        "ko": "10767"
    },
    "standardDeviation": {
        "total": "7392",
        "ok": "8629",
        "ko": "4984"
    },
    "percentiles1": {
        "total": "8876",
        "ok": "4888",
        "ko": "10287"
    },
    "percentiles2": {
        "total": "10500",
        "ok": "8887",
        "ko": "10669"
    },
    "percentiles3": {
        "total": "18488",
        "ok": "26459",
        "ko": "12922"
    },
    "percentiles4": {
        "total": "44918",
        "ok": "45714",
        "ko": "18134"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 652,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 539,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.756",
        "ko": "2.227"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "1206",
        "ok": "686",
        "ko": "520"
    },
    "minResponseTime": {
        "total": "17",
        "ok": "17",
        "ko": "290"
    },
    "maxResponseTime": {
        "total": "60028",
        "ok": "56175",
        "ko": "60028"
    },
    "meanResponseTime": {
        "total": "8019",
        "ok": "6203",
        "ko": "10415"
    },
    "standardDeviation": {
        "total": "6067",
        "ok": "6888",
        "ko": "3563"
    },
    "percentiles1": {
        "total": "8678",
        "ok": "3971",
        "ko": "10275"
    },
    "percentiles2": {
        "total": "10385",
        "ok": "7759",
        "ko": "10659"
    },
    "percentiles3": {
        "total": "14953",
        "ok": "19990",
        "ko": "13023"
    },
    "percentiles4": {
        "total": "31240",
        "ok": "32817",
        "ko": "16952"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 29,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 28,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 629,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 520,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.835",
        "ko": "2.149"
    }
}
    },"req_request-21-taxo-4adce": {
        type: "REQUEST",
        name: "request_21_TaxonMedia",
path: "request_21_TaxonMedia",
pathFormatted: "req_request-21-taxo-4adce",
stats: {
    "name": "request_21_TaxonMedia",
    "numberOfRequests": {
        "total": "1206",
        "ok": "658",
        "ko": "548"
    },
    "minResponseTime": {
        "total": "234",
        "ok": "234",
        "ko": "246"
    },
    "maxResponseTime": {
        "total": "60329",
        "ok": "56314",
        "ko": "60329"
    },
    "meanResponseTime": {
        "total": "8462",
        "ok": "6786",
        "ko": "10475"
    },
    "standardDeviation": {
        "total": "6146",
        "ok": "7264",
        "ko": "3511"
    },
    "percentiles1": {
        "total": "8752",
        "ok": "4691",
        "ko": "10317"
    },
    "percentiles2": {
        "total": "10477",
        "ok": "8124",
        "ko": "10732"
    },
    "percentiles3": {
        "total": "15207",
        "ok": "19164",
        "ko": "13176"
    },
    "percentiles4": {
        "total": "32553",
        "ok": "40509",
        "ko": "16775"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 646,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 548,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.719",
        "ko": "2.264"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "1206",
        "ok": "700",
        "ko": "506"
    },
    "minResponseTime": {
        "total": "77",
        "ok": "77",
        "ko": "213"
    },
    "maxResponseTime": {
        "total": "60079",
        "ok": "53215",
        "ko": "60079"
    },
    "meanResponseTime": {
        "total": "8650",
        "ok": "7270",
        "ko": "10559"
    },
    "standardDeviation": {
        "total": "6919",
        "ok": "7854",
        "ko": "4741"
    },
    "percentiles1": {
        "total": "8735",
        "ok": "4698",
        "ko": "10272"
    },
    "percentiles2": {
        "total": "10514",
        "ok": "8865",
        "ko": "10691"
    },
    "percentiles3": {
        "total": "17686",
        "ok": "21372",
        "ko": "12460"
    },
    "percentiles4": {
        "total": "40008",
        "ok": "40418",
        "ko": "18175"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 27,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 11,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 662,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 506,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.893",
        "ko": "2.091"
    }
}
    },"req_favicon-ico-8af3a": {
        type: "REQUEST",
        name: "favicon.ico",
path: "favicon.ico",
pathFormatted: "req_favicon-ico-8af3a",
stats: {
    "name": "favicon.ico",
    "numberOfRequests": {
        "total": "1206",
        "ok": "721",
        "ko": "485"
    },
    "minResponseTime": {
        "total": "70",
        "ok": "70",
        "ko": "311"
    },
    "maxResponseTime": {
        "total": "60021",
        "ok": "58460",
        "ko": "60021"
    },
    "meanResponseTime": {
        "total": "8853",
        "ok": "7813",
        "ko": "10399"
    },
    "standardDeviation": {
        "total": "7087",
        "ok": "8700",
        "ko": "2896"
    },
    "percentiles1": {
        "total": "8763",
        "ok": "4997",
        "ko": "10346"
    },
    "percentiles2": {
        "total": "10575",
        "ok": "9111",
        "ko": "10777"
    },
    "percentiles3": {
        "total": "19810",
        "ok": "26905",
        "ko": "12922"
    },
    "percentiles4": {
        "total": "37522",
        "ok": "47467",
        "ko": "16648"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 23,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 18,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 680,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 485,
    "percentage": 40
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.979",
        "ko": "2.004"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "1206",
        "ok": "679",
        "ko": "527"
    },
    "minResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "245"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "59558",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "8476",
        "ok": "6826",
        "ko": "10603"
    },
    "standardDeviation": {
        "total": "6478",
        "ok": "7432",
        "ko": "4104"
    },
    "percentiles1": {
        "total": "8742",
        "ok": "4622",
        "ko": "10277"
    },
    "percentiles2": {
        "total": "10523",
        "ok": "8414",
        "ko": "10740"
    },
    "percentiles3": {
        "total": "16724",
        "ok": "19964",
        "ko": "13221"
    },
    "percentiles4": {
        "total": "33654",
        "ok": "40933",
        "ko": "17590"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 31,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 15,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 633,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 527,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.983",
        "ok": "2.806",
        "ko": "2.178"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
