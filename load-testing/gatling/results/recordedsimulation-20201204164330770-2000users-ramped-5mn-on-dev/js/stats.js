var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "71230",
        "ok": "48741",
        "ko": "22489"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "252"
    },
    "maxResponseTime": {
        "total": "60608",
        "ok": "60254",
        "ko": "60608"
    },
    "meanResponseTime": {
        "total": "7996",
        "ok": "6680",
        "ko": "10849"
    },
    "standardDeviation": {
        "total": "7056",
        "ok": "7396",
        "ko": "5222"
    },
    "percentiles1": {
        "total": "7812",
        "ok": "4370",
        "ko": "10239"
    },
    "percentiles2": {
        "total": "10322",
        "ok": "8324",
        "ko": "10682"
    },
    "percentiles3": {
        "total": "17533",
        "ok": "20617",
        "ko": "14382"
    },
    "percentiles4": {
        "total": "39317",
        "ok": "39376",
        "ko": "19877"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2877,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1011,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 44853,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 22489,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "206.464",
        "ok": "141.278",
        "ko": "65.186"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "2000",
        "ok": "1505",
        "ko": "495"
    },
    "minResponseTime": {
        "total": "80",
        "ok": "80",
        "ko": "347"
    },
    "maxResponseTime": {
        "total": "58400",
        "ok": "58400",
        "ko": "17361"
    },
    "meanResponseTime": {
        "total": "6723",
        "ok": "5616",
        "ko": "10089"
    },
    "standardDeviation": {
        "total": "5871",
        "ok": "6335",
        "ko": "1479"
    },
    "percentiles1": {
        "total": "5213",
        "ok": "3533",
        "ko": "10036"
    },
    "percentiles2": {
        "total": "10001",
        "ok": "6883",
        "ko": "10414"
    },
    "percentiles3": {
        "total": "14561",
        "ok": "16271",
        "ko": "12872"
    },
    "percentiles4": {
        "total": "32412",
        "ok": "36053",
        "ko": "15359"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 18,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 54,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1433,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 495,
    "percentage": 25
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.797",
        "ok": "4.362",
        "ko": "1.435"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1025",
        "ko": "480"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "268"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "44254",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "7256",
        "ok": "5713",
        "ko": "10552"
    },
    "standardDeviation": {
        "total": "5724",
        "ok": "5860",
        "ko": "3669"
    },
    "percentiles1": {
        "total": "6702",
        "ok": "3817",
        "ko": "10231"
    },
    "percentiles2": {
        "total": "10220",
        "ok": "7195",
        "ko": "10679"
    },
    "percentiles3": {
        "total": "14697",
        "ok": "16150",
        "ko": "13645"
    },
    "percentiles4": {
        "total": "31508",
        "ok": "32384",
        "ko": "17534"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 38,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 37,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 950,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 480,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.971",
        "ko": "1.391"
    }
}
    },"req_request-4-matom-e3907": {
        type: "REQUEST",
        name: "request_4_matomo",
path: "request_4_matomo",
pathFormatted: "req_request-4-matom-e3907",
stats: {
    "name": "request_4_matomo",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1503",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "107",
        "ok": "107",
        "ko": "11099"
    },
    "maxResponseTime": {
        "total": "11157",
        "ok": "10782",
        "ko": "11157"
    },
    "meanResponseTime": {
        "total": "1169",
        "ok": "1156",
        "ko": "11128"
    },
    "standardDeviation": {
        "total": "2099",
        "ok": "2069",
        "ko": "29"
    },
    "percentiles1": {
        "total": "264",
        "ok": "264",
        "ko": "11128"
    },
    "percentiles2": {
        "total": "1228",
        "ok": "1221",
        "ko": "11143"
    },
    "percentiles3": {
        "total": "7211",
        "ok": "7134",
        "ko": "11154"
    },
    "percentiles4": {
        "total": "9580",
        "ok": "9467",
        "ko": "11156"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1077,
    "percentage": 72
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 45,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 381,
    "percentage": 25
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "4.357",
        "ko": "0.006"
    }
}
    },"req_graphql-riveran-e540f": {
        type: "REQUEST",
        name: "graphql_RiverAndGroundWaterObservation",
path: "graphql_RiverAndGroundWaterObservation",
pathFormatted: "req_graphql-riveran-e540f",
stats: {
    "name": "graphql_RiverAndGroundWaterObservation",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1032",
        "ko": "473"
    },
    "minResponseTime": {
        "total": "60",
        "ok": "60",
        "ko": "415"
    },
    "maxResponseTime": {
        "total": "60163",
        "ok": "52609",
        "ko": "60163"
    },
    "meanResponseTime": {
        "total": "8703",
        "ok": "7594",
        "ko": "11123"
    },
    "standardDeviation": {
        "total": "7485",
        "ok": "7753",
        "ko": "6209"
    },
    "percentiles1": {
        "total": "8529",
        "ok": "5148",
        "ko": "10241"
    },
    "percentiles2": {
        "total": "10438",
        "ok": "9298",
        "ko": "10664"
    },
    "percentiles3": {
        "total": "20456",
        "ok": "23292",
        "ko": "14612"
    },
    "percentiles4": {
        "total": "45259",
        "ok": "41387",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 28,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 984,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 473,
    "percentage": 31
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.991",
        "ko": "1.371"
    }
}
    },"req_request-29-taxo-1f5dd": {
        type: "REQUEST",
        name: "request_29_TaxonMedia",
path: "request_29_TaxonMedia",
pathFormatted: "req_request-29-taxo-1f5dd",
stats: {
    "name": "request_29_TaxonMedia",
    "numberOfRequests": {
        "total": "1505",
        "ok": "983",
        "ko": "522"
    },
    "minResponseTime": {
        "total": "357",
        "ok": "385",
        "ko": "357"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "58663",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "8207",
        "ok": "7034",
        "ko": "10416"
    },
    "standardDeviation": {
        "total": "6575",
        "ok": "7606",
        "ko": "2867"
    },
    "percentiles1": {
        "total": "8516",
        "ok": "4613",
        "ko": "10259"
    },
    "percentiles2": {
        "total": "10388",
        "ok": "8656",
        "ko": "10724"
    },
    "percentiles3": {
        "total": "16508",
        "ok": "20488",
        "ko": "13054"
    },
    "percentiles4": {
        "total": "35813",
        "ok": "42119",
        "ko": "16858"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 19,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 961,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 522,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.849",
        "ko": "1.513"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1021",
        "ko": "484"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "401"
    },
    "maxResponseTime": {
        "total": "22596",
        "ok": "22596",
        "ko": "17552"
    },
    "meanResponseTime": {
        "total": "6209",
        "ok": "4252",
        "ko": "10337"
    },
    "standardDeviation": {
        "total": "3960",
        "ok": "3103",
        "ko": "1825"
    },
    "percentiles1": {
        "total": "5567",
        "ok": "3213",
        "ko": "10238"
    },
    "percentiles2": {
        "total": "10010",
        "ok": "5731",
        "ko": "10621"
    },
    "percentiles3": {
        "total": "11369",
        "ok": "10121",
        "ko": "14020"
    },
    "percentiles4": {
        "total": "15832",
        "ok": "13312",
        "ko": "16065"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 34,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 32,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 955,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 484,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.959",
        "ko": "1.403"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1008",
        "ko": "497"
    },
    "minResponseTime": {
        "total": "51",
        "ok": "51",
        "ko": "401"
    },
    "maxResponseTime": {
        "total": "60012",
        "ok": "58094",
        "ko": "60012"
    },
    "meanResponseTime": {
        "total": "8473",
        "ok": "7342",
        "ko": "10765"
    },
    "standardDeviation": {
        "total": "7380",
        "ok": "7992",
        "ko": "5247"
    },
    "percentiles1": {
        "total": "8320",
        "ok": "4875",
        "ko": "10233"
    },
    "percentiles2": {
        "total": "10382",
        "ok": "8536",
        "ko": "10699"
    },
    "percentiles3": {
        "total": "18908",
        "ok": "23352",
        "ko": "13668"
    },
    "percentiles4": {
        "total": "44510",
        "ok": "44449",
        "ko": "19835"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 22,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 22,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 964,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 497,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.922",
        "ko": "1.441"
    }
}
    },"req_request-28-taxo-f261b": {
        type: "REQUEST",
        name: "request_28_TaxonMedia",
path: "request_28_TaxonMedia",
pathFormatted: "req_request-28-taxo-f261b",
stats: {
    "name": "request_28_TaxonMedia",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1010",
        "ko": "495"
    },
    "minResponseTime": {
        "total": "252",
        "ok": "629",
        "ko": "252"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "58102",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "8766",
        "ok": "7556",
        "ko": "11236"
    },
    "standardDeviation": {
        "total": "7276",
        "ok": "6975",
        "ko": "7254"
    },
    "percentiles1": {
        "total": "8573",
        "ok": "5385",
        "ko": "10231"
    },
    "percentiles2": {
        "total": "10457",
        "ok": "9507",
        "ko": "10658"
    },
    "percentiles3": {
        "total": "17939",
        "ok": "19420",
        "ko": "14632"
    },
    "percentiles4": {
        "total": "51765",
        "ok": "37412",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1002,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 495,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.928",
        "ko": "1.435"
    }
}
    },"req_favicon-16x16-p-7181b": {
        type: "REQUEST",
        name: "favicon-16x16.png",
path: "favicon-16x16.png",
pathFormatted: "req_favicon-16x16-p-7181b",
stats: {
    "name": "favicon-16x16.png",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1016",
        "ko": "489"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "350"
    },
    "maxResponseTime": {
        "total": "22317",
        "ok": "22317",
        "ko": "18224"
    },
    "meanResponseTime": {
        "total": "6324",
        "ok": "4351",
        "ko": "10422"
    },
    "standardDeviation": {
        "total": "4003",
        "ok": "3169",
        "ko": "1888"
    },
    "percentiles1": {
        "total": "5698",
        "ok": "3394",
        "ko": "10237"
    },
    "percentiles2": {
        "total": "10044",
        "ok": "5880",
        "ko": "10751"
    },
    "percentiles3": {
        "total": "11921",
        "ok": "10433",
        "ko": "14403"
    },
    "percentiles4": {
        "total": "16219",
        "ok": "14055",
        "ko": "17152"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 34,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 41,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 941,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 489,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.945",
        "ko": "1.417"
    }
}
    },"req_request-25-taxo-2081e": {
        type: "REQUEST",
        name: "request_25_TaxonMedia",
path: "request_25_TaxonMedia",
pathFormatted: "req_request-25-taxo-2081e",
stats: {
    "name": "request_25_TaxonMedia",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1013",
        "ko": "492"
    },
    "minResponseTime": {
        "total": "268",
        "ok": "447",
        "ko": "268"
    },
    "maxResponseTime": {
        "total": "60363",
        "ok": "58664",
        "ko": "60363"
    },
    "meanResponseTime": {
        "total": "8773",
        "ok": "7403",
        "ko": "11593"
    },
    "standardDeviation": {
        "total": "8031",
        "ok": "7758",
        "ko": "7847"
    },
    "percentiles1": {
        "total": "8462",
        "ok": "5029",
        "ko": "10228"
    },
    "percentiles2": {
        "total": "10419",
        "ok": "8913",
        "ko": "10717"
    },
    "percentiles3": {
        "total": "20619",
        "ok": "23351",
        "ko": "15031"
    },
    "percentiles4": {
        "total": "56662",
        "ok": "39747",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 12,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 997,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 492,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.936",
        "ko": "1.426"
    }
}
    },"req_request-35-3745a": {
        type: "REQUEST",
        name: "request_35",
path: "request_35",
pathFormatted: "req_request-35-3745a",
stats: {
    "name": "request_35",
    "numberOfRequests": {
        "total": "1505",
        "ok": "997",
        "ko": "508"
    },
    "minResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "415"
    },
    "maxResponseTime": {
        "total": "60173",
        "ok": "56995",
        "ko": "60173"
    },
    "meanResponseTime": {
        "total": "8394",
        "ok": "7113",
        "ko": "10909"
    },
    "standardDeviation": {
        "total": "6832",
        "ok": "7208",
        "ko": "5172"
    },
    "percentiles1": {
        "total": "8518",
        "ok": "4992",
        "ko": "10268"
    },
    "percentiles2": {
        "total": "10403",
        "ok": "8773",
        "ko": "10647"
    },
    "percentiles3": {
        "total": "17720",
        "ok": "20370",
        "ko": "14599"
    },
    "percentiles4": {
        "total": "34461",
        "ok": "34506",
        "ko": "18344"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 21,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 959,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 508,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.89",
        "ko": "1.472"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "1505",
        "ok": "996",
        "ko": "509"
    },
    "minResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "252"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "59770",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "8091",
        "ok": "6852",
        "ko": "10515"
    },
    "standardDeviation": {
        "total": "6521",
        "ok": "7303",
        "ko": "3534"
    },
    "percentiles1": {
        "total": "8468",
        "ok": "4505",
        "ko": "10216"
    },
    "percentiles2": {
        "total": "10366",
        "ok": "8521",
        "ko": "10625"
    },
    "percentiles3": {
        "total": "16267",
        "ok": "20410",
        "ko": "13822"
    },
    "percentiles4": {
        "total": "36511",
        "ok": "39805",
        "ko": "16926"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 21,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 945,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 509,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.887",
        "ko": "1.475"
    }
}
    },"req_graphql-taxon-f4085": {
        type: "REQUEST",
        name: "graphql_Taxon",
path: "graphql_Taxon",
pathFormatted: "req_graphql-taxon-f4085",
stats: {
    "name": "graphql_Taxon",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1020",
        "ko": "485"
    },
    "minResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "415"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "59500",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "8063",
        "ok": "6893",
        "ko": "10524"
    },
    "standardDeviation": {
        "total": "7137",
        "ok": "8192",
        "ko": "2828"
    },
    "percentiles1": {
        "total": "8055",
        "ok": "4315",
        "ko": "10264"
    },
    "percentiles2": {
        "total": "10355",
        "ok": "8442",
        "ko": "10678"
    },
    "percentiles3": {
        "total": "16258",
        "ok": "20017",
        "ko": "13875"
    },
    "percentiles4": {
        "total": "41925",
        "ok": "48061",
        "ko": "16859"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 32,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 27,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 961,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 485,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.957",
        "ko": "1.406"
    }
}
    },"req_request-24-taxo-b5f25": {
        type: "REQUEST",
        name: "request_24_TaxonMedia",
path: "request_24_TaxonMedia",
pathFormatted: "req_request-24-taxo-b5f25",
stats: {
    "name": "request_24_TaxonMedia",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1022",
        "ko": "483"
    },
    "minResponseTime": {
        "total": "252",
        "ok": "358",
        "ko": "252"
    },
    "maxResponseTime": {
        "total": "60249",
        "ok": "56155",
        "ko": "60249"
    },
    "meanResponseTime": {
        "total": "9050",
        "ok": "8042",
        "ko": "11184"
    },
    "standardDeviation": {
        "total": "7544",
        "ok": "7753",
        "ko": "6589"
    },
    "percentiles1": {
        "total": "8588",
        "ok": "5701",
        "ko": "10243"
    },
    "percentiles2": {
        "total": "10531",
        "ok": "9806",
        "ko": "10729"
    },
    "percentiles3": {
        "total": "19804",
        "ok": "23387",
        "ko": "14515"
    },
    "percentiles4": {
        "total": "46745",
        "ok": "44093",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1014,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 483,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.962",
        "ko": "1.4"
    }
}
    },"req_graphql-searchp-61328": {
        type: "REQUEST",
        name: "graphql_SearchPlaces",
path: "graphql_SearchPlaces",
pathFormatted: "req_graphql-searchp-61328",
stats: {
    "name": "graphql_SearchPlaces",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1018",
        "ko": "487"
    },
    "minResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "390"
    },
    "maxResponseTime": {
        "total": "21720",
        "ok": "21720",
        "ko": "19129"
    },
    "meanResponseTime": {
        "total": "6113",
        "ok": "4089",
        "ko": "10345"
    },
    "standardDeviation": {
        "total": "3945",
        "ok": "3013",
        "ko": "1622"
    },
    "percentiles1": {
        "total": "5489",
        "ok": "3045",
        "ko": "10257"
    },
    "percentiles2": {
        "total": "10039",
        "ok": "5650",
        "ko": "10689"
    },
    "percentiles3": {
        "total": "11513",
        "ok": "10169",
        "ko": "12799"
    },
    "percentiles4": {
        "total": "14634",
        "ok": "12076",
        "ko": "16240"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 38,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 47,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 933,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 487,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.951",
        "ko": "1.412"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "1505",
        "ok": "991",
        "ko": "514"
    },
    "minResponseTime": {
        "total": "89",
        "ok": "89",
        "ko": "357"
    },
    "maxResponseTime": {
        "total": "60187",
        "ok": "57104",
        "ko": "60187"
    },
    "meanResponseTime": {
        "total": "8601",
        "ok": "7207",
        "ko": "11287"
    },
    "standardDeviation": {
        "total": "7692",
        "ok": "7620",
        "ko": "7093"
    },
    "percentiles1": {
        "total": "8494",
        "ok": "4672",
        "ko": "10276"
    },
    "percentiles2": {
        "total": "10385",
        "ok": "8611",
        "ko": "10696"
    },
    "percentiles3": {
        "total": "20011",
        "ok": "23546",
        "ko": "14637"
    },
    "percentiles4": {
        "total": "43489",
        "ok": "36660",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 22,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 21,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 948,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 514,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.872",
        "ko": "1.49"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1505",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "114",
        "ok": "114",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13313",
        "ok": "13313",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1547",
        "ok": "1547",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "percentiles1": {
        "total": "515",
        "ok": "515",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1751",
        "ok": "1751",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7637",
        "ok": "7637",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9927",
        "ok": "9927",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 809,
    "percentage": 54
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 40,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 656,
    "percentage": 44
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "4.362",
        "ko": "-"
    }
}
    },"req_request-34-d9757": {
        type: "REQUEST",
        name: "request_34",
path: "request_34",
pathFormatted: "req_request-34-d9757",
stats: {
    "name": "request_34",
    "numberOfRequests": {
        "total": "1505",
        "ok": "998",
        "ko": "507"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "357"
    },
    "maxResponseTime": {
        "total": "60325",
        "ok": "58899",
        "ko": "60325"
    },
    "meanResponseTime": {
        "total": "8187",
        "ok": "6870",
        "ko": "10781"
    },
    "standardDeviation": {
        "total": "7232",
        "ok": "7752",
        "ko": "5181"
    },
    "percentiles1": {
        "total": "8181",
        "ok": "4460",
        "ko": "10232"
    },
    "percentiles2": {
        "total": "10316",
        "ok": "8161",
        "ko": "10638"
    },
    "percentiles3": {
        "total": "17311",
        "ok": "22071",
        "ko": "13264"
    },
    "percentiles4": {
        "total": "44027",
        "ok": "44175",
        "ko": "18221"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 25,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 18,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 955,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 507,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.893",
        "ko": "1.47"
    }
}
    },"req_synaptix-client-ff009": {
        type: "REQUEST",
        name: "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
path: "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
pathFormatted: "req_synaptix-client-ff009",
stats: {
    "name": "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1005",
        "ko": "500"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "7590"
    },
    "maxResponseTime": {
        "total": "60278",
        "ok": "58321",
        "ko": "60278"
    },
    "meanResponseTime": {
        "total": "8726",
        "ok": "7555",
        "ko": "11080"
    },
    "standardDeviation": {
        "total": "7456",
        "ok": "7947",
        "ko": "5667"
    },
    "percentiles1": {
        "total": "8521",
        "ok": "5152",
        "ko": "10261"
    },
    "percentiles2": {
        "total": "10445",
        "ok": "9196",
        "ko": "10774"
    },
    "percentiles3": {
        "total": "20868",
        "ok": "24282",
        "ko": "14859"
    },
    "percentiles4": {
        "total": "40383",
        "ok": "39947",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 21,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 23,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 961,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 500,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.913",
        "ko": "1.449"
    }
}
    },"req_vendors-e4fad40-0b16d": {
        type: "REQUEST",
        name: "vendors.e4fad40d75305c1c3562.js",
path: "vendors.e4fad40d75305c1c3562.js",
pathFormatted: "req_vendors-e4fad40-0b16d",
stats: {
    "name": "vendors.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1030",
        "ko": "475"
    },
    "minResponseTime": {
        "total": "416",
        "ok": "1060",
        "ko": "416"
    },
    "maxResponseTime": {
        "total": "60384",
        "ok": "59427",
        "ko": "60384"
    },
    "meanResponseTime": {
        "total": "15151",
        "ok": "16344",
        "ko": "12564"
    },
    "standardDeviation": {
        "total": "10298",
        "ok": "10278",
        "ko": "9857"
    },
    "percentiles1": {
        "total": "11056",
        "ok": "13592",
        "ko": "10368"
    },
    "percentiles2": {
        "total": "17689",
        "ok": "20590",
        "ko": "10915"
    },
    "percentiles3": {
        "total": "34872",
        "ok": "36782",
        "ko": "17840"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "53506",
        "ko": "60044"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1028,
    "percentage": 68
},
    "group4": {
    "name": "failed",
    "count": 475,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.986",
        "ko": "1.377"
    }
}
    },"req_request-30-taxo-7acf0": {
        type: "REQUEST",
        name: "request_30_TaxonMedia",
path: "request_30_TaxonMedia",
pathFormatted: "req_request-30-taxo-7acf0",
stats: {
    "name": "request_30_TaxonMedia",
    "numberOfRequests": {
        "total": "1505",
        "ok": "996",
        "ko": "509"
    },
    "minResponseTime": {
        "total": "315",
        "ok": "315",
        "ko": "358"
    },
    "maxResponseTime": {
        "total": "60254",
        "ok": "60254",
        "ko": "60193"
    },
    "meanResponseTime": {
        "total": "8944",
        "ok": "8039",
        "ko": "10713"
    },
    "standardDeviation": {
        "total": "7127",
        "ok": "7764",
        "ko": "5244"
    },
    "percentiles1": {
        "total": "8638",
        "ok": "5597",
        "ko": "10234"
    },
    "percentiles2": {
        "total": "10528",
        "ok": "9895",
        "ko": "10654"
    },
    "percentiles3": {
        "total": "20001",
        "ok": "23338",
        "ko": "13739"
    },
    "percentiles4": {
        "total": "37547",
        "ok": "37634",
        "ko": "18252"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 988,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 509,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.887",
        "ko": "1.475"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "1505",
        "ok": "998",
        "ko": "507"
    },
    "minResponseTime": {
        "total": "192",
        "ok": "192",
        "ko": "356"
    },
    "maxResponseTime": {
        "total": "60116",
        "ok": "59076",
        "ko": "60116"
    },
    "meanResponseTime": {
        "total": "9447",
        "ok": "8427",
        "ko": "11453"
    },
    "standardDeviation": {
        "total": "8179",
        "ok": "8174",
        "ko": "7808"
    },
    "percentiles1": {
        "total": "8703",
        "ok": "6026",
        "ko": "10215"
    },
    "percentiles2": {
        "total": "10666",
        "ok": "10278",
        "ko": "10765"
    },
    "percentiles3": {
        "total": "21560",
        "ok": "23907",
        "ko": "14806"
    },
    "percentiles4": {
        "total": "52224",
        "ok": "42136",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 14,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 971,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 507,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.893",
        "ko": "1.47"
    }
}
    },"req_request-36-9ad97": {
        type: "REQUEST",
        name: "request_36",
path: "request_36",
pathFormatted: "req_request-36-9ad97",
stats: {
    "name": "request_36",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1011",
        "ko": "494"
    },
    "minResponseTime": {
        "total": "57",
        "ok": "57",
        "ko": "392"
    },
    "maxResponseTime": {
        "total": "60115",
        "ok": "59560",
        "ko": "60115"
    },
    "meanResponseTime": {
        "total": "8838",
        "ok": "7950",
        "ko": "10656"
    },
    "standardDeviation": {
        "total": "7745",
        "ok": "8718",
        "ko": "4719"
    },
    "percentiles1": {
        "total": "8584",
        "ok": "4998",
        "ko": "10239"
    },
    "percentiles2": {
        "total": "10456",
        "ok": "9473",
        "ko": "10571"
    },
    "percentiles3": {
        "total": "20939",
        "ok": "25989",
        "ko": "12943"
    },
    "percentiles4": {
        "total": "44065",
        "ok": "48302",
        "ko": "15978"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 22,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 16,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 973,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 494,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.93",
        "ko": "1.432"
    }
}
    },"req_favicon-48x48-p-1e880": {
        type: "REQUEST",
        name: "favicon-48x48.png",
path: "favicon-48x48.png",
pathFormatted: "req_favicon-48x48-p-1e880",
stats: {
    "name": "favicon-48x48.png",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1020",
        "ko": "485"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "362"
    },
    "maxResponseTime": {
        "total": "60182",
        "ok": "55946",
        "ko": "60182"
    },
    "meanResponseTime": {
        "total": "7565",
        "ok": "6086",
        "ko": "10676"
    },
    "standardDeviation": {
        "total": "5828",
        "ok": "6086",
        "ko": "3638"
    },
    "percentiles1": {
        "total": "7627",
        "ok": "3980",
        "ko": "10279"
    },
    "percentiles2": {
        "total": "10319",
        "ok": "7979",
        "ko": "10727"
    },
    "percentiles3": {
        "total": "15681",
        "ok": "16805",
        "ko": "13936"
    },
    "percentiles4": {
        "total": "30433",
        "ok": "32209",
        "ko": "17966"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 36,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 19,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 965,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 485,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.957",
        "ko": "1.406"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "1505",
        "ok": "992",
        "ko": "513"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "401"
    },
    "maxResponseTime": {
        "total": "20568",
        "ok": "20568",
        "ko": "17389"
    },
    "meanResponseTime": {
        "total": "6281",
        "ok": "4202",
        "ko": "10300"
    },
    "standardDeviation": {
        "total": "3998",
        "ok": "3184",
        "ko": "1664"
    },
    "percentiles1": {
        "total": "5898",
        "ok": "3122",
        "ko": "10216"
    },
    "percentiles2": {
        "total": "10043",
        "ok": "5829",
        "ko": "10713"
    },
    "percentiles3": {
        "total": "11626",
        "ok": "10101",
        "ko": "13077"
    },
    "percentiles4": {
        "total": "15217",
        "ok": "14582",
        "ko": "15540"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 33,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 910,
    "percentage": 60
},
    "group4": {
    "name": "failed",
    "count": 513,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.875",
        "ko": "1.487"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1003",
        "ko": "502"
    },
    "minResponseTime": {
        "total": "284",
        "ok": "284",
        "ko": "355"
    },
    "maxResponseTime": {
        "total": "60182",
        "ok": "59324",
        "ko": "60182"
    },
    "meanResponseTime": {
        "total": "9797",
        "ok": "9033",
        "ko": "11325"
    },
    "standardDeviation": {
        "total": "8832",
        "ok": "9342",
        "ko": "7484"
    },
    "percentiles1": {
        "total": "8769",
        "ok": "5907",
        "ko": "10215"
    },
    "percentiles2": {
        "total": "10712",
        "ok": "10916",
        "ko": "10654"
    },
    "percentiles3": {
        "total": "25805",
        "ok": "29211",
        "ko": "14517"
    },
    "percentiles4": {
        "total": "55561",
        "ok": "45410",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 14,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 981,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 502,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.907",
        "ko": "1.455"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "1505",
        "ok": "998",
        "ko": "507"
    },
    "minResponseTime": {
        "total": "55",
        "ok": "55",
        "ko": "268"
    },
    "maxResponseTime": {
        "total": "60041",
        "ok": "59796",
        "ko": "60041"
    },
    "meanResponseTime": {
        "total": "8729",
        "ok": "7642",
        "ko": "10868"
    },
    "standardDeviation": {
        "total": "7438",
        "ok": "7991",
        "ko": "5625"
    },
    "percentiles1": {
        "total": "8574",
        "ok": "4988",
        "ko": "10214"
    },
    "percentiles2": {
        "total": "10425",
        "ok": "9459",
        "ko": "10609"
    },
    "percentiles3": {
        "total": "19212",
        "ok": "23647",
        "ko": "13622"
    },
    "percentiles4": {
        "total": "43916",
        "ok": "43217",
        "ko": "57493"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 25,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 15,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 958,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 507,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.893",
        "ko": "1.47"
    }
}
    },"req_request-20-taxo-0d791": {
        type: "REQUEST",
        name: "request_20_TaxonMedia",
path: "request_20_TaxonMedia",
pathFormatted: "req_request-20-taxo-0d791",
stats: {
    "name": "request_20_TaxonMedia",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1003",
        "ko": "502"
    },
    "minResponseTime": {
        "total": "252",
        "ok": "311",
        "ko": "252"
    },
    "maxResponseTime": {
        "total": "60353",
        "ok": "57028",
        "ko": "60353"
    },
    "meanResponseTime": {
        "total": "8261",
        "ok": "7008",
        "ko": "10764"
    },
    "standardDeviation": {
        "total": "6914",
        "ok": "7305",
        "ko": "5224"
    },
    "percentiles1": {
        "total": "8480",
        "ok": "4799",
        "ko": "10214"
    },
    "percentiles2": {
        "total": "10340",
        "ok": "8707",
        "ko": "10623"
    },
    "percentiles3": {
        "total": "16254",
        "ok": "19611",
        "ko": "13341"
    },
    "percentiles4": {
        "total": "43114",
        "ok": "43205",
        "ko": "18282"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 22,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 976,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 502,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.907",
        "ko": "1.455"
    }
}
    },"req_favicon-32x32-p-14e88": {
        type: "REQUEST",
        name: "favicon-32x32.png",
path: "favicon-32x32.png",
pathFormatted: "req_favicon-32x32-p-14e88",
stats: {
    "name": "favicon-32x32.png",
    "numberOfRequests": {
        "total": "1505",
        "ok": "990",
        "ko": "515"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "277"
    },
    "maxResponseTime": {
        "total": "60043",
        "ok": "59595",
        "ko": "60043"
    },
    "meanResponseTime": {
        "total": "7738",
        "ok": "6166",
        "ko": "10760"
    },
    "standardDeviation": {
        "total": "6310",
        "ok": "6819",
        "ko": "3616"
    },
    "percentiles1": {
        "total": "7756",
        "ok": "4073",
        "ko": "10258"
    },
    "percentiles2": {
        "total": "10291",
        "ok": "7646",
        "ko": "10794"
    },
    "percentiles3": {
        "total": "16477",
        "ok": "18148",
        "ko": "14884"
    },
    "percentiles4": {
        "total": "33227",
        "ok": "34524",
        "ko": "17828"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 38,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 26,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 926,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 515,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.87",
        "ko": "1.493"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1029",
        "ko": "476"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "357"
    },
    "maxResponseTime": {
        "total": "18987",
        "ok": "18987",
        "ko": "18226"
    },
    "meanResponseTime": {
        "total": "6220",
        "ok": "4340",
        "ko": "10285"
    },
    "standardDeviation": {
        "total": "3894",
        "ok": "3072",
        "ko": "1834"
    },
    "percentiles1": {
        "total": "5666",
        "ok": "3392",
        "ko": "10209"
    },
    "percentiles2": {
        "total": "10008",
        "ok": "5981",
        "ko": "10592"
    },
    "percentiles3": {
        "total": "11553",
        "ok": "10238",
        "ko": "13341"
    },
    "percentiles4": {
        "total": "15295",
        "ok": "13788",
        "ko": "16849"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 41,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 31,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 957,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 476,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.983",
        "ko": "1.38"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "1505",
        "ok": "985",
        "ko": "520"
    },
    "minResponseTime": {
        "total": "44",
        "ok": "44",
        "ko": "399"
    },
    "maxResponseTime": {
        "total": "60124",
        "ok": "57214",
        "ko": "60124"
    },
    "meanResponseTime": {
        "total": "8932",
        "ok": "7769",
        "ko": "11135"
    },
    "standardDeviation": {
        "total": "7862",
        "ok": "8321",
        "ko": "6351"
    },
    "percentiles1": {
        "total": "8547",
        "ok": "5077",
        "ko": "10245"
    },
    "percentiles2": {
        "total": "10459",
        "ok": "9121",
        "ko": "10803"
    },
    "percentiles3": {
        "total": "21914",
        "ok": "25348",
        "ko": "14412"
    },
    "percentiles4": {
        "total": "46349",
        "ok": "43019",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 25,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 11,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 949,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 520,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.855",
        "ko": "1.507"
    }
}
    },"req_request-6-font-15e7e": {
        type: "REQUEST",
        name: "request_6_font",
path: "request_6_font",
pathFormatted: "req_request-6-font-15e7e",
stats: {
    "name": "request_6_font",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1012",
        "ko": "493"
    },
    "minResponseTime": {
        "total": "106",
        "ok": "106",
        "ko": "366"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "59072",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "8602",
        "ok": "7509",
        "ko": "10847"
    },
    "standardDeviation": {
        "total": "7603",
        "ok": "8290",
        "ko": "5279"
    },
    "percentiles1": {
        "total": "8507",
        "ok": "4844",
        "ko": "10245"
    },
    "percentiles2": {
        "total": "10418",
        "ok": "9081",
        "ko": "10694"
    },
    "percentiles3": {
        "total": "19749",
        "ok": "23924",
        "ko": "13933"
    },
    "percentiles4": {
        "total": "45247",
        "ok": "45186",
        "ko": "21675"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 25,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 18,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 969,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 493,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.933",
        "ko": "1.429"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1008",
        "ko": "497"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "415"
    },
    "maxResponseTime": {
        "total": "60149",
        "ok": "55755",
        "ko": "60149"
    },
    "meanResponseTime": {
        "total": "8954",
        "ok": "8022",
        "ko": "10845"
    },
    "standardDeviation": {
        "total": "7048",
        "ok": "7615",
        "ko": "5243"
    },
    "percentiles1": {
        "total": "8581",
        "ok": "5729",
        "ko": "10232"
    },
    "percentiles2": {
        "total": "10528",
        "ok": "9766",
        "ko": "10663"
    },
    "percentiles3": {
        "total": "20985",
        "ok": "24946",
        "ko": "14396"
    },
    "percentiles4": {
        "total": "36693",
        "ok": "36677",
        "ko": "20765"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 995,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 497,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.922",
        "ko": "1.441"
    }
}
    },"req_coast-228x228-p-35307": {
        type: "REQUEST",
        name: "coast-228x228.png",
path: "coast-228x228.png",
pathFormatted: "req_coast-228x228-p-35307",
stats: {
    "name": "coast-228x228.png",
    "numberOfRequests": {
        "total": "1505",
        "ok": "986",
        "ko": "519"
    },
    "minResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "277"
    },
    "maxResponseTime": {
        "total": "60026",
        "ok": "59632",
        "ko": "60026"
    },
    "meanResponseTime": {
        "total": "8415",
        "ok": "7123",
        "ko": "10869"
    },
    "standardDeviation": {
        "total": "7049",
        "ok": "7690",
        "ko": "4747"
    },
    "percentiles1": {
        "total": "8491",
        "ok": "4718",
        "ko": "10238"
    },
    "percentiles2": {
        "total": "10380",
        "ok": "8640",
        "ko": "10762"
    },
    "percentiles3": {
        "total": "18064",
        "ok": "21396",
        "ko": "15045"
    },
    "percentiles4": {
        "total": "39729",
        "ok": "40533",
        "ko": "18038"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 29,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 25,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 932,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 519,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.858",
        "ko": "1.504"
    }
}
    },"req_favicon-ico-8af3a": {
        type: "REQUEST",
        name: "favicon.ico",
path: "favicon.ico",
pathFormatted: "req_favicon-ico-8af3a",
stats: {
    "name": "favicon.ico",
    "numberOfRequests": {
        "total": "1505",
        "ok": "991",
        "ko": "514"
    },
    "minResponseTime": {
        "total": "62",
        "ok": "62",
        "ko": "265"
    },
    "maxResponseTime": {
        "total": "60608",
        "ok": "59927",
        "ko": "60608"
    },
    "meanResponseTime": {
        "total": "9085",
        "ok": "7813",
        "ko": "11538"
    },
    "standardDeviation": {
        "total": "8045",
        "ok": "8054",
        "ko": "7438"
    },
    "percentiles1": {
        "total": "8631",
        "ok": "5327",
        "ko": "10238"
    },
    "percentiles2": {
        "total": "10547",
        "ok": "9555",
        "ko": "10809"
    },
    "percentiles3": {
        "total": "19982",
        "ok": "23832",
        "ko": "16388"
    },
    "percentiles4": {
        "total": "51506",
        "ok": "42124",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 22,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 952,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 514,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.872",
        "ko": "1.49"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "1505",
        "ok": "995",
        "ko": "510"
    },
    "minResponseTime": {
        "total": "96",
        "ok": "96",
        "ko": "252"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "58175",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "8572",
        "ok": "7388",
        "ko": "10884"
    },
    "standardDeviation": {
        "total": "7248",
        "ok": "7693",
        "ko": "5609"
    },
    "percentiles1": {
        "total": "8533",
        "ok": "4976",
        "ko": "10234"
    },
    "percentiles2": {
        "total": "10408",
        "ok": "8946",
        "ko": "10657"
    },
    "percentiles3": {
        "total": "18704",
        "ok": "22735",
        "ko": "13622"
    },
    "percentiles4": {
        "total": "42813",
        "ok": "42577",
        "ko": "56243"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 22,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 12,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 961,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 510,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.884",
        "ko": "1.478"
    }
}
    },"req_graphql-news-4c309": {
        type: "REQUEST",
        name: "graphql_News",
path: "graphql_News",
pathFormatted: "req_graphql-news-4c309",
stats: {
    "name": "graphql_News",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1031",
        "ko": "474"
    },
    "minResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "391"
    },
    "maxResponseTime": {
        "total": "23116",
        "ok": "23116",
        "ko": "17563"
    },
    "meanResponseTime": {
        "total": "6076",
        "ok": "4097",
        "ko": "10379"
    },
    "standardDeviation": {
        "total": "3951",
        "ok": "3032",
        "ko": "1588"
    },
    "percentiles1": {
        "total": "5212",
        "ok": "3095",
        "ko": "10214"
    },
    "percentiles2": {
        "total": "10007",
        "ok": "5503",
        "ko": "10587"
    },
    "percentiles3": {
        "total": "11575",
        "ok": "10024",
        "ko": "13465"
    },
    "percentiles4": {
        "total": "15066",
        "ok": "12417",
        "ko": "16991"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 45,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 49,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 937,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 474,
    "percentage": 31
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.988",
        "ko": "1.374"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "1505",
        "ok": "987",
        "ko": "518"
    },
    "minResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "357"
    },
    "maxResponseTime": {
        "total": "60353",
        "ok": "58671",
        "ko": "60353"
    },
    "meanResponseTime": {
        "total": "8488",
        "ok": "7349",
        "ko": "10657"
    },
    "standardDeviation": {
        "total": "7299",
        "ok": "8133",
        "ko": "4645"
    },
    "percentiles1": {
        "total": "8548",
        "ok": "4765",
        "ko": "10228"
    },
    "percentiles2": {
        "total": "10410",
        "ok": "9076",
        "ko": "10591"
    },
    "percentiles3": {
        "total": "18276",
        "ok": "22489",
        "ko": "13117"
    },
    "percentiles4": {
        "total": "43274",
        "ok": "45400",
        "ko": "16835"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 35,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 19,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 933,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 518,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.861",
        "ko": "1.501"
    }
}
    },"req_request-26-taxo-41b6f": {
        type: "REQUEST",
        name: "request_26_TaxonMedia",
path: "request_26_TaxonMedia",
pathFormatted: "req_request-26-taxo-41b6f",
stats: {
    "name": "request_26_TaxonMedia",
    "numberOfRequests": {
        "total": "1505",
        "ok": "993",
        "ko": "512"
    },
    "minResponseTime": {
        "total": "355",
        "ok": "355",
        "ko": "393"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "59527",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "8636",
        "ok": "7626",
        "ko": "10596"
    },
    "standardDeviation": {
        "total": "7178",
        "ok": "8121",
        "ko": "4211"
    },
    "percentiles1": {
        "total": "8477",
        "ok": "5328",
        "ko": "10234"
    },
    "percentiles2": {
        "total": "10396",
        "ok": "8840",
        "ko": "10682"
    },
    "percentiles3": {
        "total": "17602",
        "ok": "23102",
        "ko": "13879"
    },
    "percentiles4": {
        "total": "41088",
        "ok": "42295",
        "ko": "17145"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 14,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 975,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 512,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.878",
        "ko": "1.484"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "1505",
        "ok": "982",
        "ko": "523"
    },
    "minResponseTime": {
        "total": "252",
        "ok": "497",
        "ko": "252"
    },
    "maxResponseTime": {
        "total": "60325",
        "ok": "59408",
        "ko": "60325"
    },
    "meanResponseTime": {
        "total": "9662",
        "ok": "8908",
        "ko": "11079"
    },
    "standardDeviation": {
        "total": "7638",
        "ok": "8150",
        "ko": "6331"
    },
    "percentiles1": {
        "total": "9157",
        "ok": "6486",
        "ko": "10219"
    },
    "percentiles2": {
        "total": "10637",
        "ok": "10784",
        "ko": "10608"
    },
    "percentiles3": {
        "total": "21791",
        "ok": "25570",
        "ko": "13953"
    },
    "percentiles4": {
        "total": "45198",
        "ok": "42024",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 9,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 968,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 523,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.846",
        "ko": "1.516"
    }
}
    },"req_main-e4fad40d75-406e2": {
        type: "REQUEST",
        name: "main.e4fad40d75305c1c3562.js",
path: "main.e4fad40d75305c1c3562.js",
pathFormatted: "req_main-e4fad40d75-406e2",
stats: {
    "name": "main.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "1505",
        "ok": "991",
        "ko": "514"
    },
    "minResponseTime": {
        "total": "181",
        "ok": "181",
        "ko": "277"
    },
    "maxResponseTime": {
        "total": "60246",
        "ok": "54748",
        "ko": "60246"
    },
    "meanResponseTime": {
        "total": "9852",
        "ok": "8926",
        "ko": "11636"
    },
    "standardDeviation": {
        "total": "7884",
        "ok": "7979",
        "ko": "7377"
    },
    "percentiles1": {
        "total": "9043",
        "ok": "6370",
        "ko": "10274"
    },
    "percentiles2": {
        "total": "10814",
        "ok": "10720",
        "ko": "10834"
    },
    "percentiles3": {
        "total": "22751",
        "ok": "25187",
        "ko": "16450"
    },
    "percentiles4": {
        "total": "48996",
        "ok": "40770",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 982,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 514,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.872",
        "ko": "1.49"
    }
}
    },"req_request-21-taxo-4adce": {
        type: "REQUEST",
        name: "request_21_TaxonMedia",
path: "request_21_TaxonMedia",
pathFormatted: "req_request-21-taxo-4adce",
stats: {
    "name": "request_21_TaxonMedia",
    "numberOfRequests": {
        "total": "1505",
        "ok": "984",
        "ko": "521"
    },
    "minResponseTime": {
        "total": "383",
        "ok": "383",
        "ko": "7876"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "59330",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "8404",
        "ok": "7156",
        "ko": "10759"
    },
    "standardDeviation": {
        "total": "6797",
        "ok": "7589",
        "ko": "4027"
    },
    "percentiles1": {
        "total": "8501",
        "ok": "4913",
        "ko": "10252"
    },
    "percentiles2": {
        "total": "10406",
        "ok": "8481",
        "ko": "10755"
    },
    "percentiles3": {
        "total": "17011",
        "ok": "20918",
        "ko": "13629"
    },
    "percentiles4": {
        "total": "36476",
        "ok": "38612",
        "ko": "17106"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 11,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 970,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 521,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.852",
        "ko": "1.51"
    }
}
    },"req_app-css-13ea8": {
        type: "REQUEST",
        name: "App.css",
path: "App.css",
pathFormatted: "req_app-css-13ea8",
stats: {
    "name": "App.css",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1015",
        "ko": "490"
    },
    "minResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "277"
    },
    "maxResponseTime": {
        "total": "60312",
        "ok": "59310",
        "ko": "60312"
    },
    "meanResponseTime": {
        "total": "8442",
        "ok": "7139",
        "ko": "11141"
    },
    "standardDeviation": {
        "total": "7548",
        "ok": "7940",
        "ko": "5797"
    },
    "percentiles1": {
        "total": "8149",
        "ok": "4540",
        "ko": "10267"
    },
    "percentiles2": {
        "total": "10382",
        "ok": "8390",
        "ko": "10818"
    },
    "percentiles3": {
        "total": "19583",
        "ok": "22537",
        "ko": "15176"
    },
    "percentiles4": {
        "total": "43487",
        "ok": "43045",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 29,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 966,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 490,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.942",
        "ko": "1.42"
    }
}
    },"req_graphql-weather-e1b31": {
        type: "REQUEST",
        name: "graphql_weather",
path: "graphql_weather",
pathFormatted: "req_graphql-weather-e1b31",
stats: {
    "name": "graphql_weather",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1004",
        "ko": "501"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "265"
    },
    "maxResponseTime": {
        "total": "60020",
        "ok": "59190",
        "ko": "60020"
    },
    "meanResponseTime": {
        "total": "7266",
        "ok": "5667",
        "ko": "10471"
    },
    "standardDeviation": {
        "total": "5990",
        "ok": "6494",
        "ko": "2806"
    },
    "percentiles1": {
        "total": "7105",
        "ok": "3701",
        "ko": "10250"
    },
    "percentiles2": {
        "total": "10229",
        "ok": "7170",
        "ko": "10703"
    },
    "percentiles3": {
        "total": "13633",
        "ok": "13892",
        "ko": "13171"
    },
    "percentiles4": {
        "total": "30863",
        "ok": "35669",
        "ko": "17002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 34,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 31,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 939,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 501,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.91",
        "ko": "1.452"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1017",
        "ko": "488"
    },
    "minResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "417"
    },
    "maxResponseTime": {
        "total": "60386",
        "ok": "58947",
        "ko": "60386"
    },
    "meanResponseTime": {
        "total": "7893",
        "ok": "6620",
        "ko": "10546"
    },
    "standardDeviation": {
        "total": "6667",
        "ok": "7395",
        "ko": "3567"
    },
    "percentiles1": {
        "total": "7813",
        "ok": "4181",
        "ko": "10204"
    },
    "percentiles2": {
        "total": "10344",
        "ok": "8006",
        "ko": "10659"
    },
    "percentiles3": {
        "total": "16714",
        "ok": "20073",
        "ko": "13821"
    },
    "percentiles4": {
        "total": "33842",
        "ok": "41080",
        "ko": "16963"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 29,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 22,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 966,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 488,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.948",
        "ko": "1.414"
    }
}
    },"req_graphql-news-d7bbe": {
        type: "REQUEST",
        name: "graphQL_news",
path: "graphQL_news",
pathFormatted: "req_graphql-news-d7bbe",
stats: {
    "name": "graphQL_news",
    "numberOfRequests": {
        "total": "1505",
        "ok": "1027",
        "ko": "478"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "354"
    },
    "maxResponseTime": {
        "total": "21225",
        "ok": "21225",
        "ko": "18266"
    },
    "meanResponseTime": {
        "total": "6244",
        "ok": "4349",
        "ko": "10314"
    },
    "standardDeviation": {
        "total": "3899",
        "ok": "3090",
        "ko": "1753"
    },
    "percentiles1": {
        "total": "5718",
        "ok": "3331",
        "ko": "10229"
    },
    "percentiles2": {
        "total": "10013",
        "ok": "6052",
        "ko": "10607"
    },
    "percentiles3": {
        "total": "11532",
        "ok": "10178",
        "ko": "13565"
    },
    "percentiles4": {
        "total": "14855",
        "ok": "13673",
        "ko": "15577"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 42,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 35,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 950,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 478,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.977",
        "ko": "1.386"
    }
}
    },"req_request-23-taxo-3ea91": {
        type: "REQUEST",
        name: "request_23_TaxonMedia",
path: "request_23_TaxonMedia",
pathFormatted: "req_request-23-taxo-3ea91",
stats: {
    "name": "request_23_TaxonMedia",
    "numberOfRequests": {
        "total": "1505",
        "ok": "995",
        "ko": "510"
    },
    "minResponseTime": {
        "total": "316",
        "ok": "316",
        "ko": "414"
    },
    "maxResponseTime": {
        "total": "60183",
        "ok": "58714",
        "ko": "60183"
    },
    "meanResponseTime": {
        "total": "8378",
        "ok": "7058",
        "ko": "10953"
    },
    "standardDeviation": {
        "total": "7247",
        "ok": "7635",
        "ko": "5590"
    },
    "percentiles1": {
        "total": "8450",
        "ok": "4524",
        "ko": "10243"
    },
    "percentiles2": {
        "total": "10345",
        "ok": "8499",
        "ko": "10666"
    },
    "percentiles3": {
        "total": "17079",
        "ok": "23004",
        "ko": "14394"
    },
    "percentiles4": {
        "total": "40545",
        "ok": "40499",
        "ko": "56147"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 14,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 976,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 510,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.362",
        "ok": "2.884",
        "ko": "1.478"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
