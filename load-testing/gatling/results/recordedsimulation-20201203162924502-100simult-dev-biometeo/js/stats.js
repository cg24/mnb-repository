var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4700",
        "ok": "3161",
        "ko": "1539"
    },
    "minResponseTime": {
        "total": "51",
        "ok": "51",
        "ko": "1082"
    },
    "maxResponseTime": {
        "total": "50143",
        "ok": "50143",
        "ko": "12011"
    },
    "meanResponseTime": {
        "total": "10584",
        "ok": "10962",
        "ko": "9808"
    },
    "standardDeviation": {
        "total": "5507",
        "ok": "6663",
        "ko": "736"
    },
    "percentiles1": {
        "total": "9708",
        "ok": "10381",
        "ko": "9569"
    },
    "percentiles2": {
        "total": "11708",
        "ok": "13072",
        "ko": "10025"
    },
    "percentiles3": {
        "total": "20456",
        "ok": "21951",
        "ko": "11138"
    },
    "percentiles4": {
        "total": "33764",
        "ok": "35291",
        "ko": "11825"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 77,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 41,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3043,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 1539,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "90.385",
        "ok": "60.788",
        "ko": "29.596"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "512",
        "ok": "512",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2112",
        "ok": "2112",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1446",
        "ok": "1446",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "381",
        "ok": "381",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1481",
        "ok": "1481",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1767",
        "ok": "1767",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1951",
        "ok": "1951",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2041",
        "ok": "2041",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 9,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 17
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 74,
    "percentage": 74
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.923",
        "ko": "-"
    }
}
    },"req_request-29-taxo-1f5dd": {
        type: "REQUEST",
        name: "request_29_TaxonMedia",
path: "request_29_TaxonMedia",
pathFormatted: "req_request-29-taxo-1f5dd",
stats: {
    "name": "request_29_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "62",
        "ko": "38"
    },
    "minResponseTime": {
        "total": "3666",
        "ok": "3666",
        "ko": "9114"
    },
    "maxResponseTime": {
        "total": "34343",
        "ok": "34343",
        "ko": "11316"
    },
    "meanResponseTime": {
        "total": "11003",
        "ok": "11736",
        "ko": "9805"
    },
    "standardDeviation": {
        "total": "5716",
        "ok": "7140",
        "ko": "694"
    },
    "percentiles1": {
        "total": "9596",
        "ok": "9877",
        "ko": "9526"
    },
    "percentiles2": {
        "total": "11295",
        "ok": "12550",
        "ko": "10603"
    },
    "percentiles3": {
        "total": "21320",
        "ok": "32677",
        "ko": "10969"
    },
    "percentiles4": {
        "total": "33770",
        "ok": "33990",
        "ko": "11235"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 38,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.192",
        "ko": "0.731"
    }
}
    },"req_request-30-taxo-7acf0": {
        type: "REQUEST",
        name: "request_30_TaxonMedia",
path: "request_30_TaxonMedia",
pathFormatted: "req_request-30-taxo-7acf0",
stats: {
    "name": "request_30_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "2551",
        "ok": "2551",
        "ko": "9118"
    },
    "maxResponseTime": {
        "total": "35726",
        "ok": "35726",
        "ko": "11251"
    },
    "meanResponseTime": {
        "total": "12037",
        "ok": "13069",
        "ko": "9844"
    },
    "standardDeviation": {
        "total": "5845",
        "ok": "6833",
        "ko": "694"
    },
    "percentiles1": {
        "total": "10482",
        "ok": "11180",
        "ko": "9546"
    },
    "percentiles2": {
        "total": "12545",
        "ok": "15155",
        "ko": "10685"
    },
    "percentiles3": {
        "total": "25903",
        "ok": "26286",
        "ko": "10962"
    },
    "percentiles4": {
        "total": "34891",
        "ok": "35161",
        "ko": "11163"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 68,
    "percentage": 68
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.308",
        "ko": "0.615"
    }
}
    },"req_request-26-taxo-41b6f": {
        type: "REQUEST",
        name: "request_26_TaxonMedia",
path: "request_26_TaxonMedia",
pathFormatted: "req_request-26-taxo-41b6f",
stats: {
    "name": "request_26_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "3110",
        "ok": "3110",
        "ko": "9108"
    },
    "maxResponseTime": {
        "total": "31015",
        "ok": "31015",
        "ko": "11133"
    },
    "meanResponseTime": {
        "total": "11103",
        "ok": "11738",
        "ko": "9813"
    },
    "standardDeviation": {
        "total": "5003",
        "ok": "5993",
        "ko": "661"
    },
    "percentiles1": {
        "total": "9865",
        "ok": "11104",
        "ko": "9564"
    },
    "percentiles2": {
        "total": "12957",
        "ok": "13753",
        "ko": "9923"
    },
    "percentiles3": {
        "total": "21925",
        "ok": "23113",
        "ko": "11007"
    },
    "percentiles4": {
        "total": "29881",
        "ok": "30259",
        "ko": "11112"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.288",
        "ko": "0.635"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "184",
        "ok": "184",
        "ko": "9099"
    },
    "maxResponseTime": {
        "total": "50143",
        "ok": "50143",
        "ko": "11172"
    },
    "meanResponseTime": {
        "total": "10821",
        "ok": "11370",
        "ko": "9705"
    },
    "standardDeviation": {
        "total": "8537",
        "ok": "10375",
        "ko": "679"
    },
    "percentiles1": {
        "total": "9372",
        "ok": "9249",
        "ko": "9413"
    },
    "percentiles2": {
        "total": "10958",
        "ok": "11792",
        "ko": "9899"
    },
    "percentiles3": {
        "total": "33278",
        "ok": "39032",
        "ko": "11031"
    },
    "percentiles4": {
        "total": "46680",
        "ok": "47834",
        "ko": "11137"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.288",
        "ko": "0.635"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "9036"
    },
    "maxResponseTime": {
        "total": "34516",
        "ok": "34516",
        "ko": "11066"
    },
    "meanResponseTime": {
        "total": "9884",
        "ok": "9987",
        "ko": "9666"
    },
    "standardDeviation": {
        "total": "5451",
        "ok": "6594",
        "ko": "612"
    },
    "percentiles1": {
        "total": "9517",
        "ok": "10469",
        "ko": "9486"
    },
    "percentiles2": {
        "total": "12054",
        "ok": "13042",
        "ko": "9651"
    },
    "percentiles3": {
        "total": "18883",
        "ok": "21170",
        "ko": "10944"
    },
    "percentiles4": {
        "total": "26231",
        "ok": "28909",
        "ko": "11029"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 5
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 59,
    "percentage": 59
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.308",
        "ko": "0.615"
    }
}
    },"req_request-36-9ad97": {
        type: "REQUEST",
        name: "request_36",
path: "request_36",
pathFormatted: "req_request-36-9ad97",
stats: {
    "name": "request_36",
    "numberOfRequests": {
        "total": "100",
        "ok": "62",
        "ko": "38"
    },
    "minResponseTime": {
        "total": "330",
        "ok": "330",
        "ko": "9096"
    },
    "maxResponseTime": {
        "total": "29601",
        "ok": "29601",
        "ko": "11141"
    },
    "meanResponseTime": {
        "total": "10327",
        "ok": "10717",
        "ko": "9691"
    },
    "standardDeviation": {
        "total": "4650",
        "ok": "5852",
        "ko": "611"
    },
    "percentiles1": {
        "total": "9614",
        "ok": "10106",
        "ko": "9498"
    },
    "percentiles2": {
        "total": "11150",
        "ok": "12770",
        "ko": "9823"
    },
    "percentiles3": {
        "total": "20537",
        "ok": "21019",
        "ko": "10935"
    },
    "percentiles4": {
        "total": "22028",
        "ok": "24935",
        "ko": "11069"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 58,
    "percentage": 58
},
    "group4": {
    "name": "failed",
    "count": 38,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.192",
        "ko": "0.731"
    }
}
    },"req_favicon-48x48-p-1e880": {
        type: "REQUEST",
        name: "favicon-48x48.png",
path: "favicon-48x48.png",
pathFormatted: "req_favicon-48x48-p-1e880",
stats: {
    "name": "favicon-48x48.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "653",
        "ok": "653",
        "ko": "9238"
    },
    "maxResponseTime": {
        "total": "44668",
        "ok": "44668",
        "ko": "11994"
    },
    "meanResponseTime": {
        "total": "14075",
        "ok": "16218",
        "ko": "10095"
    },
    "standardDeviation": {
        "total": "9131",
        "ok": "10712",
        "ko": "865"
    },
    "percentiles1": {
        "total": "11059",
        "ok": "11707",
        "ko": "9672"
    },
    "percentiles2": {
        "total": "12135",
        "ok": "18913",
        "ko": "10499"
    },
    "percentiles3": {
        "total": "36508",
        "ok": "38776",
        "ko": "11817"
    },
    "percentiles4": {
        "total": "43128",
        "ok": "43672",
        "ko": "11937"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 63,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.25",
        "ko": "0.673"
    }
}
    },"req_request-24-taxo-b5f25": {
        type: "REQUEST",
        name: "request_24_TaxonMedia",
path: "request_24_TaxonMedia",
pathFormatted: "req_request-24-taxo-b5f25",
stats: {
    "name": "request_24_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "5074",
        "ok": "5074",
        "ko": "9103"
    },
    "maxResponseTime": {
        "total": "26664",
        "ok": "26664",
        "ko": "11138"
    },
    "meanResponseTime": {
        "total": "11874",
        "ok": "12926",
        "ko": "9738"
    },
    "standardDeviation": {
        "total": "4421",
        "ok": "5059",
        "ko": "675"
    },
    "percentiles1": {
        "total": "10643",
        "ok": "11445",
        "ko": "9529"
    },
    "percentiles2": {
        "total": "13866",
        "ok": "15843",
        "ko": "9829"
    },
    "percentiles3": {
        "total": "20151",
        "ok": "21722",
        "ko": "11068"
    },
    "percentiles4": {
        "total": "26258",
        "ok": "26393",
        "ko": "11117"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.288",
        "ko": "0.635"
    }
}
    },"req_favicon-32x32-p-14e88": {
        type: "REQUEST",
        name: "favicon-32x32.png",
path: "favicon-32x32.png",
pathFormatted: "req_favicon-32x32-p-14e88",
stats: {
    "name": "favicon-32x32.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "61",
        "ko": "39"
    },
    "minResponseTime": {
        "total": "370",
        "ok": "370",
        "ko": "9251"
    },
    "maxResponseTime": {
        "total": "47696",
        "ok": "47696",
        "ko": "12007"
    },
    "meanResponseTime": {
        "total": "12958",
        "ok": "14822",
        "ko": "10043"
    },
    "standardDeviation": {
        "total": "8888",
        "ok": "10963",
        "ko": "779"
    },
    "percentiles1": {
        "total": "10865",
        "ok": "11513",
        "ko": "9748"
    },
    "percentiles2": {
        "total": "11820",
        "ok": "14267",
        "ko": "10096"
    },
    "percentiles3": {
        "total": "39040",
        "ok": "43101",
        "ko": "11809"
    },
    "percentiles4": {
        "total": "47071",
        "ok": "47317",
        "ko": "11945"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 58,
    "percentage": 58
},
    "group4": {
    "name": "failed",
    "count": 39,
    "percentage": 39
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.173",
        "ko": "0.75"
    }
}
    },"req_request-28-taxo-f261b": {
        type: "REQUEST",
        name: "request_28_TaxonMedia",
path: "request_28_TaxonMedia",
pathFormatted: "req_request-28-taxo-f261b",
stats: {
    "name": "request_28_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "71",
        "ko": "29"
    },
    "minResponseTime": {
        "total": "3900",
        "ok": "3900",
        "ko": "9118"
    },
    "maxResponseTime": {
        "total": "38182",
        "ok": "38182",
        "ko": "11009"
    },
    "meanResponseTime": {
        "total": "11743",
        "ok": "12556",
        "ko": "9752"
    },
    "standardDeviation": {
        "total": "4815",
        "ok": "5497",
        "ko": "627"
    },
    "percentiles1": {
        "total": "10389",
        "ok": "10945",
        "ko": "9457"
    },
    "percentiles2": {
        "total": "13406",
        "ok": "15041",
        "ko": "9887"
    },
    "percentiles3": {
        "total": "20221",
        "ok": "20461",
        "ko": "10891"
    },
    "percentiles4": {
        "total": "25675",
        "ok": "29339",
        "ko": "10984"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 71,
    "percentage": 71
},
    "group4": {
    "name": "failed",
    "count": 29,
    "percentage": 29
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.365",
        "ko": "0.558"
    }
}
    },"req_favicon-ico-8af3a": {
        type: "REQUEST",
        name: "favicon.ico",
path: "favicon.ico",
pathFormatted: "req_favicon-ico-8af3a",
stats: {
    "name": "favicon.ico",
    "numberOfRequests": {
        "total": "100",
        "ok": "58",
        "ko": "42"
    },
    "minResponseTime": {
        "total": "593",
        "ok": "593",
        "ko": "9234"
    },
    "maxResponseTime": {
        "total": "27243",
        "ok": "27243",
        "ko": "12003"
    },
    "meanResponseTime": {
        "total": "12148",
        "ok": "13727",
        "ko": "9969"
    },
    "standardDeviation": {
        "total": "4459",
        "ok": "5289",
        "ko": "714"
    },
    "percentiles1": {
        "total": "10764",
        "ok": "12684",
        "ko": "9806"
    },
    "percentiles2": {
        "total": "13951",
        "ok": "16480",
        "ko": "10039"
    },
    "percentiles3": {
        "total": "21040",
        "ok": "23674",
        "ko": "11412"
    },
    "percentiles4": {
        "total": "26140",
        "ok": "26608",
        "ko": "11937"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 56,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 42,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.115",
        "ko": "0.808"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "289",
        "ok": "289",
        "ko": "9096"
    },
    "maxResponseTime": {
        "total": "21858",
        "ok": "21858",
        "ko": "11219"
    },
    "meanResponseTime": {
        "total": "9956",
        "ok": "10150",
        "ko": "9595"
    },
    "standardDeviation": {
        "total": "3887",
        "ok": "4793",
        "ko": "556"
    },
    "percentiles1": {
        "total": "9450",
        "ok": "9576",
        "ko": "9391"
    },
    "percentiles2": {
        "total": "11540",
        "ok": "12869",
        "ko": "9668"
    },
    "percentiles3": {
        "total": "17851",
        "ok": "20119",
        "ko": "10892"
    },
    "percentiles4": {
        "total": "20946",
        "ok": "21269",
        "ko": "11122"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 63,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.25",
        "ko": "0.673"
    }
}
    },"req_request-34-d9757": {
        type: "REQUEST",
        name: "request_34",
path: "request_34",
pathFormatted: "req_request-34-d9757",
stats: {
    "name": "request_34",
    "numberOfRequests": {
        "total": "100",
        "ok": "62",
        "ko": "38"
    },
    "minResponseTime": {
        "total": "51",
        "ok": "51",
        "ko": "9036"
    },
    "maxResponseTime": {
        "total": "29438",
        "ok": "29438",
        "ko": "11133"
    },
    "meanResponseTime": {
        "total": "9735",
        "ok": "9740",
        "ko": "9726"
    },
    "standardDeviation": {
        "total": "4040",
        "ok": "5104",
        "ko": "676"
    },
    "percentiles1": {
        "total": "9574",
        "ok": "9816",
        "ko": "9432"
    },
    "percentiles2": {
        "total": "10982",
        "ok": "12183",
        "ko": "9901"
    },
    "percentiles3": {
        "total": "13459",
        "ok": "20621",
        "ko": "10996"
    },
    "percentiles4": {
        "total": "22059",
        "ok": "24891",
        "ko": "11102"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 58,
    "percentage": 58
},
    "group4": {
    "name": "failed",
    "count": 38,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.192",
        "ko": "0.731"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "100",
        "ok": "71",
        "ko": "29"
    },
    "minResponseTime": {
        "total": "457",
        "ok": "457",
        "ko": "9105"
    },
    "maxResponseTime": {
        "total": "20962",
        "ok": "20962",
        "ko": "11120"
    },
    "meanResponseTime": {
        "total": "9712",
        "ok": "9750",
        "ko": "9620"
    },
    "standardDeviation": {
        "total": "3150",
        "ok": "3719",
        "ko": "583"
    },
    "percentiles1": {
        "total": "9500",
        "ok": "9673",
        "ko": "9406"
    },
    "percentiles2": {
        "total": "11098",
        "ok": "11472",
        "ko": "9651"
    },
    "percentiles3": {
        "total": "12350",
        "ok": "16541",
        "ko": "11004"
    },
    "percentiles4": {
        "total": "20857",
        "ok": "20888",
        "ko": "11098"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 68,
    "percentage": 68
},
    "group4": {
    "name": "failed",
    "count": 29,
    "percentage": 29
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.365",
        "ko": "0.558"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "881",
        "ok": "881",
        "ko": "9128"
    },
    "maxResponseTime": {
        "total": "26481",
        "ok": "26481",
        "ko": "11349"
    },
    "meanResponseTime": {
        "total": "11266",
        "ok": "11990",
        "ko": "9796"
    },
    "standardDeviation": {
        "total": "4620",
        "ok": "5479",
        "ko": "710"
    },
    "percentiles1": {
        "total": "10396",
        "ok": "11302",
        "ko": "9527"
    },
    "percentiles2": {
        "total": "12655",
        "ok": "14051",
        "ko": "10416"
    },
    "percentiles3": {
        "total": "20591",
        "ok": "21560",
        "ko": "11118"
    },
    "percentiles4": {
        "total": "25535",
        "ok": "25850",
        "ko": "11279"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.288",
        "ko": "0.635"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "251",
        "ok": "251",
        "ko": "9035"
    },
    "maxResponseTime": {
        "total": "26121",
        "ok": "26121",
        "ko": "11361"
    },
    "meanResponseTime": {
        "total": "9957",
        "ok": "10071",
        "ko": "9747"
    },
    "standardDeviation": {
        "total": "3903",
        "ok": "4809",
        "ko": "716"
    },
    "percentiles1": {
        "total": "9545",
        "ok": "9668",
        "ko": "9530"
    },
    "percentiles2": {
        "total": "11328",
        "ok": "12586",
        "ko": "9854"
    },
    "percentiles3": {
        "total": "16109",
        "ok": "18455",
        "ko": "11049"
    },
    "percentiles4": {
        "total": "22244",
        "ok": "23615",
        "ko": "11260"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.25",
        "ko": "0.673"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "100",
        "ok": "64",
        "ko": "36"
    },
    "minResponseTime": {
        "total": "322",
        "ok": "322",
        "ko": "9108"
    },
    "maxResponseTime": {
        "total": "36195",
        "ok": "36195",
        "ko": "11224"
    },
    "meanResponseTime": {
        "total": "10210",
        "ok": "10410",
        "ko": "9856"
    },
    "standardDeviation": {
        "total": "4856",
        "ok": "6037",
        "ko": "715"
    },
    "percentiles1": {
        "total": "9722",
        "ok": "10013",
        "ko": "9583"
    },
    "percentiles2": {
        "total": "11299",
        "ok": "12490",
        "ko": "10679"
    },
    "percentiles3": {
        "total": "18673",
        "ok": "18840",
        "ko": "11095"
    },
    "percentiles4": {
        "total": "30858",
        "ok": "32799",
        "ko": "11205"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 36,
    "percentage": 36
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.231",
        "ko": "0.692"
    }
}
    },"req_request-6-font-15e7e": {
        type: "REQUEST",
        name: "request_6_font",
path: "request_6_font",
pathFormatted: "req_request-6-font-15e7e",
stats: {
    "name": "request_6_font",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "3318",
        "ok": "3318",
        "ko": "9037"
    },
    "maxResponseTime": {
        "total": "32062",
        "ok": "32062",
        "ko": "11146"
    },
    "meanResponseTime": {
        "total": "11021",
        "ok": "11690",
        "ko": "9661"
    },
    "standardDeviation": {
        "total": "4696",
        "ok": "5601",
        "ko": "611"
    },
    "percentiles1": {
        "total": "9774",
        "ok": "10246",
        "ko": "9460"
    },
    "percentiles2": {
        "total": "11882",
        "ok": "13057",
        "ko": "9730"
    },
    "percentiles3": {
        "total": "19292",
        "ok": "24202",
        "ko": "10922"
    },
    "percentiles4": {
        "total": "26898",
        "ok": "28619",
        "ko": "11075"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.288",
        "ko": "0.635"
    }
}
    },"req_graphql-searchp-61328": {
        type: "REQUEST",
        name: "graphql_SearchPlaces",
path: "graphql_SearchPlaces",
pathFormatted: "req_graphql-searchp-61328",
stats: {
    "name": "graphql_SearchPlaces",
    "numberOfRequests": {
        "total": "100",
        "ok": "69",
        "ko": "31"
    },
    "minResponseTime": {
        "total": "3523",
        "ok": "3523",
        "ko": "9089"
    },
    "maxResponseTime": {
        "total": "21948",
        "ok": "21948",
        "ko": "11362"
    },
    "meanResponseTime": {
        "total": "10300",
        "ok": "10529",
        "ko": "9790"
    },
    "standardDeviation": {
        "total": "3533",
        "ok": "4209",
        "ko": "662"
    },
    "percentiles1": {
        "total": "9663",
        "ok": "9856",
        "ko": "9560"
    },
    "percentiles2": {
        "total": "11367",
        "ok": "11663",
        "ko": "10184"
    },
    "percentiles3": {
        "total": "19697",
        "ok": "20279",
        "ko": "10931"
    },
    "percentiles4": {
        "total": "21774",
        "ok": "21828",
        "ko": "11234"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 69,
    "percentage": 69
},
    "group4": {
    "name": "failed",
    "count": 31,
    "percentage": 31
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.327",
        "ko": "0.596"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "100",
        "ok": "61",
        "ko": "39"
    },
    "minResponseTime": {
        "total": "1082",
        "ok": "3225",
        "ko": "1082"
    },
    "maxResponseTime": {
        "total": "26605",
        "ok": "26605",
        "ko": "11380"
    },
    "meanResponseTime": {
        "total": "11385",
        "ok": "12523",
        "ko": "9606"
    },
    "standardDeviation": {
        "total": "4525",
        "ok": "5356",
        "ko": "1558"
    },
    "percentiles1": {
        "total": "10290",
        "ok": "11039",
        "ko": "9535"
    },
    "percentiles2": {
        "total": "12793",
        "ok": "15299",
        "ko": "10533"
    },
    "percentiles3": {
        "total": "20759",
        "ok": "21016",
        "ko": "11022"
    },
    "percentiles4": {
        "total": "25096",
        "ok": "25691",
        "ko": "11367"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 39,
    "percentage": 39
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.173",
        "ko": "0.75"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "872",
        "ok": "872",
        "ko": "9037"
    },
    "maxResponseTime": {
        "total": "28923",
        "ok": "28923",
        "ko": "11142"
    },
    "meanResponseTime": {
        "total": "10886",
        "ok": "11462",
        "ko": "9768"
    },
    "standardDeviation": {
        "total": "5052",
        "ok": "6117",
        "ko": "723"
    },
    "percentiles1": {
        "total": "9541",
        "ok": "9996",
        "ko": "9424"
    },
    "percentiles2": {
        "total": "12146",
        "ok": "14310",
        "ko": "9900"
    },
    "percentiles3": {
        "total": "20010",
        "ok": "22382",
        "ko": "11089"
    },
    "percentiles4": {
        "total": "27854",
        "ok": "28221",
        "ko": "11139"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.269",
        "ko": "0.654"
    }
}
    },"req_request-25-taxo-2081e": {
        type: "REQUEST",
        name: "request_25_TaxonMedia",
path: "request_25_TaxonMedia",
pathFormatted: "req_request-25-taxo-2081e",
stats: {
    "name": "request_25_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "66",
        "ko": "34"
    },
    "minResponseTime": {
        "total": "2605",
        "ok": "2605",
        "ko": "9033"
    },
    "maxResponseTime": {
        "total": "35520",
        "ok": "35520",
        "ko": "11151"
    },
    "meanResponseTime": {
        "total": "10985",
        "ok": "11621",
        "ko": "9749"
    },
    "standardDeviation": {
        "total": "4815",
        "ok": "5805",
        "ko": "679"
    },
    "percentiles1": {
        "total": "9859",
        "ok": "10334",
        "ko": "9446"
    },
    "percentiles2": {
        "total": "12285",
        "ok": "13890",
        "ko": "9992"
    },
    "percentiles3": {
        "total": "18346",
        "ok": "18965",
        "ko": "11025"
    },
    "percentiles4": {
        "total": "34984",
        "ok": "35168",
        "ko": "11145"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 34,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.269",
        "ko": "0.654"
    }
}
    },"req_graphql-riveran-e540f": {
        type: "REQUEST",
        name: "graphql_RiverAndGroundWaterObservation",
path: "graphql_RiverAndGroundWaterObservation",
pathFormatted: "req_graphql-riveran-e540f",
stats: {
    "name": "graphql_RiverAndGroundWaterObservation",
    "numberOfRequests": {
        "total": "100",
        "ok": "62",
        "ko": "38"
    },
    "minResponseTime": {
        "total": "3254",
        "ok": "3254",
        "ko": "9109"
    },
    "maxResponseTime": {
        "total": "34971",
        "ok": "34971",
        "ko": "11187"
    },
    "meanResponseTime": {
        "total": "10796",
        "ok": "11422",
        "ko": "9774"
    },
    "standardDeviation": {
        "total": "5088",
        "ok": "6359",
        "ko": "680"
    },
    "percentiles1": {
        "total": "9595",
        "ok": "10022",
        "ko": "9454"
    },
    "percentiles2": {
        "total": "11124",
        "ok": "13258",
        "ko": "10278"
    },
    "percentiles3": {
        "total": "21669",
        "ok": "25430",
        "ko": "10969"
    },
    "percentiles4": {
        "total": "34427",
        "ok": "34636",
        "ko": "11124"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 38,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.192",
        "ko": "0.731"
    }
}
    },"req_synaptix-client-ff009": {
        type: "REQUEST",
        name: "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
path: "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
pathFormatted: "req_synaptix-client-ff009",
stats: {
    "name": "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "56",
        "ko": "44"
    },
    "minResponseTime": {
        "total": "723",
        "ok": "723",
        "ko": "9231"
    },
    "maxResponseTime": {
        "total": "33548",
        "ok": "33548",
        "ko": "12011"
    },
    "meanResponseTime": {
        "total": "11713",
        "ok": "12976",
        "ko": "10104"
    },
    "standardDeviation": {
        "total": "4396",
        "ok": "5515",
        "ko": "770"
    },
    "percentiles1": {
        "total": "10778",
        "ok": "12306",
        "ko": "9897"
    },
    "percentiles2": {
        "total": "12655",
        "ok": "14425",
        "ko": "10677"
    },
    "percentiles3": {
        "total": "20071",
        "ok": "20998",
        "ko": "11407"
    },
    "percentiles4": {
        "total": "26208",
        "ok": "29470",
        "ko": "11930"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 55,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 44,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.077",
        "ko": "0.846"
    }
}
    },"req_request-21-taxo-4adce": {
        type: "REQUEST",
        name: "request_21_TaxonMedia",
path: "request_21_TaxonMedia",
pathFormatted: "req_request-21-taxo-4adce",
stats: {
    "name": "request_21_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "63",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "3745",
        "ok": "3745",
        "ko": "9091"
    },
    "maxResponseTime": {
        "total": "35787",
        "ok": "35787",
        "ko": "11222"
    },
    "meanResponseTime": {
        "total": "11101",
        "ok": "11889",
        "ko": "9758"
    },
    "standardDeviation": {
        "total": "5052",
        "ok": "6209",
        "ko": "695"
    },
    "percentiles1": {
        "total": "9773",
        "ok": "10285",
        "ko": "9414"
    },
    "percentiles2": {
        "total": "11277",
        "ok": "13115",
        "ko": "9914"
    },
    "percentiles3": {
        "total": "20627",
        "ok": "24611",
        "ko": "11003"
    },
    "percentiles4": {
        "total": "34539",
        "ok": "35005",
        "ko": "11204"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 63,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 37
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.212",
        "ko": "0.712"
    }
}
    },"req_graphql-news-4c309": {
        type: "REQUEST",
        name: "graphql_News",
path: "graphql_News",
pathFormatted: "req_graphql-news-4c309",
stats: {
    "name": "graphql_News",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "270",
        "ok": "270",
        "ko": "9101"
    },
    "maxResponseTime": {
        "total": "21836",
        "ok": "21836",
        "ko": "11157"
    },
    "meanResponseTime": {
        "total": "9538",
        "ok": "9461",
        "ko": "9703"
    },
    "standardDeviation": {
        "total": "3008",
        "ok": "3618",
        "ko": "657"
    },
    "percentiles1": {
        "total": "9531",
        "ok": "9701",
        "ko": "9451"
    },
    "percentiles2": {
        "total": "11141",
        "ok": "11480",
        "ko": "9830"
    },
    "percentiles3": {
        "total": "12295",
        "ok": "12907",
        "ko": "11020"
    },
    "percentiles4": {
        "total": "21105",
        "ok": "21342",
        "ko": "11134"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.308",
        "ko": "0.615"
    }
}
    },"req_favicon-16x16-p-7181b": {
        type: "REQUEST",
        name: "favicon-16x16.png",
path: "favicon-16x16.png",
pathFormatted: "req_favicon-16x16-p-7181b",
stats: {
    "name": "favicon-16x16.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "285",
        "ok": "285",
        "ko": "9234"
    },
    "maxResponseTime": {
        "total": "23380",
        "ok": "23380",
        "ko": "12011"
    },
    "meanResponseTime": {
        "total": "11294",
        "ok": "11988",
        "ko": "9818"
    },
    "standardDeviation": {
        "total": "4238",
        "ok": "4970",
        "ko": "670"
    },
    "percentiles1": {
        "total": "10676",
        "ok": "11375",
        "ko": "9626"
    },
    "percentiles2": {
        "total": "11841",
        "ok": "12310",
        "ko": "9922"
    },
    "percentiles3": {
        "total": "21056",
        "ok": "22360",
        "ko": "11607"
    },
    "percentiles4": {
        "total": "22490",
        "ok": "22778",
        "ko": "11955"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.308",
        "ko": "0.615"
    }
}
    },"req_coast-228x228-p-35307": {
        type: "REQUEST",
        name: "coast-228x228.png",
path: "coast-228x228.png",
pathFormatted: "req_coast-228x228-p-35307",
stats: {
    "name": "coast-228x228.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "63",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "200",
        "ok": "200",
        "ko": "9233"
    },
    "maxResponseTime": {
        "total": "38355",
        "ok": "38355",
        "ko": "12006"
    },
    "meanResponseTime": {
        "total": "11736",
        "ok": "12825",
        "ko": "9881"
    },
    "standardDeviation": {
        "total": "6510",
        "ok": "7989",
        "ko": "640"
    },
    "percentiles1": {
        "total": "10274",
        "ok": "12108",
        "ko": "9669"
    },
    "percentiles2": {
        "total": "13323",
        "ok": "16811",
        "ko": "9961"
    },
    "percentiles3": {
        "total": "22992",
        "ok": "25771",
        "ko": "11290"
    },
    "percentiles4": {
        "total": "38325",
        "ok": "38336",
        "ko": "11794"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 54,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 37
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.212",
        "ko": "0.712"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "613",
        "ok": "613",
        "ko": "9089"
    },
    "maxResponseTime": {
        "total": "34533",
        "ok": "34533",
        "ko": "11144"
    },
    "meanResponseTime": {
        "total": "11658",
        "ok": "12578",
        "ko": "9701"
    },
    "standardDeviation": {
        "total": "6268",
        "ok": "7411",
        "ko": "669"
    },
    "percentiles1": {
        "total": "9655",
        "ok": "10100",
        "ko": "9446"
    },
    "percentiles2": {
        "total": "12515",
        "ok": "14613",
        "ko": "9723"
    },
    "percentiles3": {
        "total": "27554",
        "ok": "28029",
        "ko": "11043"
    },
    "percentiles4": {
        "total": "33801",
        "ok": "34038",
        "ko": "11140"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.308",
        "ko": "0.615"
    }
}
    },"req_request-23-taxo-3ea91": {
        type: "REQUEST",
        name: "request_23_TaxonMedia",
path: "request_23_TaxonMedia",
pathFormatted: "req_request-23-taxo-3ea91",
stats: {
    "name": "request_23_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "62",
        "ko": "38"
    },
    "minResponseTime": {
        "total": "2611",
        "ok": "2611",
        "ko": "9062"
    },
    "maxResponseTime": {
        "total": "29192",
        "ok": "29192",
        "ko": "11146"
    },
    "meanResponseTime": {
        "total": "10778",
        "ok": "11301",
        "ko": "9925"
    },
    "standardDeviation": {
        "total": "4010",
        "ok": "4989",
        "ko": "728"
    },
    "percentiles1": {
        "total": "9785",
        "ok": "10691",
        "ko": "9656"
    },
    "percentiles2": {
        "total": "12121",
        "ok": "13056",
        "ko": "10795"
    },
    "percentiles3": {
        "total": "18463",
        "ok": "19819",
        "ko": "11063"
    },
    "percentiles4": {
        "total": "25161",
        "ok": "26708",
        "ko": "11116"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 38,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.192",
        "ko": "0.731"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "100",
        "ok": "63",
        "ko": "37"
    },
    "minResponseTime": {
        "total": "490",
        "ok": "490",
        "ko": "9109"
    },
    "maxResponseTime": {
        "total": "35139",
        "ok": "35139",
        "ko": "11064"
    },
    "meanResponseTime": {
        "total": "10663",
        "ok": "11192",
        "ko": "9761"
    },
    "standardDeviation": {
        "total": "4707",
        "ok": "5845",
        "ko": "649"
    },
    "percentiles1": {
        "total": "9663",
        "ok": "10055",
        "ko": "9532"
    },
    "percentiles2": {
        "total": "11915",
        "ok": "13193",
        "ko": "9908"
    },
    "percentiles3": {
        "total": "17991",
        "ok": "21988",
        "ko": "10970"
    },
    "percentiles4": {
        "total": "29755",
        "ok": "31767",
        "ko": "11035"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 37,
    "percentage": 37
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.212",
        "ko": "0.712"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "100",
        "ok": "62",
        "ko": "38"
    },
    "minResponseTime": {
        "total": "3753",
        "ok": "3753",
        "ko": "9102"
    },
    "maxResponseTime": {
        "total": "36908",
        "ok": "36908",
        "ko": "11344"
    },
    "meanResponseTime": {
        "total": "11312",
        "ok": "12275",
        "ko": "9742"
    },
    "standardDeviation": {
        "total": "5142",
        "ok": "6318",
        "ko": "689"
    },
    "percentiles1": {
        "total": "10075",
        "ok": "10835",
        "ko": "9405"
    },
    "percentiles2": {
        "total": "11223",
        "ok": "13680",
        "ko": "9999"
    },
    "percentiles3": {
        "total": "19364",
        "ok": "22023",
        "ko": "11029"
    },
    "percentiles4": {
        "total": "36625",
        "ok": "36734",
        "ko": "11267"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 38,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.192",
        "ko": "0.731"
    }
}
    },"req_graphql-taxon-f4085": {
        type: "REQUEST",
        name: "graphql_Taxon",
path: "graphql_Taxon",
pathFormatted: "req_graphql-taxon-f4085",
stats: {
    "name": "graphql_Taxon",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "847",
        "ok": "847",
        "ko": "9123"
    },
    "maxResponseTime": {
        "total": "26238",
        "ok": "26238",
        "ko": "11347"
    },
    "meanResponseTime": {
        "total": "10664",
        "ok": "11085",
        "ko": "9769"
    },
    "standardDeviation": {
        "total": "4011",
        "ok": "4785",
        "ko": "667"
    },
    "percentiles1": {
        "total": "9669",
        "ok": "10086",
        "ko": "9547"
    },
    "percentiles2": {
        "total": "11881",
        "ok": "12737",
        "ko": "9895"
    },
    "percentiles3": {
        "total": "20295",
        "ok": "20816",
        "ko": "11025"
    },
    "percentiles4": {
        "total": "23378",
        "ok": "24302",
        "ko": "11277"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.308",
        "ko": "0.615"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "100",
        "ok": "70",
        "ko": "30"
    },
    "minResponseTime": {
        "total": "427",
        "ok": "427",
        "ko": "9037"
    },
    "maxResponseTime": {
        "total": "35119",
        "ok": "35119",
        "ko": "11175"
    },
    "meanResponseTime": {
        "total": "10644",
        "ok": "11040",
        "ko": "9722"
    },
    "standardDeviation": {
        "total": "4763",
        "ok": "5629",
        "ko": "695"
    },
    "percentiles1": {
        "total": "9743",
        "ok": "10040",
        "ko": "9449"
    },
    "percentiles2": {
        "total": "12208",
        "ok": "12638",
        "ko": "9877"
    },
    "percentiles3": {
        "total": "18676",
        "ok": "19776",
        "ko": "10956"
    },
    "percentiles4": {
        "total": "27139",
        "ok": "29557",
        "ko": "11114"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 67,
    "percentage": 67
},
    "group4": {
    "name": "failed",
    "count": 30,
    "percentage": 30
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.346",
        "ko": "0.577"
    }
}
    },"req_vendors-e4fad40-0b16d": {
        type: "REQUEST",
        name: "vendors.e4fad40d75305c1c3562.js",
path: "vendors.e4fad40d75305c1c3562.js",
pathFormatted: "req_vendors-e4fad40-0b16d",
stats: {
    "name": "vendors.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "61",
        "ko": "39"
    },
    "minResponseTime": {
        "total": "9233",
        "ok": "9846",
        "ko": "9233"
    },
    "maxResponseTime": {
        "total": "36752",
        "ok": "36752",
        "ko": "12000"
    },
    "meanResponseTime": {
        "total": "15107",
        "ok": "18317",
        "ko": "10086"
    },
    "standardDeviation": {
        "total": "4856",
        "ok": "3441",
        "ko": "778"
    },
    "percentiles1": {
        "total": "16600",
        "ok": "17825",
        "ko": "9869"
    },
    "percentiles2": {
        "total": "18049",
        "ok": "18791",
        "ko": "10261"
    },
    "percentiles3": {
        "total": "21494",
        "ok": "25088",
        "ko": "11572"
    },
    "percentiles4": {
        "total": "26395",
        "ok": "30475",
        "ko": "11935"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 39,
    "percentage": 39
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.173",
        "ko": "0.75"
    }
}
    },"req_request-35-3745a": {
        type: "REQUEST",
        name: "request_35",
path: "request_35",
pathFormatted: "req_request-35-3745a",
stats: {
    "name": "request_35",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "476",
        "ok": "476",
        "ko": "9128"
    },
    "maxResponseTime": {
        "total": "22025",
        "ok": "22025",
        "ko": "11120"
    },
    "meanResponseTime": {
        "total": "10474",
        "ok": "10838",
        "ko": "9733"
    },
    "standardDeviation": {
        "total": "3921",
        "ok": "4726",
        "ko": "652"
    },
    "percentiles1": {
        "total": "9822",
        "ok": "10285",
        "ko": "9456"
    },
    "percentiles2": {
        "total": "12103",
        "ok": "13490",
        "ko": "9908"
    },
    "percentiles3": {
        "total": "18264",
        "ok": "19987",
        "ko": "10974"
    },
    "percentiles4": {
        "total": "22014",
        "ok": "22018",
        "ko": "11105"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 65,
    "percentage": 65
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.288",
        "ko": "0.635"
    }
}
    },"req_graphql-weather-e1b31": {
        type: "REQUEST",
        name: "graphql_weather",
path: "graphql_weather",
pathFormatted: "req_graphql-weather-e1b31",
stats: {
    "name": "graphql_weather",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "885",
        "ok": "885",
        "ko": "9107"
    },
    "maxResponseTime": {
        "total": "46081",
        "ok": "46081",
        "ko": "11069"
    },
    "meanResponseTime": {
        "total": "11906",
        "ok": "13042",
        "ko": "9795"
    },
    "standardDeviation": {
        "total": "8264",
        "ok": "10058",
        "ko": "651"
    },
    "percentiles1": {
        "total": "9694",
        "ok": "10279",
        "ko": "9639"
    },
    "percentiles2": {
        "total": "11500",
        "ok": "11947",
        "ko": "10281"
    },
    "percentiles3": {
        "total": "32712",
        "ok": "40801",
        "ko": "10936"
    },
    "percentiles4": {
        "total": "43536",
        "ok": "44436",
        "ko": "11035"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.25",
        "ko": "0.673"
    }
}
    },"req_main-e4fad40d75-406e2": {
        type: "REQUEST",
        name: "main.e4fad40d75305c1c3562.js",
path: "main.e4fad40d75305c1c3562.js",
pathFormatted: "req_main-e4fad40d75-406e2",
stats: {
    "name": "main.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "65",
        "ko": "35"
    },
    "minResponseTime": {
        "total": "259",
        "ok": "259",
        "ko": "9267"
    },
    "maxResponseTime": {
        "total": "35034",
        "ok": "35034",
        "ko": "11993"
    },
    "meanResponseTime": {
        "total": "12875",
        "ok": "14442",
        "ko": "9966"
    },
    "standardDeviation": {
        "total": "5239",
        "ok": "5911",
        "ko": "712"
    },
    "percentiles1": {
        "total": "11944",
        "ok": "14062",
        "ko": "9693"
    },
    "percentiles2": {
        "total": "15058",
        "ok": "18464",
        "ko": "10056"
    },
    "percentiles3": {
        "total": "21036",
        "ok": "21160",
        "ko": "11538"
    },
    "percentiles4": {
        "total": "28920",
        "ok": "31081",
        "ko": "11937"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 35,
    "percentage": 35
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.25",
        "ko": "0.673"
    }
}
    },"req_graphql-news-d7bbe": {
        type: "REQUEST",
        name: "graphQL_news",
path: "graphQL_news",
pathFormatted: "req_graphql-news-d7bbe",
stats: {
    "name": "graphQL_news",
    "numberOfRequests": {
        "total": "100",
        "ok": "69",
        "ko": "31"
    },
    "minResponseTime": {
        "total": "568",
        "ok": "568",
        "ko": "9122"
    },
    "maxResponseTime": {
        "total": "23265",
        "ok": "23265",
        "ko": "11363"
    },
    "meanResponseTime": {
        "total": "9690",
        "ok": "9546",
        "ko": "10011"
    },
    "standardDeviation": {
        "total": "4115",
        "ok": "4918",
        "ko": "797"
    },
    "percentiles1": {
        "total": "9629",
        "ok": "9611",
        "ko": "9653"
    },
    "percentiles2": {
        "total": "11299",
        "ok": "11494",
        "ko": "10914"
    },
    "percentiles3": {
        "total": "19816",
        "ok": "20953",
        "ko": "11277"
    },
    "percentiles4": {
        "total": "21850",
        "ok": "22293",
        "ko": "11358"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 31,
    "percentage": 31
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.327",
        "ko": "0.596"
    }
}
    },"req_app-css-13ea8": {
        type: "REQUEST",
        name: "App.css",
path: "App.css",
pathFormatted: "req_app-css-13ea8",
stats: {
    "name": "App.css",
    "numberOfRequests": {
        "total": "100",
        "ok": "62",
        "ko": "38"
    },
    "minResponseTime": {
        "total": "252",
        "ok": "252",
        "ko": "9232"
    },
    "maxResponseTime": {
        "total": "28821",
        "ok": "28821",
        "ko": "12005"
    },
    "meanResponseTime": {
        "total": "11607",
        "ok": "12577",
        "ko": "10024"
    },
    "standardDeviation": {
        "total": "4873",
        "ok": "5956",
        "ko": "749"
    },
    "percentiles1": {
        "total": "10609",
        "ok": "12735",
        "ko": "9877"
    },
    "percentiles2": {
        "total": "13029",
        "ok": "16998",
        "ko": "10067"
    },
    "percentiles3": {
        "total": "20862",
        "ok": "20991",
        "ko": "11469"
    },
    "percentiles4": {
        "total": "21247",
        "ok": "24154",
        "ko": "11943"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 57,
    "percentage": 57
},
    "group4": {
    "name": "failed",
    "count": 38,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.192",
        "ko": "0.731"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "100",
        "ok": "64",
        "ko": "36"
    },
    "minResponseTime": {
        "total": "254",
        "ok": "254",
        "ko": "9035"
    },
    "maxResponseTime": {
        "total": "23383",
        "ok": "23383",
        "ko": "11066"
    },
    "meanResponseTime": {
        "total": "9780",
        "ok": "9739",
        "ko": "9854"
    },
    "standardDeviation": {
        "total": "3419",
        "ok": "4240",
        "ko": "698"
    },
    "percentiles1": {
        "total": "9565",
        "ok": "9561",
        "ko": "9608"
    },
    "percentiles2": {
        "total": "11051",
        "ok": "11571",
        "ko": "10743"
    },
    "percentiles3": {
        "total": "13168",
        "ok": "18493",
        "ko": "10962"
    },
    "percentiles4": {
        "total": "21852",
        "ok": "22409",
        "ko": "11031"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 62,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 36,
    "percentage": 36
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.231",
        "ko": "0.692"
    }
}
    },"req_request-4-matom-e3907": {
        type: "REQUEST",
        name: "request_4_matomo",
path: "request_4_matomo",
pathFormatted: "req_request-4-matom-e3907",
stats: {
    "name": "request_4_matomo",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1624",
        "ok": "1624",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10694",
        "ok": "10694",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4078",
        "ok": "4078",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2826",
        "ok": "2826",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2836",
        "ok": "2836",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3606",
        "ok": "3606",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10098",
        "ok": "10098",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10549",
        "ok": "10549",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 100,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.923",
        "ko": "-"
    }
}
    },"req_request-20-taxo-0d791": {
        type: "REQUEST",
        name: "request_20_TaxonMedia",
path: "request_20_TaxonMedia",
pathFormatted: "req_request-20-taxo-0d791",
stats: {
    "name": "request_20_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "61",
        "ko": "39"
    },
    "minResponseTime": {
        "total": "3727",
        "ok": "3727",
        "ko": "9100"
    },
    "maxResponseTime": {
        "total": "27592",
        "ok": "27592",
        "ko": "11040"
    },
    "meanResponseTime": {
        "total": "10743",
        "ok": "11400",
        "ko": "9715"
    },
    "standardDeviation": {
        "total": "4265",
        "ok": "5334",
        "ko": "631"
    },
    "percentiles1": {
        "total": "9659",
        "ok": "10145",
        "ko": "9533"
    },
    "percentiles2": {
        "total": "11367",
        "ok": "12639",
        "ko": "9856"
    },
    "percentiles3": {
        "total": "20844",
        "ok": "24033",
        "ko": "10969"
    },
    "percentiles4": {
        "total": "27092",
        "ok": "27289",
        "ko": "11018"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 61,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 39,
    "percentage": 39
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.173",
        "ko": "0.75"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1706",
        "ok": "1706",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13040",
        "ok": "13040",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3557",
        "ok": "3557",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2170",
        "ok": "2170",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2979",
        "ok": "2979",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3362",
        "ok": "3362",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8579",
        "ok": "8579",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11420",
        "ok": "11420",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 100,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.923",
        "ko": "-"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "100",
        "ok": "67",
        "ko": "33"
    },
    "minResponseTime": {
        "total": "455",
        "ok": "455",
        "ko": "9116"
    },
    "maxResponseTime": {
        "total": "35058",
        "ok": "35058",
        "ko": "11100"
    },
    "meanResponseTime": {
        "total": "11373",
        "ok": "12244",
        "ko": "9602"
    },
    "standardDeviation": {
        "total": "5221",
        "ok": "6183",
        "ko": "555"
    },
    "percentiles1": {
        "total": "9858",
        "ok": "11266",
        "ko": "9396"
    },
    "percentiles2": {
        "total": "13356",
        "ok": "13980",
        "ko": "9666"
    },
    "percentiles3": {
        "total": "18955",
        "ok": "21126",
        "ko": "10933"
    },
    "percentiles4": {
        "total": "34945",
        "ok": "34983",
        "ko": "11049"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 33,
    "percentage": 33
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.288",
        "ko": "0.635"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "100",
        "ok": "68",
        "ko": "32"
    },
    "minResponseTime": {
        "total": "254",
        "ok": "254",
        "ko": "9114"
    },
    "maxResponseTime": {
        "total": "20962",
        "ok": "20962",
        "ko": "11242"
    },
    "meanResponseTime": {
        "total": "9552",
        "ok": "9415",
        "ko": "9841"
    },
    "standardDeviation": {
        "total": "2710",
        "ok": "3238",
        "ko": "735"
    },
    "percentiles1": {
        "total": "9550",
        "ok": "9636",
        "ko": "9541"
    },
    "percentiles2": {
        "total": "11074",
        "ok": "11402",
        "ko": "10499"
    },
    "percentiles3": {
        "total": "11870",
        "ok": "12103",
        "ko": "11112"
    },
    "percentiles4": {
        "total": "17825",
        "ok": "18839",
        "ko": "11210"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 66
},
    "group4": {
    "name": "failed",
    "count": 32,
    "percentage": 32
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.923",
        "ok": "1.308",
        "ko": "0.615"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
