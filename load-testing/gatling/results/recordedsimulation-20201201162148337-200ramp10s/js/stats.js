var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "7378",
        "ok": "7134",
        "ko": "244"
    },
    "minResponseTime": {
        "total": "13",
        "ok": "13",
        "ko": "421"
    },
    "maxResponseTime": {
        "total": "60007",
        "ok": "31166",
        "ko": "60007"
    },
    "meanResponseTime": {
        "total": "3883",
        "ok": "3662",
        "ko": "10344"
    },
    "standardDeviation": {
        "total": "3527",
        "ok": "3313",
        "ko": "3450"
    },
    "percentiles1": {
        "total": "2298",
        "ok": "2188",
        "ko": "10123"
    },
    "percentiles2": {
        "total": "6151",
        "ok": "5730",
        "ko": "10444"
    },
    "percentiles3": {
        "total": "10142",
        "ok": "9970",
        "ko": "11369"
    },
    "percentiles4": {
        "total": "13850",
        "ok": "14003",
        "ko": "11647"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 566,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1009,
    "percentage": 14
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5559,
    "percentage": 75
},
    "group4": {
    "name": "failed",
    "count": 244,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "106.928",
        "ok": "103.391",
        "ko": "3.536"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "200",
        "ok": "194",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "67",
        "ok": "67",
        "ko": "10002"
    },
    "maxResponseTime": {
        "total": "11117",
        "ok": "10342",
        "ko": "11117"
    },
    "meanResponseTime": {
        "total": "2222",
        "ok": "1971",
        "ko": "10329"
    },
    "standardDeviation": {
        "total": "2341",
        "ok": "1884",
        "ko": "436"
    },
    "percentiles1": {
        "total": "1403",
        "ok": "1383",
        "ko": "10058"
    },
    "percentiles2": {
        "total": "2232",
        "ok": "2161",
        "ko": "10572"
    },
    "percentiles3": {
        "total": "8710",
        "ok": "6681",
        "ko": "11020"
    },
    "percentiles4": {
        "total": "10346",
        "ok": "9481",
        "ko": "11098"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 34,
    "percentage": 17
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 41,
    "percentage": 21
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 119,
    "percentage": 60
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.899",
        "ok": "2.812",
        "ko": "0.087"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "194",
        "ok": "188",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "72",
        "ok": "72",
        "ko": "10010"
    },
    "maxResponseTime": {
        "total": "11647",
        "ok": "10093",
        "ko": "11647"
    },
    "meanResponseTime": {
        "total": "3186",
        "ok": "2954",
        "ko": "10444"
    },
    "standardDeviation": {
        "total": "2856",
        "ok": "2582",
        "ko": "561"
    },
    "percentiles1": {
        "total": "1822",
        "ok": "1750",
        "ko": "10212"
    },
    "percentiles2": {
        "total": "4594",
        "ok": "3932",
        "ko": "10446"
    },
    "percentiles3": {
        "total": "9930",
        "ok": "8529",
        "ko": "11359"
    },
    "percentiles4": {
        "total": "10316",
        "ok": "10015",
        "ko": "11589"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 17,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 31,
    "percentage": 16
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 140,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.725",
        "ko": "0.087"
    }
}
    },"req_request-23-taxo-3ea91": {
        type: "REQUEST",
        name: "request_23_TaxonMedia",
path: "request_23_TaxonMedia",
pathFormatted: "req_request-23-taxo-3ea91",
stats: {
    "name": "request_23_TaxonMedia",
    "numberOfRequests": {
        "total": "194",
        "ok": "183",
        "ko": "11"
    },
    "minResponseTime": {
        "total": "145",
        "ok": "145",
        "ko": "10010"
    },
    "maxResponseTime": {
        "total": "16372",
        "ok": "16372",
        "ko": "11380"
    },
    "meanResponseTime": {
        "total": "5192",
        "ok": "4877",
        "ko": "10431"
    },
    "standardDeviation": {
        "total": "3491",
        "ok": "3340",
        "ko": "479"
    },
    "percentiles1": {
        "total": "4955",
        "ok": "4070",
        "ko": "10299"
    },
    "percentiles2": {
        "total": "8014",
        "ok": "7377",
        "ko": "10498"
    },
    "percentiles3": {
        "total": "10620",
        "ok": "10310",
        "ko": "11375"
    },
    "percentiles4": {
        "total": "12014",
        "ok": "12225",
        "ko": "11379"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 10
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 155,
    "percentage": 80
},
    "group4": {
    "name": "failed",
    "count": 11,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.652",
        "ko": "0.159"
    }
}
    },"req_request-6-font-15e7e": {
        type: "REQUEST",
        name: "request_6_font",
path: "request_6_font",
pathFormatted: "req_request-6-font-15e7e",
stats: {
    "name": "request_6_font",
    "numberOfRequests": {
        "total": "194",
        "ok": "186",
        "ko": "8"
    },
    "minResponseTime": {
        "total": "13",
        "ok": "13",
        "ko": "10007"
    },
    "maxResponseTime": {
        "total": "11369",
        "ok": "10107",
        "ko": "11369"
    },
    "meanResponseTime": {
        "total": "3259",
        "ok": "2953",
        "ko": "10379"
    },
    "standardDeviation": {
        "total": "2887",
        "ok": "2532",
        "ko": "434"
    },
    "percentiles1": {
        "total": "1959",
        "ok": "1841",
        "ko": "10188"
    },
    "percentiles2": {
        "total": "4275",
        "ok": "3846",
        "ko": "10539"
    },
    "percentiles3": {
        "total": "9980",
        "ok": "8283",
        "ko": "11124"
    },
    "percentiles4": {
        "total": "10508",
        "ok": "9983",
        "ko": "11320"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 18,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 27,
    "percentage": 14
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 141,
    "percentage": 73
},
    "group4": {
    "name": "failed",
    "count": 8,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.696",
        "ko": "0.116"
    }
}
    },"req_request-36-9ad97": {
        type: "REQUEST",
        name: "request_36",
path: "request_36",
pathFormatted: "req_request-36-9ad97",
stats: {
    "name": "request_36",
    "numberOfRequests": {
        "total": "194",
        "ok": "186",
        "ko": "8"
    },
    "minResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "14455",
        "ok": "14455",
        "ko": "10481"
    },
    "meanResponseTime": {
        "total": "3283",
        "ok": "2985",
        "ko": "10214"
    },
    "standardDeviation": {
        "total": "2916",
        "ok": "2591",
        "ko": "170"
    },
    "percentiles1": {
        "total": "2005",
        "ok": "1965",
        "ko": "10188"
    },
    "percentiles2": {
        "total": "4150",
        "ok": "3865",
        "ko": "10336"
    },
    "percentiles3": {
        "total": "10039",
        "ok": "8250",
        "ko": "10464"
    },
    "percentiles4": {
        "total": "10435",
        "ok": "10112",
        "ko": "10478"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 12,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 35,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 139,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 8,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.696",
        "ko": "0.116"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "194",
        "ok": "188",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "72",
        "ok": "72",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "10494",
        "ok": "10124",
        "ko": "10494"
    },
    "meanResponseTime": {
        "total": "3080",
        "ok": "2854",
        "ko": "10163"
    },
    "standardDeviation": {
        "total": "2805",
        "ok": "2543",
        "ko": "180"
    },
    "percentiles1": {
        "total": "1843",
        "ok": "1810",
        "ko": "10075"
    },
    "percentiles2": {
        "total": "4049",
        "ok": "3751",
        "ko": "10259"
    },
    "percentiles3": {
        "total": "9931",
        "ok": "8600",
        "ko": "10447"
    },
    "percentiles4": {
        "total": "10137",
        "ok": "10074",
        "ko": "10485"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 21,
    "percentage": 11
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 32,
    "percentage": 16
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 135,
    "percentage": 70
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.725",
        "ko": "0.087"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "194",
        "ok": "189",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "104",
        "ok": "104",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "10548",
        "ok": "10156",
        "ko": "10548"
    },
    "meanResponseTime": {
        "total": "3138",
        "ok": "2950",
        "ko": "10242"
    },
    "standardDeviation": {
        "total": "2752",
        "ok": "2531",
        "ko": "209"
    },
    "percentiles1": {
        "total": "1846",
        "ok": "1828",
        "ko": "10122"
    },
    "percentiles2": {
        "total": "3927",
        "ok": "3854",
        "ko": "10430"
    },
    "percentiles3": {
        "total": "9787",
        "ok": "8948",
        "ko": "10524"
    },
    "percentiles4": {
        "total": "10175",
        "ok": "10084",
        "ko": "10543"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 10,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 34,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 145,
    "percentage": 75
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.739",
        "ko": "0.072"
    }
}
    },"req_request-4-matom-e3907": {
        type: "REQUEST",
        name: "request_4_matomo",
path: "request_4_matomo",
pathFormatted: "req_request-4-matom-e3907",
stats: {
    "name": "request_4_matomo",
    "numberOfRequests": {
        "total": "194",
        "ok": "194",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "121",
        "ok": "121",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10396",
        "ok": "10396",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2056",
        "ok": "2056",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1724",
        "ok": "1724",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1676",
        "ok": "1676",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5198",
        "ok": "5198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7531",
        "ok": "7531",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 57,
    "percentage": 29
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 27,
    "percentage": 14
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 110,
    "percentage": 57
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.812",
        "ko": "-"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "194",
        "ok": "189",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "61",
        "ok": "61",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "11477",
        "ok": "11477",
        "ko": "10523"
    },
    "meanResponseTime": {
        "total": "3191",
        "ok": "3007",
        "ko": "10153"
    },
    "standardDeviation": {
        "total": "2850",
        "ok": "2650",
        "ko": "190"
    },
    "percentiles1": {
        "total": "1998",
        "ok": "1974",
        "ko": "10097"
    },
    "percentiles2": {
        "total": "4221",
        "ok": "3826",
        "ko": "10121"
    },
    "percentiles3": {
        "total": "9916",
        "ok": "8657",
        "ko": "10443"
    },
    "percentiles4": {
        "total": "10446",
        "ok": "10370",
        "ko": "10507"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 17,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 35,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 137,
    "percentage": 71
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.739",
        "ko": "0.072"
    }
}
    },"req_request-35-3745a": {
        type: "REQUEST",
        name: "request_35",
path: "request_35",
pathFormatted: "req_request-35-3745a",
stats: {
    "name": "request_35",
    "numberOfRequests": {
        "total": "194",
        "ok": "184",
        "ko": "10"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "10684",
        "ok": "10149",
        "ko": "10684"
    },
    "meanResponseTime": {
        "total": "3353",
        "ok": "2976",
        "ko": "10299"
    },
    "standardDeviation": {
        "total": "3037",
        "ok": "2637",
        "ko": "245"
    },
    "percentiles1": {
        "total": "1843",
        "ok": "1797",
        "ko": "10366"
    },
    "percentiles2": {
        "total": "4799",
        "ok": "3879",
        "ko": "10495"
    },
    "percentiles3": {
        "total": "10022",
        "ok": "8733",
        "ko": "10606"
    },
    "percentiles4": {
        "total": "10496",
        "ok": "10077",
        "ko": "10668"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 14,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 35,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 135,
    "percentage": 70
},
    "group4": {
    "name": "failed",
    "count": 10,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.667",
        "ko": "0.145"
    }
}
    },"req_request-26-taxo-41b6f": {
        type: "REQUEST",
        name: "request_26_TaxonMedia",
path: "request_26_TaxonMedia",
pathFormatted: "req_request-26-taxo-41b6f",
stats: {
    "name": "request_26_TaxonMedia",
    "numberOfRequests": {
        "total": "194",
        "ok": "186",
        "ko": "8"
    },
    "minResponseTime": {
        "total": "179",
        "ok": "179",
        "ko": "423"
    },
    "maxResponseTime": {
        "total": "22971",
        "ok": "22971",
        "ko": "11631"
    },
    "meanResponseTime": {
        "total": "5126",
        "ok": "4957",
        "ko": "9070"
    },
    "standardDeviation": {
        "total": "3838",
        "ok": "3767",
        "ko": "3308"
    },
    "percentiles1": {
        "total": "4642",
        "ok": "4306",
        "ko": "10061"
    },
    "percentiles2": {
        "total": "7555",
        "ok": "7334",
        "ko": "10149"
    },
    "percentiles3": {
        "total": "11046",
        "ok": "10837",
        "ko": "11140"
    },
    "percentiles4": {
        "total": "15071",
        "ok": "15468",
        "ko": "11533"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 23,
    "percentage": 12
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 158,
    "percentage": 81
},
    "group4": {
    "name": "failed",
    "count": 8,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.696",
        "ko": "0.116"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "194",
        "ok": "188",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "90",
        "ok": "90",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "11394",
        "ok": "11394",
        "ko": "10492"
    },
    "meanResponseTime": {
        "total": "3136",
        "ok": "2911",
        "ko": "10164"
    },
    "standardDeviation": {
        "total": "2803",
        "ok": "2546",
        "ko": "165"
    },
    "percentiles1": {
        "total": "1966",
        "ok": "1893",
        "ko": "10113"
    },
    "percentiles2": {
        "total": "3824",
        "ok": "3685",
        "ko": "10209"
    },
    "percentiles3": {
        "total": "9928",
        "ok": "8653",
        "ko": "10429"
    },
    "percentiles4": {
        "total": "10256",
        "ok": "10078",
        "ko": "10479"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 14,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 33,
    "percentage": 17
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 141,
    "percentage": 73
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.725",
        "ko": "0.087"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "194",
        "ok": "185",
        "ko": "9"
    },
    "minResponseTime": {
        "total": "16",
        "ok": "16",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "10694",
        "ok": "10156",
        "ko": "10694"
    },
    "meanResponseTime": {
        "total": "3167",
        "ok": "2819",
        "ko": "10302"
    },
    "standardDeviation": {
        "total": "2857",
        "ok": "2442",
        "ko": "245"
    },
    "percentiles1": {
        "total": "1953",
        "ok": "1840",
        "ko": "10252"
    },
    "percentiles2": {
        "total": "3975",
        "ok": "3237",
        "ko": "10481"
    },
    "percentiles3": {
        "total": "10046",
        "ok": "7961",
        "ko": "10657"
    },
    "percentiles4": {
        "total": "10489",
        "ok": "10106",
        "ko": "10687"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 16,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 26,
    "percentage": 13
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 143,
    "percentage": 74
},
    "group4": {
    "name": "failed",
    "count": 9,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.681",
        "ko": "0.13"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "194",
        "ok": "189",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "16",
        "ok": "16",
        "ko": "10007"
    },
    "maxResponseTime": {
        "total": "10493",
        "ok": "10131",
        "ko": "10493"
    },
    "meanResponseTime": {
        "total": "3194",
        "ok": "3010",
        "ko": "10134"
    },
    "standardDeviation": {
        "total": "2853",
        "ok": "2655",
        "ko": "185"
    },
    "percentiles1": {
        "total": "1812",
        "ok": "1798",
        "ko": "10025"
    },
    "percentiles2": {
        "total": "4551",
        "ok": "3863",
        "ko": "10125"
    },
    "percentiles3": {
        "total": "9915",
        "ok": "8838",
        "ko": "10419"
    },
    "percentiles4": {
        "total": "10125",
        "ok": "10004",
        "ko": "10478"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 18,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 31,
    "percentage": 16
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 140,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.739",
        "ko": "0.072"
    }
}
    },"req_request-28-taxo-f261b": {
        type: "REQUEST",
        name: "request_28_TaxonMedia",
path: "request_28_TaxonMedia",
pathFormatted: "req_request-28-taxo-f261b",
stats: {
    "name": "request_28_TaxonMedia",
    "numberOfRequests": {
        "total": "194",
        "ok": "187",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "205",
        "ok": "205",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "20537",
        "ok": "20537",
        "ko": "10484"
    },
    "meanResponseTime": {
        "total": "6189",
        "ok": "6037",
        "ko": "10242"
    },
    "standardDeviation": {
        "total": "4164",
        "ok": "4165",
        "ko": "164"
    },
    "percentiles1": {
        "total": "6461",
        "ok": "6372",
        "ko": "10243"
    },
    "percentiles2": {
        "total": "9395",
        "ok": "8854",
        "ko": "10368"
    },
    "percentiles3": {
        "total": "12985",
        "ok": "13307",
        "ko": "10468"
    },
    "percentiles4": {
        "total": "15021",
        "ok": "15194",
        "ko": "10481"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 14,
    "percentage": 7
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 168,
    "percentage": 87
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.71",
        "ko": "0.101"
    }
}
    },"req_graphql-riveran-e540f": {
        type: "REQUEST",
        name: "graphql_RiverAndGroundWaterObservation",
path: "graphql_RiverAndGroundWaterObservation",
pathFormatted: "req_graphql-riveran-e540f",
stats: {
    "name": "graphql_RiverAndGroundWaterObservation",
    "numberOfRequests": {
        "total": "194",
        "ok": "186",
        "ko": "8"
    },
    "minResponseTime": {
        "total": "208",
        "ok": "208",
        "ko": "10018"
    },
    "maxResponseTime": {
        "total": "28953",
        "ok": "28953",
        "ko": "10521"
    },
    "meanResponseTime": {
        "total": "8229",
        "ok": "8142",
        "ko": "10250"
    },
    "standardDeviation": {
        "total": "5696",
        "ok": "5801",
        "ko": "209"
    },
    "percentiles1": {
        "total": "7330",
        "ok": "7117",
        "ko": "10168"
    },
    "percentiles2": {
        "total": "10140",
        "ok": "9799",
        "ko": "10499"
    },
    "percentiles3": {
        "total": "19354",
        "ok": "19421",
        "ko": "10518"
    },
    "percentiles4": {
        "total": "26447",
        "ok": "26460",
        "ko": "10520"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 176,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "count": 8,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.696",
        "ko": "0.116"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "194",
        "ok": "187",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "10007"
    },
    "maxResponseTime": {
        "total": "10493",
        "ok": "10227",
        "ko": "10493"
    },
    "meanResponseTime": {
        "total": "3117",
        "ok": "2851",
        "ko": "10206"
    },
    "standardDeviation": {
        "total": "2841",
        "ok": "2534",
        "ko": "185"
    },
    "percentiles1": {
        "total": "1719",
        "ok": "1686",
        "ko": "10098"
    },
    "percentiles2": {
        "total": "4469",
        "ok": "3853",
        "ko": "10366"
    },
    "percentiles3": {
        "total": "9950",
        "ok": "8447",
        "ko": "10475"
    },
    "percentiles4": {
        "total": "10308",
        "ok": "10083",
        "ko": "10489"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 16,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 31,
    "percentage": 16
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 140,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.71",
        "ko": "0.101"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "194",
        "ok": "187",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "94",
        "ok": "94",
        "ko": "10007"
    },
    "maxResponseTime": {
        "total": "12965",
        "ok": "12965",
        "ko": "10494"
    },
    "meanResponseTime": {
        "total": "3056",
        "ok": "2789",
        "ko": "10205"
    },
    "standardDeviation": {
        "total": "2778",
        "ok": "2453",
        "ko": "201"
    },
    "percentiles1": {
        "total": "1832",
        "ok": "1777",
        "ko": "10098"
    },
    "percentiles2": {
        "total": "3802",
        "ok": "3647",
        "ko": "10400"
    },
    "percentiles3": {
        "total": "9987",
        "ok": "8221",
        "ko": "10475"
    },
    "percentiles4": {
        "total": "10435",
        "ok": "10075",
        "ko": "10490"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 14,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 36,
    "percentage": 19
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 137,
    "percentage": 71
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.71",
        "ko": "0.101"
    }
}
    },"req_graphql-taxon-f4085": {
        type: "REQUEST",
        name: "graphql_Taxon",
path: "graphql_Taxon",
pathFormatted: "req_graphql-taxon-f4085",
stats: {
    "name": "graphql_Taxon",
    "numberOfRequests": {
        "total": "194",
        "ok": "186",
        "ko": "8"
    },
    "minResponseTime": {
        "total": "180",
        "ok": "180",
        "ko": "10010"
    },
    "maxResponseTime": {
        "total": "14351",
        "ok": "14351",
        "ko": "11379"
    },
    "meanResponseTime": {
        "total": "4763",
        "ok": "4525",
        "ko": "10300"
    },
    "standardDeviation": {
        "total": "3113",
        "ok": "2953",
        "ko": "435"
    },
    "percentiles1": {
        "total": "4082",
        "ok": "4016",
        "ko": "10111"
    },
    "percentiles2": {
        "total": "7033",
        "ok": "6494",
        "ko": "10313"
    },
    "percentiles3": {
        "total": "10199",
        "ok": "10073",
        "ko": "11068"
    },
    "percentiles4": {
        "total": "11519",
        "ok": "11429",
        "ko": "11317"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 173,
    "percentage": 89
},
    "group4": {
    "name": "failed",
    "count": 8,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.696",
        "ko": "0.116"
    }
}
    },"req_request-34-d9757": {
        type: "REQUEST",
        name: "request_34",
path: "request_34",
pathFormatted: "req_request-34-d9757",
stats: {
    "name": "request_34",
    "numberOfRequests": {
        "total": "194",
        "ok": "183",
        "ko": "11"
    },
    "minResponseTime": {
        "total": "106",
        "ok": "106",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "11609",
        "ok": "10155",
        "ko": "11609"
    },
    "meanResponseTime": {
        "total": "3275",
        "ok": "2848",
        "ko": "10379"
    },
    "standardDeviation": {
        "total": "2964",
        "ok": "2467",
        "ko": "440"
    },
    "percentiles1": {
        "total": "1854",
        "ok": "1811",
        "ko": "10253"
    },
    "percentiles2": {
        "total": "4246",
        "ok": "3814",
        "ko": "10462"
    },
    "percentiles3": {
        "total": "10095",
        "ok": "8536",
        "ko": "11146"
    },
    "percentiles4": {
        "total": "10506",
        "ok": "10099",
        "ko": "11516"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 14,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 34,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 135,
    "percentage": 70
},
    "group4": {
    "name": "failed",
    "count": 11,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.652",
        "ko": "0.159"
    }
}
    },"req_request-21-taxo-4adce": {
        type: "REQUEST",
        name: "request_21_TaxonMedia",
path: "request_21_TaxonMedia",
pathFormatted: "req_request-21-taxo-4adce",
stats: {
    "name": "request_21_TaxonMedia",
    "numberOfRequests": {
        "total": "194",
        "ok": "188",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "180",
        "ok": "180",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "21920",
        "ok": "21920",
        "ko": "10484"
    },
    "meanResponseTime": {
        "total": "5013",
        "ok": "4849",
        "ko": "10181"
    },
    "standardDeviation": {
        "total": "3571",
        "ok": "3504",
        "ko": "199"
    },
    "percentiles1": {
        "total": "4707",
        "ok": "4380",
        "ko": "10074"
    },
    "percentiles2": {
        "total": "7654",
        "ok": "7351",
        "ko": "10353"
    },
    "percentiles3": {
        "total": "10285",
        "ok": "10132",
        "ko": "10470"
    },
    "percentiles4": {
        "total": "12661",
        "ok": "12799",
        "ko": "10481"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 10
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 161,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.725",
        "ko": "0.087"
    }
}
    },"req_graphql-weather-e1b31": {
        type: "REQUEST",
        name: "graphql_weather",
path: "graphql_weather",
pathFormatted: "req_graphql-weather-e1b31",
stats: {
    "name": "graphql_weather",
    "numberOfRequests": {
        "total": "194",
        "ok": "188",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "88",
        "ok": "88",
        "ko": "10010"
    },
    "maxResponseTime": {
        "total": "14454",
        "ok": "14454",
        "ko": "10482"
    },
    "meanResponseTime": {
        "total": "3473",
        "ok": "3257",
        "ko": "10235"
    },
    "standardDeviation": {
        "total": "3017",
        "ok": "2809",
        "ko": "196"
    },
    "percentiles1": {
        "total": "2044",
        "ok": "2021",
        "ko": "10234"
    },
    "percentiles2": {
        "total": "5186",
        "ok": "4619",
        "ko": "10413"
    },
    "percentiles3": {
        "total": "10014",
        "ok": "9522",
        "ko": "10468"
    },
    "percentiles4": {
        "total": "11034",
        "ok": "11058",
        "ko": "10479"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 11,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 35,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 142,
    "percentage": 73
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.725",
        "ko": "0.087"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "194",
        "ok": "188",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "57",
        "ok": "57",
        "ko": "10010"
    },
    "maxResponseTime": {
        "total": "11368",
        "ok": "10349",
        "ko": "11368"
    },
    "meanResponseTime": {
        "total": "3123",
        "ok": "2894",
        "ko": "10307"
    },
    "standardDeviation": {
        "total": "2811",
        "ok": "2539",
        "ko": "485"
    },
    "percentiles1": {
        "total": "1758",
        "ok": "1727",
        "ko": "10075"
    },
    "percentiles2": {
        "total": "4758",
        "ok": "3971",
        "ko": "10254"
    },
    "percentiles3": {
        "total": "9952",
        "ok": "8472",
        "ko": "11100"
    },
    "percentiles4": {
        "total": "10301",
        "ok": "10096",
        "ko": "11314"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 17,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 32,
    "percentage": 16
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 139,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.725",
        "ko": "0.087"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "194",
        "ok": "193",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "120",
        "ok": "120",
        "ko": "60007"
    },
    "maxResponseTime": {
        "total": "60007",
        "ok": "10632",
        "ko": "60007"
    },
    "meanResponseTime": {
        "total": "3702",
        "ok": "3410",
        "ko": "60007"
    },
    "standardDeviation": {
        "total": "4842",
        "ok": "2656",
        "ko": "0"
    },
    "percentiles1": {
        "total": "2836",
        "ok": "2828",
        "ko": "60007"
    },
    "percentiles2": {
        "total": "5522",
        "ok": "5476",
        "ko": "60007"
    },
    "percentiles3": {
        "total": "8236",
        "ok": "8194",
        "ko": "60007"
    },
    "percentiles4": {
        "total": "10177",
        "ok": "9965",
        "ko": "60007"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 42,
    "percentage": 22
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 16,
    "percentage": 8
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 135,
    "percentage": 70
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.797",
        "ko": "0.014"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "194",
        "ok": "190",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "10010"
    },
    "maxResponseTime": {
        "total": "15304",
        "ok": "15304",
        "ko": "10492"
    },
    "meanResponseTime": {
        "total": "3218",
        "ok": "3071",
        "ko": "10196"
    },
    "standardDeviation": {
        "total": "2934",
        "ok": "2783",
        "ko": "182"
    },
    "percentiles1": {
        "total": "2002",
        "ok": "1959",
        "ko": "10141"
    },
    "percentiles2": {
        "total": "4460",
        "ok": "4117",
        "ko": "10260"
    },
    "percentiles3": {
        "total": "9984",
        "ok": "9415",
        "ko": "10446"
    },
    "percentiles4": {
        "total": "10442",
        "ok": "10187",
        "ko": "10483"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 19,
    "percentage": 10
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 29,
    "percentage": 15
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 142,
    "percentage": 73
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.754",
        "ko": "0.058"
    }
}
    },"req_request-24-taxo-b5f25": {
        type: "REQUEST",
        name: "request_24_TaxonMedia",
path: "request_24_TaxonMedia",
pathFormatted: "req_request-24-taxo-b5f25",
stats: {
    "name": "request_24_TaxonMedia",
    "numberOfRequests": {
        "total": "194",
        "ok": "190",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "227",
        "ok": "227",
        "ko": "10012"
    },
    "maxResponseTime": {
        "total": "18273",
        "ok": "18273",
        "ko": "10025"
    },
    "meanResponseTime": {
        "total": "6437",
        "ok": "6361",
        "ko": "10019"
    },
    "standardDeviation": {
        "total": "4095",
        "ok": "4104",
        "ko": "5"
    },
    "percentiles1": {
        "total": "6982",
        "ok": "6777",
        "ko": "10019"
    },
    "percentiles2": {
        "total": "9290",
        "ok": "9078",
        "ko": "10021"
    },
    "percentiles3": {
        "total": "13527",
        "ok": "13545",
        "ko": "10024"
    },
    "percentiles4": {
        "total": "15522",
        "ok": "15567",
        "ko": "10025"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 7
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 171,
    "percentage": 88
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.754",
        "ko": "0.058"
    }
}
    },"req_request-29-taxo-1f5dd": {
        type: "REQUEST",
        name: "request_29_TaxonMedia",
path: "request_29_TaxonMedia",
pathFormatted: "req_request-29-taxo-1f5dd",
stats: {
    "name": "request_29_TaxonMedia",
    "numberOfRequests": {
        "total": "194",
        "ok": "189",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "176",
        "ok": "176",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "22019",
        "ok": "22019",
        "ko": "10491"
    },
    "meanResponseTime": {
        "total": "4472",
        "ok": "4321",
        "ko": "10159"
    },
    "standardDeviation": {
        "total": "3551",
        "ok": "3473",
        "ko": "190"
    },
    "percentiles1": {
        "total": "3511",
        "ok": "3244",
        "ko": "10024"
    },
    "percentiles2": {
        "total": "6719",
        "ok": "6536",
        "ko": "10252"
    },
    "percentiles3": {
        "total": "10300",
        "ok": "10177",
        "ko": "10443"
    },
    "percentiles4": {
        "total": "14844",
        "ok": "14901",
        "ko": "10481"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 26,
    "percentage": 13
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 155,
    "percentage": 80
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.739",
        "ko": "0.072"
    }
}
    },"req_request-20-taxo-0d791": {
        type: "REQUEST",
        name: "request_20_TaxonMedia",
path: "request_20_TaxonMedia",
pathFormatted: "req_request-20-taxo-0d791",
stats: {
    "name": "request_20_TaxonMedia",
    "numberOfRequests": {
        "total": "194",
        "ok": "190",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "10012"
    },
    "maxResponseTime": {
        "total": "21696",
        "ok": "21696",
        "ko": "11629"
    },
    "meanResponseTime": {
        "total": "5287",
        "ok": "5178",
        "ko": "10467"
    },
    "standardDeviation": {
        "total": "4132",
        "ok": "4105",
        "ko": "672"
    },
    "percentiles1": {
        "total": "4724",
        "ok": "4709",
        "ko": "10113"
    },
    "percentiles2": {
        "total": "7303",
        "ok": "7227",
        "ko": "10500"
    },
    "percentiles3": {
        "total": "12480",
        "ok": "12815",
        "ko": "11403"
    },
    "percentiles4": {
        "total": "20020",
        "ok": "20032",
        "ko": "11584"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 18,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 164,
    "percentage": 85
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.754",
        "ko": "0.058"
    }
}
    },"req_graphql-searchp-61328": {
        type: "REQUEST",
        name: "graphql_SearchPlaces",
path: "graphql_SearchPlaces",
pathFormatted: "req_graphql-searchp-61328",
stats: {
    "name": "graphql_SearchPlaces",
    "numberOfRequests": {
        "total": "194",
        "ok": "187",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "113",
        "ok": "113",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "10495",
        "ok": "10157",
        "ko": "10495"
    },
    "meanResponseTime": {
        "total": "3124",
        "ok": "2859",
        "ko": "10193"
    },
    "standardDeviation": {
        "total": "2800",
        "ok": "2489",
        "ko": "166"
    },
    "percentiles1": {
        "total": "1814",
        "ok": "1727",
        "ko": "10125"
    },
    "percentiles2": {
        "total": "3898",
        "ok": "3718",
        "ko": "10302"
    },
    "percentiles3": {
        "total": "9997",
        "ok": "8146",
        "ko": "10438"
    },
    "percentiles4": {
        "total": "10299",
        "ok": "10090",
        "ko": "10484"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 15,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 33,
    "percentage": 17
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 139,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.71",
        "ko": "0.101"
    }
}
    },"req_request-25-taxo-2081e": {
        type: "REQUEST",
        name: "request_25_TaxonMedia",
path: "request_25_TaxonMedia",
pathFormatted: "req_request-25-taxo-2081e",
stats: {
    "name": "request_25_TaxonMedia",
    "numberOfRequests": {
        "total": "194",
        "ok": "190",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "198",
        "ok": "198",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "25241",
        "ok": "25241",
        "ko": "10482"
    },
    "meanResponseTime": {
        "total": "5335",
        "ok": "5232",
        "ko": "10213"
    },
    "standardDeviation": {
        "total": "3893",
        "ok": "3868",
        "ko": "178"
    },
    "percentiles1": {
        "total": "5183",
        "ok": "5125",
        "ko": "10179"
    },
    "percentiles2": {
        "total": "7985",
        "ok": "7802",
        "ko": "10311"
    },
    "percentiles3": {
        "total": "10600",
        "ok": "10519",
        "ko": "10448"
    },
    "percentiles4": {
        "total": "18253",
        "ok": "18301",
        "ko": "10475"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 23,
    "percentage": 12
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 161,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.754",
        "ko": "0.058"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "194",
        "ok": "187",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "124",
        "ok": "124",
        "ko": "10008"
    },
    "maxResponseTime": {
        "total": "11476",
        "ok": "11476",
        "ko": "10750"
    },
    "meanResponseTime": {
        "total": "3413",
        "ok": "3156",
        "ko": "10285"
    },
    "standardDeviation": {
        "total": "3008",
        "ok": "2748",
        "ko": "291"
    },
    "percentiles1": {
        "total": "2028",
        "ok": "1936",
        "ko": "10124"
    },
    "percentiles2": {
        "total": "5563",
        "ok": "3969",
        "ko": "10500"
    },
    "percentiles3": {
        "total": "10083",
        "ok": "8913",
        "ko": "10734"
    },
    "percentiles4": {
        "total": "10796",
        "ok": "10324",
        "ko": "10747"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 16,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 31,
    "percentage": 16
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 140,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.71",
        "ko": "0.101"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "194",
        "ok": "188",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "69",
        "ok": "69",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "13421",
        "ok": "13421",
        "ko": "11043"
    },
    "meanResponseTime": {
        "total": "3157",
        "ok": "2928",
        "ko": "10349"
    },
    "standardDeviation": {
        "total": "2883",
        "ok": "2621",
        "ko": "364"
    },
    "percentiles1": {
        "total": "1817",
        "ok": "1785",
        "ko": "10266"
    },
    "percentiles2": {
        "total": "3874",
        "ok": "3748",
        "ko": "10477"
    },
    "percentiles3": {
        "total": "10013",
        "ok": "8810",
        "ko": "10905"
    },
    "percentiles4": {
        "total": "10531",
        "ok": "10165",
        "ko": "11015"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 13,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 35,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 140,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.725",
        "ko": "0.087"
    }
}
    },"req_graphql-news-4c309": {
        type: "REQUEST",
        name: "graphql_News",
path: "graphql_News",
pathFormatted: "req_graphql-news-4c309",
stats: {
    "name": "graphql_News",
    "numberOfRequests": {
        "total": "194",
        "ok": "183",
        "ko": "11"
    },
    "minResponseTime": {
        "total": "154",
        "ok": "154",
        "ko": "421"
    },
    "maxResponseTime": {
        "total": "15048",
        "ok": "15048",
        "ko": "13222"
    },
    "meanResponseTime": {
        "total": "3558",
        "ok": "3182",
        "ko": "9814"
    },
    "standardDeviation": {
        "total": "2934",
        "ok": "2460",
        "ko": "3106"
    },
    "percentiles1": {
        "total": "2321",
        "ok": "2249",
        "ko": "10304"
    },
    "percentiles2": {
        "total": "4193",
        "ok": "3928",
        "ko": "10762"
    },
    "percentiles3": {
        "total": "10173",
        "ok": "8141",
        "ko": "12434"
    },
    "percentiles4": {
        "total": "11756",
        "ok": "10371",
        "ko": "13064"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 9,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 7
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 161,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 11,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.652",
        "ko": "0.159"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "194",
        "ok": "188",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "109",
        "ok": "109",
        "ko": "10009"
    },
    "maxResponseTime": {
        "total": "13100",
        "ok": "13100",
        "ko": "11367"
    },
    "meanResponseTime": {
        "total": "3307",
        "ok": "3081",
        "ko": "10395"
    },
    "standardDeviation": {
        "total": "2953",
        "ok": "2709",
        "ko": "471"
    },
    "percentiles1": {
        "total": "1973",
        "ok": "1940",
        "ko": "10245"
    },
    "percentiles2": {
        "total": "4295",
        "ok": "3944",
        "ko": "10459"
    },
    "percentiles3": {
        "total": "10013",
        "ok": "8731",
        "ko": "11146"
    },
    "percentiles4": {
        "total": "10986",
        "ok": "10259",
        "ko": "11323"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 11,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 30,
    "percentage": 15
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 147,
    "percentage": 76
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.725",
        "ko": "0.087"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "194",
        "ok": "187",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "73",
        "ok": "73",
        "ko": "10009"
    },
    "maxResponseTime": {
        "total": "11609",
        "ok": "11045",
        "ko": "11609"
    },
    "meanResponseTime": {
        "total": "3041",
        "ok": "2763",
        "ko": "10481"
    },
    "standardDeviation": {
        "total": "2821",
        "ok": "2468",
        "ko": "645"
    },
    "percentiles1": {
        "total": "1800",
        "ok": "1767",
        "ko": "10102"
    },
    "percentiles2": {
        "total": "4338",
        "ok": "3803",
        "ko": "10810"
    },
    "percentiles3": {
        "total": "9968",
        "ok": "8452",
        "ko": "11536"
    },
    "percentiles4": {
        "total": "11068",
        "ok": "10079",
        "ko": "11594"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 24,
    "percentage": 12
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 29,
    "percentage": 15
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 134,
    "percentage": 69
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.71",
        "ko": "0.101"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "194",
        "ok": "188",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "105",
        "ok": "105",
        "ko": "422"
    },
    "maxResponseTime": {
        "total": "15419",
        "ok": "15419",
        "ko": "11033"
    },
    "meanResponseTime": {
        "total": "3273",
        "ok": "3103",
        "ko": "8588"
    },
    "standardDeviation": {
        "total": "2956",
        "ok": "2767",
        "ko": "3671"
    },
    "percentiles1": {
        "total": "1886",
        "ok": "1824",
        "ko": "10019"
    },
    "percentiles2": {
        "total": "4747",
        "ok": "4047",
        "ko": "10025"
    },
    "percentiles3": {
        "total": "9987",
        "ok": "8807",
        "ko": "10782"
    },
    "percentiles4": {
        "total": "11058",
        "ok": "10406",
        "ko": "10983"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 15,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 33,
    "percentage": 17
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 140,
    "percentage": 72
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.725",
        "ko": "0.087"
    }
}
    },"req_graphql-news-d7bbe": {
        type: "REQUEST",
        name: "graphQL_news",
path: "graphQL_news",
pathFormatted: "req_graphql-news-d7bbe",
stats: {
    "name": "graphQL_news",
    "numberOfRequests": {
        "total": "194",
        "ok": "189",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "113",
        "ok": "113",
        "ko": "423"
    },
    "maxResponseTime": {
        "total": "10492",
        "ok": "10145",
        "ko": "10492"
    },
    "meanResponseTime": {
        "total": "3509",
        "ok": "3385",
        "ko": "8195"
    },
    "standardDeviation": {
        "total": "2734",
        "ok": "2583",
        "ko": "3890"
    },
    "percentiles1": {
        "total": "2398",
        "ok": "2380",
        "ko": "10020"
    },
    "percentiles2": {
        "total": "4943",
        "ok": "4706",
        "ko": "10028"
    },
    "percentiles3": {
        "total": "9675",
        "ok": "8720",
        "ko": "10399"
    },
    "percentiles4": {
        "total": "10136",
        "ok": "10102",
        "ko": "10473"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 11,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 10
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 158,
    "percentage": 81
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.739",
        "ko": "0.072"
    }
}
    },"req_request-30-taxo-7acf0": {
        type: "REQUEST",
        name: "request_30_TaxonMedia",
path: "request_30_TaxonMedia",
pathFormatted: "req_request-30-taxo-7acf0",
stats: {
    "name": "request_30_TaxonMedia",
    "numberOfRequests": {
        "total": "194",
        "ok": "186",
        "ko": "8"
    },
    "minResponseTime": {
        "total": "196",
        "ok": "196",
        "ko": "10011"
    },
    "maxResponseTime": {
        "total": "31166",
        "ok": "31166",
        "ko": "11611"
    },
    "meanResponseTime": {
        "total": "5956",
        "ok": "5764",
        "ko": "10435"
    },
    "standardDeviation": {
        "total": "4453",
        "ok": "4446",
        "ko": "550"
    },
    "percentiles1": {
        "total": "5628",
        "ok": "5226",
        "ko": "10175"
    },
    "percentiles2": {
        "total": "8639",
        "ok": "8171",
        "ko": "10581"
    },
    "percentiles3": {
        "total": "12081",
        "ok": "12349",
        "ko": "11411"
    },
    "percentiles4": {
        "total": "19996",
        "ok": "20081",
        "ko": "11571"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 163,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 8,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.812",
        "ok": "2.696",
        "ko": "0.116"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
