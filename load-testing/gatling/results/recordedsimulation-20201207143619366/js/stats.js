var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4700",
        "ok": "4388",
        "ko": "312"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "11526"
    },
    "maxResponseTime": {
        "total": "51917",
        "ok": "51917",
        "ko": "21608"
    },
    "meanResponseTime": {
        "total": "16964",
        "ok": "17061",
        "ko": "15594"
    },
    "standardDeviation": {
        "total": "6833",
        "ok": "7017",
        "ko": "2956"
    },
    "percentiles1": {
        "total": "16252",
        "ok": "16293",
        "ko": "16001"
    },
    "percentiles2": {
        "total": "20653",
        "ok": "20883",
        "ko": "17498"
    },
    "percentiles3": {
        "total": "29138",
        "ok": "29839",
        "ko": "21132"
    },
    "percentiles4": {
        "total": "35467",
        "ok": "35601",
        "ko": "21598"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 72,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 54,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4262,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "count": 312,
    "percentage": 7
},
    "meanNumberOfRequestsPerSecond": {
        "total": "88.679",
        "ok": "82.792",
        "ko": "5.887"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "488",
        "ok": "488",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4280",
        "ok": "4280",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1340",
        "ok": "1340",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "804",
        "ok": "804",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1436",
        "ok": "1436",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3224",
        "ok": "3224",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3977",
        "ok": "3977",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 16,
    "percentage": 16
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 48,
    "percentage": 48
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 36,
    "percentage": 36
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.887",
        "ko": "-"
    }
}
    },"req_graphql-news-d7bbe": {
        type: "REQUEST",
        name: "graphQL_news",
path: "graphQL_news",
pathFormatted: "req_graphql-news-d7bbe",
stats: {
    "name": "graphQL_news",
    "numberOfRequests": {
        "total": "100",
        "ok": "96",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "572",
        "ok": "572",
        "ko": "11574"
    },
    "maxResponseTime": {
        "total": "46145",
        "ok": "46145",
        "ko": "17056"
    },
    "meanResponseTime": {
        "total": "13661",
        "ok": "13636",
        "ko": "14269"
    },
    "standardDeviation": {
        "total": "5628",
        "ok": "5724",
        "ko": "2242"
    },
    "percentiles1": {
        "total": "13649",
        "ok": "13649",
        "ko": "14223"
    },
    "percentiles2": {
        "total": "14532",
        "ok": "14480",
        "ko": "16125"
    },
    "percentiles3": {
        "total": "22094",
        "ok": "22781",
        "ko": "16870"
    },
    "percentiles4": {
        "total": "26734",
        "ok": "27518",
        "ko": "17019"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 95,
    "percentage": 95
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.811",
        "ko": "0.075"
    }
}
    },"req_request-28-taxo-f261b": {
        type: "REQUEST",
        name: "request_28_TaxonMedia",
path: "request_28_TaxonMedia",
pathFormatted: "req_request-28-taxo-f261b",
stats: {
    "name": "request_28_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "96",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "4682",
        "ok": "4682",
        "ko": "12631"
    },
    "maxResponseTime": {
        "total": "47742",
        "ok": "47742",
        "ko": "17499"
    },
    "meanResponseTime": {
        "total": "20592",
        "ok": "20826",
        "ko": "14978"
    },
    "standardDeviation": {
        "total": "6274",
        "ok": "6278",
        "ko": "2347"
    },
    "percentiles1": {
        "total": "21196",
        "ok": "21276",
        "ko": "14890"
    },
    "percentiles2": {
        "total": "23257",
        "ok": "23328",
        "ko": "17233"
    },
    "percentiles3": {
        "total": "30840",
        "ok": "30982",
        "ko": "17446"
    },
    "percentiles4": {
        "total": "36699",
        "ok": "37145",
        "ko": "17488"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 96,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.811",
        "ko": "0.075"
    }
}
    },"req_favicon-16x16-p-7181b": {
        type: "REQUEST",
        name: "favicon-16x16.png",
path: "favicon-16x16.png",
pathFormatted: "req_favicon-16x16-p-7181b",
stats: {
    "name": "favicon-16x16.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "87",
        "ko": "13"
    },
    "minResponseTime": {
        "total": "501",
        "ok": "501",
        "ko": "12636"
    },
    "maxResponseTime": {
        "total": "21118",
        "ok": "17994",
        "ko": "21118"
    },
    "meanResponseTime": {
        "total": "14161",
        "ok": "13799",
        "ko": "16579"
    },
    "standardDeviation": {
        "total": "2165",
        "ok": "1833",
        "ko": "2619"
    },
    "percentiles1": {
        "total": "14144",
        "ok": "14092",
        "ko": "16318"
    },
    "percentiles2": {
        "total": "14664",
        "ok": "14533",
        "ko": "18838"
    },
    "percentiles3": {
        "total": "17750",
        "ok": "15389",
        "ko": "20194"
    },
    "percentiles4": {
        "total": "19593",
        "ok": "17030",
        "ko": "20933"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 86,
    "percentage": 86
},
    "group4": {
    "name": "failed",
    "count": 13,
    "percentage": 13
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.642",
        "ko": "0.245"
    }
}
    },"req_request-23-taxo-3ea91": {
        type: "REQUEST",
        name: "request_23_TaxonMedia",
path: "request_23_TaxonMedia",
pathFormatted: "req_request-23-taxo-3ea91",
stats: {
    "name": "request_23_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "93",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "6208",
        "ok": "6208",
        "ko": "11566"
    },
    "maxResponseTime": {
        "total": "40671",
        "ok": "40671",
        "ko": "17683"
    },
    "meanResponseTime": {
        "total": "17578",
        "ok": "17792",
        "ko": "14738"
    },
    "standardDeviation": {
        "total": "5451",
        "ok": "5561",
        "ko": "2236"
    },
    "percentiles1": {
        "total": "17610",
        "ok": "17990",
        "ko": "15817"
    },
    "percentiles2": {
        "total": "19617",
        "ok": "19760",
        "ko": "16416"
    },
    "percentiles3": {
        "total": "28774",
        "ok": "29141",
        "ko": "17469"
    },
    "percentiles4": {
        "total": "33395",
        "ok": "33910",
        "ko": "17640"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 7
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.755",
        "ko": "0.132"
    }
}
    },"req_graphql-riveran-e540f": {
        type: "REQUEST",
        name: "graphql_RiverAndGroundWaterObservation",
path: "graphql_RiverAndGroundWaterObservation",
pathFormatted: "req_graphql-riveran-e540f",
stats: {
    "name": "graphql_RiverAndGroundWaterObservation",
    "numberOfRequests": {
        "total": "100",
        "ok": "94",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "4718",
        "ok": "4718",
        "ko": "11529"
    },
    "maxResponseTime": {
        "total": "33666",
        "ok": "33666",
        "ko": "17591"
    },
    "meanResponseTime": {
        "total": "17212",
        "ok": "17381",
        "ko": "14565"
    },
    "standardDeviation": {
        "total": "5886",
        "ok": "6002",
        "ko": "2386"
    },
    "percentiles1": {
        "total": "17608",
        "ok": "17811",
        "ko": "14249"
    },
    "percentiles2": {
        "total": "20970",
        "ok": "21128",
        "ko": "16822"
    },
    "percentiles3": {
        "total": "26062",
        "ok": "26960",
        "ko": "17479"
    },
    "percentiles4": {
        "total": "31234",
        "ok": "31381",
        "ko": "17569"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.774",
        "ko": "0.113"
    }
}
    },"req_request-30-taxo-7acf0": {
        type: "REQUEST",
        name: "request_30_TaxonMedia",
path: "request_30_TaxonMedia",
pathFormatted: "req_request-30-taxo-7acf0",
stats: {
    "name": "request_30_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "93",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "4706",
        "ok": "4706",
        "ko": "11529"
    },
    "maxResponseTime": {
        "total": "35365",
        "ok": "35365",
        "ko": "17491"
    },
    "meanResponseTime": {
        "total": "17722",
        "ok": "18017",
        "ko": "13802"
    },
    "standardDeviation": {
        "total": "5712",
        "ok": "5782",
        "ko": "2325"
    },
    "percentiles1": {
        "total": "18437",
        "ok": "18797",
        "ko": "12636"
    },
    "percentiles2": {
        "total": "21253",
        "ok": "21507",
        "ko": "15380"
    },
    "percentiles3": {
        "total": "25925",
        "ok": "25943",
        "ko": "17393"
    },
    "percentiles4": {
        "total": "29693",
        "ok": "30094",
        "ko": "17471"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 7
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.755",
        "ko": "0.132"
    }
}
    },"req_app-css-13ea8": {
        type: "REQUEST",
        name: "App.css",
path: "App.css",
pathFormatted: "req_app-css-13ea8",
stats: {
    "name": "App.css",
    "numberOfRequests": {
        "total": "100",
        "ok": "87",
        "ko": "13"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "12637"
    },
    "maxResponseTime": {
        "total": "39906",
        "ok": "39906",
        "ko": "21604"
    },
    "meanResponseTime": {
        "total": "16424",
        "ok": "16354",
        "ko": "16893"
    },
    "standardDeviation": {
        "total": "5763",
        "ok": "6070",
        "ko": "2936"
    },
    "percentiles1": {
        "total": "16138",
        "ok": "16089",
        "ko": "16354"
    },
    "percentiles2": {
        "total": "18939",
        "ok": "18858",
        "ko": "18975"
    },
    "percentiles3": {
        "total": "23721",
        "ok": "24443",
        "ko": "21337"
    },
    "percentiles4": {
        "total": "32410",
        "ok": "33394",
        "ko": "21551"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 82,
    "percentage": 82
},
    "group4": {
    "name": "failed",
    "count": 13,
    "percentage": 13
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.642",
        "ko": "0.245"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "100",
        "ok": "93",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "406",
        "ok": "406",
        "ko": "11565"
    },
    "maxResponseTime": {
        "total": "51782",
        "ok": "51782",
        "ko": "18244"
    },
    "meanResponseTime": {
        "total": "18482",
        "ok": "18776",
        "ko": "14580"
    },
    "standardDeviation": {
        "total": "7976",
        "ok": "8163",
        "ko": "2705"
    },
    "percentiles1": {
        "total": "18369",
        "ok": "18670",
        "ko": "12636"
    },
    "percentiles2": {
        "total": "21092",
        "ok": "21566",
        "ko": "17374"
    },
    "percentiles3": {
        "total": "31108",
        "ok": "31339",
        "ko": "18019"
    },
    "percentiles4": {
        "total": "46982",
        "ok": "47322",
        "ko": "18199"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 91,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 7
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.755",
        "ko": "0.132"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "100",
        "ok": "96",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "243",
        "ok": "243",
        "ko": "12631"
    },
    "maxResponseTime": {
        "total": "21387",
        "ok": "21387",
        "ko": "17603"
    },
    "meanResponseTime": {
        "total": "12899",
        "ok": "12812",
        "ko": "14989"
    },
    "standardDeviation": {
        "total": "3049",
        "ok": "3043",
        "ko": "2362"
    },
    "percentiles1": {
        "total": "13537",
        "ok": "13537",
        "ko": "14860"
    },
    "percentiles2": {
        "total": "14176",
        "ok": "14133",
        "ko": "17214"
    },
    "percentiles3": {
        "total": "15540",
        "ok": "15460",
        "ko": "17525"
    },
    "percentiles4": {
        "total": "17641",
        "ok": "17095",
        "ko": "17587"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.811",
        "ko": "0.075"
    }
}
    },"req_request-36-9ad97": {
        type: "REQUEST",
        name: "request_36",
path: "request_36",
pathFormatted: "req_request-36-9ad97",
stats: {
    "name": "request_36",
    "numberOfRequests": {
        "total": "100",
        "ok": "94",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "396",
        "ok": "396",
        "ko": "11529"
    },
    "maxResponseTime": {
        "total": "37539",
        "ok": "37539",
        "ko": "17505"
    },
    "meanResponseTime": {
        "total": "16686",
        "ok": "16868",
        "ko": "13837"
    },
    "standardDeviation": {
        "total": "6593",
        "ok": "6730",
        "ko": "2511"
    },
    "percentiles1": {
        "total": "17349",
        "ok": "17468",
        "ko": "12634"
    },
    "percentiles2": {
        "total": "19954",
        "ok": "20072",
        "ko": "16024"
    },
    "percentiles3": {
        "total": "27783",
        "ok": "27866",
        "ko": "17417"
    },
    "percentiles4": {
        "total": "33961",
        "ok": "34178",
        "ko": "17487"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 89,
    "percentage": 89
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.774",
        "ko": "0.113"
    }
}
    },"req_graphql-taxon-f4085": {
        type: "REQUEST",
        name: "graphql_Taxon",
path: "graphql_Taxon",
pathFormatted: "req_graphql-taxon-f4085",
stats: {
    "name": "graphql_Taxon",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "268",
        "ok": "268",
        "ko": "11566"
    },
    "maxResponseTime": {
        "total": "35635",
        "ok": "35635",
        "ko": "21084"
    },
    "meanResponseTime": {
        "total": "14392",
        "ok": "14308",
        "ko": "15984"
    },
    "standardDeviation": {
        "total": "5001",
        "ok": "5054",
        "ko": "3476"
    },
    "percentiles1": {
        "total": "14125",
        "ok": "14077",
        "ko": "17138"
    },
    "percentiles2": {
        "total": "16505",
        "ok": "16280",
        "ko": "17502"
    },
    "percentiles3": {
        "total": "21453",
        "ok": "21498",
        "ko": "20368"
    },
    "percentiles4": {
        "total": "28717",
        "ok": "29066",
        "ko": "20941"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_request-6-font-15e7e": {
        type: "REQUEST",
        name: "request_6_font",
path: "request_6_font",
pathFormatted: "req_request-6-font-15e7e",
stats: {
    "name": "request_6_font",
    "numberOfRequests": {
        "total": "100",
        "ok": "94",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "12237"
    },
    "maxResponseTime": {
        "total": "42633",
        "ok": "42633",
        "ko": "21097"
    },
    "meanResponseTime": {
        "total": "17063",
        "ok": "17147",
        "ko": "15738"
    },
    "standardDeviation": {
        "total": "6585",
        "ok": "6735",
        "ko": "3190"
    },
    "percentiles1": {
        "total": "17630",
        "ok": "17720",
        "ko": "15403"
    },
    "percentiles2": {
        "total": "20555",
        "ok": "20602",
        "ko": "17545"
    },
    "percentiles3": {
        "total": "27673",
        "ok": "27858",
        "ko": "20237"
    },
    "percentiles4": {
        "total": "33155",
        "ok": "33729",
        "ko": "20925"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 91,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.774",
        "ko": "0.113"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "329",
        "ok": "329",
        "ko": "12631"
    },
    "maxResponseTime": {
        "total": "37505",
        "ok": "37505",
        "ko": "17659"
    },
    "meanResponseTime": {
        "total": "14354",
        "ok": "14308",
        "ko": "15217"
    },
    "standardDeviation": {
        "total": "5259",
        "ok": "5369",
        "ko": "2189"
    },
    "percentiles1": {
        "total": "14025",
        "ok": "13992",
        "ko": "15899"
    },
    "percentiles2": {
        "total": "15297",
        "ok": "15064",
        "ko": "17262"
    },
    "percentiles3": {
        "total": "21368",
        "ok": "21865",
        "ko": "17580"
    },
    "percentiles4": {
        "total": "33243",
        "ok": "33458",
        "ko": "17643"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 92,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_graphql-news-4c309": {
        type: "REQUEST",
        name: "graphql_News",
path: "graphql_News",
pathFormatted: "req_graphql-news-4c309",
stats: {
    "name": "graphql_News",
    "numberOfRequests": {
        "total": "100",
        "ok": "96",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "1260",
        "ok": "1260",
        "ko": "12631"
    },
    "maxResponseTime": {
        "total": "27517",
        "ok": "27517",
        "ko": "17634"
    },
    "meanResponseTime": {
        "total": "13546",
        "ok": "13484",
        "ko": "15038"
    },
    "standardDeviation": {
        "total": "3728",
        "ok": "3760",
        "ko": "2408"
    },
    "percentiles1": {
        "total": "13676",
        "ok": "13676",
        "ko": "14943"
    },
    "percentiles2": {
        "total": "14528",
        "ok": "14499",
        "ko": "17345"
    },
    "percentiles3": {
        "total": "20328",
        "ok": "20453",
        "ko": "17576"
    },
    "percentiles4": {
        "total": "22585",
        "ok": "22784",
        "ko": "17622"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 96,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.811",
        "ko": "0.075"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "100",
        "ok": "94",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "1241",
        "ok": "1241",
        "ko": "11529"
    },
    "maxResponseTime": {
        "total": "36168",
        "ok": "36168",
        "ko": "17598"
    },
    "meanResponseTime": {
        "total": "18472",
        "ok": "18760",
        "ko": "13952"
    },
    "standardDeviation": {
        "total": "6331",
        "ok": "6393",
        "ko": "2427"
    },
    "percentiles1": {
        "total": "19324",
        "ok": "19630",
        "ko": "12634"
    },
    "percentiles2": {
        "total": "21952",
        "ok": "22336",
        "ko": "15969"
    },
    "percentiles3": {
        "total": "27424",
        "ok": "27549",
        "ko": "17468"
    },
    "percentiles4": {
        "total": "33774",
        "ok": "33919",
        "ko": "17572"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.774",
        "ko": "0.113"
    }
}
    },"req_synaptix-client-ff009": {
        type: "REQUEST",
        name: "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
path: "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
pathFormatted: "req_synaptix-client-ff009",
stats: {
    "name": "synaptix-client-toolkit.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "86",
        "ko": "14"
    },
    "minResponseTime": {
        "total": "340",
        "ok": "340",
        "ko": "12636"
    },
    "maxResponseTime": {
        "total": "33759",
        "ok": "33759",
        "ko": "21538"
    },
    "meanResponseTime": {
        "total": "17002",
        "ok": "16970",
        "ko": "17197"
    },
    "standardDeviation": {
        "total": "5001",
        "ok": "5252",
        "ko": "3022"
    },
    "percentiles1": {
        "total": "16660",
        "ok": "16660",
        "ko": "16756"
    },
    "percentiles2": {
        "total": "19744",
        "ok": "19858",
        "ko": "19505"
    },
    "percentiles3": {
        "total": "24816",
        "ok": "25372",
        "ko": "21302"
    },
    "percentiles4": {
        "total": "28699",
        "ok": "29415",
        "ko": "21491"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 83,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 14,
    "percentage": 14
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.623",
        "ko": "0.264"
    }
}
    },"req_vendors-e4fad40-0b16d": {
        type: "REQUEST",
        name: "vendors.e4fad40d75305c1c3562.js",
path: "vendors.e4fad40d75305c1c3562.js",
pathFormatted: "req_vendors-e4fad40-0b16d",
stats: {
    "name": "vendors.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "87",
        "ko": "13"
    },
    "minResponseTime": {
        "total": "12634",
        "ok": "13640",
        "ko": "12634"
    },
    "maxResponseTime": {
        "total": "39376",
        "ok": "39376",
        "ko": "21608"
    },
    "meanResponseTime": {
        "total": "30195",
        "ok": "32196",
        "ko": "16808"
    },
    "standardDeviation": {
        "total": "6195",
        "ok": "3458",
        "ko": "3028"
    },
    "percentiles1": {
        "total": "32474",
        "ok": "33083",
        "ko": "16304"
    },
    "percentiles2": {
        "total": "33959",
        "ok": "34011",
        "ko": "18978"
    },
    "percentiles3": {
        "total": "34571",
        "ok": "34633",
        "ko": "21332"
    },
    "percentiles4": {
        "total": "35860",
        "ok": "36321",
        "ko": "21553"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 87,
    "percentage": 87
},
    "group4": {
    "name": "failed",
    "count": 13,
    "percentage": 13
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.642",
        "ko": "0.245"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "487",
        "ok": "487",
        "ko": "11530"
    },
    "maxResponseTime": {
        "total": "40154",
        "ok": "40154",
        "ko": "17493"
    },
    "meanResponseTime": {
        "total": "20948",
        "ok": "21297",
        "ko": "14309"
    },
    "standardDeviation": {
        "total": "7057",
        "ok": "7046",
        "ko": "2536"
    },
    "percentiles1": {
        "total": "22195",
        "ok": "22481",
        "ko": "12637"
    },
    "percentiles2": {
        "total": "24297",
        "ok": "24712",
        "ko": "17255"
    },
    "percentiles3": {
        "total": "32620",
        "ok": "32702",
        "ko": "17445"
    },
    "percentiles4": {
        "total": "34224",
        "ok": "34523",
        "ko": "17483"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_request-35-3745a": {
        type: "REQUEST",
        name: "request_35",
path: "request_35",
pathFormatted: "req_request-35-3745a",
stats: {
    "name": "request_35",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "338",
        "ok": "338",
        "ko": "11566"
    },
    "maxResponseTime": {
        "total": "51917",
        "ok": "51917",
        "ko": "17498"
    },
    "meanResponseTime": {
        "total": "19559",
        "ok": "19835",
        "ko": "14315"
    },
    "standardDeviation": {
        "total": "7829",
        "ok": "7916",
        "ko": "2527"
    },
    "percentiles1": {
        "total": "19196",
        "ok": "19576",
        "ko": "12636"
    },
    "percentiles2": {
        "total": "22295",
        "ok": "22477",
        "ko": "17246"
    },
    "percentiles3": {
        "total": "32220",
        "ok": "32406",
        "ko": "17448"
    },
    "percentiles4": {
        "total": "46723",
        "ok": "46986",
        "ko": "17488"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "100",
        "ok": "94",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "305",
        "ok": "305",
        "ko": "11529"
    },
    "maxResponseTime": {
        "total": "38824",
        "ok": "38824",
        "ko": "17640"
    },
    "meanResponseTime": {
        "total": "17840",
        "ok": "18095",
        "ko": "13848"
    },
    "standardDeviation": {
        "total": "6598",
        "ok": "6694",
        "ko": "2530"
    },
    "percentiles1": {
        "total": "18103",
        "ok": "18367",
        "ko": "12634"
    },
    "percentiles2": {
        "total": "20787",
        "ok": "21240",
        "ko": "15974"
    },
    "percentiles3": {
        "total": "28061",
        "ok": "29814",
        "ko": "17502"
    },
    "percentiles4": {
        "total": "37556",
        "ok": "37633",
        "ko": "17612"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 92,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.774",
        "ko": "0.113"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "437",
        "ok": "437",
        "ko": "12631"
    },
    "maxResponseTime": {
        "total": "35383",
        "ok": "35383",
        "ko": "21094"
    },
    "meanResponseTime": {
        "total": "21301",
        "ok": "21537",
        "ko": "16825"
    },
    "standardDeviation": {
        "total": "7360",
        "ok": "7451",
        "ko": "2734"
    },
    "percentiles1": {
        "total": "23042",
        "ok": "23139",
        "ko": "17075"
    },
    "percentiles2": {
        "total": "25222",
        "ok": "25341",
        "ko": "17515"
    },
    "percentiles3": {
        "total": "31897",
        "ok": "31989",
        "ko": "20378"
    },
    "percentiles4": {
        "total": "35125",
        "ok": "35138",
        "ko": "20951"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 92,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_coast-228x228-p-35307": {
        type: "REQUEST",
        name: "coast-228x228.png",
path: "coast-228x228.png",
pathFormatted: "req_coast-228x228-p-35307",
stats: {
    "name": "coast-228x228.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "85",
        "ko": "15"
    },
    "minResponseTime": {
        "total": "278",
        "ok": "278",
        "ko": "12636"
    },
    "maxResponseTime": {
        "total": "36058",
        "ok": "36058",
        "ko": "21583"
    },
    "meanResponseTime": {
        "total": "17873",
        "ok": "18040",
        "ko": "16931"
    },
    "standardDeviation": {
        "total": "6220",
        "ok": "6621",
        "ko": "2914"
    },
    "percentiles1": {
        "total": "17411",
        "ok": "17670",
        "ko": "16308"
    },
    "percentiles2": {
        "total": "20351",
        "ok": "20634",
        "ko": "19279"
    },
    "percentiles3": {
        "total": "30709",
        "ok": "31843",
        "ko": "21306"
    },
    "percentiles4": {
        "total": "35473",
        "ok": "35562",
        "ko": "21528"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 83,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 15,
    "percentage": 15
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.604",
        "ko": "0.283"
    }
}
    },"req_request-26-taxo-41b6f": {
        type: "REQUEST",
        name: "request_26_TaxonMedia",
path: "request_26_TaxonMedia",
pathFormatted: "req_request-26-taxo-41b6f",
stats: {
    "name": "request_26_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "94",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "4718",
        "ok": "4718",
        "ko": "11526"
    },
    "maxResponseTime": {
        "total": "40427",
        "ok": "40427",
        "ko": "17592"
    },
    "meanResponseTime": {
        "total": "17662",
        "ok": "17860",
        "ko": "14559"
    },
    "standardDeviation": {
        "total": "5381",
        "ok": "5458",
        "ko": "2384"
    },
    "percentiles1": {
        "total": "17914",
        "ok": "18315",
        "ko": "14229"
    },
    "percentiles2": {
        "total": "20088",
        "ok": "20174",
        "ko": "16816"
    },
    "percentiles3": {
        "total": "26221",
        "ok": "26773",
        "ko": "17481"
    },
    "percentiles4": {
        "total": "30437",
        "ok": "31042",
        "ko": "17570"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.774",
        "ko": "0.113"
    }
}
    },"req_request-25-taxo-2081e": {
        type: "REQUEST",
        name: "request_25_TaxonMedia",
path: "request_25_TaxonMedia",
pathFormatted: "req_request-25-taxo-2081e",
stats: {
    "name": "request_25_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "4523",
        "ok": "4523",
        "ko": "11528"
    },
    "maxResponseTime": {
        "total": "43108",
        "ok": "43108",
        "ko": "17650"
    },
    "meanResponseTime": {
        "total": "17664",
        "ok": "17839",
        "ko": "14341"
    },
    "standardDeviation": {
        "total": "6134",
        "ok": "6217",
        "ko": "2578"
    },
    "percentiles1": {
        "total": "17599",
        "ok": "17811",
        "ko": "12635"
    },
    "percentiles2": {
        "total": "20970",
        "ok": "21192",
        "ko": "17260"
    },
    "percentiles3": {
        "total": "27736",
        "ok": "28045",
        "ko": "17572"
    },
    "percentiles4": {
        "total": "33290",
        "ok": "33786",
        "ko": "17634"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 95,
    "percentage": 95
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_graphql-searchp-61328": {
        type: "REQUEST",
        name: "graphql_SearchPlaces",
path: "graphql_SearchPlaces",
pathFormatted: "req_graphql-searchp-61328",
stats: {
    "name": "graphql_SearchPlaces",
    "numberOfRequests": {
        "total": "100",
        "ok": "96",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "3898",
        "ok": "3898",
        "ko": "12632"
    },
    "maxResponseTime": {
        "total": "17771",
        "ok": "17771",
        "ko": "17489"
    },
    "meanResponseTime": {
        "total": "12783",
        "ok": "12691",
        "ko": "14993"
    },
    "standardDeviation": {
        "total": "2443",
        "ok": "2402",
        "ko": "2361"
    },
    "percentiles1": {
        "total": "13432",
        "ok": "13432",
        "ko": "14926"
    },
    "percentiles2": {
        "total": "14084",
        "ok": "14070",
        "ko": "17284"
    },
    "percentiles3": {
        "total": "15535",
        "ok": "15220",
        "ko": "17448"
    },
    "percentiles4": {
        "total": "17492",
        "ok": "15676",
        "ko": "17481"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 96,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.811",
        "ko": "0.075"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "410",
        "ok": "410",
        "ko": "11566"
    },
    "maxResponseTime": {
        "total": "17505",
        "ok": "16242",
        "ko": "17505"
    },
    "meanResponseTime": {
        "total": "12968",
        "ok": "12896",
        "ko": "14328"
    },
    "standardDeviation": {
        "total": "2129",
        "ok": "2081",
        "ko": "2543"
    },
    "percentiles1": {
        "total": "13452",
        "ok": "13466",
        "ko": "12635"
    },
    "percentiles2": {
        "total": "14074",
        "ok": "14072",
        "ko": "17305"
    },
    "percentiles3": {
        "total": "14713",
        "ok": "14488",
        "ko": "17465"
    },
    "percentiles4": {
        "total": "17307",
        "ok": "15607",
        "ko": "17497"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "100",
        "ok": "92",
        "ko": "8"
    },
    "minResponseTime": {
        "total": "6264",
        "ok": "6264",
        "ko": "11529"
    },
    "maxResponseTime": {
        "total": "50378",
        "ok": "50378",
        "ko": "17586"
    },
    "meanResponseTime": {
        "total": "17335",
        "ok": "17576",
        "ko": "14567"
    },
    "standardDeviation": {
        "total": "5941",
        "ok": "6095",
        "ko": "2374"
    },
    "percentiles1": {
        "total": "16857",
        "ok": "17013",
        "ko": "14244"
    },
    "percentiles2": {
        "total": "19901",
        "ok": "20142",
        "ko": "16944"
    },
    "percentiles3": {
        "total": "26861",
        "ok": "26888",
        "ko": "17453"
    },
    "percentiles4": {
        "total": "32245",
        "ok": "33710",
        "ko": "17559"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 92,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 8,
    "percentage": 8
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.736",
        "ko": "0.151"
    }
}
    },"req_request-20-taxo-0d791": {
        type: "REQUEST",
        name: "request_20_TaxonMedia",
path: "request_20_TaxonMedia",
pathFormatted: "req_request-20-taxo-0d791",
stats: {
    "name": "request_20_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "96",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "4566",
        "ok": "4566",
        "ko": "12632"
    },
    "maxResponseTime": {
        "total": "37160",
        "ok": "37160",
        "ko": "21086"
    },
    "meanResponseTime": {
        "total": "17604",
        "ok": "17624",
        "ko": "17135"
    },
    "standardDeviation": {
        "total": "5220",
        "ok": "5291",
        "ko": "3007"
    },
    "percentiles1": {
        "total": "17036",
        "ok": "16892",
        "ko": "17410"
    },
    "percentiles2": {
        "total": "19760",
        "ok": "19760",
        "ko": "18512"
    },
    "percentiles3": {
        "total": "27997",
        "ok": "28010",
        "ko": "20571"
    },
    "percentiles4": {
        "total": "30738",
        "ok": "30997",
        "ko": "20983"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 96,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.811",
        "ko": "0.075"
    }
}
    },"req_request-29-taxo-1f5dd": {
        type: "REQUEST",
        name: "request_29_TaxonMedia",
path: "request_29_TaxonMedia",
pathFormatted: "req_request-29-taxo-1f5dd",
stats: {
    "name": "request_29_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "94",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "11528"
    },
    "maxResponseTime": {
        "total": "46182",
        "ok": "46182",
        "ko": "17647"
    },
    "meanResponseTime": {
        "total": "16873",
        "ok": "17019",
        "ko": "14584"
    },
    "standardDeviation": {
        "total": "5553",
        "ok": "5664",
        "ko": "2405"
    },
    "percentiles1": {
        "total": "16017",
        "ok": "16114",
        "ko": "14269"
    },
    "percentiles2": {
        "total": "19036",
        "ok": "19400",
        "ko": "16847"
    },
    "percentiles3": {
        "total": "26513",
        "ok": "26592",
        "ko": "17526"
    },
    "percentiles4": {
        "total": "31243",
        "ok": "32148",
        "ko": "17623"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 94,
    "percentage": 94
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.774",
        "ko": "0.113"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "100",
        "ok": "99",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "6410",
        "ok": "6410",
        "ko": "16306"
    },
    "maxResponseTime": {
        "total": "23061",
        "ok": "23061",
        "ko": "16306"
    },
    "meanResponseTime": {
        "total": "16478",
        "ok": "16480",
        "ko": "16306"
    },
    "standardDeviation": {
        "total": "3214",
        "ok": "3230",
        "ko": "0"
    },
    "percentiles1": {
        "total": "17200",
        "ok": "17201",
        "ko": "16306"
    },
    "percentiles2": {
        "total": "18696",
        "ok": "18698",
        "ko": "16306"
    },
    "percentiles3": {
        "total": "20867",
        "ok": "20869",
        "ko": "16306"
    },
    "percentiles4": {
        "total": "21864",
        "ok": "21876",
        "ko": "16306"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 99,
    "percentage": 99
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.868",
        "ko": "0.019"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "458",
        "ok": "458",
        "ko": "11528"
    },
    "maxResponseTime": {
        "total": "41871",
        "ok": "41871",
        "ko": "17596"
    },
    "meanResponseTime": {
        "total": "22051",
        "ok": "22517",
        "ko": "13191"
    },
    "standardDeviation": {
        "total": "7907",
        "ok": "7823",
        "ko": "2255"
    },
    "percentiles1": {
        "total": "24475",
        "ok": "24652",
        "ko": "12631"
    },
    "percentiles2": {
        "total": "25978",
        "ok": "26065",
        "ko": "12636"
    },
    "percentiles3": {
        "total": "32779",
        "ok": "33058",
        "ko": "16604"
    },
    "percentiles4": {
        "total": "37475",
        "ok": "37697",
        "ko": "17398"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 92,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_graphql-weather-e1b31": {
        type: "REQUEST",
        name: "graphql_weather",
path: "graphql_weather",
pathFormatted: "req_graphql-weather-e1b31",
stats: {
    "name": "graphql_weather",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "1100",
        "ok": "1100",
        "ko": "11530"
    },
    "maxResponseTime": {
        "total": "34970",
        "ok": "34970",
        "ko": "17614"
    },
    "meanResponseTime": {
        "total": "14113",
        "ok": "14103",
        "ko": "14300"
    },
    "standardDeviation": {
        "total": "4412",
        "ok": "4489",
        "ko": "2528"
    },
    "percentiles1": {
        "total": "13813",
        "ok": "13829",
        "ko": "12637"
    },
    "percentiles2": {
        "total": "15033",
        "ok": "14916",
        "ko": "17086"
    },
    "percentiles3": {
        "total": "20456",
        "ok": "20778",
        "ko": "17508"
    },
    "percentiles4": {
        "total": "34368",
        "ok": "34398",
        "ko": "17593"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_request-21-taxo-4adce": {
        type: "REQUEST",
        name: "request_21_TaxonMedia",
path: "request_21_TaxonMedia",
pathFormatted: "req_request-21-taxo-4adce",
stats: {
    "name": "request_21_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "96",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "4709",
        "ok": "4709",
        "ko": "12631"
    },
    "maxResponseTime": {
        "total": "31442",
        "ok": "31442",
        "ko": "17508"
    },
    "meanResponseTime": {
        "total": "17162",
        "ok": "17253",
        "ko": "14982"
    },
    "standardDeviation": {
        "total": "4679",
        "ok": "4730",
        "ko": "2351"
    },
    "percentiles1": {
        "total": "17894",
        "ok": "18090",
        "ko": "14894"
    },
    "percentiles2": {
        "total": "19813",
        "ok": "19890",
        "ko": "17240"
    },
    "percentiles3": {
        "total": "23086",
        "ok": "23539",
        "ko": "17454"
    },
    "percentiles4": {
        "total": "29223",
        "ok": "29313",
        "ko": "17497"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 96,
    "percentage": 96
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.811",
        "ko": "0.075"
    }
}
    },"req_request-4-matom-e3907": {
        type: "REQUEST",
        name: "request_4_matomo",
path: "request_4_matomo",
pathFormatted: "req_request-4-matom-e3907",
stats: {
    "name": "request_4_matomo",
    "numberOfRequests": {
        "total": "100",
        "ok": "98",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "4707",
        "ok": "4707",
        "ko": "16253"
    },
    "maxResponseTime": {
        "total": "27712",
        "ok": "27712",
        "ko": "17304"
    },
    "meanResponseTime": {
        "total": "14205",
        "ok": "14153",
        "ko": "16779"
    },
    "standardDeviation": {
        "total": "2729",
        "ok": "2731",
        "ko": "526"
    },
    "percentiles1": {
        "total": "14043",
        "ok": "14003",
        "ko": "16779"
    },
    "percentiles2": {
        "total": "14990",
        "ok": "14719",
        "ko": "17041"
    },
    "percentiles3": {
        "total": "17699",
        "ok": "17856",
        "ko": "17251"
    },
    "percentiles4": {
        "total": "19647",
        "ok": "19810",
        "ko": "17293"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 98,
    "percentage": 98
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.849",
        "ko": "0.038"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "100",
        "ok": "92",
        "ko": "8"
    },
    "minResponseTime": {
        "total": "4596",
        "ok": "4596",
        "ko": "11529"
    },
    "maxResponseTime": {
        "total": "39604",
        "ok": "39604",
        "ko": "21088"
    },
    "meanResponseTime": {
        "total": "20861",
        "ok": "21336",
        "ko": "15403"
    },
    "standardDeviation": {
        "total": "6506",
        "ok": "6508",
        "ko": "3120"
    },
    "percentiles1": {
        "total": "22080",
        "ok": "22465",
        "ko": "15833"
    },
    "percentiles2": {
        "total": "25024",
        "ok": "25144",
        "ko": "17314"
    },
    "percentiles3": {
        "total": "29342",
        "ok": "29577",
        "ko": "19829"
    },
    "percentiles4": {
        "total": "37022",
        "ok": "37231",
        "ko": "20836"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 92,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 8,
    "percentage": 8
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.736",
        "ko": "0.151"
    }
}
    },"req_favicon-48x48-p-1e880": {
        type: "REQUEST",
        name: "favicon-48x48.png",
path: "favicon-48x48.png",
pathFormatted: "req_favicon-48x48-p-1e880",
stats: {
    "name": "favicon-48x48.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "86",
        "ko": "14"
    },
    "minResponseTime": {
        "total": "3635",
        "ok": "3635",
        "ko": "12647"
    },
    "maxResponseTime": {
        "total": "39968",
        "ok": "39968",
        "ko": "21603"
    },
    "meanResponseTime": {
        "total": "16278",
        "ok": "16118",
        "ko": "17261"
    },
    "standardDeviation": {
        "total": "5204",
        "ok": "5494",
        "ko": "2630"
    },
    "percentiles1": {
        "total": "15026",
        "ok": "14771",
        "ko": "16760"
    },
    "percentiles2": {
        "total": "16986",
        "ok": "16510",
        "ko": "18952"
    },
    "percentiles3": {
        "total": "22061",
        "ok": "28243",
        "ko": "21327"
    },
    "percentiles4": {
        "total": "36741",
        "ok": "37197",
        "ko": "21548"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 86,
    "percentage": 86
},
    "group4": {
    "name": "failed",
    "count": 14,
    "percentage": 14
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.623",
        "ko": "0.264"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "100",
        "ok": "94",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "367",
        "ok": "367",
        "ko": "12238"
    },
    "maxResponseTime": {
        "total": "46697",
        "ok": "46697",
        "ko": "17643"
    },
    "meanResponseTime": {
        "total": "18085",
        "ok": "18290",
        "ko": "14877"
    },
    "standardDeviation": {
        "total": "6662",
        "ok": "6793",
        "ko": "2390"
    },
    "percentiles1": {
        "total": "18185",
        "ok": "18504",
        "ko": "14747"
    },
    "percentiles2": {
        "total": "20613",
        "ok": "20743",
        "ko": "17158"
    },
    "percentiles3": {
        "total": "29750",
        "ok": "30047",
        "ko": "17547"
    },
    "percentiles4": {
        "total": "36691",
        "ok": "37297",
        "ko": "17624"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.774",
        "ko": "0.113"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "100",
        "ok": "93",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "349",
        "ok": "349",
        "ko": "11529"
    },
    "maxResponseTime": {
        "total": "31982",
        "ok": "31982",
        "ko": "21091"
    },
    "meanResponseTime": {
        "total": "17198",
        "ok": "17339",
        "ko": "15324"
    },
    "standardDeviation": {
        "total": "5933",
        "ok": "6060",
        "ko": "3324"
    },
    "percentiles1": {
        "total": "16959",
        "ok": "17129",
        "ko": "15804"
    },
    "percentiles2": {
        "total": "20338",
        "ok": "20338",
        "ko": "17325"
    },
    "percentiles3": {
        "total": "28314",
        "ok": "28404",
        "ko": "20015"
    },
    "percentiles4": {
        "total": "30899",
        "ok": "30976",
        "ko": "20876"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 91,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 7
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.755",
        "ko": "0.132"
    }
}
    },"req_favicon-32x32-p-14e88": {
        type: "REQUEST",
        name: "favicon-32x32.png",
path: "favicon-32x32.png",
pathFormatted: "req_favicon-32x32-p-14e88",
stats: {
    "name": "favicon-32x32.png",
    "numberOfRequests": {
        "total": "100",
        "ok": "87",
        "ko": "13"
    },
    "minResponseTime": {
        "total": "336",
        "ok": "336",
        "ko": "12637"
    },
    "maxResponseTime": {
        "total": "26739",
        "ok": "26739",
        "ko": "21594"
    },
    "meanResponseTime": {
        "total": "14827",
        "ok": "14528",
        "ko": "16830"
    },
    "standardDeviation": {
        "total": "4327",
        "ok": "4425",
        "ko": "2894"
    },
    "percentiles1": {
        "total": "14572",
        "ok": "14483",
        "ko": "16312"
    },
    "percentiles2": {
        "total": "16930",
        "ok": "16568",
        "ko": "18854"
    },
    "percentiles3": {
        "total": "20998",
        "ok": "20049",
        "ko": "21347"
    },
    "percentiles4": {
        "total": "25744",
        "ok": "25875",
        "ko": "21545"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 84,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 13,
    "percentage": 13
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.642",
        "ko": "0.245"
    }
}
    },"req_request-34-d9757": {
        type: "REQUEST",
        name: "request_34",
path: "request_34",
pathFormatted: "req_request-34-d9757",
stats: {
    "name": "request_34",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "321",
        "ok": "321",
        "ko": "12631"
    },
    "maxResponseTime": {
        "total": "39429",
        "ok": "39429",
        "ko": "17487"
    },
    "meanResponseTime": {
        "total": "15796",
        "ok": "15830",
        "ko": "15166"
    },
    "standardDeviation": {
        "total": "5573",
        "ok": "5695",
        "ko": "2139"
    },
    "percentiles1": {
        "total": "15119",
        "ok": "15040",
        "ko": "15867"
    },
    "percentiles2": {
        "total": "17512",
        "ok": "17766",
        "ko": "17210"
    },
    "percentiles3": {
        "total": "26578",
        "ok": "26669",
        "ko": "17432"
    },
    "percentiles4": {
        "total": "30950",
        "ok": "31378",
        "ko": "17476"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 93,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_main-e4fad40d75-406e2": {
        type: "REQUEST",
        name: "main.e4fad40d75305c1c3562.js",
path: "main.e4fad40d75305c1c3562.js",
pathFormatted: "req_main-e4fad40d75-406e2",
stats: {
    "name": "main.e4fad40d75305c1c3562.js",
    "numberOfRequests": {
        "total": "100",
        "ok": "89",
        "ko": "11"
    },
    "minResponseTime": {
        "total": "479",
        "ok": "479",
        "ko": "12636"
    },
    "maxResponseTime": {
        "total": "43174",
        "ok": "43174",
        "ko": "21586"
    },
    "meanResponseTime": {
        "total": "23248",
        "ok": "24030",
        "ko": "16913"
    },
    "standardDeviation": {
        "total": "5855",
        "ok": "5622",
        "ko": "3294"
    },
    "percentiles1": {
        "total": "24118",
        "ok": "24390",
        "ko": "16045"
    },
    "percentiles2": {
        "total": "25748",
        "ok": "25809",
        "ko": "20083"
    },
    "percentiles3": {
        "total": "29816",
        "ok": "29884",
        "ko": "21392"
    },
    "percentiles4": {
        "total": "35673",
        "ok": "36506",
        "ko": "21547"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 87,
    "percentage": 87
},
    "group4": {
    "name": "failed",
    "count": 11,
    "percentage": 11
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.679",
        "ko": "0.208"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "100",
        "ok": "96",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "272",
        "ok": "272",
        "ko": "12632"
    },
    "maxResponseTime": {
        "total": "40183",
        "ok": "40183",
        "ko": "17600"
    },
    "meanResponseTime": {
        "total": "16337",
        "ok": "16393",
        "ko": "15006"
    },
    "standardDeviation": {
        "total": "5935",
        "ok": "6031",
        "ko": "2377"
    },
    "percentiles1": {
        "total": "15847",
        "ok": "15847",
        "ko": "14896"
    },
    "percentiles2": {
        "total": "18445",
        "ok": "19074",
        "ko": "17266"
    },
    "percentiles3": {
        "total": "27603",
        "ok": "27660",
        "ko": "17533"
    },
    "percentiles4": {
        "total": "32787",
        "ok": "33086",
        "ko": "17587"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 95,
    "percentage": 95
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 4
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.811",
        "ko": "0.075"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "100",
        "ok": "94",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "449",
        "ok": "449",
        "ko": "11564"
    },
    "maxResponseTime": {
        "total": "22535",
        "ok": "22535",
        "ko": "17666"
    },
    "meanResponseTime": {
        "total": "12779",
        "ok": "12664",
        "ko": "14576"
    },
    "standardDeviation": {
        "total": "3161",
        "ok": "3169",
        "ko": "2401"
    },
    "percentiles1": {
        "total": "13540",
        "ok": "13540",
        "ko": "14174"
    },
    "percentiles2": {
        "total": "14068",
        "ok": "14058",
        "ko": "16862"
    },
    "percentiles3": {
        "total": "15757",
        "ok": "14647",
        "ko": "17561"
    },
    "percentiles4": {
        "total": "17715",
        "ok": "17449",
        "ko": "17645"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 91,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.774",
        "ko": "0.113"
    }
}
    },"req_request-24-taxo-b5f25": {
        type: "REQUEST",
        name: "request_24_TaxonMedia",
path: "request_24_TaxonMedia",
pathFormatted: "req_request-24-taxo-b5f25",
stats: {
    "name": "request_24_TaxonMedia",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "4606",
        "ok": "4606",
        "ko": "12237"
    },
    "maxResponseTime": {
        "total": "47670",
        "ok": "47670",
        "ko": "17637"
    },
    "meanResponseTime": {
        "total": "20769",
        "ok": "21101",
        "ko": "14460"
    },
    "standardDeviation": {
        "total": "6655",
        "ok": "6641",
        "ko": "2407"
    },
    "percentiles1": {
        "total": "22240",
        "ok": "22640",
        "ko": "12636"
    },
    "percentiles2": {
        "total": "23806",
        "ok": "23868",
        "ko": "17157"
    },
    "percentiles3": {
        "total": "28767",
        "ok": "29020",
        "ko": "17541"
    },
    "percentiles4": {
        "total": "36325",
        "ok": "36898",
        "ko": "17618"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 95,
    "percentage": 95
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    },"req_favicon-ico-8af3a": {
        type: "REQUEST",
        name: "favicon.ico",
path: "favicon.ico",
pathFormatted: "req_favicon-ico-8af3a",
stats: {
    "name": "favicon.ico",
    "numberOfRequests": {
        "total": "100",
        "ok": "87",
        "ko": "13"
    },
    "minResponseTime": {
        "total": "476",
        "ok": "476",
        "ko": "12636"
    },
    "maxResponseTime": {
        "total": "39483",
        "ok": "39483",
        "ko": "21599"
    },
    "meanResponseTime": {
        "total": "19884",
        "ok": "20334",
        "ko": "16875"
    },
    "standardDeviation": {
        "total": "6279",
        "ok": "6518",
        "ko": "2917"
    },
    "percentiles1": {
        "total": "19639",
        "ok": "19850",
        "ko": "16358"
    },
    "percentiles2": {
        "total": "21948",
        "ok": "23301",
        "ko": "18845"
    },
    "percentiles3": {
        "total": "32950",
        "ok": "33128",
        "ko": "21306"
    },
    "percentiles4": {
        "total": "38895",
        "ok": "38972",
        "ko": "21540"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 86,
    "percentage": 86
},
    "group4": {
    "name": "failed",
    "count": 13,
    "percentage": 13
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.642",
        "ko": "0.245"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "100",
        "ok": "95",
        "ko": "5"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "11573"
    },
    "maxResponseTime": {
        "total": "35594",
        "ok": "35594",
        "ko": "17490"
    },
    "meanResponseTime": {
        "total": "15030",
        "ok": "15071",
        "ko": "14260"
    },
    "standardDeviation": {
        "total": "5535",
        "ok": "5647",
        "ko": "2461"
    },
    "percentiles1": {
        "total": "14464",
        "ok": "14488",
        "ko": "12637"
    },
    "percentiles2": {
        "total": "17557",
        "ok": "17567",
        "ko": "16969"
    },
    "percentiles3": {
        "total": "24599",
        "ok": "24712",
        "ko": "17386"
    },
    "percentiles4": {
        "total": "32744",
        "ok": "32888",
        "ko": "17469"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 91,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "count": 5,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.887",
        "ok": "1.792",
        "ko": "0.094"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
