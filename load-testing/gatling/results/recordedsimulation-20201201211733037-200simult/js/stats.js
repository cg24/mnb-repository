var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "7600",
        "ok": "4702",
        "ko": "2898"
    },
    "minResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "8674"
    },
    "maxResponseTime": {
        "total": "60006",
        "ok": "27783",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "8555",
        "ok": "7140",
        "ko": "10850"
    },
    "standardDeviation": {
        "total": "3591",
        "ok": "3699",
        "ko": "1763"
    },
    "percentiles1": {
        "total": "10361",
        "ok": "6998",
        "ko": "10566"
    },
    "percentiles2": {
        "total": "10665",
        "ok": "10140",
        "ko": "10793"
    },
    "percentiles3": {
        "total": "12133",
        "ok": "12172",
        "ko": "12030"
    },
    "percentiles4": {
        "total": "15461",
        "ok": "16419",
        "ko": "15210"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 219,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 115,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4368,
    "percentage": 57
},
    "group4": {
    "name": "failed",
    "count": 2898,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "122.581",
        "ok": "75.839",
        "ko": "46.742"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "200",
        "ok": "200",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1562",
        "ok": "1562",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "306",
        "ok": "306",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1018",
        "ok": "1018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1245",
        "ok": "1245",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1398",
        "ok": "1398",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1462",
        "ok": "1462",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 47,
    "percentage": 24
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 90,
    "percentage": 45
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 63,
    "percentage": 32
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "3.226",
        "ko": "-"
    }
}
    },"req_graphql-weather-e1b31": {
        type: "REQUEST",
        name: "graphql_weather",
path: "graphql_weather",
pathFormatted: "req_graphql-weather-e1b31",
stats: {
    "name": "graphql_weather",
    "numberOfRequests": {
        "total": "200",
        "ok": "113",
        "ko": "87"
    },
    "minResponseTime": {
        "total": "95",
        "ok": "95",
        "ko": "10310"
    },
    "maxResponseTime": {
        "total": "26867",
        "ok": "26867",
        "ko": "15529"
    },
    "meanResponseTime": {
        "total": "8572",
        "ok": "6842",
        "ko": "10820"
    },
    "standardDeviation": {
        "total": "3749",
        "ok": "4182",
        "ko": "804"
    },
    "percentiles1": {
        "total": "10452",
        "ok": "6316",
        "ko": "10573"
    },
    "percentiles2": {
        "total": "10649",
        "ok": "9946",
        "ko": "10818"
    },
    "percentiles3": {
        "total": "11881",
        "ok": "11898",
        "ko": "11834"
    },
    "percentiles4": {
        "total": "15538",
        "ok": "16228",
        "ko": "15256"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 11,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 101,
    "percentage": 51
},
    "group4": {
    "name": "failed",
    "count": 87,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.823",
        "ko": "1.403"
    }
}
    },"req_request-24-taxo-b5f25": {
        type: "REQUEST",
        name: "request_24_TaxonMedia",
path: "request_24_TaxonMedia",
pathFormatted: "req_request-24-taxo-b5f25",
stats: {
    "name": "request_24_TaxonMedia",
    "numberOfRequests": {
        "total": "200",
        "ok": "113",
        "ko": "87"
    },
    "minResponseTime": {
        "total": "4833",
        "ok": "4833",
        "ko": "10285"
    },
    "maxResponseTime": {
        "total": "25857",
        "ok": "25857",
        "ko": "15192"
    },
    "meanResponseTime": {
        "total": "10940",
        "ok": "11077",
        "ko": "10763"
    },
    "standardDeviation": {
        "total": "1723",
        "ok": "2209",
        "ko": "661"
    },
    "percentiles1": {
        "total": "10594",
        "ok": "10805",
        "ko": "10581"
    },
    "percentiles2": {
        "total": "11209",
        "ok": "11785",
        "ko": "10764"
    },
    "percentiles3": {
        "total": "13348",
        "ok": "13880",
        "ko": "11611"
    },
    "percentiles4": {
        "total": "16226",
        "ok": "16600",
        "ko": "13573"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 113,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 87,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.823",
        "ko": "1.403"
    }
}
    },"req_request-34-d9757": {
        type: "REQUEST",
        name: "request_34",
path: "request_34",
pathFormatted: "req_request-34-d9757",
stats: {
    "name": "request_34",
    "numberOfRequests": {
        "total": "200",
        "ok": "117",
        "ko": "83"
    },
    "minResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "10272"
    },
    "maxResponseTime": {
        "total": "13197",
        "ok": "12543",
        "ko": "13197"
    },
    "meanResponseTime": {
        "total": "8023",
        "ok": "6097",
        "ko": "10739"
    },
    "standardDeviation": {
        "total": "3291",
        "ok": "3068",
        "ko": "477"
    },
    "percentiles1": {
        "total": "10190",
        "ok": "5758",
        "ko": "10581"
    },
    "percentiles2": {
        "total": "10610",
        "ok": "7980",
        "ko": "10768"
    },
    "percentiles3": {
        "total": "11549",
        "ok": "11445",
        "ko": "11651"
    },
    "percentiles4": {
        "total": "12232",
        "ok": "11976",
        "ko": "12403"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 9,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 108,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 83,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.887",
        "ko": "1.339"
    }
}
    },"req_graphql-taxon-f4085": {
        type: "REQUEST",
        name: "graphql_Taxon",
path: "graphql_Taxon",
pathFormatted: "req_graphql-taxon-f4085",
stats: {
    "name": "graphql_Taxon",
    "numberOfRequests": {
        "total": "200",
        "ok": "120",
        "ko": "80"
    },
    "minResponseTime": {
        "total": "280",
        "ok": "280",
        "ko": "10181"
    },
    "maxResponseTime": {
        "total": "24638",
        "ok": "24638",
        "ko": "15503"
    },
    "meanResponseTime": {
        "total": "9657",
        "ok": "8945",
        "ko": "10725"
    },
    "standardDeviation": {
        "total": "3506",
        "ok": "4353",
        "ko": "646"
    },
    "percentiles1": {
        "total": "10485",
        "ok": "9356",
        "ko": "10539"
    },
    "percentiles2": {
        "total": "10934",
        "ok": "11355",
        "ko": "10735"
    },
    "percentiles3": {
        "total": "13447",
        "ok": "15764",
        "ko": "11537"
    },
    "percentiles4": {
        "total": "19318",
        "ok": "19899",
        "ko": "12913"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 109,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 80,
    "percentage": 40
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.935",
        "ko": "1.29"
    }
}
    },"req_request-19-10d85": {
        type: "REQUEST",
        name: "request_19",
path: "request_19",
pathFormatted: "req_request-19-10d85",
stats: {
    "name": "request_19",
    "numberOfRequests": {
        "total": "200",
        "ok": "115",
        "ko": "85"
    },
    "minResponseTime": {
        "total": "310",
        "ok": "310",
        "ko": "9858"
    },
    "maxResponseTime": {
        "total": "19289",
        "ok": "12420",
        "ko": "19289"
    },
    "meanResponseTime": {
        "total": "8232",
        "ok": "6326",
        "ko": "10811"
    },
    "standardDeviation": {
        "total": "3411",
        "ok": "3299",
        "ko": "1039"
    },
    "percentiles1": {
        "total": "10300",
        "ok": "5584",
        "ko": "10554"
    },
    "percentiles2": {
        "total": "10630",
        "ok": "8846",
        "ko": "10776"
    },
    "percentiles3": {
        "total": "11600",
        "ok": "11571",
        "ko": "11689"
    },
    "percentiles4": {
        "total": "12426",
        "ok": "11974",
        "ko": "14053"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 9,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 105,
    "percentage": 53
},
    "group4": {
    "name": "failed",
    "count": 85,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.855",
        "ko": "1.371"
    }
}
    },"req_request-21-taxo-4adce": {
        type: "REQUEST",
        name: "request_21_TaxonMedia",
path: "request_21_TaxonMedia",
pathFormatted: "req_request-21-taxo-4adce",
stats: {
    "name": "request_21_TaxonMedia",
    "numberOfRequests": {
        "total": "200",
        "ok": "124",
        "ko": "76"
    },
    "minResponseTime": {
        "total": "3447",
        "ok": "3447",
        "ko": "9237"
    },
    "maxResponseTime": {
        "total": "25439",
        "ok": "25439",
        "ko": "11724"
    },
    "meanResponseTime": {
        "total": "10098",
        "ok": "9775",
        "ko": "10625"
    },
    "standardDeviation": {
        "total": "2553",
        "ok": "3189",
        "ko": "332"
    },
    "percentiles1": {
        "total": "10517",
        "ok": "9816",
        "ko": "10533"
    },
    "percentiles2": {
        "total": "11096",
        "ok": "11449",
        "ko": "10688"
    },
    "percentiles3": {
        "total": "13343",
        "ok": "14716",
        "ko": "11246"
    },
    "percentiles4": {
        "total": "16744",
        "ok": "16948",
        "ko": "11540"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 124,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 76,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "2",
        "ko": "1.226"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "200",
        "ok": "121",
        "ko": "79"
    },
    "minResponseTime": {
        "total": "200",
        "ok": "200",
        "ko": "8674"
    },
    "maxResponseTime": {
        "total": "15060",
        "ok": "14389",
        "ko": "15060"
    },
    "meanResponseTime": {
        "total": "7977",
        "ok": "6178",
        "ko": "10733"
    },
    "standardDeviation": {
        "total": "3117",
        "ok": "2741",
        "ko": "731"
    },
    "percentiles1": {
        "total": "8805",
        "ok": "5773",
        "ko": "10551"
    },
    "percentiles2": {
        "total": "10561",
        "ok": "7734",
        "ko": "10713"
    },
    "percentiles3": {
        "total": "11359",
        "ok": "11307",
        "ko": "11473"
    },
    "percentiles4": {
        "total": "13355",
        "ok": "11592",
        "ko": "13722"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 116,
    "percentage": 58
},
    "group4": {
    "name": "failed",
    "count": 79,
    "percentage": 40
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.952",
        "ko": "1.274"
    }
}
    },"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "200",
        "ok": "182",
        "ko": "18"
    },
    "minResponseTime": {
        "total": "3589",
        "ok": "3589",
        "ko": "11706"
    },
    "maxResponseTime": {
        "total": "60006",
        "ok": "13074",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "9350",
        "ok": "8430",
        "ko": "18651"
    },
    "standardDeviation": {
        "total": "5489",
        "ok": "1552",
        "ko": "14674"
    },
    "percentiles1": {
        "total": "8540",
        "ok": "8430",
        "ko": "13087"
    },
    "percentiles2": {
        "total": "9793",
        "ok": "9498",
        "ko": "15048"
    },
    "percentiles3": {
        "total": "12959",
        "ok": "10851",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "17166",
        "ok": "12073",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 182,
    "percentage": 91
},
    "group4": {
    "name": "failed",
    "count": 18,
    "percentage": 9
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "2.935",
        "ko": "0.29"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "200",
        "ok": "116",
        "ko": "84"
    },
    "minResponseTime": {
        "total": "147",
        "ok": "147",
        "ko": "9208"
    },
    "maxResponseTime": {
        "total": "19081",
        "ok": "19081",
        "ko": "15741"
    },
    "meanResponseTime": {
        "total": "8127",
        "ok": "6188",
        "ko": "10805"
    },
    "standardDeviation": {
        "total": "3499",
        "ok": "3410",
        "ko": "853"
    },
    "percentiles1": {
        "total": "10257",
        "ok": "4998",
        "ko": "10581"
    },
    "percentiles2": {
        "total": "10595",
        "ok": "8655",
        "ko": "10846"
    },
    "percentiles3": {
        "total": "11546",
        "ok": "11602",
        "ko": "11518"
    },
    "percentiles4": {
        "total": "15197",
        "ok": "14354",
        "ko": "15285"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 109,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 84,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.871",
        "ko": "1.355"
    }
}
    },"req_request-14-a0e30": {
        type: "REQUEST",
        name: "request_14",
path: "request_14",
pathFormatted: "req_request-14-a0e30",
stats: {
    "name": "request_14",
    "numberOfRequests": {
        "total": "200",
        "ok": "124",
        "ko": "76"
    },
    "minResponseTime": {
        "total": "143",
        "ok": "143",
        "ko": "10192"
    },
    "maxResponseTime": {
        "total": "18670",
        "ok": "18670",
        "ko": "15479"
    },
    "meanResponseTime": {
        "total": "8060",
        "ok": "6403",
        "ko": "10764"
    },
    "standardDeviation": {
        "total": "3453",
        "ok": "3417",
        "ko": "727"
    },
    "percentiles1": {
        "total": "9974",
        "ok": "5494",
        "ko": "10541"
    },
    "percentiles2": {
        "total": "10597",
        "ok": "8617",
        "ko": "10713"
    },
    "percentiles3": {
        "total": "11587",
        "ok": "11579",
        "ko": "11643"
    },
    "percentiles4": {
        "total": "15479",
        "ok": "15084",
        "ko": "14032"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 117,
    "percentage": 59
},
    "group4": {
    "name": "failed",
    "count": 76,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "2",
        "ko": "1.226"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "200",
        "ok": "117",
        "ko": "83"
    },
    "minResponseTime": {
        "total": "318",
        "ok": "318",
        "ko": "10192"
    },
    "maxResponseTime": {
        "total": "13610",
        "ok": "12723",
        "ko": "13610"
    },
    "meanResponseTime": {
        "total": "8127",
        "ok": "6280",
        "ko": "10730"
    },
    "standardDeviation": {
        "total": "3309",
        "ok": "3203",
        "ko": "587"
    },
    "percentiles1": {
        "total": "10304",
        "ok": "6091",
        "ko": "10558"
    },
    "percentiles2": {
        "total": "10595",
        "ok": "8447",
        "ko": "10708"
    },
    "percentiles3": {
        "total": "11538",
        "ok": "11786",
        "ko": "11512"
    },
    "percentiles4": {
        "total": "13285",
        "ok": "12419",
        "ko": "13388"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 108,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 83,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.887",
        "ko": "1.339"
    }
}
    },"req_request-28-taxo-f261b": {
        type: "REQUEST",
        name: "request_28_TaxonMedia",
path: "request_28_TaxonMedia",
pathFormatted: "req_request-28-taxo-f261b",
stats: {
    "name": "request_28_TaxonMedia",
    "numberOfRequests": {
        "total": "200",
        "ok": "116",
        "ko": "84"
    },
    "minResponseTime": {
        "total": "3839",
        "ok": "3839",
        "ko": "10181"
    },
    "maxResponseTime": {
        "total": "20336",
        "ok": "20336",
        "ko": "13506"
    },
    "meanResponseTime": {
        "total": "10927",
        "ok": "11057",
        "ko": "10747"
    },
    "standardDeviation": {
        "total": "1879",
        "ok": "2423",
        "ko": "501"
    },
    "percentiles1": {
        "total": "10583",
        "ok": "10692",
        "ko": "10547"
    },
    "percentiles2": {
        "total": "11388",
        "ok": "11905",
        "ko": "10878"
    },
    "percentiles3": {
        "total": "15212",
        "ok": "15800",
        "ko": "11658"
    },
    "percentiles4": {
        "total": "16950",
        "ok": "17700",
        "ko": "12480"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 116,
    "percentage": 58
},
    "group4": {
    "name": "failed",
    "count": 84,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.871",
        "ko": "1.355"
    }
}
    },"req_graphql-news-4c309": {
        type: "REQUEST",
        name: "graphql_News",
path: "graphql_News",
pathFormatted: "req_graphql-news-4c309",
stats: {
    "name": "graphql_News",
    "numberOfRequests": {
        "total": "200",
        "ok": "120",
        "ko": "80"
    },
    "minResponseTime": {
        "total": "290",
        "ok": "290",
        "ko": "10183"
    },
    "maxResponseTime": {
        "total": "15411",
        "ok": "12736",
        "ko": "15411"
    },
    "meanResponseTime": {
        "total": "8059",
        "ok": "6244",
        "ko": "10783"
    },
    "standardDeviation": {
        "total": "3262",
        "ok": "3021",
        "ko": "742"
    },
    "percentiles1": {
        "total": "9730",
        "ok": "5983",
        "ko": "10542"
    },
    "percentiles2": {
        "total": "10581",
        "ok": "7910",
        "ko": "10716"
    },
    "percentiles3": {
        "total": "11835",
        "ok": "11647",
        "ko": "11883"
    },
    "percentiles4": {
        "total": "13101",
        "ok": "12218",
        "ko": "13627"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 113,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 80,
    "percentage": 40
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.935",
        "ko": "1.29"
    }
}
    },"req_request-20-taxo-0d791": {
        type: "REQUEST",
        name: "request_20_TaxonMedia",
path: "request_20_TaxonMedia",
pathFormatted: "req_request-20-taxo-0d791",
stats: {
    "name": "request_20_TaxonMedia",
    "numberOfRequests": {
        "total": "200",
        "ok": "123",
        "ko": "77"
    },
    "minResponseTime": {
        "total": "2993",
        "ok": "2993",
        "ko": "10271"
    },
    "maxResponseTime": {
        "total": "18987",
        "ok": "18987",
        "ko": "15383"
    },
    "meanResponseTime": {
        "total": "9919",
        "ok": "9369",
        "ko": "10797"
    },
    "standardDeviation": {
        "total": "2709",
        "ok": "3272",
        "ko": "841"
    },
    "percentiles1": {
        "total": "10509",
        "ok": "9447",
        "ko": "10549"
    },
    "percentiles2": {
        "total": "11032",
        "ok": "11491",
        "ko": "10727"
    },
    "percentiles3": {
        "total": "13347",
        "ok": "14420",
        "ko": "11833"
    },
    "percentiles4": {
        "total": "17604",
        "ok": "18215",
        "ko": "15369"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 123,
    "percentage": 62
},
    "group4": {
    "name": "failed",
    "count": 77,
    "percentage": 39
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.984",
        "ko": "1.242"
    }
}
    },"req_request-11-f11e8": {
        type: "REQUEST",
        name: "request_11",
path: "request_11",
pathFormatted: "req_request-11-f11e8",
stats: {
    "name": "request_11",
    "numberOfRequests": {
        "total": "200",
        "ok": "115",
        "ko": "85"
    },
    "minResponseTime": {
        "total": "391",
        "ok": "391",
        "ko": "10189"
    },
    "maxResponseTime": {
        "total": "15210",
        "ok": "12091",
        "ko": "15210"
    },
    "meanResponseTime": {
        "total": "8048",
        "ok": "6049",
        "ko": "10753"
    },
    "standardDeviation": {
        "total": "3219",
        "ok": "2887",
        "ko": "617"
    },
    "percentiles1": {
        "total": "10028",
        "ok": "5211",
        "ko": "10579"
    },
    "percentiles2": {
        "total": "10596",
        "ok": "7977",
        "ko": "10838"
    },
    "percentiles3": {
        "total": "11529",
        "ok": "11222",
        "ko": "11599"
    },
    "percentiles4": {
        "total": "12094",
        "ok": "11920",
        "ko": "12840"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 108,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 85,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.855",
        "ko": "1.371"
    }
}
    },"req_request-18-f5b64": {
        type: "REQUEST",
        name: "request_18",
path: "request_18",
pathFormatted: "req_request-18-f5b64",
stats: {
    "name": "request_18",
    "numberOfRequests": {
        "total": "200",
        "ok": "116",
        "ko": "84"
    },
    "minResponseTime": {
        "total": "462",
        "ok": "462",
        "ko": "10183"
    },
    "maxResponseTime": {
        "total": "18493",
        "ok": "18493",
        "ko": "15479"
    },
    "meanResponseTime": {
        "total": "8301",
        "ok": "6451",
        "ko": "10857"
    },
    "standardDeviation": {
        "total": "3350",
        "ok": "3266",
        "ko": "856"
    },
    "percentiles1": {
        "total": "10303",
        "ok": "5611",
        "ko": "10584"
    },
    "percentiles2": {
        "total": "10622",
        "ok": "8916",
        "ko": "10881"
    },
    "percentiles3": {
        "total": "11760",
        "ok": "11571",
        "ko": "12334"
    },
    "percentiles4": {
        "total": "15064",
        "ok": "12336",
        "ko": "15131"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 2
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 110,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 84,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.871",
        "ko": "1.355"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "200",
        "ok": "113",
        "ko": "87"
    },
    "minResponseTime": {
        "total": "83",
        "ok": "83",
        "ko": "10271"
    },
    "maxResponseTime": {
        "total": "15362",
        "ok": "12787",
        "ko": "15362"
    },
    "meanResponseTime": {
        "total": "8198",
        "ok": "6237",
        "ko": "10746"
    },
    "standardDeviation": {
        "total": "3404",
        "ok": "3375",
        "ko": "599"
    },
    "percentiles1": {
        "total": "10405",
        "ok": "5722",
        "ko": "10578"
    },
    "percentiles2": {
        "total": "10625",
        "ok": "9122",
        "ko": "10808"
    },
    "percentiles3": {
        "total": "11515",
        "ok": "11690",
        "ko": "11467"
    },
    "percentiles4": {
        "total": "12777",
        "ok": "12733",
        "ko": "12658"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 103,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 87,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.823",
        "ko": "1.403"
    }
}
    },"req_graphql-news-d7bbe": {
        type: "REQUEST",
        name: "graphQL_news",
path: "graphQL_news",
pathFormatted: "req_graphql-news-d7bbe",
stats: {
    "name": "graphQL_news",
    "numberOfRequests": {
        "total": "200",
        "ok": "113",
        "ko": "87"
    },
    "minResponseTime": {
        "total": "395",
        "ok": "395",
        "ko": "10270"
    },
    "maxResponseTime": {
        "total": "17791",
        "ok": "17791",
        "ko": "15162"
    },
    "meanResponseTime": {
        "total": "8512",
        "ok": "6759",
        "ko": "10790"
    },
    "standardDeviation": {
        "total": "3114",
        "ok": "3125",
        "ko": "647"
    },
    "percentiles1": {
        "total": "10381",
        "ok": "6724",
        "ko": "10586"
    },
    "percentiles2": {
        "total": "10642",
        "ok": "8706",
        "ko": "10897"
    },
    "percentiles3": {
        "total": "11543",
        "ok": "11526",
        "ko": "11545"
    },
    "percentiles4": {
        "total": "13408",
        "ok": "12457",
        "ko": "13638"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 108,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 87,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.823",
        "ko": "1.403"
    }
}
    },"req_request-31-261d3": {
        type: "REQUEST",
        name: "request_31",
path: "request_31",
pathFormatted: "req_request-31-261d3",
stats: {
    "name": "request_31",
    "numberOfRequests": {
        "total": "200",
        "ok": "111",
        "ko": "89"
    },
    "minResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "8756"
    },
    "maxResponseTime": {
        "total": "13401",
        "ok": "12417",
        "ko": "13401"
    },
    "meanResponseTime": {
        "total": "8109",
        "ok": "6091",
        "ko": "10625"
    },
    "standardDeviation": {
        "total": "3192",
        "ok": "3004",
        "ko": "481"
    },
    "percentiles1": {
        "total": "10318",
        "ok": "5321",
        "ko": "10532"
    },
    "percentiles2": {
        "total": "10576",
        "ok": "8249",
        "ko": "10726"
    },
    "percentiles3": {
        "total": "11184",
        "ok": "11241",
        "ko": "11123"
    },
    "percentiles4": {
        "total": "12104",
        "ok": "12045",
        "ko": "12111"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 103,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 89,
    "percentage": 45
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.79",
        "ko": "1.435"
    }
}
    },"req_request-32-84a21": {
        type: "REQUEST",
        name: "request_32",
path: "request_32",
pathFormatted: "req_request-32-84a21",
stats: {
    "name": "request_32",
    "numberOfRequests": {
        "total": "200",
        "ok": "123",
        "ko": "77"
    },
    "minResponseTime": {
        "total": "281",
        "ok": "281",
        "ko": "10181"
    },
    "maxResponseTime": {
        "total": "13293",
        "ok": "13293",
        "ko": "13098"
    },
    "meanResponseTime": {
        "total": "7961",
        "ok": "6240",
        "ko": "10711"
    },
    "standardDeviation": {
        "total": "3294",
        "ok": "3130",
        "ko": "489"
    },
    "percentiles1": {
        "total": "9407",
        "ok": "5753",
        "ko": "10537"
    },
    "percentiles2": {
        "total": "10609",
        "ok": "8319",
        "ko": "10724"
    },
    "percentiles3": {
        "total": "11499",
        "ok": "11421",
        "ko": "11522"
    },
    "percentiles4": {
        "total": "12871",
        "ok": "12259",
        "ko": "12924"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 113,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 77,
    "percentage": 39
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.984",
        "ko": "1.242"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "200",
        "ok": "125",
        "ko": "75"
    },
    "minResponseTime": {
        "total": "127",
        "ok": "127",
        "ko": "9204"
    },
    "maxResponseTime": {
        "total": "15365",
        "ok": "12141",
        "ko": "15365"
    },
    "meanResponseTime": {
        "total": "7815",
        "ok": "5992",
        "ko": "10854"
    },
    "standardDeviation": {
        "total": "3427",
        "ok": "3052",
        "ko": "1008"
    },
    "percentiles1": {
        "total": "8751",
        "ok": "5212",
        "ko": "10545"
    },
    "percentiles2": {
        "total": "10560",
        "ok": "8388",
        "ko": "10746"
    },
    "percentiles3": {
        "total": "11545",
        "ok": "11176",
        "ko": "12224"
    },
    "percentiles4": {
        "total": "15162",
        "ok": "11556",
        "ko": "15241"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 9,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 115,
    "percentage": 57
},
    "group4": {
    "name": "failed",
    "count": 75,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "2.016",
        "ko": "1.21"
    }
}
    },"req_graphql-riveran-e540f": {
        type: "REQUEST",
        name: "graphql_RiverAndGroundWaterObservation",
path: "graphql_RiverAndGroundWaterObservation",
pathFormatted: "req_graphql-riveran-e540f",
stats: {
    "name": "graphql_RiverAndGroundWaterObservation",
    "numberOfRequests": {
        "total": "200",
        "ok": "107",
        "ko": "93"
    },
    "minResponseTime": {
        "total": "322",
        "ok": "322",
        "ko": "10189"
    },
    "maxResponseTime": {
        "total": "26831",
        "ok": "26831",
        "ko": "13284"
    },
    "meanResponseTime": {
        "total": "10409",
        "ok": "10132",
        "ko": "10727"
    },
    "standardDeviation": {
        "total": "2542",
        "ok": "3423",
        "ko": "474"
    },
    "percentiles1": {
        "total": "10571",
        "ok": "10618",
        "ko": "10564"
    },
    "percentiles2": {
        "total": "11158",
        "ok": "11453",
        "ko": "10717"
    },
    "percentiles3": {
        "total": "12630",
        "ok": "13350",
        "ko": "11727"
    },
    "percentiles4": {
        "total": "18081",
        "ok": "19375",
        "ko": "12592"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 104,
    "percentage": 52
},
    "group4": {
    "name": "failed",
    "count": 93,
    "percentage": 47
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.726",
        "ko": "1.5"
    }
}
    },"req_request-6-font-15e7e": {
        type: "REQUEST",
        name: "request_6_font",
path: "request_6_font",
pathFormatted: "req_request-6-font-15e7e",
stats: {
    "name": "request_6_font",
    "numberOfRequests": {
        "total": "200",
        "ok": "117",
        "ko": "83"
    },
    "minResponseTime": {
        "total": "105",
        "ok": "105",
        "ko": "9771"
    },
    "maxResponseTime": {
        "total": "15362",
        "ok": "11979",
        "ko": "15362"
    },
    "meanResponseTime": {
        "total": "8387",
        "ok": "6677",
        "ko": "10796"
    },
    "standardDeviation": {
        "total": "3139",
        "ok": "3076",
        "ko": "694"
    },
    "percentiles1": {
        "total": "10388",
        "ok": "6111",
        "ko": "10578"
    },
    "percentiles2": {
        "total": "10748",
        "ok": "9387",
        "ko": "10887"
    },
    "percentiles3": {
        "total": "11525",
        "ok": "11516",
        "ko": "11582"
    },
    "percentiles4": {
        "total": "12201",
        "ok": "11941",
        "ko": "13767"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 112,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 83,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.887",
        "ko": "1.339"
    }
}
    },"req_request-30-taxo-7acf0": {
        type: "REQUEST",
        name: "request_30_TaxonMedia",
path: "request_30_TaxonMedia",
pathFormatted: "req_request-30-taxo-7acf0",
stats: {
    "name": "request_30_TaxonMedia",
    "numberOfRequests": {
        "total": "200",
        "ok": "113",
        "ko": "87"
    },
    "minResponseTime": {
        "total": "4183",
        "ok": "4183",
        "ko": "10270"
    },
    "maxResponseTime": {
        "total": "18481",
        "ok": "18481",
        "ko": "15411"
    },
    "meanResponseTime": {
        "total": "10587",
        "ok": "10413",
        "ko": "10814"
    },
    "standardDeviation": {
        "total": "1974",
        "ok": "2509",
        "ko": "831"
    },
    "percentiles1": {
        "total": "10571",
        "ok": "10661",
        "ko": "10538"
    },
    "percentiles2": {
        "total": "11221",
        "ok": "11619",
        "ko": "10769"
    },
    "percentiles3": {
        "total": "13281",
        "ok": "13742",
        "ko": "11746"
    },
    "percentiles4": {
        "total": "17295",
        "ok": "18196",
        "ko": "15370"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 113,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 87,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.823",
        "ko": "1.403"
    }
}
    },"req_request-35-3745a": {
        type: "REQUEST",
        name: "request_35",
path: "request_35",
pathFormatted: "req_request-35-3745a",
stats: {
    "name": "request_35",
    "numberOfRequests": {
        "total": "200",
        "ok": "133",
        "ko": "67"
    },
    "minResponseTime": {
        "total": "194",
        "ok": "194",
        "ko": "10271"
    },
    "maxResponseTime": {
        "total": "14092",
        "ok": "14092",
        "ko": "11550"
    },
    "meanResponseTime": {
        "total": "7827",
        "ok": "6407",
        "ko": "10645"
    },
    "standardDeviation": {
        "total": "3173",
        "ok": "3014",
        "ko": "261"
    },
    "percentiles1": {
        "total": "8553",
        "ok": "5921",
        "ko": "10585"
    },
    "percentiles2": {
        "total": "10593",
        "ok": "8464",
        "ko": "10722"
    },
    "percentiles3": {
        "total": "11483",
        "ok": "11562",
        "ko": "11159"
    },
    "percentiles4": {
        "total": "12516",
        "ok": "12645",
        "ko": "11525"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 127,
    "percentage": 64
},
    "group4": {
    "name": "failed",
    "count": 67,
    "percentage": 34
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "2.145",
        "ko": "1.081"
    }
}
    },"req_request-22-8ecb1": {
        type: "REQUEST",
        name: "request_22",
path: "request_22",
pathFormatted: "req_request-22-8ecb1",
stats: {
    "name": "request_22",
    "numberOfRequests": {
        "total": "200",
        "ok": "124",
        "ko": "76"
    },
    "minResponseTime": {
        "total": "82",
        "ok": "82",
        "ko": "9763"
    },
    "maxResponseTime": {
        "total": "19085",
        "ok": "19085",
        "ko": "15363"
    },
    "meanResponseTime": {
        "total": "8123",
        "ok": "6489",
        "ko": "10789"
    },
    "standardDeviation": {
        "total": "3344",
        "ok": "3266",
        "ko": "750"
    },
    "percentiles1": {
        "total": "9783",
        "ok": "5888",
        "ko": "10552"
    },
    "percentiles2": {
        "total": "10599",
        "ok": "8223",
        "ko": "10716"
    },
    "percentiles3": {
        "total": "11993",
        "ok": "11717",
        "ko": "12123"
    },
    "percentiles4": {
        "total": "14785",
        "ok": "14325",
        "ko": "13852"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 118,
    "percentage": 59
},
    "group4": {
    "name": "failed",
    "count": 76,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "2",
        "ko": "1.226"
    }
}
    },"req_request-4-matom-e3907": {
        type: "REQUEST",
        name: "request_4_matomo",
path: "request_4_matomo",
pathFormatted: "req_request-4-matom-e3907",
stats: {
    "name": "request_4_matomo",
    "numberOfRequests": {
        "total": "200",
        "ok": "184",
        "ko": "16"
    },
    "minResponseTime": {
        "total": "1877",
        "ok": "1877",
        "ko": "11720"
    },
    "maxResponseTime": {
        "total": "16733",
        "ok": "11869",
        "ko": "16733"
    },
    "meanResponseTime": {
        "total": "6679",
        "ok": "6077",
        "ko": "13602"
    },
    "standardDeviation": {
        "total": "2958",
        "ok": "2204",
        "ko": "1191"
    },
    "percentiles1": {
        "total": "6001",
        "ok": "5746",
        "ko": "13682"
    },
    "percentiles2": {
        "total": "8145",
        "ok": "7619",
        "ko": "14256"
    },
    "percentiles3": {
        "total": "12955",
        "ok": "10153",
        "ko": "15113"
    },
    "percentiles4": {
        "total": "14271",
        "ok": "11446",
        "ko": "16409"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 184,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 16,
    "percentage": 8
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "2.968",
        "ko": "0.258"
    }
}
    },"req_graphql-searchp-61328": {
        type: "REQUEST",
        name: "graphql_SearchPlaces",
path: "graphql_SearchPlaces",
pathFormatted: "req_graphql-searchp-61328",
stats: {
    "name": "graphql_SearchPlaces",
    "numberOfRequests": {
        "total": "200",
        "ok": "118",
        "ko": "82"
    },
    "minResponseTime": {
        "total": "270",
        "ok": "270",
        "ko": "10270"
    },
    "maxResponseTime": {
        "total": "15163",
        "ok": "13841",
        "ko": "15163"
    },
    "meanResponseTime": {
        "total": "7953",
        "ok": "5974",
        "ko": "10799"
    },
    "standardDeviation": {
        "total": "3412",
        "ok": "3137",
        "ko": "707"
    },
    "percentiles1": {
        "total": "9883",
        "ok": "5762",
        "ko": "10579"
    },
    "percentiles2": {
        "total": "10593",
        "ok": "7831",
        "ko": "10830"
    },
    "percentiles3": {
        "total": "11666",
        "ok": "11470",
        "ko": "11748"
    },
    "percentiles4": {
        "total": "13427",
        "ok": "12016",
        "ko": "13754"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 108,
    "percentage": 54
},
    "group4": {
    "name": "failed",
    "count": 82,
    "percentage": 41
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.903",
        "ko": "1.323"
    }
}
    },"req_request-26-taxo-41b6f": {
        type: "REQUEST",
        name: "request_26_TaxonMedia",
path: "request_26_TaxonMedia",
pathFormatted: "req_request-26-taxo-41b6f",
stats: {
    "name": "request_26_TaxonMedia",
    "numberOfRequests": {
        "total": "200",
        "ok": "113",
        "ko": "87"
    },
    "minResponseTime": {
        "total": "3621",
        "ok": "3621",
        "ko": "10310"
    },
    "maxResponseTime": {
        "total": "26338",
        "ok": "26338",
        "ko": "15362"
    },
    "meanResponseTime": {
        "total": "10402",
        "ok": "10120",
        "ko": "10767"
    },
    "standardDeviation": {
        "total": "2669",
        "ok": "3470",
        "ko": "710"
    },
    "percentiles1": {
        "total": "10538",
        "ok": "10278",
        "ko": "10561"
    },
    "percentiles2": {
        "total": "10964",
        "ok": "11390",
        "ko": "10752"
    },
    "percentiles3": {
        "total": "13556",
        "ok": "15568",
        "ko": "11619"
    },
    "percentiles4": {
        "total": "20357",
        "ok": "21574",
        "ko": "13845"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 113,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 87,
    "percentage": 44
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.823",
        "ko": "1.403"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "200",
        "ok": "118",
        "ko": "82"
    },
    "minResponseTime": {
        "total": "74",
        "ok": "74",
        "ko": "10271"
    },
    "maxResponseTime": {
        "total": "18628",
        "ok": "18628",
        "ko": "15304"
    },
    "meanResponseTime": {
        "total": "7996",
        "ok": "6083",
        "ko": "10750"
    },
    "standardDeviation": {
        "total": "3387",
        "ok": "3200",
        "ko": "629"
    },
    "percentiles1": {
        "total": "9972",
        "ok": "4977",
        "ko": "10579"
    },
    "percentiles2": {
        "total": "10595",
        "ok": "8238",
        "ko": "10760"
    },
    "percentiles3": {
        "total": "11503",
        "ok": "11506",
        "ko": "11370"
    },
    "percentiles4": {
        "total": "12599",
        "ok": "12430",
        "ko": "13091"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 113,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 82,
    "percentage": 41
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.903",
        "ko": "1.323"
    }
}
    },"req_request-29-taxo-1f5dd": {
        type: "REQUEST",
        name: "request_29_TaxonMedia",
path: "request_29_TaxonMedia",
pathFormatted: "req_request-29-taxo-1f5dd",
stats: {
    "name": "request_29_TaxonMedia",
    "numberOfRequests": {
        "total": "200",
        "ok": "117",
        "ko": "83"
    },
    "minResponseTime": {
        "total": "2653",
        "ok": "2653",
        "ko": "10271"
    },
    "maxResponseTime": {
        "total": "26036",
        "ok": "26036",
        "ko": "15363"
    },
    "meanResponseTime": {
        "total": "9972",
        "ok": "9345",
        "ko": "10856"
    },
    "standardDeviation": {
        "total": "2722",
        "ok": "3345",
        "ko": "861"
    },
    "percentiles1": {
        "total": "10527",
        "ok": "9268",
        "ko": "10585"
    },
    "percentiles2": {
        "total": "10987",
        "ok": "11156",
        "ko": "10860"
    },
    "percentiles3": {
        "total": "13307",
        "ok": "14245",
        "ko": "12014"
    },
    "percentiles4": {
        "total": "18958",
        "ok": "19928",
        "ko": "15197"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 117,
    "percentage": 59
},
    "group4": {
    "name": "failed",
    "count": 83,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.887",
        "ko": "1.339"
    }
}
    },"req_request-27-649b0": {
        type: "REQUEST",
        name: "request_27",
path: "request_27",
pathFormatted: "req_request-27-649b0",
stats: {
    "name": "request_27",
    "numberOfRequests": {
        "total": "200",
        "ok": "125",
        "ko": "75"
    },
    "minResponseTime": {
        "total": "255",
        "ok": "255",
        "ko": "10191"
    },
    "maxResponseTime": {
        "total": "15461",
        "ok": "11948",
        "ko": "15461"
    },
    "meanResponseTime": {
        "total": "7779",
        "ok": "5976",
        "ko": "10784"
    },
    "standardDeviation": {
        "total": "3333",
        "ok": "2972",
        "ko": "668"
    },
    "percentiles1": {
        "total": "8451",
        "ok": "5151",
        "ko": "10592"
    },
    "percentiles2": {
        "total": "10610",
        "ok": "8167",
        "ko": "10854"
    },
    "percentiles3": {
        "total": "11422",
        "ok": "11325",
        "ko": "11746"
    },
    "percentiles4": {
        "total": "12031",
        "ok": "11786",
        "ko": "13070"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 117,
    "percentage": 59
},
    "group4": {
    "name": "failed",
    "count": 75,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "2.016",
        "ko": "1.21"
    }
}
    },"req_request-23-taxo-3ea91": {
        type: "REQUEST",
        name: "request_23_TaxonMedia",
path: "request_23_TaxonMedia",
pathFormatted: "req_request-23-taxo-3ea91",
stats: {
    "name": "request_23_TaxonMedia",
    "numberOfRequests": {
        "total": "200",
        "ok": "116",
        "ko": "84"
    },
    "minResponseTime": {
        "total": "2933",
        "ok": "2933",
        "ko": "10191"
    },
    "maxResponseTime": {
        "total": "27783",
        "ok": "27783",
        "ko": "13355"
    },
    "meanResponseTime": {
        "total": "10010",
        "ok": "9529",
        "ko": "10673"
    },
    "standardDeviation": {
        "total": "2710",
        "ok": "3459",
        "ko": "449"
    },
    "percentiles1": {
        "total": "10507",
        "ok": "9643",
        "ko": "10538"
    },
    "percentiles2": {
        "total": "10971",
        "ok": "11548",
        "ko": "10687"
    },
    "percentiles3": {
        "total": "12736",
        "ok": "13325",
        "ko": "11458"
    },
    "percentiles4": {
        "total": "18203",
        "ok": "18208",
        "ko": "12361"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 116,
    "percentage": 58
},
    "group4": {
    "name": "failed",
    "count": 84,
    "percentage": 42
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.871",
        "ko": "1.355"
    }
}
    },"req_request-36-9ad97": {
        type: "REQUEST",
        name: "request_36",
path: "request_36",
pathFormatted: "req_request-36-9ad97",
stats: {
    "name": "request_36",
    "numberOfRequests": {
        "total": "200",
        "ok": "124",
        "ko": "76"
    },
    "minResponseTime": {
        "total": "96",
        "ok": "96",
        "ko": "10191"
    },
    "maxResponseTime": {
        "total": "18214",
        "ok": "18214",
        "ko": "15383"
    },
    "meanResponseTime": {
        "total": "8084",
        "ok": "6341",
        "ko": "10927"
    },
    "standardDeviation": {
        "total": "3452",
        "ok": "3258",
        "ko": "1001"
    },
    "percentiles1": {
        "total": "9367",
        "ok": "5697",
        "ko": "10590"
    },
    "percentiles2": {
        "total": "10622",
        "ok": "8308",
        "ko": "10967"
    },
    "percentiles3": {
        "total": "12110",
        "ok": "11985",
        "ko": "12655"
    },
    "percentiles4": {
        "total": "15196",
        "ok": "14099",
        "ko": "15241"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 116,
    "percentage": 58
},
    "group4": {
    "name": "failed",
    "count": 76,
    "percentage": 38
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "2",
        "ko": "1.226"
    }
}
    },"req_request-12-61da2": {
        type: "REQUEST",
        name: "request_12",
path: "request_12",
pathFormatted: "req_request-12-61da2",
stats: {
    "name": "request_12",
    "numberOfRequests": {
        "total": "200",
        "ok": "121",
        "ko": "79"
    },
    "minResponseTime": {
        "total": "280",
        "ok": "280",
        "ko": "9233"
    },
    "maxResponseTime": {
        "total": "12416",
        "ok": "12416",
        "ko": "12156"
    },
    "meanResponseTime": {
        "total": "8115",
        "ok": "6456",
        "ko": "10655"
    },
    "standardDeviation": {
        "total": "3185",
        "ok": "3117",
        "ko": "369"
    },
    "percentiles1": {
        "total": "10226",
        "ok": "6201",
        "ko": "10536"
    },
    "percentiles2": {
        "total": "10555",
        "ok": "8824",
        "ko": "10777"
    },
    "percentiles3": {
        "total": "11285",
        "ok": "11322",
        "ko": "11274"
    },
    "percentiles4": {
        "total": "12157",
        "ok": "12170",
        "ko": "11828"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 113,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 79,
    "percentage": 40
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.952",
        "ko": "1.274"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "200",
        "ok": "115",
        "ko": "85"
    },
    "minResponseTime": {
        "total": "185",
        "ok": "185",
        "ko": "10191"
    },
    "maxResponseTime": {
        "total": "15210",
        "ok": "13151",
        "ko": "15210"
    },
    "meanResponseTime": {
        "total": "8245",
        "ok": "6317",
        "ko": "10854"
    },
    "standardDeviation": {
        "total": "3327",
        "ok": "3147",
        "ko": "906"
    },
    "percentiles1": {
        "total": "10343",
        "ok": "5388",
        "ko": "10587"
    },
    "percentiles2": {
        "total": "10637",
        "ok": "8532",
        "ko": "10790"
    },
    "percentiles3": {
        "total": "11610",
        "ok": "11501",
        "ko": "13141"
    },
    "percentiles4": {
        "total": "13388",
        "ok": "12025",
        "ko": "15198"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 3
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 110,
    "percentage": 55
},
    "group4": {
    "name": "failed",
    "count": 85,
    "percentage": 43
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.855",
        "ko": "1.371"
    }
}
    },"req_request-25-taxo-2081e": {
        type: "REQUEST",
        name: "request_25_TaxonMedia",
path: "request_25_TaxonMedia",
pathFormatted: "req_request-25-taxo-2081e",
stats: {
    "name": "request_25_TaxonMedia",
    "numberOfRequests": {
        "total": "200",
        "ok": "120",
        "ko": "80"
    },
    "minResponseTime": {
        "total": "3119",
        "ok": "3119",
        "ko": "10192"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "18465",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "10511",
        "ok": "9874",
        "ko": "11468"
    },
    "standardDeviation": {
        "total": "4162",
        "ok": "2719",
        "ko": "5540"
    },
    "percentiles1": {
        "total": "10517",
        "ok": "10033",
        "ko": "10579"
    },
    "percentiles2": {
        "total": "11193",
        "ok": "11456",
        "ko": "10765"
    },
    "percentiles3": {
        "total": "13648",
        "ok": "13664",
        "ko": "13230"
    },
    "percentiles4": {
        "total": "17485",
        "ok": "17162",
        "ko": "25156"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 120,
    "percentage": 60
},
    "group4": {
    "name": "failed",
    "count": 80,
    "percentage": 40
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "1.935",
        "ko": "1.29"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
