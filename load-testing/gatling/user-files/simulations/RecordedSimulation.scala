
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class RecordedSimulation extends Simulation {

	val httpProtocol = http
		//.baseUrl("https://biometeo.dordogne.fr/")
		.baseUrl("https://dev-mnb-biometeo.mnemotix.com/")
		.inferHtmlResources()
		.basicAuth("biometeo06", "devbiometeo06")
		.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36")

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"Cache-Control" -> "max-age=0",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"1ee7-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "none",
		"Sec-Fetch-User" -> "?1",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_2 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-None-Match" -> """W/"348-aYRT5VddmSPdXPu2vPtAiYbkeQA"""",
		"Sec-Fetch-Dest" -> "empty",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_3 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"10b1-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_4 = Map(
		"accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"accept-encoding" -> "gzip, deflate, br",
		"accept-language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"sec-fetch-dest" -> "image",
		"sec-fetch-mode" -> "no-cors",
		"sec-fetch-site" -> "cross-site")

	val headers_5 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"13439-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_6 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"4dc0-175ff7edaa8"""",
		"Origin" -> "https://test-mnb-biometeo.mnemotix.com",
		"Sec-Fetch-Dest" -> "font",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_7 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"41de-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_8 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"a020-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_9 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"17379-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_10 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"1c1c0-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_11 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"2f1b4-175ff7edaa8"""",
		"Origin" -> "https://test-mnb-biometeo.mnemotix.com",
		"Sec-Fetch-Dest" -> "font",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_12 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"5df0-175ff7edaa8"""",
		"Origin" -> "https://test-mnb-biometeo.mnemotix.com",
		"Sec-Fetch-Dest" -> "font",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_13 = Map(
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"Lang" -> "fr",
		"Origin" -> "https://test-mnb-biometeo.mnemotix.com",
		"Sec-Fetch-Dest" -> "empty",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin",
		"accept" -> "*/*",
		"content-type" -> "application/json")

	val headers_14 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"5634-175ff7edaa8"""",
		"Origin" -> "https://test-mnb-biometeo.mnemotix.com",
		"Sec-Fetch-Dest" -> "font",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_18 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"54f-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "empty",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_19 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"1b7-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_22 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"246e-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_27 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"2413-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_31 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"1b6-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_32 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"5e44-175ff7edaa8"""",
		"Origin" -> "https://test-mnb-biometeo.mnemotix.com",
		"Sec-Fetch-Dest" -> "font",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_34 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"15b4-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_35 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"82be-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_36 = Map(
		"Accept" -> "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, br",
		"Accept-Language" -> "fr,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6",
		"If-Modified-Since" -> "Wed, 25 Nov 2020 13:01:45 GMT",
		"If-None-Match" -> """W/"3d98-175ff7edaa8"""",
		"Sec-Fetch-Dest" -> "image",
		"Sec-Fetch-Mode" -> "no-cors",
		"Sec-Fetch-Site" -> "same-origin")

    val uri1 = "https://matomo.dordogne.fr"
    val uri2 = "https://images.mnemotix.com/unsafe"

	val scn = scenario("RecordedSimulation")
		.exec(http("request_0")
			.get("/")
			.headers(headers_0)
			.resources(http("request_1")
			.get(uri1 + "/matomo.js"),
            http("request_2")
			.get("/locales/fr")
			.headers(headers_2),
            http("request_3")
			.get("/src/client/public/icons/biometeoDesktop.2e7d6b968707b9d6fb87c4a16d0cc718.png")
			.headers(headers_3),
            http("request_4_matomo")
			.get(uri1 + "/matomo.php?action_name=Bio%20M%C3%A9t%C3%A9o%20Dordogne&idsite=11&rec=1&r=785976&h=15&m=41&s=0&url=https%3A%2F%2Ftest-mnb-biometeo.mnemotix.com%2F&_id=ffdd122f1c7ce1c5&_idts=1606747232&_idvc=1&_idn=0&_refts=0&_viewts=1606747232&send_image=1&pdf=1&qt=0&realp=0&wma=0&dir=0&fla=0&java=0&gears=0&ag=0&cookie=1&res=1920x1080&gt_ms=15&pv_id=cdRiTX")
			.headers(headers_4),
            http("request_5")
			.get("/src/client/public/backgrounds/air.ac92dba1d9a1d64cf9e1c407e624d7f2.jpg")
			.headers(headers_5),
            http("request_6_font")
			.get("/293e692f583af77a347b4d8d325582d7.woff")
			.headers(headers_6),
            http("request_7")
			.get("/src/client/public/icons/eauW.b51be7724f0e1e60ea5a17dfe0ec3f1d.png")
			.headers(headers_7),
            http("request_8")
			.get("/src/client/public/backgrounds/air_m.a1a83cb04d45a5a1957aef39920bcb52.jpg")
			.headers(headers_8),
            http("request_9")
			.get("/src/client/public/backgrounds/sol_m.ce00df26431d120188625add6e697273.jpg")
			.headers(headers_9),
            http("request_10")
			.get("/src/client/public/backgrounds/eau_m.d0f7d7e71c35d5e49553530a0879486e.jpg")
			.headers(headers_10),
            http("request_11")
			.get("/abaaa98155d53c0b454c3a9c68f9d89f.woff")
			.headers(headers_11),
            http("request_12")
			.get("/f637f795e3577ef09cac2284f5afdc3a.woff")
			.headers(headers_12),
            http("graphQL_news")
			.post("/graphql")
			.headers(headers_13)
			.body(RawFileBody("recordedsimulation/0013_request.json")),
            http("request_14")
			.get("/3fa36c21b8dcf577e0a42415494d0fae.woff")
			.headers(headers_14),
            http("graphql_weather")
			.post("/graphql")
			.headers(headers_13)
			.body(RawFileBody("recordedsimulation/0015_request.json")),
            http("graphql_RiverAndGroundWaterObservation")
			.post("/graphql")
			.headers(headers_13)
			.body(RawFileBody("recordedsimulation/0016_request.json")),
            http("graphql_Taxon")
			.post("/graphql")
			.headers(headers_13)
			.body(RawFileBody("recordedsimulation/0017_request.json")),
            http("request_18")
			.get("/assets/manifest.json")
			.headers(headers_18),
            http("request_19")
			.get("/src/client/public/icons/down.f5ce218366edb2c4b2bdc33e99235481.png")
			.headers(headers_19),
            http("request_20_TaxonMedia")
			.get("https://images.mnemotix.com/unsafe/800x450/https%3A%2F%2Ftaxref.mnhn.fr%2Fapi%2Fmedia%2Fdownload%2Finpn%2F101533"),
            http("request_21_TaxonMedia")
			.get("https://images.mnemotix.com/unsafe/400x275/https%3A%2F%2Ftaxref.mnhn.fr%2Fapi%2Fmedia%2Fdownload%2Finpn%2F102969"),
            http("request_22")
			.get("/src/client/public/icons/share-blue.d31b41985680cfcfed7c8a9e3636bb32.png")
			.headers(headers_22),
            http("request_23_TaxonMedia")
			.get("https://images.mnemotix.com/unsafe/400x275/https%3A%2F%2Ftaxref.mnhn.fr%2Fapi%2Fmedia%2Fdownload%2Finpn%2F146527"),
            http("request_24_TaxonMedia")
			.get("https://images.mnemotix.com/unsafe/800x450/https%3A%2F%2Ftaxref.mnhn.fr%2Fapi%2Fmedia%2Fdownload%2Finpn%2F160935"),
            http("request_25_TaxonMedia")
			.get("https://images.mnemotix.com/unsafe/400x275/https%3A%2F%2Ftaxref.mnhn.fr%2Fapi%2Fmedia%2Fdownload%2Finpn%2F254874"),
            http("request_26_TaxonMedia")
			.get("https://images.mnemotix.com/unsafe/400x275/https%3A%2F%2Ftaxref.mnhn.fr%2Fapi%2Fmedia%2Fdownload%2Finpn%2F219538"),
            http("request_27")
			.get("/src/client/public/icons/heart-blue.6f0d447a9f92fe063cf3332f3fc08206.png")
			.headers(headers_27),
            http("request_28_TaxonMedia")
			.get("https://images.mnemotix.com/unsafe/800x450/https%3A%2F%2Ftaxref.mnhn.fr%2Fapi%2Fmedia%2Fdownload%2Finpn%2F152207"),
            http("request_29_TaxonMedia")
			.get("https://images.mnemotix.com/unsafe/400x275/https%3A%2F%2Ftaxref.mnhn.fr%2Fapi%2Fmedia%2Fdownload%2Finpn%2F143130"),
            http("request_30_TaxonMedia")
			.get("https://images.mnemotix.com/unsafe/400x275/https%3A%2F%2Ftaxref.mnhn.fr%2Fapi%2Fmedia%2Fdownload%2Finpn%2F184888"),
            http("request_31")
			.get("/src/client/public/icons/up.e016190848dfa8eeb3494896bc38839f.png")
			.headers(headers_31),
            http("request_32")
			.get("/eb9dca895cf2de5deeda0ab80d2123e2.woff")
			.headers(headers_32),
            http("graphql_News")
			.post("/graphql")
			.headers(headers_13)
			.body(RawFileBody("recordedsimulation/0033_request.json")),
            http("request_34")
			.get("/assets/android-chrome-144x144.png")
			.headers(headers_34),
            http("request_35")
			.get("/src/client/public/icons/twitter.7dfe4528b5843cec2c5afb490e4d4ab7.png")
			.headers(headers_35),
            http("request_36")
			.get("/src/client/public/icons/cercle-next-x1.c8cc820804f8ed8c407b4b9a1cc3e4f0.png")
			.headers(headers_36),
            http("graphql_SearchPlaces")
			.post("/graphql")
			.headers(headers_13)
			.body(RawFileBody("recordedsimulation/0037_request.json"))))

	//setUp(scn.inject(rampUsers(2000) during (200 seconds))).protocols(httpProtocol)
	setUp(scn.inject(atOnceUsers(100))).protocols(httpProtocol)

}

