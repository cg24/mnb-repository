# Résultats de load testing


## 200 utilisateurs sur 10s

https://cg24.gitlab.io/mnb-repository/gatling/recordedsimulation-20201201162148337-200ramp10s/index.html

## 100 utilisateurs simultanés

https://cg24.gitlab.io/mnb-repository/gatling/recordedsimulation-20201201211036410-100simult/index.html

## 200 utilisateurs simultanés

https://cg24.gitlab.io/mnb-repository/gatling/recordedsimulation-20201201211733037-200simult/index.html     

## 100 utilisateurs simultanés avec cache

[Sur dev-biometeo](https://dev-mnb-biometeo.mnemotix.com/)

https://cg24.gitlab.io/mnb-repository/gatling/recordedsimulation-20201203162924502-100simult-dev-biometeo


## 2000 utilisateurs sur 5mn 

Sur l'instance test- 

https://cg24.gitlab.io/mnb-repository/gatling/recordedsimulation-20201204152914423-2000users-ramped-5mn-on-test

## 2000 utilisateurs sur 5mn 

Sur l'instance dev- 

https://cg24.gitlab.io/mnb-repository/gatling/recordedsimulation-20201204164330770-2000users-ramped-5mn-on-dev

