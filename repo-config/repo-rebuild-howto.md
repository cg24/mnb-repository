How to rebuild a repositoritory for the MNB - Biométéo application
==================================================================


# Cluster

Create 3 worker nodes with config file provided [dev-crash-mnb-config.ttl](dev-crash-mnb-config.ttl)
Create a worker and connect it.

# Loading data

* http://mnb.dordogne.fr/data/taxrefNG/	
    * all .zip from https://github.com/frmichel/taxref-ld/tree/12.0/dataset/12.0
    * all .ttl from https://github.com/frmichel/taxref-ld/tree/12.0/dataset 
* http://mnb.dordogne.fr/data/areaMappingsNG
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterarea-mappings/soil-area-mappings.trig
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterarea-mappings/water-area-mappings.trig
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterarea-mappings/air-area-mappings.trig
* http://mnb.dordogne.fr/data/taxrefMediaNG/
    * mnb-repository/media/taxref-media.ttl
* http://mnb.dordogne.fr/data/importedModelsNG 
    * https://gitlab.com/cg24/mnb-models/-/raw/master/mnb.ttl
    * https://gitlab.com/cg24/mnb-models/-/raw/master/controlled-vocs/areas.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/dsw.owl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/dwciri.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/dwcterms.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/geonames_ontology_v3.1.rdf
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/insee-geo-onto.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/prov-o-20130430.owl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/sosa.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/wgs84_pos.rdf
* http://mnb.dordogne.fr/data/riverstationNG 
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterriver-stations/river_station_on_service_in_dordogne.trig
* http://mnb.dordogne.fr/data/groundwaterStationsNG
    * https://gitlab.com/cg24/mnb-repository/-/raw/mastergroundwater-stations/groundwater-stations.trig
* http://mnb.dordogne.fr/data/airqualityNG
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterindice-atmo/airquality-stations.trig
* http://mnb.dordogne.fr/data/geolocalisationNG : 
    * https://gitlab.com/cg24/mnb-repository/-/raw/mastergeolocalisation/canton-commune-geonames-mapping.trig

# Running patches

Some design-patterns adopted in Taxref-ld makes it cumbersome to exploit it, especially regarding the indexing. To overcome these limitations we need to run the following queries:

## Media Taxref 

### filtering-images-with-wrong-ratio

```SQL
# When executed => Removed 35202 statements. 
DELETE {
    ?img a foaf:Image .
    ?img ma:frameWidth ?w .
    ?img ma:frameHeight ?h .
    ?taxon foaf:depiction ?img .
}
where { 
    ?taxon foaf:depiction ?img .
	?img a foaf:Image .
    ?img ma:frameWidth ?w .
    ?img ma:frameHeight ?h .
    BIND((?w/?h) AS ?ratio) .
    FILTER(?ratio < 1.3  || ?ratio > 2) .
} 
```

## Filtrage dans taxref

### Remove non-GBIF pages

```SQL
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX mnb: <http://mnb.dordogne.fr/ontology/>
DELETE {
    ?taxon foaf:page ?nogbifpage .
}
where 
{ 
    ?taxon a mnb:Taxon .
	?taxon foaf:page ?nogbifpage .
    FILTER(!REGEX(STR(?nogbifpage),"^https:\\/\\/www\\.gbif\\.org")) .
} 
```

### taxon-type-gbifid-status-ranks.insert
```SQL
INSERT {
    GRAPH <http://mnb.dordogne.fr/data/taxrefNG/> {
        #typage des instances de Taxon => inutile pour l'index
        #?taxon a mnb:Taxon .
        # Inutile avec Taxref13
        ?taxon <http://www.wikidata.org/entity/P846> ?gbifTaxonKey  . 
        # Pour faciliter l'indexation
        ?taxon mnb:hasBioGeographicalStatusFranceMetro ?status_clean .
        ?taxon mnb:className ?className .
        ?taxon mnb:familyName ?familyName .
        ?taxon mnb:orderName ?orderName .
    }
}
#SELECT DISTINCT ?taxon ?gbifTaxonKey
WHERE {
   	?taxon rdfs:isDefinedBy	<http://taxref.mnhn.fr/lod/taxref-ld/12.0> .
    ?taxon schema:identifier ?id .
    ?id schema:value	?gbifTaxonKey  .
    ?id schema:propertyID <http://www.wikidata.org/entity/P846> .  
    OPTIONAL{
	?taxon taxrefprop:hasStatus [rdfs:label ?status_val ;
         wd:P3005	<http://taxref.mnhn.fr/lod/location/TERFXFR> ;
         rdf:type <http://taxref.mnhn.fr/lod/status/BioGeographicalStatus>  ] .
	}
    BIND(REPLACE( ?status_val, "^[^\\(]*\\((.*)\\).*$", "$1" ) as ?status_clean)
    # Classe, ordre, famille
     OPTIONAL{
        ?taxon rdfs:subClassOf     ?order .
    	?order rdfs:label      ?orderName;
    		taxrefprop:hasRank  ?orderRk  .
    FILTER((?orderRk = taxrefrk:Order) || (?orderRk = taxrefrk:SubOrder) || (?orderRk = taxrefrk:SuperOrder) )
    }
    OPTIONAL{
        ?taxon rdfs:subClassOf 	?class .
        ?class rdfs:label ?className ;
           taxrefprop:hasRank  ?classRk  .
    FILTER((?classRk  = taxrefrk:Class) || (?classRk  = taxrefrk:SubClass) || (?classRk  = taxrefrk:SuperClass) )
    }
    OPTIONAL{
        ?taxon rdfs:subClassOf ?family .
	    ?family rdfs:label ?familyName ;
           taxrefprop:hasRank  ?familyRk  .
    FILTER((?familyRk  = taxrefrk:Family) || (?classRk  = taxrefrk:SubFamily) || (?classRk  = taxrefrk:SuperFamily) )}
    
}
```

* https://gitlab.com/cg24/mnb-repository/-/raw/mastermedia/filtering-images-with-wrong-ratio.sparql : 
* https://gitlab.com/cg24/mnb-repository/-/raw/mastertaxref-extensions/taxon-type-gbifid-status-ranks.insert.sparql


## Retirer les occurrences sans date

```SQL
#SELECT DISTINCT ?occurence
DELETE {
    ?occurence a mnb:TaxonOccurrence ;
            	 mnb:gbifTaxonKey ?gbifTaxonKey ;
        	     sosa:resultTime ?eventDate ;
				 dwciri:inDataset ?dataset ;
	             mnx:hasGeometry ?occurPoint .
  ?occurPoint geosparql:asWKT ?wktPoint;
   			  geosparql:asGML ?gmlPoint .
}   
WHERE {
   # VALUES ?gbifTaxonKey {"5229167"} .
    ?occurence a mnb:TaxonOccurrence ;
                mnb:gbifTaxonKey ?gbifTaxonKey ;
                dwciri:inDataset ?dataset ;
                mnx:hasGeometry ?occurPoint .
    ?occurPoint geosparql:asWKT ?wktPoint;
    			geosparql:asGML ?gmlPoint .
    FILTER NOT EXISTS {
       ?occurence  sosa:resultTime ?eventDate 
    }
}
```
