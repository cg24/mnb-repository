Catalogue des données ouvertes produites pour le projet MNB - BioMétéo
======================================================================



# Données disponibles

Une partie des données du Hub de Données ont été exportés sous forme de fichier téléchargeables dont les liens et la description sont donnés ici.

## Stations de mesure du niveau des rivières en Dordogne

**Mots clés** : Web Sémantique, Open Linked Data, Données Liées Ouvertes, Graphes de Connaissances, Dordogne, Hubeau, BRGM, Rivières, Stations de mesures

**description** : Localisation et informations sur les stations actives de mesure de débit et niveau des rivières localisées en Dordogne. Il s'agit ici des données Hubeau sémantisée en RDF à l'aide d'une ontologie dédiée (MNB-ontology) basée sur les standards du W3C (RDF, RDFS/S, ontologie SOSA, etc.).

**zonage géographique** : Selon ["bounding box"](https://fr.wikipedia.org/wiki/Rectangle_%C3%A0_limite_minimum) de la dordogne :`[-0.042,44.5707,1.4483,45.7146]` (source : https://github.com/etalab/fr-bounding-boxes)

**Taille** :
* 43 stations
* fichier : 28Ko
* Graphe : 478 triplets

**source** : [Hubeau, API hydrometrie](https://hubeau.eaufrance.fr/page/api-hydrometrie)

**origine de la donnée** :  Service Central d’Hydrométéorologie et d’Appui à la Prévision des Inondations (SCHAPI) et BRGM (Bureau de Recherches Géologiques et Minieres)

**format**	: RDF / Turtle

**documentation modèle** : https://gitlab.com/cg24/mnb-models 

**ressources disponibles** :
* [river_station_on_service_in_dordogne.trig](river-stations/river_station_on_service_in_dordogne.trig)
* [river_station_on_service_in_dordogne.jsonld.json (Données RDF au format jsonLD (compatible json))](river-stations/river_station_on_service_in_dordogne.jsonld.json)
* [river_station_on_service_in_dordogne.sparql-results.csv (Résultats de requêtes SPARQL au format CSV)](river-stations/river_station_on_service_in_dordogne.sparql-results.csv)



**exemples de requêtes** : https://gitlab.com/cg24/mnb-repository/-/blob/master/river-stations/river-stations-exemple-queries.sparql

## Stations de mesure du niveau des nappes phréatiques en Dordogne

**description** : Localisation des stations de mesure de niveau des nappes phréatiques en Dordogne. Données Hubeau et données fournies directement par le BRGM (code et label AQUI sur les formations hydrogéologique, et nature des stations) sémantisées en RDF à l'aide d'une ontologie dédiée (MNB-ontology) basée sur les standards du W3C (RDF, RDFS/S, ontologie SOSA, etc.)

**zonage géographique** : Selon ["bounding box"](https://fr.wikipedia.org/wiki/Rectangle_%C3%A0_limite_minimum) de la dordogne :`[-0.042,44.5707,1.4483,45.7146]` (source : https://github.com/etalab/fr-bounding-boxes)

**Taille** :
* 97 stations
* fichier : 68Ko
* Graphe : 1101 triplets

**source** : [Hubeau, API hydrometrie](https://hubeau.eaufrance.fr/page/api-hydrometrie)

**origine de la donnée** :  Service Central d’Hydrométéorologie et d’Appui à la Prévision des Inondations (SCHAPI) et BRGM (Bureau de Recherches Géologiques et Minières)

**format**	: RDF / Turtle

**documentation modèle** : https://gitlab.com/cg24/mnb-models 

**ressources disponibles** :
* [groundwater-stations.trig (Données RDF au format .trig)](groundwater-stations/groundwater-stations.trig)
* [groundwater-stations.jsonld.json (Données RDF au format jsonLD (compatible json))](groundwater-stations/groundwater-stations.jsonld.json)
* [groundwater-stations.sparql-results.csv (Résultats de requêtes SPARQL au format CSV)](groundwater-stations/groundwater-stations.sparql-results.csv)

**exemples de requêtes** : https://gitlab.com/cg24/mnb-repository/-/blob/master/groundwater-stations/groundwater-stations-exemple.sparql

## Occurrence espèces GBIF

**description** : Localisation, clé de taxon GBIF, et date des occurrences validées par des experts ("research grade") d'espèces localisées en Dordogne. Il s'agit ici des données d'occurrence du GBIF sémantisée en RDF à l'aide d'une ontologie dédiée (MNB-ontology) basée sur les standards du W3C (RDF, RDFS/S, ontologie SOSA, etc.). 

**zonage géographique** : Selon contour simplifiés du département de Dordogne (cf fichier [dordogne-polygon.json]()) 

**Taille** :
* 562971 occurrences
* fichier : zippé = 13,6 Mo, décompressé = 297,4 Mo 
* Graphe : 4 503 712 triplets

**source** : [Occurrence GBIF en Dordogne](https://www.gbif.org/occurrence/download/0065750-200221144449610) 

**origine de la donnée** :  Données aggrégées par le GBIF à partir de 290 jeux de données (lien de citation : https://doi.org/10.15468/dl.96cvwg, listes des jeux de données gbif-dordogne-occurrences-static-bkup/citations.txt)

**format**	: RDF / Turtle

**documentation modèle** : https://gitlab.com/cg24/mnb-models 

**ressources disponibles** : 
* [gbif-occurrence-dordogne-18052020.trig.zip (Données RDF au format .trig zippé)](gbif-occurrences/gbif-occurrence-dordogne-18052020.trig.zip)
* [100 premiers résultats de requêtes SPARQL au format CSV](gbif-occurrences/gbif-occurrence-dordogne-18052020.100-first-sparql-results.csv)


## Liens vers formations
**Introduction au Web de Données Ouvertes Liées (LOD)** : Une introduction au Web de Données Liées : [Formation Web3.0 (Dordogne).pdf](https://gitlab.com/cg24/mnb-repository/-/blob/master/doc)

**Tutoriel SPARQL (langage de requête des données LOD)** :

Wikidata propose cet excellent tutoriel avec de nombreux exemples que l'on peut essayer directement sur la base de données Wikidata: https://www.wikidata.org/wiki/Wikidata:SPARQL_tutorial/fr

# Comment utiliser les données

Des exemples de requêtes sont proposés en lien pour chaque donnée proposée ici. Les données peuvent être requêtées via l'interface SPARQL du Hub de Données, ou via un triple store installé par vos soins.

## 1. via l'interface SPARQL du Hub de Données de la Biométéo

Les données proposées ici sont chargées dans la base de données, appelé "Hub de Données", utilisée par l'application BioMétéo.

Disponible à l'URL https://graphdb.mnemotix.com/sparql avec les autentifiants suivant : login = `biometeo-visiteur`, mot de passe = `MNBlod2020` et en choisissant le "repository" `prod-mnb`

Les requêtes exemples sont directement exécutables dans l'interface SPARQL du Hub de Données.

## 2. via un triple store

Pour pouvoir requêter les données via un triple store vous devez:

1. télécharger les fichiers de votre choix
2. télécharger et installer un triple store comme e.g graphDB : https://www.ontotext.com/try-graphdb-se/
3. charger les données dans le triplestore
4. charger les modèles (ontologies):
    * https://gitlab.com/cg24/mnb-models/-/raw/master/mnb.ttl
    * https://gitlab.com/cg24/mnb-models/-/raw/master/controlled-vocs/areas.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/dsw.owl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/dwciri.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/dwcterms.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/geonames_ontology_v3.1.rdf
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/insee-geo-onto.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/prov-o-20130430.owl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/sosa.ttl
    * https://gitlab.com/cg24/mnb-repository/-/raw/masterimported-models/wgs84_pos.rdf
5. exécuter les requêtes via l'interface sparql du triple store installé
