
Documentation du projet MNB - Biométéo
======================================


[Ces pages](https://cg24.gitlab.io/mnb-repository/) détaillent le fonctionnement de l'application Biométéo et du Hub de données. Elles servent de documentation à la fois pour les usagers et ceux qui sont chargés de l'animer au travers de la connexion des actualités avec le compte Twitter @biometeo24.

L'application biométéo est disponible à l'URL https://biometeo.dordogne.fr

Projet porté par le département de Dordogne et réalisé par Mnémotix

<img src="dordogne-logo-gris.png" style="width:20%">
<img src="logo-couleurs-01.png" style="width:70%">

