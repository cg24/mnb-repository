Zonage Air-Sol-Eau de la Biométéo
==================================


# Associer espèces aux "zones"

On peut définir 3 zones Air / Sol / Eau:

```ttl
@PREFIX mnbv: <http://mnb.dordogne.fr/vocabulary#>

mnbv:AirArea  a skos:Concept .
mnbv:SoilArea a skos:Concept .
mnbv:WaterArea a skos:Concept .
```
# Construction des données d'association entre taxons et zone

NB: On associe les taxons de tous les rangs à priori, sauf pour les oiseaux où on ne garde que les rangs en dessous de "ordre".

```SQL 
PREFIX dwc: <http://rs.tdwg.org/dwc/terms/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX taxrefprop: <http://taxref.mnhn.fr/lod/property/>
PREFIX taxrefrk: <http://taxref.mnhn.fr/lod/taxrank/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX mnb: <http://mnb.dordogne.fr/ontology#>
PREFIX mnbv: <http://mnb.dordogne.fr/vocabulary#>
PREFIX taxref: <http://taxref.mnhn.fr/lod/>

# Tous les oiseaux sont dans AirArea
construct {
    ?taxon mnb:area mnbv:AirArea .
} where {
    ?taxon a                owl:Class;
        #taxrefprop:hasRank  taxrefrk:Species;
    	rdfs:subClassOf 	<http://taxref.mnhn.fr/lod/taxon/185961/12.0> .
    }

# Tous ceux qui ont pour habitat "Terrestrial", sauf les oiseaux, sont dans la zone SoilArea
construct {    
    ?earthTax mnb:area mnbv:SoilArea .
} 
where {
    ?earthTax a owl:Class ;
	    taxrefprop:habitat taxrefhab:Terrestrial .
    MINUS{
       ?earthTax rdfs:subClassOf 	<http://taxref.mnhn.fr/lod/taxon/185961/12.0> . 
    }
}

# Habitat "Marine/FreshWater/BrackishWater", sauf les oiseaux => WaterArea
construct {
    ?waterTax mnb:area mnbv:WaterArea .
} 
where {
    ?waterTax a owl:Class ;
        taxrefprop:habitat ?waterHab .
    FILTER((?waterHab = taxrefhab:Marine) ||(?waterHab = taxrefhab:FreshWater )||(?waterHab = taxrefhab:BrackishWater )) .
    MINUS{
       ?waterTax rdfs:subClassOf 	<http://taxref.mnhn.fr/lod/taxon/185961/12.0> . 
    }
}
```

# Requête exemple


```

PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <http://schema.org/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX taxrefstatus: <http://taxref.mnhn.fr/lod/status/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX taxrefbgs: <http://taxref.mnhn.fr/lod/bioGeoStatus/>
PREFIX dwc: <http://rs.tdwg.org/dwc/terms/>
prefix mnb: <http://mnb.dordogne.fr/ontology/> 
prefix mnbv: <http://mnb.dordogne.fr/vocabulary/> 
PREFIX taxrefrk: <http://taxref.mnhn.fr/lod/taxrank/>
PREFIX taxrefprop: <http://taxref.mnhn.fr/lod/property/>  
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
prefix sosa: <http://www.w3.org/ns/sosa/> 
PREFIX ma: <http://www.w3.org/ns/ma-ont#> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 

# Récupérer 3 taxons de la zone "air" ayant des données d'occurrence
SELECT DISTINCT ?taxon ?nom_scientifique ?vname  ?countOccurrence ?gbifTaxonKey 
WHERE{  
    # Lien entre ?taxon comme classe TaxRef, et la clé GBIF 
    ?taxon taxrefprop:hasReferenceName/schema:identifier/schema:propertyID wd:P846 .
    ?taxon taxrefprop:hasReferenceName/schema:identifier/ schema:value	?gbifTaxonKey .
    ?taxon rdfs:isDefinedBy	<http://taxref.mnhn.fr/lod/taxref-ld/12.0> .
    # Taxon dans la zone "Air" 
    ?taxon mnb:area mnbv:AirArea
    # Nombre d'occurrence pour le taxon
    {   SELECT ?gbifTaxonKey (COUNT(?occurrence) as ?countOccurrence)
        WHERE {
        	 ?occurrence a mnb:TaxonOccurrence ;
                      mnb:gbifTaxonKey ?gbifTaxonKey.  
        }
        GROUP BY ?gbifTaxonKey 
    } .
    # détails sur le taxon
    ?taxon rdfs:label ?nom_scientifique .
    ?taxon taxrefprop:vernacularName ?vname .
    FILTER(LANG(?vname) = "fr") .
    
}
LIMIT 3

```


Résultat attendu. Les données précises peuvent différer, mais la structure de la réponse doit être similaire.

|   |          taxon         | nom_scientifique |         vname         |   countOccurrence  | gbifTaxonKey |
|:-:|:----------------------:|:----------------:|:---------------------:|:------------------:|:------------:|
| 1 | taxref:taxon/1958/12.0 | Anas crecca      | "Sarcelle d'hiver"@fr | "125"^^xsd:integer | 8214667      |
| 2 | taxref:taxon/1973/12.0 | Anas acuta       | "Canard pilet"@fr     | "27"^^xsd:integer  | 2498112      |
| 3 | taxref:taxon/3297/12.0 | Larus fuscus     | "Goéland brun"@fr     | "16"^^xsd:integer  | 2481174      |


Autres requêtes pour taxons: [taxons-example-requests.sparql](taxons-example-requests.sparql)