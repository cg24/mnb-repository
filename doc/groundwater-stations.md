Groundwater stations
=====================

# How to generate static data about groundwater stations
Explanations on how to build and update data about Groundwater stations within the BBOX of Dordogne Département.

## Geographic boundaries

Here we use the BBox surrounding the geographic boundaries of the Département in order to include stations in the vicinity which are relevant to the status of Groundwater ressources of the Département.

BBOX dordogne
`{"id":"fr:departement:24","name":"DORDOGNE","bbox":[-0.042,44.5707,1.4483,45.7146]}`

BBOX aggrandie de 30km ~ 0,3 degree

`{"id":"fr:departement:24","name":"DORDOGNE","bbox":[-0.342,44.2707,1.7483,46.0146]}`

[[[-0.2396311335,44.4589811735],[1.6312010717,44.4589811735],[1.6312010717,45.8786638859],[-0.2396311335,45.8786638859],[-0.2396311335,44.4589811735]]]
Obtenue via https://github.com/etalab/fr-bounding-boxes

## Source data 

Data is generated from `points_mesures_niveaux_nappes_synthese.tsv` which is itself built from the alignement of 3 files:
* labels_aqui.csv : This file reports the labels of each of th the AQUI code given in the mapping_BSS_Aqui_stationType file. AQUI codes corresponds to stratigraphic informations linked to the hydrogeologic entities the station is a sample of. 

* mapping_BSS_AQUI_stationType.tsv: This file contains info given by BRGM. They have been manually reconstructed by Brue Ayach (Ayache Bruce <b.ayache@brgm.fr>). 
    * The "Aqui code" corresponds to stratigraphic infos linked to the station. When a given station  have several "Aqui code", it is reported in a new line. Each row can have only 1 "Aqui Code"
    * "stationType" reports the type of the station with respect to the method of extraction.

* station_ades.csv : This file is a direct extraction of the Hubeau API and contains all the basis about each station : localisation, commune, BDLisa entities, etc.

    * API call : https://hubeau.eaufrance.fr/api/v1/niveaux_nappes/stations.csv?bbox=-0.042%2C44.5707%2C1.4483%2C45.7146&date_recherche=2020-01-01
    * Parameters :  
        * bbox = Dordogne BBox (see above)
        * date_recherche = 2020-01-01.  
    * RMQ : the extraction has been done the 10th of may, but we took some margin with "date_recherche" here Óbecause this info is not accurately linked to the date of the latest measure available. Nor is the "data_fin_mesure" in the resulting table. Many stations actually have measures available after that date.
    * RMQ2 : to get the same data in json, simply use the endpoint `stations` e.g https://hubeau.eaufrance.fr/api/v1/niveaux_nappes/stations?bbox=-0.042%2C44.5707%2C1.4483%2C45.7146&date_recherche=2020-01-01

## Transformation with Ontorefine

1. Create a new project on OntoText Ontorefine from the TSV file `points_mesures_niveaux_nappes_synthese.tsv`. No transformation needed on the tabular data
2. Adjust the RDF settings from the menu:
  * click on RDF / Data / RDF settings
  * set prefix and namespace to `mnb: <http://mnb.dordogne.fr/ontology/>`, save
  * click "Generate CONSTRUCT", then "Open in main SPARQL editor"
  * Copy paste the query located in [groundwater_construct_queries.sparql](../groundwater-stations/groundwater_construct_queries.sparql), and execute it
  * download as .TRIG file 
  * open downloaded file and set named graph as `<http://mnb.dordogne.fr/data/groundwaterNG>`

# Requêtes exemples

Requête SPARQL pour lister les stations et toutes les informations disponibles

 
```SQL
PREFIX mnb: <http://mnb.dordogne.fr/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
prefix geo: <http://www.opengis.net/ont/geosparql#> 

select distinct *
where { 
	?station a mnb:GroundWaterLevelMS.
    ?station mnb:bssCode ?bssCode .
    ?station rdfs:label ?stationLabel .
    ?station mnx:hasGeometry/geo:asWKT ?locCoord . 
    OPTIONAL{?station sosa:isSampleOf ?hydrogeo .}
	  ?station mnb:labelAQUI ?labelAQUI .
    ?station mnb:stationType ?stationType .
} 
``` 

Résultat attendu

56 résultats. Voir 1ère ligne ci-après:

| station                                                     | bssCode          | stationLabel               | locCoord                       | hydrogeo                                           | labelAQUI             | stationType        |
|-------------------------------------------------------------|------------------|----------------------------|--------------------------------|----------------------------------------------------|-----------------------|--------------------|
| http://services.ades.eaufrance.fr/pointeau/08081X0026/SE.20 | 08081X0026/SE.20 | COMBE BOYER (MARQUAY - 24) | POINT(1.148944586 44.92323244) | https://data.geoscience.fr/id/hydrogeounit/348AA01 |  Coniacien-Santonien  | Forage Peu Profond |


