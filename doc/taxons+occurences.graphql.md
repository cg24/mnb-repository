Taxons + Occurrences GraphQL endpoint
=====================================


# Description du service

Renvoie une sélection de 3 taxons par zone Air/Sol/Eau regroupant les données : 

* taxonomiques (classes, noms sci., habitat, statut, etc)
* media
* occurrences (mises à jour quotidiennement) :  nombre sur la Dordogne et calcul de la distance de l’occurrence la plus proche


# Requêtes

## Documentation 

This service returns a random list of taxons filtered by following parameters :
       
Parameters :
  - geonamesId: [REQUIRED] Geonames place id.
  - area: [Optional] Taxon area to filter on (Eau, Sol, Air)
  - seed: [Optional] Use it for testing purposes to test results controlled randomness
  - taxonId: [Optional] Use it to force taxon presence in the top of the list


## Exemple

RQM : For easy testing, go to https://test-mnb-biometeo.mnemotix.com/graphql/playground in order to test the following graphQL requests:

Request to fetch a selection of 3 taxons per area "Air/Sol/Eau" for the geonames location id = 6429505 ("Coly-Saint-Amand")

```js 
{
  taxonsSol: taxons(geonamesId: "geonames:6429505", area: Sol) 
  {
      edges {
        node {
          ...TaxonFragment
      }
    }
  }
  taxonsAir: taxons(geonamesId: "geonames:6429505", area: Air)
  {
    edges {
      node {
        ...TaxonFragment
      }
    }
  }
  taxonsEau: taxons(geonamesId: "geonames:6429505", area: Eau)
  {
    edges {
      node {
        ...TaxonFragment
      }
    }
  }
}
fragment TaxonFragment on Taxon {
  id
  area
  label
  pictures{edges{node{
    id
    copyright
    license
  }}}
  familyName
  vernacularName
  orderName
  bioGeographicalStatus
  habitats
  className
  homepage
  page
  occurrencesCount
  closestOccurrence(geonamesId: "geonames:6429505")
  __typename
}

```

1 exemple de résultat attendu. Les données précises peuvent différer, mais la structure de la réponse doit être similaire (sauf évolution de l'API par rapport au 06/08/2020)

```json
{
  "data": {
    "taxonsSol": {
      "edges": [
        {
          "node": {
            "id": "http://taxref.mnhn.fr/lod/taxon/110221/12.0",
            "area": "Sol",
            "label": "Ononis pusilla",
            "pictures": {
              "edges": [
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/93444",
                    "copyright": "P. Gourdain",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/93445",
                    "copyright": "P. Gourdain",
                    "license": "CC BY-NC-SA"
                  }
                }
              ]
            },
            "familyName": "Fabaceae",
            "vernacularName": "Bugrane naine, Ononis de Colonna, Ononis grêle, Bugrane de Colonna",
            "orderName": "Fabales",
            "bioGeographicalStatus": "Présent (indigène ou indéterminé)",
            "habitat": "Eau douce",
            "className": "Equisetopsida",
            "homepage": "https://inpn.mnhn.fr/espece/cd_nom/110221?lg=en",
            "page": "https://www.tela-botanica.org/bdtfx-nn-44863",
            "occurrencesCount": 429,
            "closestOccurrence": 4190.972548522283,
            "__typename": "Taxon"
          }
        },
        {
          "node": {
            "id": "http://taxref.mnhn.fr/lod/taxon/111420/12.0",
            "area": "Sol",
            "label": "Ornithopus pinnatus",
            "pictures": {
              "edges": [
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/252179",
                    "copyright": "E. VALLEZ / CBNSA",
                    "license": "CC BY-SA"
                  }
                }
              ]
            },
            "familyName": "Fabaceae",
            "vernacularName": "Ornithope penné",
            "orderName": "Fabales",
            "bioGeographicalStatus": "Présent (indigène ou indéterminé)",
            "habitat": "Eau douce",
            "className": "Equisetopsida",
            "homepage": "https://inpn.mnhn.fr/espece/cd_nom/111420?lg=en",
            "page": "https://www.tela-botanica.org/bdtfx-nn-46571",
            "occurrencesCount": 3,
            "closestOccurrence": 13263.460459029644,
            "__typename": "Taxon"
          }
        },
        {
          "node": {
            "id": "http://taxref.mnhn.fr/lod/taxon/102974/12.0",
            "area": "Sol",
            "label": "Hordeum murinum",
            "pictures": {
              "edges": [
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/150908",
                    "copyright": "O. Roquinarc'h",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/94718",
                    "copyright": "P. Gourdain",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/94719",
                    "copyright": "P. Gourdain",
                    "license": "CC BY-NC-SA"
                  }
                }
              ]
            },
            "familyName": "Poaceae",
            "vernacularName": "Orge sauvage, Orge Queue-de-rat",
            "orderName": "Poales",
            "bioGeographicalStatus": "Présent (indigène ou indéterminé)",
            "habitat": "Eau douce",
            "className": "Equisetopsida",
            "homepage": "https://inpn.mnhn.fr/espece/cd_nom/102974?lg=en",
            "page": "https://www.tela-botanica.org/bdtfx-nn-34857",
            "occurrencesCount": 161,
            "closestOccurrence": 4190.972548522283,
            "__typename": "Taxon"
          }
        }
      ]
    },
    "taxonsAir": {
      "edges": [
        {
          "node": {
            "id": "http://taxref.mnhn.fr/lod/taxon/3603/12.0",
            "area": "Air",
            "label": "Picus viridis",
            "pictures": {
              "edges": [
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/136016",
                    "copyright": "O. Roquinarc'h",
                    "license": "CC BY-NC-SA"
                  }
                }
              ]
            },
            "familyName": "Picidae",
            "vernacularName": "Pic vert, Pivert",
            "orderName": "Piciformes",
            "bioGeographicalStatus": "Présent (indigène ou indéterminé)",
            "habitat": "Terrestre",
            "className": "Aves",
            "homepage": "https://inpn.mnhn.fr/espece/cd_nom/3603?lg=en",
            "page": "https://www.gbif.org/species/2478523",
            "occurrencesCount": 603,
            "closestOccurrence": 4190.972548522283,
            "__typename": "Taxon"
          }
        },
        {
          "node": {
            "id": "http://taxref.mnhn.fr/lod/taxon/2489/12.0",
            "area": "Air",
            "label": "Bubulcus ibis",
            "pictures": {
              "edges": [
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/102747",
                    "copyright": "A. Horellou",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/102748",
                    "copyright": "A. Horellou",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/102750",
                    "copyright": "J.P. Siblet",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/129681",
                    "copyright": "S. Siblet",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/137266",
                    "copyright": "J. LAIGNEL",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/195189",
                    "copyright": "J.P. Siblet",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/230416",
                    "copyright": "J. LAIGNEL",
                    "license": "CC BY-SA"
                  }
                }
              ]
            },
            "familyName": "Ardeidae",
            "vernacularName": "Héron garde-boeufs, Pique bœufs",
            "orderName": "Pelecaniformes",
            "bioGeographicalStatus": "Présent (indigène ou indéterminé)",
            "habitat": "Eau douce",
            "className": "Aves",
            "homepage": "https://inpn.mnhn.fr/espece/cd_nom/2489?lg=en",
            "page": "https://www.gbif.org/species/2480830",
            "occurrencesCount": 216,
            "closestOccurrence": 18410.998942634622,
            "__typename": "Taxon"
          }
        },
        {
          "node": {
            "id": "http://taxref.mnhn.fr/lod/taxon/3493/12.0",
            "area": "Air",
            "label": "Bubo bubo",
            "pictures": {
              "edges": [
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/103128",
                    "copyright": "P. Gourdain",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/176668",
                    "copyright": "C. Thierry",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/218270",
                    "copyright": "J.-P. Siblet",
                    "license": "CC BY-NC-SA"
                  }
                }
              ]
            },
            "familyName": "Strigidae",
            "vernacularName": "Grand-duc d'Europe",
            "orderName": "Strigiformes",
            "bioGeographicalStatus": "Présent (indigène ou indéterminé)",
            "habitat": "Eau douce",
            "className": "Aves",
            "homepage": "https://inpn.mnhn.fr/espece/cd_nom/3493?lg=en",
            "page": "https://www.gbif.org/species/5959092",
            "occurrencesCount": 28,
            "closestOccurrence": 4190.972548522283,
            "__typename": "Taxon"
          }
        }
      ]
    },
    "taxonsEau": {
      "edges": [
        {
          "node": {
            "id": "http://taxref.mnhn.fr/lod/taxon/115228/12.0",
            "area": "Eau",
            "label": "Potamogeton alpinus",
            "pictures": {
              "edges": [
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/154754",
                    "copyright": "O. Nawrot",
                    "license": "CC BY-NC-SA"
                  }
                }
              ]
            },
            "familyName": "Potamogetonaceae",
            "vernacularName": "Potamot des Alpes",
            "orderName": "Alismatales",
            "bioGeographicalStatus": "Présent (indigène ou indéterminé)",
            "habitat": "Eau douce",
            "className": "Equisetopsida",
            "homepage": "https://inpn.mnhn.fr/espece/cd_nom/115228?lg=en",
            "page": "https://www.tela-botanica.org/bdtfx-nn-52125",
            "occurrencesCount": 2,
            "closestOccurrence": 21379.571790898266,
            "__typename": "Taxon"
          }
        },
        {
          "node": {
            "id": "http://taxref.mnhn.fr/lod/taxon/106742/12.0",
            "area": "Eau",
            "label": "Ludwigia grandiflora",
            "pictures": {
              "edges": [
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/195031",
                    "copyright": "O. Roquinarc'h",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/195033",
                    "copyright": "O. Roquinarc'h",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/208442",
                    "copyright": "R. Dupré MNHN/CBNBP",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/208443",
                    "copyright": "R. Dupré MNHN/CBNBP",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/208444",
                    "copyright": "R. Dupré MNHN/CBNBP",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/256394",
                    "copyright": "None",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/299141",
                    "copyright": "None",
                    "license": "CC BY-NC-SA"
                  }
                },
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/299142",
                    "copyright": "None",
                    "license": "CC BY-NC-SA"
                  }
                }
              ]
            },
            "familyName": "Onagraceae",
            "vernacularName": "Ludwigie à grandes fleurs, Jussie à grandes fleurs",
            "orderName": "Myrtales",
            "bioGeographicalStatus": "Introduit envahissant",
            "habitat": "Eau douce",
            "className": "Equisetopsida",
            "homepage": "https://inpn.mnhn.fr/espece/cd_nom/106742?lg=en",
            "page": "https://www.gbif.org/species/5421039",
            "occurrencesCount": 40,
            "closestOccurrence": 14781.27960178275,
            "__typename": "Taxon"
          }
        },
        {
          "node": {
            "id": "http://taxref.mnhn.fr/lod/taxon/75857/12.0",
            "area": "Eau",
            "label": "Gomphonema augur",
            "pictures": {
              "edges": [
                {
                  "node": {
                    "id": "https://taxref.mnhn.fr/api/media/download/inpn/285275",
                    "copyright": "D. Milan",
                    "license": "CC BY-NC-SA"
                  }
                }
              ]
            },
            "familyName": "Gomphonemataceae",
            "vernacularName": null,
            "orderName": "Cymbellales",
            "bioGeographicalStatus": "Présent (indigène ou indéterminé)",
            "habitat": "Eau douce",
            "className": "Bacillariophyceae",
            "homepage": "https://inpn.mnhn.fr/espece/cd_nom/75857?lg=en",
            "page": "https://www.gbif.org/species/8170732",
            "occurrencesCount": 1,
            "closestOccurrence": 78527.88735138297,
            "__typename": "Taxon"
          }
        }
      ]
    }
  }
}
```
