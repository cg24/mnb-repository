River level stations of measurement
===================================

locations and details about each river station in Dordogne

# How to construct the static data ?

## Geographic boundaries

Merely the dordogne department using `code_departement=24` parameter in the API

## Data source

Using Hubeau hydrometrie API:


https://hubeau.eaufrance.fr/api/v1/hydrometrie/referentiel/stations.csv?bbox=-0.342,44.2707,1.7483,46.0146&en_service=true

CSV Result file available at [river-stations/river_station_on_service_in_dordogne.csv](../river-stations/river_station_on_service_in_dordogne.csv)

Map showing the localisation of the stations https://datawrapper.dwcdn.net/y1Zg0/3/ 

## Transformation with Ontorefine

1. Create a new project on OntoText Ontorefine from the CSV file river_station_on_service_in_dordogne.csv. No transformation needed on the tabular data
2. Adjust the RDF settings from the menu:
  * click on RDF / Data / RDF settings
  * set prefix and namespace to `mnb:http://mnb.dordogne.fr/ontology/`, save
  * click "Generate CONSTRUCT", then "Open in main SPARQL editor"
  * Copy paste the query located in [river-construct-queries.sparql](../river-stations/river-construct-queries.sparql), and execute it
  * download as .TRIG file 
  * open downloaded file and set named graph as `<http://mnb.dordogne.fr/data/riverstationNG>`


# Requête exemple

Requête SPARQL listant les stations de rivières:
  
```SQL
# Query listing all river stations (43) 
select distinct *
where { 
	?station a mnb:RiverLevelMS .
    ?station mnb:stationCode ?stationCode .
    ?station rdfs:label ?stationLabel .
    ?river a mnb:River .
    ?station sosa:isSampleOf ?river .
    ?river rdfs:label ?river_label .
    ?station mnx:hasGeometry/geo:asWKT ?locCoord . 
} 

```

1 exemple de résultat attendu. Les données précises peuvent différer, mais la structure de la réponse doit être similaire (sauf évolution de l'API par rapport au 06/08/2020)

| station                                             | stationCode | stationLabel                                  | river                               | river_label | locCoord                                    |
|-----------------------------------------------------|-------------|-----------------------------------------------|-------------------------------------|-------------|---------------------------------------------|
| http://mnb.dordogne.fr/data/riverstation/O925500101 | O925500101  | La Banège à Plaisance [Moulin de la Ferrière] | http://id.eaufrance.fr/CEA/O9250500 | La Banège   | POINT(0.5495414360057992 44.68974723833916) |
|

