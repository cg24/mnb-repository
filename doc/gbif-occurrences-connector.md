GBIF Taxon Occurrences in Dordogne connector
============================================

# Abstract
* The aim of this connector is to retrieve **daily** the taxons occurrence data for the Dordogne department available from GBIF.org
* Each occurrence data point will be transformed in RDF and added in the triplestore (or updated if already present in the graph)

# Occurrence API calls 

## Parameters

API doc : https://www.gbif.org/developer/occurrence

Limitations : 

* individual requests for each page
* maximum 300 records per page
* hard limit for any query >= 100,000 records. Use https://www.gbif.org/developer/occurrence#download instead

Ressource URL : https://api.gbif.org/v1/occurrence/search

Parameters :

* `has_coordinate=true`
* `has_geospatial_issue=false`
* `year=*,*` (to filter out occurrences without a `eventDate`)
* `lastInterpreted={{yesterdays-date}},*` : YYYY-MM-DD format; value set to `D-1` to get new increments, given that we collect data on a daily basis.
* `geometry=POLYGON((0.62974%2045.71457,0.30267%2045.45906,0.2666%2045.29775,-0.01785%2045.16922,0.07329%2045.07012,-0.03421%2044.85211,0.28515%2044.86456,0.35459%2044.65481,0.79772%2044.70177,0.8425%2044.60096,1.10321%2044.57173,1.44193%2044.87758,1.41289%2045.12509,1.22781%2045.20603,1.32279%2045.38266,1.02333%2045.60893,0.8115%2045.57587,0.62974%2045.71457))` 

Pagination parameters :
* `limit=300` : max number of results. Cannot exceed 300
* `offset=300` : offset in the results set for accessing subsequent page(s)


** RMK on `geometry`** : simplified (by GBIF search interface) version of the following geometry: https://france-geojson.gregoiredavid.fr/repo/departements/24-dordogne/departement-24-dordogne.geojson


## Example API call JSON response

Example call with parameter `lastInterpretedDate=2020-05-24,*`: 

https://api.gbif.org/v1/occurrence/search?has_coordinate=true&has_geospatial_issue=false&year=*,*&lastInterpreted=2020-06-07,*&geometry=POLYGON((0.62974%2045.71457,0.30267%2045.45906,0.2666%2045.29775,-0.01785%2045.16922,0.07329%2045.07012,-0.03421%2044.85211,0.28515%2044.86456,0.35459%2044.65481,0.79772%2044.70177,0.8425%2044.60096,1.10321%2044.57173,1.44193%2044.87758,1.41289%2045.12509,1.22781%2045.20603,1.32279%2045.38266,1.02333%2045.60893,0.8115%2045.57587,0.62974%2045.71457))


```json

{
  "offset": 0,
  "limit": 20,
  "endOfRecords": true,
  "count": 15,
  "results": [
    {
      "key": 2609680048,
      "datasetKey": "b1047888-ae52-4179-9dd5-5448ea342a24",
      "publishingOrgKey": "1f00d75c-f6fc-4224-a595-975e82d7689c",
      "installationKey": "85ccfd1a-a837-48d6-9a87-96c99f6fe012",
      "publishingCountry": "NL",
      "protocol": "DWC_ARCHIVE",
      "lastCrawled": "2020-05-25T08:31:20.855+0000",
      "lastParsed": "2020-05-25T08:40:03.469+0000",
      "crawlId": 75,
      "extensions": {
        
      },
      "basisOfRecord": "HUMAN_OBSERVATION",
      "taxonKey": 2487879,
      "kingdomKey": 1,
      "phylumKey": 44,
      "classKey": 212,
      "orderKey": 729,
      "familyKey": 9327,
      "genusKey": 2487875,
      "speciesKey": 2487879,
      "acceptedTaxonKey": 2487879,
      "scientificName": "Cyanistes caeruleus (Linnaeus, 1758)",
      "acceptedScientificName": "Cyanistes caeruleus (Linnaeus, 1758)",
      "kingdom": "Animalia",
      "phylum": "Chordata",
      "order": "Passeriformes",
      "family": "Paridae",
      "genus": "Cyanistes",
      "species": "Cyanistes caeruleus",
      "genericName": "Cyanistes",
      "specificEpithet": "caeruleus",
      "taxonRank": "SPECIES",
      "taxonomicStatus": "ACCEPTED",
      "decimalLongitude": 1.3707,
      "decimalLatitude": 44.8583,
      "year": 2020,
      "month": 1,
      "day": 26,
      "eventDate": "2020-01-26T00:00:00",
      "issues": [
        
      ],
      "lastInterpreted": "2020-05-25T08:40:03.469+0000",
      "references": "https://data.biodiversitydata.nl/xeno-canto/observation/XC525301",
      "license": "http://creativecommons.org/licenses/by-nc/4.0/legalcode",
      "identifiers": [
        
      ],
      "media": [
        {
          "type": "Sound",
          "format": "audio/mpeg",
          "identifier": "https://www.xeno-canto.org/sounds/uploaded/OYXLQBTHRH/XC525301-MixPre-050_Cyanistes%20caeruleus_YB.mp3",
          "description": "21 s",
          "creator": "Yoann Blanchon",
          "license": "http://creativecommons.org/licenses/by-nc-sa/4.0/",
          "rightsHolder": "Yoann Blanchon"
        },
        {
          "type": "StillImage",
          "format": "image/png",
          "identifier": "https://www.xeno-canto.org/sounds/uploaded/OYXLQBTHRH/ffts/XC525301-large.png",
          "description": "Sonogram of the first ten seconds of the sound recording",
          "creator": "Stichting Xeno-canto voor Natuurgeluiden",
          "license": "http://creativecommons.org/licenses/by-nc-sa/4.0/",
          "rightsHolder": "Stichting Xeno-canto voor Natuurgeluiden"
        }
      ],
      "facts": [
        
      ],
      "relations": [
        
      ],
      "geodeticDatum": "WGS84",
      "class": "Aves",
      "countryCode": "FR",
      "recordedByIDs": [
        
      ],
      "identifiedByIDs": [
        
      ],
      "country": "France",
      "rightsHolder": "Yoann Blanchon",
      "identifier": "525301@XC",
      "verbatimEventDate": "2020-01-26",
      "nomenclaturalCode": "ICZN",
      "locality": "Saint-Julien-de-Lampon, Dordogne, Nouvelle-Aquitaine",
      "gbifID": "2609680048",
      "collectionCode": "Bird sounds",
      "occurrenceID": "https://data.biodiversitydata.nl/xeno-canto/observation/XC525301",
      "recordedBy": "Yoann Blanchon",
      "catalogNumber": "XC525301",
      "vernacularName": "Eurasian Blue Tit",
      "fieldNotes": "SennheiserME66-K6/MixPre 3 II ; High Pass Filter 2000Hz 6db  bird-seen:yes  playback-used:no",
      "eventTime": "12:00",
      "verbatimElevation": "110 m",
      "behavior": "call",
      "higherClassification": "Animalia|Paridae"
    },
    {...}
  ]}
``` 

# JSON to RDF mapping

## JSON fields to keep

```json

{
  "key": 2609680048,
  "datasetKey": "b1047888-ae52-4179-9dd5-5448ea342a24",
  "taxonKey": 2487879,
  "decimalLongitude": 1.3707,
  "decimalLatitude": 44.8583,
  "eventDate": "2020-01-26T00:00:00",
  "lastInterpreted": "2020-05-25T08:40:03.469+0000", 
  }
```

## URI Scheme

* Instances of `mnb:TaxonOccurrence` : `<https://www.gbif.org/occurrence/{{key}}>`
* Instances of `mnx:Geometry` : `<https://www.gbif.org/occurrence/{{key}}#point>`
* Targets of `prov:hadPrimarySource` : `<https://www.gbif.org/dataset/{{datasetKey}}>`

**NB**:  `mnb:TaxonOccurrence` instances are actual browsable ressources, as well as `prov:hadPrimarySource` targets.

### target RDF template 
For each item in the JSON response list `{{results}}`  create:
```turtle
  <https://www.gbif.org/occurrence/{{key}}> a mnb:TaxonOccurrence;
    sosa:resultTime "{{eventDate}}"^^xsd:dateTime;
    mnx:hasGeometry <https://www.gbif.org/occurrence/{{key}}#point>;
    dwciri:inDataset <https://www.gbif.org/dataset/{{datasetKey}}>;
    mnb:gbifTaxonKey "{{taxonKey}}" .
  
  <https://www.gbif.org/occurrence/{{key}}#point> a mnx:Geometry;
    geosparql:asWKT "POINT({{decimalLongitude}} {{decimalLatitude}})"^^geosparql:wktLiteral ;
    geosparql:asGML "{{decimalLatitude}},{{decimalLongitude}}"^^geosparql:gmlLiteral .

```
**NB** : as some occurrences are reinterpreted, already collected data will change over time. Therefore, each resulting list of occurrences' instances will probably contain already existing instances in the the graph, so we should first delete from the graph those instances that are in the new set, and then load all new instances.

## Example target RDF

```turtle
PREFIX mnb: <http://mnb.dordogne.fr/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
prefix geosparql: <http://www.opengis.net/ont/geosparql#> 
prefix prov: <http://www.w3.org/ns/prov#> 

  <https://www.gbif.org/occurrence/2609681306> a mnb:TaxonOccurrence;
    sosa:resultTime "2020-01-28T00:00:00"^^xsd:dateTime;
    mnx:hasGeometry <https://www.gbif.org/occurrence/2609681306#point>;
    dwciri:inDataset <https://www.gbif.org/dataset/b1047888-ae52-4179-9dd5-5448ea342a24>;
    mnb:gbifTaxonKey "2487878" .
  
  <https://www.gbif.org/occurrence/2609681306#point> a mnx:Geometry;
    geosparql:asWKT "POINT(1.4707 44.00)"^^geosparql:wktLiteral ;
    geosparql:asGML "44.00,1.4707"^^geosparql:gmlLiteral.

```

# Requêtes exemples


Requête SPARQL pour lister les  occurrences => 562k occurrences

```
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <http://schema.org/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX taxrefstatus: <http://taxref.mnhn.fr/lod/status/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX taxrefbgs: <http://taxref.mnhn.fr/lod/bioGeoStatus/>
PREFIX dwc: <http://rs.tdwg.org/dwc/terms/>
prefix mnb: <http://mnb.dordogne.fr/ontology/> 
prefix mnbv: <http://mnb.dordogne.fr/vocabulary/> 
PREFIX taxrefrk: <http://taxref.mnhn.fr/lod/taxrank/>
PREFIX taxrefprop: <http://taxref.mnhn.fr/lod/property/>  
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
prefix sosa: <http://www.w3.org/ns/sosa/> 
PREFIX ma: <http://www.w3.org/ns/ma-ont#> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 

SELECT *
WHERE {
	?occurrence a mnb:TaxonOccurrence ;
		sosa:resultTime ?resultTime ;
    	mnx:hasGeometry/geosparql:asWKT ?geometryCOord ;
     	prov:hadPrimarySource ?datasetURI ;
		mnb:gbifTaxonKey ?acceptedTaxonKey .
}
```

Résultat attendu:
* 562,964 lignes
* Exemple de résultat pour 1 valeur:

|                 occurrence                 |              resultTime             |                                 geometryCOord                                |                             datasetURI                            | acceptedTaxonKey |
|:------------------------------------------:|:-----------------------------------:|:----------------------------------------------------------------------------:|:-----------------------------------------------------------------:|:----------------:|
| https://www.gbif.org/occurrence/2613748473 | "1891-04-15T00:00:00"^^xsd:dateTime | "POINT(1.14779 45.35907)"^^<http://www.opengis.net/ont/geosparql#wktLiteral> | https://www.gbif.org/dataset/c2c48578-dc0c-4125-a6da-1f18f42caf9d | 2235300          |
