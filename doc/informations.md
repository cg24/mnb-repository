# BioMétéo - Infos pratiques


# Mentions légales

Le site BioMétéo est édité par le Département de la Dordogne.

Le Directeur de la publication est le Président du Conseil départemental de la Dordogne, Germinal PEIRO.

Les contenus proposés proviennent de différentes sources de données ouvertes qui sont listées dans la page “Crédits”.

L’hébergement et la maintenance sont assurés par la société Mnémotix.com 


# Présentation du projet

Le projet BioMétéo fait partie de la Maison Numérique de la Biodiversité qui est portée par le Département de Dordogne en partenariat avec d’autres acteurs territoriaux ou fournisseurs de données. Cette « météo de la biodiversité » a pour objectif de sensibiliser le grand public à la biodiversité et l’accompagner vers une meilleure connaissance et prise en compte de l’écologie au quotidien.

Le projet s’inscrit dans une pratique quotidienne (consultation de la météo) avec la diffusion d’informations actualisées sur l’état des observations de la biodiversité locale : état des lieux quotidien des espèces présentes et observées en Dordogne, dans les trois compartiments qui nous entourent : l’air, le sol, l’eau…

L’application est également conçue comme une première approche culturelle de la biodiversité et comme le point d’entrée vers des informations plus complètes initiant aux enjeux de biodiversité.

C’est un prototype innovant mettant en scène des données publiques de biodiversité, d’origines différentes, croisées avec des données météorologiques. Le développement de cet outil pédagogique s’inscrit dans un processus itératif qui permettra de futures évolutions avec d’autres jeux de données.


# Données personnelles 

Le Département s'engage à ce que la collecte et le traitement de données à caractère personnel, effectués à partir du présent site, soient conformes à la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés, et au Règlement européen 2016/679. Aucune donnée collectée ne fait l’objet d’un transfert en dehors de l’Union européenne.

Les seules données pouvant être collectées sont :

*   La géolocalisation (optionnelle) de l’utilisateur rend possible l’affichage d’informations sur l’environnement et la biodiversité à proximité de l’utilisateur. La géolocalisation automatique est systématiquement conditionnée par le consentement explicite et utilise les fonctions du navigateur. La géolocalisation peut également être manuelle, et dans ce cas la géolocalisation est imprécise, le maillage le plus fin retenu s’arrêtant au niveau de la commune.
*   Les adresses IP des utilisateurs et leurs différentes connections sont collectées pour permettre le suivi statistique de l’audience du site biometeo.dordogne.fr au moyen de l’outil Matomo (version à code ouvert disponible sur [https://fr.matomo.org/](https://fr.matomo.org/)).
*   Votre adresse mail ainsi que toute donnée personnelle que vous nous envoyez sur l’adresse [biometeo@dordogne.fr](mailto:biometeo@dordogne.fr) . Elles ne font pas l’objet d’une prise de décision automatisée ou de profilage. Elles ne sont destinées qu’à contribuer à l’amélioration de la web application BioMétéo.

Le Département de la Dordogne est le responsable du traitement et les destinataires des données sont les suivants : Direction de la Communication et Service des Milieux Naturels et de la Biodiversité. Les données seront utilisées uniquement par les services habilités du Département

La durée de conservation maximale des données est de 1 an.

Conformément au cadre juridique sur la protection des données personnelles en vigueur (Règlement général de Protection des Données - RGPD et Loi informatique et informatique modifiée), vous bénéficiez d’un droit d’accès, de rectification, de limitation des informations qui vous concerne. Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.

Ces demandes doivent être adressées, en justifiant de votre identité par l’envoi d’une copie d’une pièce d’identité :



*   Sur le site https://demarches.dordogne.fr/ rubrique les démarches/particuliers/papiers et citoyenneté/Exercer ses droits sur ses données personnelles ;
*   Ou par courrier électronique à l'adresse protectiondesdonnees@dordogne.fr ;
*   Ou encore par voie postale à l’adresse suivante : Monsieur le Président du Conseil Départemental de la Dordogne - Délégué à la Protection des données - Hôtel du Département - 2 rue Paul-Louis Courier - CS 11200 - 24019 PERIGUEUX Cedex.   

 

Vous pouvez également introduire une réclamation auprès de la CNIL (3, place Fontenoy – TSA 80715– 75334 Paris cedex www.cnil.fr).
