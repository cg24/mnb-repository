Medias repository
=================

Data availlable in the `media/` folder.

# taxref-media.trig
 
Extraction of all available Taxref media using [taxref-media connector ](https://gitlab.com/cg24/mnb-connectors/-/tree/master/taxref-media/src)

**Data source :** TAXREF Web API https://taxref.mnhn.fr/taxref-web/api/mentions

**Quantity of data** :
* 49025 liens taxon-image (une image peut être utilisée plusieurs fois)
* 18538 taxons ont au moins 1 image

**Example for one taxon** 

```turtle
@prefix dct: <http://purl.org/dc/terms/> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix ma: <http://www.w3.org/ns/ma-ont#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://taxref.mnhn.fr/lod/taxon/425141/12.0> foaf:depiction <https://taxref.mnhn.fr/api/media/download/inpn/120670>,
        <https://taxref.mnhn.fr/api/media/download/inpn/202670> .

<https://taxref.mnhn.fr/api/media/download/inpn/120670> a foaf:Image ;
    dc:identifier "120670" ;
    dc:rights "B. Guichard" ;
    dct:license "CC BY-NC-SA" ;
    ma:frameHeight 600 ;
    ma:frameWidth 800 .

<https://taxref.mnhn.fr/api/media/download/inpn/202670> a foaf:Image ;
    dc:identifier "202670" ;
    dc:rights "L. Poncet" ;
    dct:license "CC BY-NC-SA" ;
    ma:frameHeight 600 ;
    ma:frameWidth 800 .
```

**Available File** : [../media/taxref-media.trig](../media/taxref-media.trig) 10MB, RDF/Trig format 


# taxref_taxon_keys_without_media.txt 

This is a list of taxref taxons which do not have any media link (at the time of latest extraction)

# wikidata_media.rdf

XML/RDF extraction directly from Wikidata starting from GBIF key of the taxons having occurrence data within the Dordogne's boundaries but no taxref media

Requête utilisée pour récupérer les images wikidata des **taxons GBIF n'ayant pas d'images dans taxref** (see [gbif_keys_with_taxref_key_mapping_without_taxref_images.txt](gbif_keys_with_taxref_key_mapping_without_taxref_images.txt))

```SQL
CONSTRUCT {
  ?species wdt:P846 ?gbifID .
  ?species foaf:depiction ?imgID .}
WHERE {
    VALUES ?gbifID {"2137716" "2137953" "2134541" .. }
    ?species wdt:P846 ?gbifID .
    # get if available GBIF-TAXREF mapping
    OPTIONAL{?species wdt:P3186 ?taxrefID .}
    ?species wdt:P18 ?imgID .
}
```

Résultats 
* 1320 images 
* 1149 taxon ayant au moins 1 image (de 1 à 4 images), donc complémentaires des 3029 taxons ayant une image Taxref (sur les 6000+ ayant des données d'occurrence dans le GBIF)

# Requête exemple

Requête SPARQL permettant de lister les medias disponibles pour une série d'identifiants GBIF (?gbifTaxonKey) en filtrant les medias ayant un format "paysage" 

```SPARQL
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
prefix ma: <http://www.w3.org/ns/ma-ont#> 
PREFIX taxrefprop: <http://taxref.mnhn.fr/lod/property/>
PREFIX schema: <http://schema.org/>
PREFIX wd: <http://www.wikidata.org/entity/>

select  ?taxon (GROUP_CONCAT(?img; SEPARATOR=", ") as ?images)
where { 
    VALUES ?gbifTaxonKey {"5415455" "6096805" "2480537" "2480637" }
    ?taxon taxrefprop:hasReferenceName/schema:identifier/schema:propertyID wd:P846 .
    ?taxon taxrefprop:hasReferenceName/schema:identifier/ schema:value	?gbifTaxonKey .
	?taxon foaf:depiction ?img .
    ?img ma:frameWidth ?w .
    ?img ma:frameHeight ?h .
    BIND((?w/?h) AS ?ratio) .
    FILTER(?ratio >= 1.3  && ?ratio <= 2) .
    #FILTER(?w > 750) .
} 
GROUP BY ?taxon
```

Résultat attendu:

|           taxon          |                                                                                                                                        images                                                                                                                                       |
|:------------------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| taxref:taxon/79721/12.0  | https://taxref.mnhn.fr/api/media/download/inpn/258170                                                                                                                                                                                                                               |
| taxref:taxon/238387/12.0 | https://taxref.mnhn.fr/api/media/download/inpn/211608                                                                                                                                                                                                                               |
| taxref:taxon/2623/12.0   | https://taxref.mnhn.fr/api/media/download/inpn/102163,  https://taxref.mnhn.fr/api/media/download/inpn/102165, https://taxref.mnhn.fr/api/media/download/inpn/102166,  https://taxref.mnhn.fr/api/media/download/inpn/170912, https://taxref.mnhn.fr/api/media/download/inpn/170913 |
| taxref:taxon/2895/12.0   | https://taxref.mnhn.fr/api/media/download/inpn/136122,  https://taxref.mnhn.fr/api/media/download/inpn/206901, https://taxref.mnhn.fr/api/media/download/inpn/255940,  https://taxref.mnhn.fr/api/media/download/inpn/255942                                                        |