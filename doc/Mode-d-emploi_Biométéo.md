## Navigation grâce aux paramètres d'URL

### /?geoloc

Il est possible de passer le paramètre `geoloc` suivi de l'identifiant [geonames](http://www.geonames.org/) dans l'URL pour forcer la géolocalisation de l'application. La liste des codes géonames de toutes les communes de Dordogne est accessible à [ce lien](https://docs.google.com/spreadsheets/d/1g6KJvegNQHqyjzuot6FdGzsFuMOr86rtN__Iszi4d-4/edit#gid=0)

Par exemple l'URL suivante géolocalisera sur Périgueux :

```
https://biometeo.dordogne.fr?geoloc=6429478
```

### /?anchor

Il est possible de scroller automatiquement sur une partie de la page web désirée en passant le paramètre `anchor` suivi d'une des valeurs

- de `meteo`
- de catégorie : `air`, `eau` ou `sol`.
- de `graph` pour avoir le graphique des niveaux de rivières ou nappes
- de `actualites`pour avoir les actualités twitter

Par exemple l'URL suivante démarrera l'application directement sur la partie "eau".

```
https://biometeo.dordogne.fr?anchor=eau
```

### /?type

Il permet de paramètrer si on affiche le graphique sur les nappes ou les rivières

- ?type=nappe
- ?type=riviere

Pour que afficher directement la zone des graphiques "eau" sur un type prédéfini, il faut combiner `?type=` avec le paramètre `anchor`, e.g de cette manière :

- https://biometeo.dordogne.fr/?anchor=graph&type=nappe affichera le graphique de la nappe phréatique
- https://biometeo.dordogne.fr/?anchor=graph&type=riviere affichera le graphique de la rivière

### Combiner les paramètres pour automatiser la navigation

Il est possible de passer de combiner les paramètres `anchor` et `geoloc` pour afficher directement certaines zones de l'application sur une localisation prédéfinies. Cette fonction est notamment utilisée les affichages publics.

Exemples :

- Afficher la section "Actualité" pour la ville de Bergerac (code geonames : 6455004) https://test-mnb-biometeo.mnemotix.com/?geoloc=6455004&anchor=actualites
- Afficher la section "Graphiques du niveau de débit de la rivière" pour la ville de Périgueux (code geonames : 6429478) https://test-mnb-biometeo.mnemotix.com/?geoloc=6429478?anchor=graph&type=riviere

### Afficher directement la page détail d'un taxon /taxon/?taxonid

Il est possible de passer en paramètre l'identifiant d'un taxon pour le partager ( c'est ce qui est utilisé pour le partage sur les réseaux sociaux)
exemple :

https://test-mnb-biometeo.mnemotix.com/taxon/?taxonid=http%3A%2F%2Ftaxref.mnhn.fr%2Flod%2Ftaxon%2F3140%2F12.0

Note : il faut au préalable encoder l'identifiant passé en paramètre ( via `encodeURIComponent()` par exemple ) sinon les slash (`/`)
qu'il contient gènèrent des bugs. Cette opération est par ailleurs réalisée automatiquement lors de l'utilisation de la fonctionnalité de partage.
