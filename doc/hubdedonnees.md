Hub de données
==============

# Fonctionnalité

Le Hub de Données a pour objectif d’offrir un accès centralisé et homogène à un ensemble dispersé et hétérogène de données extérieures, et ce afin de les mettre à disposition à la fois de l’application cliente BioMétéo, qui en sera l’interface utilisée par la majorité des usagers, mais également à disposition d’autres acteurs et applications tiers. Ces autres cas d’usages incluent par exemple des applications de l’écosystème de la Maison Numérique de la Biodiversité de Dordogne, ou bien encore d’autres acteurs citoyens, académiques, ou entreprises. 

Le Hub de Donnée (backend, situé dans la moitié haute à droite du schéma ci-dessous) est basé sur une architecture réactive capable à la fois de collecter et stocker dans un triple store RDF (GraphDB) les données des sources extérieures de manière asynchrone (et donc non bloquante), et en même temps de fournir une réponse instantanée à toutes les requêtes provenant de l’application BioMétéo grâce à la mise en place d’un moteur de recherche (ElasticSearch) et d’une API (GraphQL). 

# Accès aux interfaces du Hub de Données

Accès aux interfaces de requêtes:

## SPARQL 

Disponible à l'URL https://graphdb.mnemotix.com/sparql avec les autentifiants suivant : login = `biometeo-visiteur`, mot de passe = `MNBlod2020` et en choisissant le "repository" `prod-mnb`

accéder à la console SPARQL via le menu à gauche ou à l’adresse https://graphdb.mnemotix.com/sparql  et copier-coller les requêtes données dans la documentation

## GRAPHQL 

les requêtes au format POST peuvent être passées via un client REST classique en indiquant dans le payload les données données dans la documentation ou plus simplement via la console interactive de l’API accessible à l’adresse suivante https://biometeo.dordogne.fr/graphql/playground. Sur cette console il suffit de copier-coller les requêtes GRaphQL qui sont proposés pour les sorties du Hub de Données disposant de cette interface.

# Présentation des données du Hub

|  Nom sortie de données  |     Interface       |              Source de donnée              |                                                                                                                                Données disponible                                                                                                                                |    Lien doc   |
|:-----------------------:|:---------------------------------:|:------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-------------:|
| taxref-LOD + extensions | SPARQL                             | TAXREF-LOD                                 | Infos taxonomiques, liens vers ressources externes, statuts, habitats pour tous les taxons Taxref                                                                                                                                                                                | [documentation](taxref-LOD+extensions.md) |
| taxons-media            | SPARQL                             | TAXREF-WEB                                 | Media pour une partie des taxons Taxref avec metadonnées (licence, auteur) et dimensions                                                                                                                                                                                         | [documentation](taxons-media.md) |
| mnb-area                | SPARQL                            | Projet MNB                                 | Correspondance entre taxons et zone Biométéo Air-Sol-Eau                                                                                                                                                                                                                         |[ documentation](biometeo-areas.md)|
| taxons + occurrences    | GraphQL           | TAXREF-LOD+WEB, Données MNB, Gbif.org      | Renvoie une sélection de 3 taxons par zone Air/Sol/Eau regroupant les données :  - taxonomiques (classes, noms sci., habitat, statut, etc) - media - occurrences (mises à jour quotidiennement) : nombre sur la Dordogne et calcul de la distance de l’occurrence la plus proche | [documentation](taxons+occurences.graphql.md) |
| river-stations          | SPARQL                             | Hubeau / Hydrométrie / stations            | Description des stations de mesure du niveau des rivières en Dordogne                                                                                                                                                                                                            | [documentation](river-stations.md) |
| river-level             | GraphQL                           | Hubeau / Hydrométrie / observations_tr     | Pour une station donnée et un jour donnée, Niveau observé de la rivière pour le mois écoulé                                                                                                                                                                                      | [documentation](river-level-connector.md) |
| groundwater-stations    | SPARQL                           | Hubeau / Piézométrie / stations            | Description des stations de mesure du niveau piézométriques des nappes phréatiques                                                                                                                                                                                               | [documentation](groundwater-stations.md) |
| groundwater-level       | GraphQL                            | Hubeau / Piézométrie / chroniques          | Pour une station et un jour donnée, chronique de toutes les mesures disponible du niveau piézométriques de la nappe phréatique sur l’année écoulée                                                                                                                               | [documentation](groundwater-level-connector.md) |
| gbif-occurrences        | SPARQL                             | Gbif.org                                   | Export + moissonnage quotidient des données d’occurrence de taxon du GBIF pour la Dordogne. NB : les bases de données du GBIF ne sont pas les mêmes pour le site public et l’API. Un léger différenciel du nombre d’occurrences pour un taxon donné peut donc être observé.      | [documentation](gbif-occurrences-export.md) |
| météo + indice-atmo     | SPARQL + GraphQL   | Openwathermap.org, Atmo Nouvelle-Aquitaine | Aggrégation des données météo et Atmo sur un point donné                                                                                                                                                                                                                         | [documentation](meteo+indice-atmo-station-and-connector.md) |