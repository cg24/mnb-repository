Aggrégation des données Météo et Atmo
=====================================


# Données de qualité de l'air du réseau  ATMO

Goal : collect daily the latest values of ATMO index from Atmo Aquitaine for all stations listed in mnb-repository [airquality-station](https://gitlab.com/cg24/mnb-repository/airquality-station)

NB : les métadonnées des stations sont dans le graphe (localisation, codeZone) mais les données “chaudes” sont uniquement indexées (cf schéma architecture )

## ATMO API Calls

### Parameters

**API doc**: https://opendata.atmo-na.org/dataset/indices

**Parameters**: 
* `date_deb` : YYYY-MM-DD formated begining of the desired period
* `date_fin` : YYYY-MM-DD formated end of the desired period 
* `zone`: postal code of the city of the desired station 
* `type`: desired format, default `json`

**Example call :** : for a call issued on `2020-05-28` => https://opendata.atmo-na.org/api/v1/indice/atmo/?date_deb=2020-05-27&date_fin=2020-05-29&zone=24322&type=json

The idea is to retrieve at a given day `D` the value of `D-1`, `D`, and `D+1` in order to compute tendancies of the current day and the next day, and to give the forecast for the next day: 
* `date_deb=D-1`
* `date_fin=D+1`

**RMQ** 
* This calls should yield 3 values.
* THe index can be built by taking directly the content of the `features` list.
* See example below:

```json
{
    "type": "FeatureCollection",
    "totalFeatures": 3,
    "returnFeatures": 3,
    "offset": 0,
    "features": [
        {
            "type": "Feature",
            "properties": {
                "code_zone": "24322",
                "lib_zone": "Périgueux",
                "date_ech": "2020-05-29T00:00:00.000Z",
                "valeur": 5,
                "type": "prevision"
            }
        },
        {
            "type": "Feature",
            "properties": {
                "code_zone": "24322",
                "lib_zone": "Périgueux",
                "date_ech": "2020-05-28T00:00:00.000Z",
                "valeur": 5,
                "type": "prevision"
            }
        },
        {
            "type": "Feature",
            "properties": {
                "code_zone": "24322",
                "lib_zone": "Périgueux",
                "date_ech": "2020-05-27T00:00:00.000Z",
                "valeur": 4,
                "type": "constat"
            }
        }
    ]
}
```

### Mapping numeric value and literal value

The `valeur` field gives the actual index. The literal value can be computed  by the following mapping:

* 0 < index <= 3 => "Très bon" 
* 3 < index <= 5 => "Bon" 
* 5 < index <= 6 => "Moyen" 
* 6 < index <= 9 => "Médiocre"
* 9 < index <= 10 => "Mauvais"  
* 10 < index => "Très Mauvais" 

# Requêtes exemples

## Lister les stations Atmo pour le département de Dordogne

So far there's only 1 station, but the system is ready to manage more. One can list them issuing following SPARQL:

```SQL
PREFIX mnb: <http://mnb.dordogne.fr/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
prefix geo: <http://www.opengis.net/ont/geosparql#> 

select distinct *
where { 
	?station a mnb:AtmoIndexMS .
    ?station mnb:codeZone ?codeZone.
    ?station rdfs:label ?stationLabel .
    ?station mnx:hasGeometry/geo:asWKT ?locCoord . 
	  ?station foaf:page ?page .
} 
```
In JSON format the result is

```JSON
 "results" : {
    "bindings" : [
      {
        "station" : {
          "type" : "uri",
          "value" : "   "
        },
        "codeZone" : {
          "type" : "literal",
          "value" : "24322"
        },
        "stationLabel" : {
          "type" : "literal",
          "value" : "Périgueux"
        },
        "locCoord" : {
          "datatype" : "http://www.opengis.net/ont/geosparql#wktLiteral",
          "type" : "literal",
          "value" : "POINT(0.71667 45.18333)"
        }
      }
    ]

```

For each station we need to collect the `station` URI and the `codeZone` value 

## Requête GraphQL pour récupérer l'aggrégation des données Météo et indice Atmo

RQM : For easy testing, go to https://test-mnb-biometeo.mnemotix.com/graphql/playground in order to test the following graphQL requests:

Requête donnant les infos de météo et d'indice Atmo pour la commune de Coly-Saint-Amand (geonamesId = 6429505):

```json
{
  weather(geonamesId: "geonames:6429505") 
  {
      sunrise
      dayTimeDiff
      pressure
      temperature
      sunset
      temperatureTrend
      uvIndex
      uvIndexTrend
      icon
      forcast {
              date
              icon
              temperatureMin
              temperatureMax
              __typename
              }
      atmoIndex
      atmoNextDayIndex
      atmoNextDayTrend
      atmoTrend
      __typename
  }
}
```

Résultat attendu (données au 06/08/2020)

```json

{
  "data": {
    "weather": {
      "sunrise": "2020-08-06T04:46:18.000Z",
      "dayTimeDiff": -3,
      "pressure": "1016",
      "temperature": "35.7",
      "sunset": "2020-08-06T19:15:25.000Z",
      "temperatureTrend": "increasing",
      "uvIndex": "7.97",
      "uvIndexTrend": "increasing",
      "icon": "01d",
      "forcast": [
        {
          "date": "2020-08-06T12:00:00.000Z",
          "icon": "01d",
          "temperatureMin": "20.08",
          "temperatureMax": "35.7",
          "__typename": "WeatherForcast"
        },
        {
          "date": "2020-08-07T12:00:00.000Z",
          "icon": "01d",
          "temperatureMin": "18.6",
          "temperatureMax": "38.82",
          "__typename": "WeatherForcast"
        },
        {
          "date": "2020-08-08T12:00:00.000Z",
          "icon": "10d",
          "temperatureMin": "18.7",
          "temperatureMax": "35.23",
          "__typename": "WeatherForcast"
        },
        {
          "date": "2020-08-09T12:00:00.000Z",
          "icon": "10d",
          "temperatureMin": "18.79",
          "temperatureMax": "35.94",
          "__typename": "WeatherForcast"
        },
        {
          "date": "2020-08-10T12:00:00.000Z",
          "icon": "04d",
          "temperatureMin": "19.28",
          "temperatureMax": "33.75",
          "__typename": "WeatherForcast"
        },
        {
          "date": "2020-08-11T12:00:00.000Z",
          "icon": "10d",
          "temperatureMin": "18.81",
          "temperatureMax": "35.22",
          "__typename": "WeatherForcast"
        },
        {
          "date": "2020-08-12T12:00:00.000Z",
          "icon": "10d",
          "temperatureMin": "16.48",
          "temperatureMax": "36.78",
          "__typename": "WeatherForcast"
        },
        {
          "date": "2020-08-13T11:00:00.000Z",
          "icon": "10d",
          "temperatureMin": "15.77",
          "temperatureMax": "26.35",
          "__typename": "WeatherForcast"
        }
      ],
      "atmoIndex": 50,
      "atmoNextDayIndex": 60,
      "atmoNextDayTrend": "increasing",
      "atmoTrend": "increasing",
      "__typename": "Weather"
    }
  }
}
```




