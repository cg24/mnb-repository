mnb-repository
==============

Données RDF pour la Maison Numérique de la Biodiversité


# Liste des graphes nommés 

Les données de ce dépôt sont disponibles dans le triple store du projet et sont réparties entre les graphes nommés suivant:

* http://mnb.dordogne.fr/data/gibfoccurrenceNG : données statiques d'occurrence extraites du GBIF 
* http://mnb.dordogne.fr/data/riverstationNG 
* http://mnb.dordogne.fr/data/importedModelsNG 
* http://mnb.dordogne.fr/data/groundwaterStationsNG
* http://mnb.dordogne.fr/data/airqualityNG		
* http://mnb.dordogne.fr/data/geolocalisationNG : 
    * cantons INSEE 
    * communes INSEE avec mapping geonames
    * liens [1] canton - [n] communes
FIXME : 3 graphes dédoublés
* http://data.dordogne.fr/imports/importedModelsNG/	
* http://mnb.dordogne.fr/data/importedModelsNG	
* http://data.dordogne.fr/imports/taxrefNG/	
* http://mnb.dordogne.fr/data/taxrefNG/	
* http://data.dordogne.fr/imports/areaMappingsNG	
* http://mnb.dordogne.fr/data/areaMappingsNG
FIXME2 : namespace 
* http://data.dordogne.fr/imports/taxrefMediaNG/

 
