
Sélection et Mosaïque d'espèces Air/Sol/Eau
===========================================

# Fonctionnalités
Pour chaque zone "Air/Sol/Eau" une mosaïque de 3 photos d’espèce présente sur la commune en lien avec la zone est affichée. L’interface évite les répétitions d’affichage d’espèce sur une période de 7 jours lorsque celle-ci est ouverte plusieurs fois. Pour chaque journée, la sélection d'espèce ne sera pas obligé de changer. Si l'usager revient le lendemain en étant localisé au même endroit une autre sélection d'espèces est présentée

## Mosaïque principale 
Sur chaque photo de la mosaïque sont affichés :
* Le nom vernaculaire de l’espèce (ou à défaut le nom scientifique)
* Le nombre total d’observations dans la limite du département.
* (si la place le permet) Un nombre indique la distance à laquelle se situe la dernière observation par rapport à la géolocalisation de l’utilisateur
* (si la place le permet) Le statut biogéographique de l’espèce (endémique, invasive, protégée, en danger...). 

## Fiche détaillée d'une espèce 
Au clic sur la photo, un overlay reprend ces infos complétés de détails supplémentaires:
* un lien vers la page de l'espèce sur le site de l'INPN
* un lien  vers la page GBIF des occurrences de l'espèce courante
* les infos détaillées de l'espèce:
    * statut biogéographique de l’espèce (endémique, invasive, protégée, en danger...). 
    * nom vernaculaire
    * nom scientifique
    * famille
    * ordre
    * class
    * Habitat

## Partage d'une espèce 

Au clic sur le pictogramme de partage, un overlay s’ouvre avec les différents réseaux sociaux disponibles (cf 1ère image d’espèce dans maquette “desktop”): Facebook, twitter, email. 

Au clic sur l’un des 3 choix, l’utilisateur est renvoyé vers l’application cible avec une ébauche de message reprenant:
*  le titre de la page,
* le nom de l’espèce partagée, 
* et le lien vers la page détaillée de l'espèce dans l'application biométéo (e.g https://biometeo.dordogne.fr/taxon/?taxonid=http%3A%2F%2Ftaxref.mnhn.fr%2Flod%2Ftaxon%2F3140%2F12.0). 

NB: lors de l'ouverture d'un lien vers l'application avec une taxon renseigné, ce taxon est inclus dans la sélection afin de pouvoir consulter à loisir la fiche une fois la fenêtre fermée. Si le même lien est réutilisé ultérieurement les jours suivants, ce taxon sera toujours inclus dans la sélection.

## Espèces favorites

Il est possible de sauvegarder une espèce au click sur le picto "coeur". Le picto “coeur” est cliquable depuis la mosaïque, et depuis l'écran de vue détaillée:
* Au clic sur le pictogramme cœur:
    * l’icone coeur passe en mode “actif” (blanc sur fond bleu)
    * l’espèce est enregistrée (dans la mémoire du navigateur, pas de mode connectée) dans les favoris consultable depuis le menu principal  
* Au clic successif sur le pictogramme coeur,
    * l’icone coeur passe en mode “inactif” (bleu sur fond blanc)
    * l’espèce est retirée des favoris consultable depuis le menu principal

Depuis menu latéral, on a accès à l'historique des espèces favorites. Au clic sur une l’image d’une espèce favorite, on a accès à la fiche détaillée de l’espèce. A noter: lors de la consultation d'une fiche espèce depuis les favoris, les informations relatives au nombre d'occurrence et à la distance de l'observation la plus proche sont recalculées en fonction de la localisation courante de l'utilisateur et non la localisation au moment de l'ajout dans les favoris.

Les espèces favorites sont enregistrées en utilisant le stockage local. Ceci implique quelques limitations:
* pas de conservation des données lorsque l’usager change d’appareil ou réinstalle son environnement
* les données sont liées à l’appareil et non à un usager; celui-ci ne peut donc pas retrouver ses espèces favorites si il utilise un l’appareil d’un tiers
* sur les postes partagés, l’historique des espèces sera commun
