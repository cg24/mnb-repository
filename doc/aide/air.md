Données Air & Météo
===================

# Fonctionnalités

La zone appelée AIR comprend les données de météo, l’éphéméride du jour (durée jour et nuit), l’indice UV  et l’indice Atmo. Cette zone affiche:

* Un pictogramme de météo en fonction de la météo locale. 
* La température locale. Une flèche montante ou descendante permet de voir la tendance par rapport au jour en cours. 
* En cliquant sur le pictogramme météo, les prévisions pour les 7 prochains jours s’affichent (picto + T° Min et Max)
* L’indice UV du jour, avec la tendance par rapport à la veille.
* L’éphéméride jour nuit avec le nombre de minutes gagnées ou perdues en fonction du jour
* L’indice Atmo sur 100. Au clic, les valeurs actuelles et la prévisions pour le lendemain (avec les tendances) apparaissent, ainsi que le lien vers la page du site Atmo pour la station de Périgueux (https://www.atmo-nouvelleaquitaine.org/monair/commune/24322)

# Solution technique adoptée

Développement d'un connecteur direct (sans passer par Hub de Données) à l'API OpenWeathermap.org :

* Cette API fournit toutes les données de météo (pluviométrie, couverture nuageuse, indices UV) qui sont requises pour l’application BioMétéo. Elle inclut également l’historique sur 5 jours de toutes les données météo, ce qui permet d’afficher très facilement les tendances.
* Cette API propose une tarification très intéressante car elle est gratuite jusqu’à 60 calls/minute et 1,000,000 appels/mois. Ce forfait gratuit devrait donc couvrir le trafic cible pour la future application, qui se situe entre 100 000 à 200 000 visites par mois.
* NB: la mise en place d’un proxy devrait permettre aussi de limiter le nb d’appels à l’API en centralisant les appels depuis le backend et en rendant les résultats disponibles aux terminaux clients.
