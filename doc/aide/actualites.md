Messages d'actualités postés depuis twitter
===========================================

# Fonctionnalités 

## Publication des messages d'actualités
Les administrateurs du compte du réseau social choisi, en l'occurrence le compte Twitter @biometeo24, utilisent twitter pour:

* éditer des messages (tweets)
* associer un message à une ou plusieurs communes ou zone de localisation (cf ci-après) de Dordogne par le biais de "hash tags". Par exemple "Attention aux moustiques tigres signalés dans le secteur de Bergerac #2401 #2402”

**Localisation des messages** :
La localisation s’effectue à 2 niveaux, qui peuvent être utilisé conjointements:

* Commune: en insérant le code Geonames d'une commune  (cf [tableau des communes en Dordogne](https://docs.google.com/spreadsheets/d/1g6KJvegNQHqyjzuot6FdGzsFuMOr86rtN__Iszi4d-4/edit?usp=sharing)) précédé de `#` : exemple “La pluie c'est bon pour la #biodiversité :) Episode pluvieux prévu ce week-end sur #6429495” (commune de Queyssac)
* Cantons: en insérant le code INSEE du canton précédé de `#`. (cf [tableau des communes en Dordogne](https://docs.google.com/spreadsheets/d/1g6KJvegNQHqyjzuot6FdGzsFuMOr86rtN__Iszi4d-4/edit?usp=sharing)). Le message sera alors visible pour tous les usagers présents sur les communes du ou des cantons sélectionnés. Exemple : “On attend les couleurs de l'automne et le nouveau visage des espèces locales avec impatience sur le canton #2410” (Pays de la Force)

## Visualisation des messages dans l'application

* Notification de nouveaux messages et accès direct depuis l’icone “clochette” dans  le  header : L’icone “clochette” placée en haut à droite possède un mode “nouveaux messages” avec un point qui indique la présence et le nombre de nouveaux messages non lus. Le statut de lecture de chaque message est stocké en local à la manière des espèces favorites.
* Au clic sur l’icone, l’application scrolle automatiquement sur la zone de nouvelles en bas de page
* Les messages disponibles pour la localisation de l’usager sont visibles en bas de page
* Le dernier message est affiché en premier, des flèches  permettent de naviguer entre les messages. 
* si un message correspond aux critères énoncés ci-avant (non géolocalisé ou géolocalisé dans la commune ou canton de l'usager)  le  message sera accessible dans la zone en bas de page
* Pour chaque message, l'usager peut cliquer sur l’icone “twitter” située à gauche du contenu du message; il est alors renvoyé vers la page du réseau social où le message est publié
* les codes geonames de communes ou codes INSEE de canton sont remplacés par les libellés correspondants dans l’application Biométéo.  

## Sauvegarde et consultation des messages favoris

Au clic sur l’icone “coeur” à droite du contenu des messages, le message est sauvegardé. La sauvegarde s’effectue “en local”, i.e n’est pas garantie en cas de changement de navigateur ou de terminal, ou d’effacement des données de navigation par l’usager.

* les messages favoris sont consultables depuis le lien “Actualités favorites” présent dans le menu principal (bouton “burger”) ou dans le footer de l’application.
* Au clic successif sur l’icone coeur, depuis la zone de consutation des messages en bas de page ou depuis la page des messages favoris, le message correspondant est retiré des favoris.  
* Prise en compte des messages du fil d'actualités
    * les messages publiés sont visibles jusqu'à minimum 7 jours après la publication dans l'application. 
    * si un message ne contient aucun tag de géolocalisation, il sera visible quelque soit la localisation de l'usager
    * si un message contient un ou plusieurs tags de géolocalisation, le message ne sera visible que pour les usagers localisés dans une des communes listées, ou dans une commune appartenant au canton listé. 
* Navigation entre les messages et statut “lu” des messages: 
    * Les nouveaux messages ont la mention “Non lu”
    * Un message est considéré comme  “lu” lorsqu’on passe au message suivant ou précédent. La mention “Non lu” disparait alors.

# Solution technique adoptée
La solution proposée ne repose pas sur l'usage d'un backoffice dédié. L’alternative proposée en accord avec le département repose sur l'usage de Twitter, permettant de toucher le public ne disposant pas encore de l’application BioMétéo et d’en augmenter ainsi le trafic. La solution  utilise Twitter sur un compte “MNB” officiel (et donc sécurisé) en guise de plateforme d’éditions de nouvelles, permettant de rédiger un “post” de la même manière qu’au travers d’un backoffice dédié. Un service du backend du Hub de Données écoute en temps réel le compte “MNB” et vient récupérer les nouvelles et les affiche sur l’application MNB. De plus les nouvelles ainsi diffusées sur les réseaux sociaux renvoient vers l’application BioMétéo. Le service utilise la gratuite de l’API Twitter. Le nombre maximum de tweets ainsi remontés est limité à 200 tweets. Ce nombre peut être abaissé si besoin. 

