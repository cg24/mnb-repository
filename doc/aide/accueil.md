# Accueil

L'accès à l'application se fait depuis un navigateur web via l'URL http://biometeo.dordogne.fr

A l'accueil, un écran de chargement, appelé “splash screen”, s'affiche le temps du chargement des données. 

# Géolocalisation
## géolocalisation automatique : 

Lorsque on accède à l'application,  l'application affiche par défaut le chef lieu du département de Dordogne. 

si on clique sur la barre de localisation, il est proposé le choix "Utiliser ma position actuelle"

Si on clique sur "Utiliser ma position actuelle", le service de geolocalisation du navigateur est appelé et la ville la plus proche de là où on se situe s'affiche, et l'application se recharge pour afficher les infos relatives à la nouvelle localisation.

Au click sur  "Utiliser ma position actuelle", le système peut demander une autorisation. Si l'autorisation est acceptée (ou déjà enregistrée), le navigateur donnera éventuellement une position. Cette fonctionnalité de géolocalisation automatique ne dépend pas de la BioMétéo, et il se peut que le système ne renvoie aucune coordonnée. 2 issues possibles:
* Si le système renvoie bien une coordonnée valide, la ville correspondante est affichée dans la barre de localisation. La localisation se fait au niveau "commune" et non adresse.
* le système a rencontré un problème (accès aux capteurs, connectivité, etc) et ne renvoie pas d'information de localisation. La barre de localisation affiche un message d'erreur "Geolocalisation impossible". Et l'usager peut alors saisir manuellement la commune de son choix.


## géolocalisation manuelle 
 
L'usager peut changer la localité en cliquant sur la barre de saisie de la localisation. Une fois la saisie terminée, au clic sur le bouton "loupe" ou la touche "entrée" du clavier, l'application se recharge pour afficher les infos relatives à la nouvelle localisation. 

## géolocalisation via l'URL.

Le code geonames (cf [tableau de correspondance communes et code geonames](https://docs.google.com/spreadsheets/d/1g6KJvegNQHqyjzuot6FdGzsFuMOr86rtN__Iszi4d-4/edit#gid=0)) est renseigné dans l'URL est est pris en compte par l'interface, au moment du chargement de la page. Saisir dans la barre d'URL l'adresse de l'application suivie de `?geoloc=` + code geonames.org de la commune souhaitée (cf tableau).  Exemple : http://biometeo.dordogne.fr?geoloc=6429563

L'application se (re)charge pour afficher les infos relatives à la localisation indiquée dans la barre d'adresse.

NB : Pour une localisation hors Dordogne (données non disponibles), la page d’accueil de l’interface prévient l’utilisateur que les données ne sont pas accessibles dans la commune sélectionnée, et repositionne l'usager sur la commune de Périgueux.

NB2: L’interface garde en mémoire la liste des communes saisies pour un accès direct lors de futures consultations.


## Solution technique adoptée
Le service de géolocalisation geonames.org est connecté à l'application BioMétéo. Ce service de géolocalisation permet notamment d’associer les coordonnées géographiques d’un usager aux communes du territoire. Il est basé sur le modèle Geonames.org qui est intégré dans l’ontologie MNB, ce qui procure une compatibilité parfaite avec les données du Hub. Cette API est donc proposée en version commerciale avec une tarification adaptable au besoin (120€/an si < 25 000 appels par jour)

Un composant de frontend a été développé permettant la géolocalisation automatique, ou bien la saisie avec assistance pour la saisie (auto-complétion)

l'application est paramétrée pour prendre en compte une variable dans l'URL correspondant à la localisation souhaitée et indiquée par le nom de la commune.

# Menu "burger"

Avoir accès aux espèces favorites et aux pages d’informations via le menu en haut à droite (3 traits horizontaux):
* Espèces favorites
* Actualités favorites
* Infos pratiques
* Crédits
* Contact

# Navigation directe par URL 

https://gitlab.com/cg24/mnb-biometeo#navigation-gr%C3%A2ce-aux-param%C3%A8tres-durl
