Données sur l'eau
==================

La zone appelée EAU comprend les données sur la nappe phréatique locale, le niveau de la rivière locale et les données des espèces faune et flore présentent sur la commune dans la classe EAU.

# Fonctionnalités 

## Bloc aperçu
Lorsque l'on se place sur la zone "Eau" un 1er bloc affiche:

* Pour les nappes, et  mesurés sur la station la plus proche:
    * la tendance de l'évolution du niveau de la nappe phréatique
    * la nature de la nappe (Eocène, Alluvions, etc) 
    * le type de station : profond, peu profond, puits, etc.
* Pour les rivières:
    * la tendance du niveau de la rivière mesurée à la station la plus proche. 
    * la valeur actuelle du niveau ou du débit
    * le nom de la station (Dordogne à Bergerac, L'Isle à Périgueux, etc)

## Bloc espèces "Eau"

Un 2e bloc reprend les espèces liées à l'eau (cf page Navigation espèces)

## Bloc "graphiques"

Un 3e bloc affiche un visualisation plus précise sous forme de graphique, disponible pour la nappe et la rivière au point de mesure le plus proche. Il est possible de passer du niveau des nappes au débit de la rivière la plus proche en activant l'onglet correspondant en haut à droite du graphique.

* Rivière (accessible directement via le lien https://biometeo.dordogne.fr/?anchor=graph&type=riviere)
    * Le nom de la rivière géolocalisée sur la commune. Au clic sur le nom on est renvoyé sur la page Sandre de la rivière observée; eg. http://id.eaufrance.fr/CEA/P---0000 pour la Dordogne
    * L’évolution sur le dernier mois du niveau de la rivière sous forme de courbe, avec affichage de la valeur moyenne.

* Nappes (accessible directement via le lien https://biometeo.dordogne.fr/?anchor=graph&type=nappes):
    * le rappel de la nature de la nappe (Eocène, Alluvions, etc); Au clic sur cette caractéristique de la nappe, on est renvoyé sur la page du point d’eau observé; eg: http://services.ades.eaufrance.fr/pointeau/08297X0001/F 
    * l'évolution du niveau de la nappe phréatique sur l’année écoulée mesuré sur la station la plus proche avec affichage de la valeur moyenne sur la période

### Remarques 

Le calcul de tendances est fait pour le niveau des nappes et rivière. La tendance, comme pout toutes les autres grandeurs présentées dans l’application, mesure l'évolution par rapport à la veille. La tendance sera donc basé sur le signe (> ou < 0) de la différence entre :
* la dernière valeur disponible, i.e datant de moins de 24h car les mesures sont collectées toutes les 24h à heure fixe
* la valeur disponible 24h auparavant (ou la plus proche en cas d’absence de mesure à cette date)
