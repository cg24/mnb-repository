Test data
==========



# occurrence-canton-pays-d-la-force

This data is the RDFization of the GBIF occurrence data done from occurrence-canton-pays-d-la-force.csv using the following CONSTRUCT query with OntoRefine platform

```SQL 
PREFIX mnb: <http://mnb.dordogne.fr/ontology/>
prefix mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/> 
PREFIX mnbd: <http://mnb.dordogne.fr/data/>
PREFIX spif: <http://spinrdf.org/spif#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
PREFIX geosparql: <http://www.opengis.net/ont/geosparql#> 

# Example RDF transformation using the custom IRIs and type
CONSTRUCT {
	?row a mnb:TaxonOccurrence ;
		dc:date ?eventDate ;
    	mnx:hasGeometry ?geometry ;
		geo:lat ?decimalLatitude ;
		geo:long ?decimalLongitude ;
		mnb:gbifTaxonKey ?acceptedTaxonKey .
 	?geometry a mnx:Geometry ;
    	geosparql:asWKT ?pointCoordinate .
} WHERE {
	# Triple patterns for accessing each row and the columns in contains
	# Note that no triples will be generated for NULL values in the table
	# You should inspect your data in Refine mode and add OPTIONAL accordingly
	?row a mnb:Row ;
		mnb:gbifID ?gbifID ;
		mnb:eventDate ?eventDate ;
		mnb:decimalLatitude ?decimalLatitude ;
		mnb:decimalLongitude ?decimalLongitude ;
		mnb:acceptedTaxonKey ?acceptedTaxonKey .
	BIND(IRI(CONCAT("http://mnb.dordogne.fr/data/occurrence", ?gbifID, "#point")) AS ?geometry) .
	BIND(STRDT(CONCAT("POINT(", ?decimalLongitude, " ",?decimalLatitude,")"),geosparql:wktLiteral) AS ?pointCoordinate) .
	# Example construction of new IRIs for each row
	BIND(IRI(spif:buildString("http://mnb.dordogne.fr/data/taxonoccurrence/{?1}", ENCODE_FOR_URI(?gbifID))) AS ?myRowId)
} 
```