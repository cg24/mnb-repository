# BioMétéo - Crédits


# Contributeurs du projet

**Direction de publication** : le site biometeo.dordogne.fr est édité par le Conseil départemental de la Dordogne représenté par son Président, Germinal PEIRO.

**Financement** : ce site est créé dans le cadre du projet de **Maison Numérique de la Biodiversité** cofinancé par l’Etat dans le cadre des Programmes d’Investissements d’Avenir et par l’Union européenne avec le Fonds Européen de Développement Régional (FEDER).

*   Programme d’Investissements d’Avenir – ADEME : 60%
*   FEDER – Union Européenne : 20%
*   Conseil départemental, autofinancement : 20%

**Pilotage** : le projet a été conduit par les services du Département de la Dordogne

*   Direction de l'Environnement et du Développement Durable (DEDD)
*   Direction des Systèmes d'Information et du Numérique (DSIN)

**Assistance à maîtrise d’ouvrage** : [Cap Sciences](https://www.cap-sciences.net/), Bordeaux

**Design graphique et ergonomique** : [juliencoudert.com](http://juliencoudert.com/)

**Développement informatique** : [Mnémotix](https://www.mnemotix.com/)


# Partenaires 

La création du site a été rendue possible grâce à l’implication des partenaires suivants :

*   Le Conseil en Architecture Urbanisme et Environnement de la Dordogne (CAUE)
*   L’Agence Technique Départementale (ATD)
*   Les partenaires de l’éducation et de l’environnement réunis sous la « communauté mnb24 »


# Sources des données

Le site utilise des données de différentes sources.

**Données météo**

[https://openweathermap.org](https://openweathermap.org)

**Données d’indice de qualité de l’air**

Atmo Nouvelle-Aquitaine ([https://www.atmo-nouvelleaquitaine.org/](https://www.atmo-nouvelleaquitaine.org/))

**Données taxonomiques sur les espèces**



*   Muséum National d’Histoire Naturelle (MNHN) (UMS 2006 Patrimoine naturel) met à disposition toutes les données relatives aux espèces recensées en France; le site BioMétéo exploite les données suivantes :
    *   Description taxonomique (rang, ordre, classe, famille, etc.)
    *   Noms scientifiques et vernaculaires
    *   Médias
    *   Habitats 
*   Ces données sont rendues disponibles via l’API Taxref en version web et Linked Open Data :
    *   [https://taxref.mnhn.fr/taxref-](https://taxref.mnhn.fr/taxref-w)web
    *   [https://github.com/frmichel/taxref-ld](https://github.com/frmichel/taxref-ld)

**Données d’observation d’espèces**

Le site BioMétéo exploite également des données d’occurrence de taxons provenant du consortium international GBIF ([Global Biodiversity Information System](https://www.gbif.org/)). Ces données d’occurrence attestent de l’observation d’un organisme en un lieu donné, à un moment donné. Ces données sont fournies au GBIF par un ensemble d’acteurs et de dispositifs. Chaque donnée d’occurrence est rattachée à un jeu de données. Les jeux de données exploitées pour la BioMétéo sont listés sur [cette page](https://www.gbif.org/occurrence/download/0065750-200221144449610#datasets).

**Données de niveau des nappes phréatiques et de débit des rivières**

Nous collectons aussi quotidiennement les relevés de niveau des nappes phréatiques et de débit des rivières disponibles sur l’API Hubeau ([hubeau.eaufrance.fr](https://hubeau.eaufrance.fr/)) pour les stations actives situées en Dordogne. Les débits des rivières sont donnés par l’[API Hydrométrie](https://hubeau.eaufrance.fr/page/api-hydrometrie), et les niveaux des nappes phréatiques par [l’API Piézométrie](https://hubeau.eaufrance.fr/page/api-piezometrie).
