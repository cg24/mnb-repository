Groundwater level connector
===========================


# Abstract

* This connector will collect daily the chronicles of the  groundwater's level for each station listed in the static groundwater-stations data.
* collected data will be indexed in the ES instance of the Hub de Données, and not stored in the graph; no JSON to RDF mapping is therefore required.

# How to retrieve data

## Query for listing groundwater stations

1. Load the static data from mnb-repository : `groundwater-stations/groundwater-stations.trig`
2. Run the following query:

```SQL
PREFIX mnb: <http://mnb.dordogne.fr/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
prefix geo: <http://www.opengis.net/ont/geosparql#> 

select distinct *
where { 
	?station a mnb:GroundWaterLevelMS.
    ?station mnb:bssCode ?bssCode .
    ?station rdfs:label ?stationLabel .
    ?station mnx:hasGeometry/geo:asWKT ?locCoord . 
    OPTIONAL{?station sosa:isSampleOf ?hydrogeo .}
	  ?station mnb:labelAQUI ?labelAQUI .
    ?station mnb:stationType ?stationType .
} 
``` 
Should yield 55 results. Example result:

|     station    | bssCode                                                     | stationLabel     | locCoord                   | labelAQUI                                                                           | stationType           | 
|-----|-------------------------------------------------------------|------------------|----------------------------|-------------------------------------------------------------------------------------|-----------------------|
| http://services.ades.eaufrance.fr/pointeau/08081X0026/SE.20 | 08081X0026/SE.20 | COMBE BOYER (MARQUAY - 24) | "POINT(1.148944586 44.92323244)"^^<http://www.opengis.net/ont/geosparql#wktLiteral> |  Coniacien-Santonien  | Forage Peu Profond |



## Hubeau calls

Goal is to retrieve each day the level of the stations groundwater level for the past year. The API gives access to daily chronicles for each station. The pace of measurement is  not as steady as for the rivel level measurements, and one may find "holes" within the series of values.

API doc : https://hubeau.eaufrance.fr/page/api-piezometrie#console

Calls template:

* variable parameters : 
* code_bss = {bssCode}
* date_debut_mesure = {today-366 days} : set this to 1 year ago + 1 day (we get errors if we set exactly 1 year)
* https://hubeau.eaufrance.fr/api/v1/niveaux_nappes/chroniques?code_bss={bssCode}&sort=desc&size=400&date_debut_mesure={today-366 days}&fields=niveau_nappe_eau,date_mesure
* fixed parameters explained:
    * sort=desc : sorting order
    * size= max numb of results per page; normally we get at most 365 measures
    * fields=niveau_nappe_eau,date_mesure : value and date returned.

Example call and result (sample) : 

https://hubeau.eaufrance.fr/api/v1/niveaux_nappes/chroniques?code_bss=07588X0075/PZ&sort=desc&size=400&date_debut_mesure=2019-05-16&fields=niveau_nappe_eau,date_mesure

```JSON
{
  "count": 355,
  "first": "https://hubeau.eaufrance.fr/api/v1/niveaux_nappes/chroniques?code_bss=07588X0075/PZ&sort=desc&date_debut_mesure=2019-05-16&fields=niveau_nappe_eau,date_mesure&page=1&size=400",
  "last": null,
  "prev": null,
  "next": null,
  "api_version": "1.4.0",
  "data": [
    {
      "date_mesure": "2020-05-04",
      "niveau_nappe_eau": 77.3
    },
    {
      "date_mesure": "2020-05-03",
      "niveau_nappe_eau": 77.3
    },
    {
      "date_mesure": "2020-05-02",
      "niveau_nappe_eau": 77.3
    }]}
```

# Indexing


Json fields required for each station:

```json
{
  "bssCode": "08081X0026/SE.20",
  "latestMeasureDate" : "2020-06-7",
  "measures": [
    {
      "date_mesure": "2020-05-04",
      "niveau_nappe_eau": 77.3
    },
    {...}
  ] 
}
```

# Requêtes exemples

RQM : For easy testing, go to https://test-mnb-biometeo.mnemotix.com/graphql/playground in order to test the following graphQL requests:

Requête pour obtenir les valeurs de niveau de nappes de l'année écoulée (entre le 16/07/2020 et 16/07/2019) sur la station la plus proche de Périgueux (geonames = 6429478). La requête doit retourner les valeurs moyennes sur 1 mois ainsi que:
- moyenne sur la période
- dernier niveau
- tendance évolution par rapport à la veille
- détails station de mesure 

```json
{
  groundWaterObservation(
    geonamesId: "geonames:6429478"
    # Par défaut sur aujourd'hui
    #endDate: "2020-07-16" # Par défault sur Aujourd'hui
    # Par défaut sur aujourd'hui - 1 an
    #startDate: "2019-07-16"
    # Moyenne par jour, semaine, mois. 
    stepUnit: Month
  ) {
    # moyenne sur la période
    mean 
    #dernière mesure disponible
    lastLevel 
    # tendance évolution par rapport à la veille
    trend 
    # station de mesure 
    station {
      label
    	bssCode
    	aquiferLabel
      stationType
    }
    # liste des niveaux
    levels {
      date
      level
    }
  }
}


```

1 exemple de résultat attendu. Les données précises peuvent différer, mais la structure de la réponse doit être similaire (sauf évolution de l'API par rapport au 06/08/2020)

```json 
{
  "data": {
    "groundWaterObservation": {
      "mean": 86.16,
      "lastLevel": 85.63,
      "trend": "decreasing",
      "station": {
        "label": "LABORIE DES MOUNARDS (TRELISSAC-24)",
        "bssCode": "07595X0006/F3",
        "aquiferLabel": " Coniacien-Santonien ",
        "stationType": "Forage Peu Profond"
      },
      "levels": [
        {
          "date": "2020-08",
          "level": "85.63"
        },
        {
          "date": "2020-07",
          "level": "85.81"
        },
        {
          "date": "2020-06",
          "level": "86.06"
        },
        {
          "date": "2020-05",
          "level": "86.36"
        },
        {
          "date": "2020-04",
          "level": "86.33"
        },
        {
          "date": "2020-03",
          "level": "86.67"
        },
        {
          "date": "2020-02",
          "level": "86.56"
        },
        {
          "date": "2020-01",
          "level": "86.62"
        },
        {
          "date": "2019-12",
          "level": "86.77"
        },
        {
          "date": "2019-11",
          "level": "86.49"
        },
        {
          "date": "2019-10",
          "level": "85.7"
        },
        {
          "date": "2019-09",
          "level": "85.46"
        },
        {
          "date": "2019-08",
          "level": "85.6"
        }
      ]
    }
  }
}

```


