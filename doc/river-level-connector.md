River level connector
=====================


# Abstract

* goal is to collect **daily** the level of water given by the river stations located in Dordogne. These stations are listed and described in [mnb-repository/river-stations](https://gitlab.com/cg24/mnb-repository/-/tree/master/river-stations)
* collected data will be indexed in the ES instance of the Hub de Données, and not stored in the graph; no JSON to RDF mapping is therefore required.

# API calls for retrieving rivers' level data

## Listing river stations

1. Load the static data from mnb-repository : `river-stations/river_station_on_service_in_dordogne.trig`
2. Run the following query:

```SQL
PREFIX mnb: <http://mnb.dordogne.fr/ontology/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
prefix geo: <http://www.opengis.net/ont/geosparql#> 

# Query listing all river stations (43) 
select distinct *
where { 
	?station a mnb:RiverLevelMS .
    ?station mnb:stationCode ?stationCode .
    ?station rdfs:label ?stationLabel .
    ?river a mnb:River .
    ?station sosa:isSampleOf ?river .
    ?river rdfs:label ?river_label .
    ?station mnx:hasGeometry/geo:asWKT ?locCoord . 
} 

```
Example result

| station                                             | stationCode | stationLabel                                  | river                               | river_label | locCoord                                    |
|-----------------------------------------------------|-------------|-----------------------------------------------|-------------------------------------|-------------|---------------------------------------------|
| http://mnb.dordogne.fr/data/riverstation/O925500101 | O925500101  | La Banège à Plaisance [Moulin de la Ferrière] | http://id.eaufrance.fr/CEA/O9250500 | La Banège   | POINT(0.5495414360057992 44.68974723833916) |
|

## Hubeau calls

The goal is to collect everyday, for each station, all the hourly measures of the water level for the last 30 days. This duration is the highest available from this API. 

API doc : https://hubeau.eaufrance.fr/page/api-hydrometrie#console


Calls template:
* param: stationCode
* `https://hubeau.eaufrance.fr/api/v1/hydrometrie/observations_tr?code_entite={{stationCode}}&size=720&grandeur_hydro=H&timestep=60&fields=resultat_obs,date_obs`
* fixed parameters:
    * size=720 : we should retrieve at most 24 measures by day x 30 days = 720; We take some margins in cases of getting more than 24 measures for a given day.
    * grandeur_hydro=H : H corresponds to "heighth" of the river. Other choices include "Q" for debit
    * timestep=60 : in minutes. Highest available.
    * fields=resultat_obs,date_obs : value and date returned.

Example call and result (sample) : 

https://hubeau.eaufrance.fr/api/v1/hydrometrie/observations_tr?code_entite=O9255001&size=800&grandeur_hydro=H&timestep=60&fields=resultat_obs,date_obs

```JSON
{  
  "count": 713,
  "first": "https://hubeau.eaufrance.fr/api/v1/hydrometrie/observations_tr?code_entite=O9255001&grandeur_hydro=H&timestep=60&fields=resultat_obs,date_obs&page=1&size=713",
  "prev": null,
  "next": null,
  "api_version": "1.0.1",
  "data": [
    {
      "date_obs": "2020-05-15T08:00:00Z",
      "resultat_obs": 700.0
    },
    {
      "date_obs": "2020-05-15T07:20:00Z",
      "resultat_obs": 705.0
    },
    {
      "date_obs": "2020-05-15T06:20:00Z",
      "resultat_obs": 709.0
    },
    {...}
  ]
}
```

# Indexing


Json fields required for each station:

```json
{
  "stationCode": "O9255001",
  "dateLatestMeasure" : "2020-06-7",
  "measures": [
    {
      "date_obs": "2020-05-15T08:00:00Z",
      "resultat_obs": 700.0
    },
    {...}
  ] 
}
```

# Requête exemple

RQM : For easy testing, go to https://test-mnb-biometeo.mnemotix.com/graphql/playground in order to test the following graphQL requests:

Requête GraphQL pour lister les mesures disponibles pour la station rivière la plus proche de Coly-Saint-Amand (geonames:6429505).

La requête doit retourner les valeurs moyennes sur 1 jour pour le mois écoulé ainsi que:
- moyenne sur la période
- dernier niveau
- tendance évolution par rapport à la veille
- détails station de mesure 


```py
{
  riverObservation(
    geonamesId: "geonames:6429505"
    # H pour la hauteur ou Q pour le Débit
    observationType: H
    # Par défaut sur aujourd'hui
    #endDate: "2020-08-06" # Par défault sur Aujourd'hui
    # Par défaut sur aujourd'hui - 30 jours
    #startDate: "2020-07-07"
    # Moyenne par jour, semaine, mois. 
    stepUnit: Day
  ) {
    # moyenne sur la période
    mean 
    #dernière mesure disponible
    lastLevel 
    # tendance évolution par rapport à la veille
    trend 
    # station de mesure 
    station {
      label
    	stationCode
    	city
    }
    # liste des niveaux
    levels {
      date
      level
    }
  }
}
```

1 exemple de résultat attendu. Les données précises peuvent différer, mais la structure de la réponse doit être similaire (sauf évolution de l'API par rapport au 06/08/2020)

```json
{
  "data": {
    "riverObservation": {
      "mean": 303.56,
      "lastLevel": 245.33,
      "trend": "increasing",
      "station": {
        "label": "La Chironde",
        "stationCode": "P413511001",
        "city": "SAINT-AMAND-DE-COLY"
      },
      "levels": [
        {
          "date": "2020-08-06",
          "level": "245.33"
        },
        {
          "date": "2020-08-05",
          "level": "242.38"
        },
        {
          "date": "2020-08-04",
          "level": "264.54"
        },
        {
          "date": "2020-08-03",
          "level": "272.63"
        },
        {
          "date": "2020-08-02",
          "level": "282.29"
        },
        {
          "date": "2020-08-01",
          "level": "239.75"
        },
        {
          "date": "2020-07-31",
          "level": "245.54"
        },
        {
          "date": "2020-07-30",
          "level": "262.75"
        },
        {
          "date": "2020-07-29",
          "level": "263.58"
        },
        {
          "date": "2020-07-28",
          "level": "266.21"
        },
        {
          "date": "2020-07-27",
          "level": "282.75"
        },
        {
          "date": "2020-07-26",
          "level": "288.42"
        },
        {
          "date": "2020-07-25",
          "level": "284.13"
        },
        {
          "date": "2020-07-24",
          "level": "282"
        },
        {
          "date": "2020-07-23",
          "level": "287.67"
        },
        {
          "date": "2020-07-22",
          "level": "296.42"
        },
        {
          "date": "2020-07-21",
          "level": "309.5"
        },
        {
          "date": "2020-07-20",
          "level": "316.75"
        },
        {
          "date": "2020-07-19",
          "level": "320.29"
        },
        {
          "date": "2020-07-18",
          "level": "323.11"
        },
        {
          "date": "2020-07-17",
          "level": "333.88"
        },
        {
          "date": "2020-07-16",
          "level": "344"
        },
        {
          "date": "2020-07-15",
          "level": "335.85"
        },
        {
          "date": "2020-07-14",
          "level": "321.42"
        },
        {
          "date": "2020-07-13",
          "level": "333.9"
        },
        {
          "date": "2020-07-12",
          "level": "343.94"
        },
        {
          "date": "2020-07-11",
          "level": "349"
        },
        {
          "date": "2020-07-10",
          "level": "355.25"
        },
        {
          "date": "2020-07-09",
          "level": "368.07"
        },
        {
          "date": "2020-07-08",
          "level": "373.5"
        },
        {
          "date": "2020-07-07",
          "level": "375.63"
        }
      ]
    }
  }
}
```