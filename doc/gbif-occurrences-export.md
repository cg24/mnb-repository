GIBF Occurrences data in Dordogne - static backup
=================================================


# Data Source  
The goal of this data sub-repository is to provide a backup and validation dataset for the taxon occurrences data from GBIF. It contains all the occurrence data available from GBIF as of 26/09/2020.

* GBIF download link https://www.gbif.org/occurrence/download/0069541-200613084148143
* CSV file available from https://drive.google.com/open?id=1JH7bEOTs_jRqccb3GOcKEQDRJ-SxXtJt
* Citations of all involved datasets in [citations.txt](citations.txt)
* Parameters in the search page (https://www.gbif.org/occurrence/download?has_coordinate=true&has_geospatial_issue=false&year=*,*&advanced=1&geometry=POLYGON((0.62974%2045.71457,0.30267%2045.45906,0.2666%2045.29775,-0.01785%2045.16922,0.07329%2045.07012,-0.03421%2044.85211,0.28515%2044.86456,0.35459%2044.65481,0.79772%2044.70177,0.8425%2044.60096,1.10321%2044.57173,1.44193%2044.87758,1.41289%2045.12509,1.22781%2045.20603,1.32279%2045.38266,1.02333%2045.60893,0.8115%2045.57587,0.62974%2045.71457))):
    * Year: Before end of 2020
    * Geometry: POLYGON((0.62974 45.71457,0.30267 45.45906,0.2666 45.29775,-0.01785 45.16922,0.07329 45.07012,-0.03421 44.85211,0.28515 44.86456,0.35459 44.65481,0.79772 44.70177,0.8425 44.60096,1.10321 44.57173,1.44193 44.87758,1.41289 45.12509,1.22781 45.20603,1.32279 45.38266,1.02333 45.60893,0.8115 45.57587,0.62974 45.71457))
    * Has coordinate: true
    * Has geospatial issue: false

# Importing data with OntoRefine 

1. click "create project"
2. load CSV, even zipped
3. clic "next" 
4. clic "create project"
5. clic "RDF mapping"/ Edit RDF mapping
6. add "https://www.gbif.org/occurrence/" as Base IRI
7. add one mapping wiyth gbifID as Subject, rdf:type as predicate, mnb:TaxonOccurrence as object
8. clic "Save", then "Sparql"
9. Note down the ID of the mapper , eg. "SERVICE <rdf-mapper:ontorefine:2095436961412>"
10. replace the corresponding value in the CONSTRUCT request with that value

## Construct SPARQL queries

```SQL
PREFIX mnb: <http://mnb.dordogne.fr/ontology/>
PREFIX spif: <http://spinrdf.org/spif#>

# Example RDF transformation using the custom IRIs and type
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dwciri: <http://rs.tdwg.org/dwc/iri/> 

CONSTRUCT {
	?myRowId a mnb:TaxonOccurrence ;
		sosa:resultTime ?resultTime ;
    	mnx:hasGeometry ?geometry ;
     	dwciri:inDataset ?datasetURI ;
		mnb:gbifTaxonKey ?acceptedTaxonKey .
    
 	?geometry a mnx:Geometry ;
    	geosparql:asWKT ?pointCoordinate ;
        geosparql:asGML ?pointCoordinateGML .
    
} WHERE {
	SERVICE <ontorefine:1932168470951> {
		# Triple patterns for accessing each row and the columns in contains
		# Note that no triples will be generated for NULL values in the table
		# You should inspect your data in Refine mode and add OPTIONAL accordingly
		?row a mnb:Row ;
			mnb:gbifID ?gbifID ;
			mnb:datasetKey ?datasetKey ;
			mnb:occurrenceID ?occurrenceID ;
			mnb:decimalLatitude ?decimalLatitude ;
			mnb:decimalLongitude ?decimalLongitude ;
			mnb:eventDate ?eventDate ;
			mnb:taxonKey ?acceptedTaxonKey .
	
		# Example construction of new IRIs for each row
		BIND(IRI(spif:buildString("https://www.gbif.org/occurrence/{?1}", ENCODE_FOR_URI(?gbifID))) AS ?myRowId)
        BIND(IRI(CONCAT("https://www.gbif.org/occurrence/", ENCODE_FOR_URI(?gbifID), "#point")) AS ?geometry) .
        BIND(STRDT(?eventDate, xsd:dateTime) AS ?resultTime) .
		BIND(STRDT(CONCAT("POINT(", ?decimalLongitude, " ",?decimalLatitude,")"),geosparql:wktLiteral) AS ?pointCoordinate) .
        BIND(STRDT(CONCAT(?decimalLatitude, ",",?decimalLongitude),geosparql:gmlLiteral) AS ?pointCoordinateGML) .
        BIND(IRI(CONCAT("https://www.gbif.org/dataset/", ENCODE_FOR_URI(?datasetKey))) AS ?datasetURI) .
	}
} 

```

## Dataset description

Named graph URI : <http://mnb.dordogne.fr/data/gibfoccurrenceNG>


# Requêtes exemples

Requête SPARQL pour lister les  occurrences => 616k occurrences

```
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <http://schema.org/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX taxrefstatus: <http://taxref.mnhn.fr/lod/status/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX taxrefbgs: <http://taxref.mnhn.fr/lod/bioGeoStatus/>
PREFIX dwc: <http://rs.tdwg.org/dwc/terms/>
prefix mnb: <http://mnb.dordogne.fr/ontology/> 
prefix mnbv: <http://mnb.dordogne.fr/vocabulary/> 
PREFIX taxrefrk: <http://taxref.mnhn.fr/lod/taxrank/>
PREFIX taxrefprop: <http://taxref.mnhn.fr/lod/property/>  
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
prefix sosa: <http://www.w3.org/ns/sosa/> 
PREFIX ma: <http://www.w3.org/ns/ma-ont#> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 

SELECT *
WHERE {
	?occurrence a mnb:TaxonOccurrence ;
		sosa:resultTime ?resultTime ;
    	mnx:hasGeometry/geosparql:asWKT ?geometryCOord ;
     	prov:hadPrimarySource ?datasetURI ;
		mnb:gbifTaxonKey ?acceptedTaxonKey .
}
```

Requête donnant les occurrences pour un taxon (clé GBIF) et leur éloignement d'un point fixe (Bergerac)

```SQL
SELECT ?gbifTaxonKey ?minDistance ?occurPoint
WHERE {
    VALUES ?gbifTaxonKey {"6409622"} .
    ?occurence a mnb:TaxonOccurrence ;
                mnb:gbifTaxonKey ?gbifTaxonKey ;
                mnx:hasGeometry/geosparql:asWKT ?occurPoint .
    #  ?currentPoint = coordonnées de Bergerac 
    BIND(STRDT("POINT(0.4833 44.85)", geosparql:wktLiteral) AS ?currentPoint).
    BIND(ROUND(geof:distance(?occurPoint, ?currentPoint, uom:metre)) AS ?minDistance)
}
ORDER BY ASC(?minDistance)
```

Autres requêtes SPARQL cf [taxons-example-requests.sparql](taxons-example-requests.sparql)

