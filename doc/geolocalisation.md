Données de géolocalisation - liens communes-cantons
====================================================


# Construction des données

Basée sur tableau de Wikidata 

# Querying canton data

```SQL 
PREFIX gn: <http://www.geonames.org/ontology#>
PREFIX igeo: <http://rdf.insee.fr/def/geo#>
 

PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT ?canton ?cantonLabel ?codeCanton  (GROUP_CONCAT(DISTINCT ?idGeoname ) as ?ids)
WHERE {
    ?canton a igeo:Canton2015 .
    ?canton rdfs:label ?cantonLabel .
    ?canton igeo:subdivision/owl:sameAs ?idGeoname  .
    #?commune rdfs:label ?communeLabel .
    #?commune igeo:codeINSEE ?codeINSEE .
    #?commune owl:sameAs ?idGeoname .
    ?canton igeo:codeCanton ?codeCanton .           
}
GROUP BY ?canton ?cantonLabel ?codeCanton 
```