Connecteurs GRaphDB-ES
======================


# mnb-dev-taxon

Défini par [mnb-dev-taxon.sparql](../connectors-es-graphdb/mnb-dev-taxon.sparql)

RQM-01 - Filtrage par images
   * il faut filtrer les taxons ayant au moins une image => OK (bound(?depiction))
   * il faudrait en plus pouvoir ne considérer que les images 
     format "paysage" avec un ratio largeur/hauteur comprise  
     entre 2 valeurs fixes (e.g >1,3 et <1,7)

RMQ-02 - Filtrage des page Gbif
     le lien foaf:page donnent tous les liens, dont ceux du gbif.org; hors l'idée est de ne retenir que ceux là


INdexation des occurrences => Pas direct, puisqu'on passe par 

# dev-mnb-occurrence

