Taxref-LOD + Extensions
=======================

# Description

Les informations liées aux taxons (classe, famille, etc) sont basées sur la version RDF de Taxref, [Taxref-LOD](https://github.com/frmichel/taxref-ld). Cette version RDF contient une partie des infos disponibles dans la version "WEB" de Taxref pour laquelle une API REST documentée est disponible, [Taxref-WEB](https://taxref.mnhn.fr/taxref-web/api/)

Pour des besoins d'indexation et faciliter les requêtes des données, nous avons écrit des requêtes SPARQL qui permettent d'insérer ces données supplémentaires : [taxref-extensions](../taxref-extensions/taxon-type-gbifid-status-ranks.insert.sparql).

# Requête exemple

```

PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <http://schema.org/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX taxrefstatus: <http://taxref.mnhn.fr/lod/status/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX taxrefbgs: <http://taxref.mnhn.fr/lod/bioGeoStatus/>
PREFIX dwc: <http://rs.tdwg.org/dwc/terms/>
prefix mnb: <http://mnb.dordogne.fr/ontology/> 
prefix mnbv: <http://mnb.dordogne.fr/vocabulary/> 
PREFIX taxrefrk: <http://taxref.mnhn.fr/lod/taxrank/>
PREFIX taxrefprop: <http://taxref.mnhn.fr/lod/property/>  
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
prefix sosa: <http://www.w3.org/ns/sosa/> 
PREFIX ma: <http://www.w3.org/ns/ma-ont#> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 

# Récupérer ttes les infos taxonomiques des taxons ayant au moins 1 image
SELECT DISTINCT  (GROUP_CONCAT(DISTINCT ?img; SEPARATOR=",") AS ?images) ?area ?taxon ?gbifTaxonKey ?scientificName ?vernacularName  ?habitat ?lienINPN ?lienGBIF ?statutBiogeo ?orderName  ?className ?familyName
WHERE{ 
    VALUES ?taxon { <http://taxref.mnhn.fr/lod/taxon/1952/12.0> } 
    ?taxon mnb:area/skos:prefLabel ?area .
    # Lien entre ?taxon comme classe TaxRef, et la clé GBIF 
    ?taxon wd:P846 ?gbifTaxonKey  .
    ?taxon rdfs:isDefinedBy	<http://taxref.mnhn.fr/lod/taxref-ld/12.0> .
    # détails sur le taxon
    ?taxon rdfs:label ?scientificName .
    ?taxon taxrefprop:vernacularName ?vernacularName .
    FILTER(LANG(?vernacularName) = "fr") .
    # image
    ?taxon foaf:depiction ?img .
    ?img ma:frameWidth ?w .
    ?img ma:frameHeight ?h .
    BIND((?w/?h) AS ?ratio) .
    FILTER(?ratio >= 1.3  && ?ratio <= 2) .
  	# Liens vers homage (site INPN) et page GBIF
    ?taxon foaf:homepage ?lienINPN .
    ?taxon foaf:page ?lienGBIF .
    FILTER(REGEX(STR(?lienGBIF),"^https:\\/\\/www\\.gbif\\.org")) .
    # Habitat
    ?taxon taxrefprop:habitat/skos:prefLabel ?habitat .
    FILTER(LANG(?habitat) = "fr") .
    # Status
    ?taxon mnb:hasBioGeographicalStatusFranceMetro ?statutBiogeo  .
   	# Classe, ordre, famille
     OPTIONAL{
        ?taxon mnb:orderName  ?orderName
    	}
   	OPTIONAL{
        ?taxon mnb:className  ?className
    	}
    OPTIONAL{
        ?taxon mnb:familyName  ?familyName
    	}
    
}
GROUP BY ?taxon ?scientificName ?vernacularName  ?habitat ?lienINPN ?lienGBIF ?statutBiogeo ?orderName  ?className ?familyName ?gbifTaxonKey ?area

```
Résultat attendu :

* 17,338 taxons
* exemple du premier résultat

|                                                                               images                                                                              | area |           taxon          | gbifTaxonKey |   scientificName   |                    vernacularName                    |     habitat    |                     lienINPN                    |               lienGBIF               |            statutBiogeo           |  orderName |   className   |  familyName |
|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------:|:----:|:------------------------:|:------------:|:------------------:|:----------------------------------------------------:|:--------------:|:-----------------------------------------------:|:------------------------------------:|:---------------------------------:|:----------:|:-------------:|:-----------:|
| https://taxref.mnhn.fr/api/media/download/inpn/257032,https://taxref.mnhn.fr/api/media/download/inpn/257033,https://taxref.mnhn.fr/api/media/download/inpn/257034 | Eau  | taxref:taxon/100024/12.0 | 2890636      | Geranium argenteum | "Géranium à feuilles argentées, Géranium argenté"@fr | "Eau douce"@fr | https://inpn.mnhn.fr/espece/cd_nom/100024?lg=en | https://www.gbif.org/species/2890636 | Présent (indigène ou indéterminé) | Geraniales | Equisetopsida | Geraniaceae |


